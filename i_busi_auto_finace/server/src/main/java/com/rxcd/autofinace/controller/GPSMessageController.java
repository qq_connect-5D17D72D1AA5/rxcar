/**
 * 创建者：ZhaoSheng 创建时期： 2013-07-15
 */
package com.rxcd.autofinace.controller;

import com.alibaba.fastjson.JSONObject;
import com.fida.RSA.RSAUtils;
import com.rxcd.autofinace.util.RxErpUtil;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.engine.common.WfUtil;
import org.appframe.wf.util.Param;
import org.appframe.wf.util.SafeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * GPS位置信息
 */

@Controller
@RequestMapping("/")
public class GPSMessageController {

    @Autowired
    private RxErpUtil erpUtil;

    @Autowired
    private BaseDao baseDao;

    @Autowired
    private WfUtil wfUtil;

    @Value("${api.gps.public_key}")
    private String publicKey;

    @Value("${api.gps.account_key}")
    private String accountKey;

    /**
     * GPS位置信息
     *
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping("/loan_common_GPSMessage")
    public Row execute(HttpServletRequest request) throws Exception {
        Param param = wfUtil.requestToParam(request);
        // 流程实例NO
        String review_no = param.getJsonString(param.getDataJson(), "review_no");
        String loan_no = (String) baseDao.queryObject("select table_key_no from wf_review_biz where review_no='" + SafeUtil.safe(review_no) + "'");
        Row row = new Row();
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String car_plate = erpUtil.objectIsNullGetString(loanRow.get("loan_car.car_plate"));
        JSONObject json = new JSONObject();
        json.put("CAR_NO", car_plate);
        String sign = RSAUtils.publicEncrypt(json.toJSONString(), RSAUtils.getPublicKey(publicKey));
        String url = "https://ai.renxingqiche.com/gps/index?accountKey="+accountKey+"&sign="+sign;
        row.put("type", "url");
        row.put("value", url);
        return row;
    }
}