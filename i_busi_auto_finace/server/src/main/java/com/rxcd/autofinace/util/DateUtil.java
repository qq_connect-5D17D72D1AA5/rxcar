package com.rxcd.autofinace.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil
{


	private static final SimpleDateFormat dateFormatSecond = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private static final SimpleDateFormat dateFormatMinute = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	private static final SimpleDateFormat dateFormatHour = new SimpleDateFormat("yyyy-MM-dd HH");

	private static final SimpleDateFormat dateFormatDay = new SimpleDateFormat("yyyy-MM-dd");
	
	private static final SimpleDateFormat dateFormatOnlyDay = new SimpleDateFormat("dd");
	

	/**
	 * 将日期转换为java.util.Date对象。
	 * 
	 * @param date
	 *            数据库查询出来的日期对象
	 * @return
	 * @throws Exception
	 *             date不是日期对象
	 */
//	public static Date toDate(Object date) throws Exception
//	{
//		if (date instanceof Date)
//			return (Date) date;
//		else if (date instanceof oracle.sql.DATE)
//			return ((oracle.sql.DATE) date).timestampValue();
//		else if (date instanceof oracle.sql.TIMESTAMP)
//			return ((oracle.sql.TIMESTAMP) date).timestampValue();
//		else if (date instanceof oracle.sql.TIMESTAMPLTZ)
//			return ((oracle.sql.TIMESTAMPLTZ) date).timestampValue();
//		else if (date instanceof oracle.sql.DATE)
//			return ((oracle.sql.TIMESTAMPTZ) date).timestampValue();
//		else
//			throw new Exception("unsupported Date type: " + date.getClass());
//	}

	/**
	 * @param date
	 * @return 格式为"yyyy-MM-dd HH:mm:ss"的string
	 */
	public static String date2StringSecond(Date date)
	{
		return dateFormatSecond.format(date);
	}


	/**
	 * @param date
	 * @return 格式为"yyyy-MM-dd HH:mm"的string
	 */
	public static String date2StringMinute(Date date)
	{
		return dateFormatMinute.format(date);
	}


	/**
	 * @param date
	 * @return 格式为"yyyy-MM-dd HH"的string
	 */
	public static String date2StringHour(Date date)
	{
		return dateFormatHour.format(date);
	}


	/**
	 * @param date
	 * @return 格式为"yyyy-MM-dd"的string
	 */
	public static String date2StringDay(Date date)
	{
		return dateFormatDay.format(date);
	}

	
	/**
	 * 得到天
	 * @param date
	 * @return
	 */
	public static String dateFormatOnlyDay(Date date)
	{
		return dateFormatOnlyDay.format(date);
	}

	/**
	 * 转换为日期，格式必须为 "yyyy-MM-dd HH:mm:ss" 或 "yyyy-MM-dd HH:mm" 或"yyyy-MM-dd"
	 * 
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	public static Date toDate(String date) throws ParseException
	{
		switch (date.length())
		{
			case 19:
				// "yyyy-MM-dd HH:mm:ss"
				return dateFormatSecond.parse(date);

			case 16:
				// "yyyy-MM-dd HH:mm"
				return dateFormatMinute.parse(date);

			case 13:
				// "yyyy-MM-dd HH"
				return dateFormatHour.parse(date);

			case 10:
				// "yyyy-MM-dd"
				return dateFormatDay.parse(date);
			default:
				throw new ParseException("不能识别的日期格式:" + date, -1);
		}
	}
	
	/**
	 * 去当前时间前一天
	 * @param date
	 * @return
	 */
	public static Date getNextDay(Date date) {  
        Calendar calendar = Calendar.getInstance();  
        calendar.setTime(date);  
        calendar.add(Calendar.DAY_OF_MONTH, -1);  
        date = calendar.getTime();  
        return date;  
    }  

}
