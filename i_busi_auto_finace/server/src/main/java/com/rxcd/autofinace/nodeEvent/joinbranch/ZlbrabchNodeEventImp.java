package com.rxcd.autofinace.nodeEvent.joinbranch;


import com.rxcd.autofinace.common.BizErrorCodeEnum;
import com.rxcd.autofinace.service.LoanService;
import org.apache.commons.lang.StringUtils;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.db.Store;
import org.appframe.wf.engine.AbstractWorkFlowNodeEvent;
import org.appframe.wf.util.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 网点资料
 */
@Component
public class ZlbrabchNodeEventImp extends AbstractWorkFlowNodeEvent {
    @Value("runtime")
    private String runtime;
    @Autowired
    private LoanService loanService;
    @Autowired
    private BaseDao baseDao;
    /**
     * 流程节点启动时调用
     *
     * @param flow_node_no
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowNodeStart(String flow_node_no, Param p) throws Exception {

    }

    @Override
    public void workFlowNodeFinish(String flow_node_no, Param p) throws Exception {
        checkTempletFlowNo("6");
        String branch_join_no = getTableKeyNo();
        String sql = "select join_branch_partner.partner_name,join_branch_partner.idcard,join_branch_partner.phone_1,join_branch_partner.residence_address "
                +"from join_branch "
                +"left join join_branch_partner on join_branch_partner.branch_join_no = join_branch.branch_join_no "
                +"where join_branch.branch_join_no = '"+branch_join_no+"' and join_branch_partner.status != '3'";
        Store store  = baseDao.query(sql);
        for (Row row : store) {
            if(StringUtils.isBlank(row.getString("residence_address")) || StringUtils.isBlank(row.getString("partner_name")) || StringUtils.isBlank(row.getString("idcard")) || StringUtils.isBlank(row.getString("phone_1"))){
                throw new Exception(BizErrorCodeEnum.J0001.getCode());
            }
            if(StringUtils.contains(row.getString("residence_address"), " ")|| StringUtils.contains(row.getString("partner_name"), " ") || StringUtils.contains(row.getString("idcard"), " ") || StringUtils.contains(row.getString("phone_1"), " ")){
                throw new Exception(BizErrorCodeEnum.J0002.getCode());
            }
        }
        //生成合同
        loanService.creatJoinBranchCon(branch_join_no);
    }

    /**
     * 流程节点驳回时回调方法
     *
     * @param flow_node_no
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowNodeOverrule(String flow_node_no, Param p) throws Exception {

    }

    @Override
    public void workFlowNodeVie(String flow_node_no, Param p) throws Exception {

    }
}
