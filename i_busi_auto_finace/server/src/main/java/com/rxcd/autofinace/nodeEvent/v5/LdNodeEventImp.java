package com.rxcd.autofinace.nodeEvent.v5;


import com.rxcd.autofinace.service.LoanCheckServiceImpl;
import com.rxcd.autofinace.service.LoanService;
import com.rxcd.autofinace.util.RxErpUtil;
import org.apache.commons.lang.StringUtils;
import org.appframe.wf.db.Row;
import org.appframe.wf.engine.AbstractWorkFlowNodeEvent;
import org.appframe.wf.util.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 录单
 */
@Component
public class LdNodeEventImp extends AbstractWorkFlowNodeEvent {
    @Value("runtime")
    private String runtime;
    @Autowired
    private LoanCheckServiceImpl checkService;
    @Autowired
    private RxErpUtil erpUtil;
    @Autowired
    private LoanService loanService;


    /**
     * 流程节点启动时调用
     *
     * @param flow_node_no
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowNodeStart(String flow_node_no, Param p) throws Exception {

    }

    @Override
    public void workFlowNodeFinish(String flow_node_no, Param p) throws Exception {
        checkTempletFlowNo("1");
        String loanNo = getTableKeyNo();
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loanNo);
        String cust_phone = loanRow.getString("loan_cust.cust_phone");
        //反欺诈废弃
//        checkService.check_fraud(loanNo);
        checkService.checkBasicData(loanNo);
        checkService.check_bank(loanNo);
        checkService.checkFkMax(loanNo);
        checkService.checkLoanQuota(loanNo);
        checkService.check_approve_sum(loanNo);
        checkService.check_yys_message(cust_phone);
        if (StringUtils.equals("0", runtime)) {
            String payeeName = loanRow.getString("loan_cust.cust_name");
            String payeeIdcard = loanRow.getString("loan_cust.idcard");
            String payeeBankAcct = loanRow.getString("loan_info_et.bank_accout");
            String payeeMobile = loanRow.getString("loan_cust.cust_phone");
            erpUtil.runtimeTestCheck(payeeName, payeeIdcard, payeeBankAcct, payeeMobile, loanNo);
        } else {
            checkService.check_4_element(loanNo);
        }
        loanService.updateEmployersV2(loanNo);
        //更新抵押方并推送
        erpUtil.updateDyf(loanNo);
        //匹配联系人信息
        erpUtil.mateContacts(loanNo);

        erpUtil.initCustSituation(loanRow);
        //更新GPS个数
        double contract_sum = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.contract_sum"));
        erpUtil.updateGpsCount(contract_sum, loanNo);
        //生成合同
        loanService.creatCon(loanNo);
    }

    /**
     * 流程节点驳回时回调方法
     *
     * @param flow_node_no
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowNodeOverrule(String flow_node_no, Param p) throws Exception {

    }

    @Override
    public void workFlowNodeVie(String flow_node_no, Param p) throws Exception {

    }
}
