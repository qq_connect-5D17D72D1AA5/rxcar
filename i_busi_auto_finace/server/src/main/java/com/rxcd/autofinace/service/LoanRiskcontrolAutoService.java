/**
 * 版权所有：版权所有：重庆易房网络科技股份有限公司
 * Chongqing YiFang Network Technology Co. LTD.  All Rights Reserved
 * 创建者：HeSenLin   创建时期：  2016年6月3日
 */
package com.rxcd.autofinace.service;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rxcd.autofinace.util.PropertiesUtil;
import com.rxcd.autofinace.util.RxErpUtil;
import org.apache.commons.lang.StringUtils;
import org.appframe.bigdata.*;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.db.Store;
import org.appframe.wf.util.ErpUtil;
import org.appframe.wf.util.Formula;
import org.appframe.wf.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * 获取风控得分
 *
 * @author liuchang
 */
@Component
public class LoanRiskcontrolAutoService {

    private static final String RUNTIME = PropertiesUtil.getValue("runtime");
    @Autowired
    private RxErpUtil erpUtil;
    @Autowired
    private BaseDao baseDao;
    @Autowired
    private TianXingClinet2 tianXingClinet;
    @Autowired
    private JiAoClinet2 jiAoClinet;
    @Autowired
    private TongDunClinet2 tongDunClinet;
    @Autowired
    private ShuJuMoHeClinet2 shuJuMoHeClinet;
    @Autowired
    private AcedataClinet2 acedataClinet;

    /**
     * 获取初评风控得分
     *
     * @param loan_no
     * @return
     */
    public void getFirstRiskcontrol(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        Row param = getFirstRiskDimension(loanRow);
        autoRiskcontrol(loanRow, param);
    }

    /**
     * 获取中评风控得分
     *
     * @param loan_no
     * @return
     */
    public void getMidRiskcontrol(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        Row param = getMidRiskDimension(loanRow);
        autoRiskcontrol(loanRow, param);
    }

    /**
     * 获取终评风控得分
     *
     * @param loan_no
     * @return
     */
    public void getFinalRiskcontrol(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        Row param = getFinalRiskDimension(loanRow);
        autoRiskcontrol(loanRow, param);
    }

    /**
     * 风控得分
     *
     * @throws Exception
     */
    public void autoRiskcontrol(Row loanRow, Row param) throws Exception {
        String loan_id = ErpUtil.objectIsNullGetString(loanRow.get("loan_info.loan_id"));
        String loan_no = ErpUtil.objectIsNullGetString(loanRow.get("loan_info.loan_no"));
        String riskcontrol_type = param.getString("riskcontrol_type");
        baseDao.execute("delete from loan_riskcontrol_auto where loan_no='" + loan_no + "' and riskcontrol_type='" + riskcontrol_type + "'");
        Store rules = baseDao.query("select * from loan_riskcontrol_rule where status='1' order by sort_no");

        //计算单项得分
        String riskcontrol_dimension, condition_exp, condition_remark;
        Formula conFormula = null;
        int result = 0;
        Map<String, Object> p = new HashMap<String, Object>();
        List<String> sqlList = new ArrayList<String>();
        for (Row rule : rules) {
            riskcontrol_dimension = ErpUtil.objectIsNullGetString(rule.get("riskcontrol_dimension"));
            condition_exp = ErpUtil.objectIsNullGetString(rule.get("condition_exp"));
            condition_remark = ErpUtil.objectIsNullGetString(rule.get("condition_remark"));
            result = ErpUtil.objectIsNullGetInteger(rule.get("result"));
            // 条件公式
            conFormula = new Formula(condition_exp);
            p.clear();
            p.put(riskcontrol_dimension, param.get(riskcontrol_dimension));
            conFormula.setVariable(riskcontrol_dimension, param.get(riskcontrol_dimension));
            // 条件是否成立
            if (conFormula.bool()) {
                sqlList.add("insert into loan_riskcontrol_auto "
                        + "(auto_id,auto_no,loan_id,loan_no,riskcontrol_type,riskcontrol_dimension,condition_exp,condition_remark,riskcontrol_param,riskcontrol_score,create_time,oper_time,status) "
                        + "values "
                        + "(uuid(),"
                        + " uuid(),"
                        + " '" + loan_id + "',"
                        + " '" + loan_no + "',"
                        + " '" + riskcontrol_type + "',"
                        + " '" + riskcontrol_dimension + "',"
                        + " '" + condition_exp + "',"
                        + " '" + condition_remark + "',"
                        + "'" + JSONObject.toJSONString(p) + "',"
                        + " " + result + ","
                        + " '" + Util.format(new Date()) + "',"
                        + " '" + Util.format(new Date(), "YYYY-MM-dd HH:mm:ss") + "',"
                        + " '1')");
            }
        }
        baseDao.execute(sqlList);
        //汇总所有得分并更新
        if (StringUtils.equals("01", riskcontrol_type)) {
            int score = ErpUtil.objectIsNullGetInteger(
                    baseDao.queryObject("select sum(riskcontrol_score) from loan_riskcontrol_auto where loan_no='" + loan_no + "' and riskcontrol_type = '01'"));
            baseDao.execute("update loan_ass set riskcontrol_first_score=" + score + " where loan_no='" + loan_no + "'");
        } else if (StringUtils.equals("02", riskcontrol_type)) {
            int score = ErpUtil.objectIsNullGetInteger(
                    baseDao.queryObject("select sum(riskcontrol_score) from loan_riskcontrol_auto where loan_no='" + loan_no + "' and riskcontrol_type != '03'"));
            baseDao.execute("update loan_ass set riskcontrol_mid_score=" + score + " where loan_no='" + loan_no + "'");
        } else {
            int score = ErpUtil.objectIsNullGetInteger(
                    baseDao.queryObject("select sum(riskcontrol_score) from loan_riskcontrol_auto where loan_no='" + loan_no + "'"));
            baseDao.execute("update loan_ass set riskcontrol_final_score=" + score + " where loan_no='" + loan_no + "'");
        }
    }

    /**
     * 获取初评风控纬度数据
     *
     * @param loanRow
     * @throws Exception
     */
    public Row getFirstRiskDimension(Row loanRow) throws Exception {
        Row param = new Row();
        param.put("riskcontrol_type", "01");
        initCustAge(loanRow, param);
        initHouseProperty(loanRow, param);
        initEnterpriseMember(loanRow, param);
        initMobileWhenLong(loanRow, param);
        initTongDun(loanRow, param);
        initPersonalInvolvement(loanRow, param);
        initOverdue(loanRow, param);
        initUnhealthy(loanRow, param);
        initDriving(loanRow, param);
        initBranchRating(loanRow, param);
        initBranchAge(loanRow, param);
//        initBranchBadDebt(loanRow, param);
        initSellerWorkAge(loanRow, param);

        return param;
    }

    /**
     * 获取中评风控纬度数据
     *
     * @param loanRow
     * @throws Exception
     */
    public Row getMidRiskDimension(Row loanRow) throws Exception {
        Row param = new Row();
        param.put("riskcontrol_type", "02");
        initHomeAddressRange(loanRow, param);
        initWorkAddressRange(loanRow, param);
        initIsBorrowed(loanRow, param);
        initJiAoScore(loanRow, param);
        initAssPrice(loanRow, param);
        initApprovePercentage(loanRow, param);
        initRepType(loanRow, param);
        initDuetime(loanRow, param);
        initAssScore(loanRow, param);
        initRiskWorkAge(loanRow, param);
        initRiskRating(loanRow, param);
        initMarriageState(loanRow, param);
        initChildrenState(loanRow, param);
        initResidenceState(loanRow, param);
        initYys(loanRow, param);

        return param;
    }

    /**
     * 获取终评风控纬度数据
     *
     * @param loanRow
     * @throws Exception
     */
    public Row getFinalRiskDimension(Row loanRow) throws Exception {
        Row param = new Row();
        param.put("riskcontrol_type", "03");
        initSensitivesectors(loanRow, param);

        return param;
    }

    /**
     * 获取年龄
     *
     * @param loanRow
     */
    public void initCustAge(Row loanRow, Row param) throws Exception {
        String idcard = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.idcard"));
        int custYear = Integer.parseInt(idcard.substring(6, 10));
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        int year = calendar.get(Calendar.YEAR);
        int age = year - custYear;
        param.put("loan_risk.age", age);
    }

    /**
     * 获取房产信息
     *
     * @param loanRow
     */
    public void initHouseProperty(Row loanRow, Row param) throws Exception {
        if (StringUtils.equals("1", RUNTIME)) {
            try {
                String cust_name = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_name"));
                String cust_phone = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_phone"));
                String idcard = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.idcard"));
                String resultString = jiAoClinet.housePropertyValidate(cust_name, idcard, cust_phone);
                if (resultString != null) {
                    JSONObject resultJson = JSONObject.parseObject(resultString);
                    String identify_result = resultJson.getString("identify_result");
                    if (StringUtils.equals("1", identify_result)) {
                        param.put("loan_risk.houseProperty", "01");//有房产
                        return;
                    }
                }
                param.put("loan_risk.houseProperty", "02");
            } catch (Exception e) {
                e.getStackTrace();
                param.put("loan_risk.houseProperty", "02");
            }
        } else {
            param.put("loan_risk.houseProperty", "02");
        }
    }

    /**
     * 获取营业执照信息
     *
     * @param loanRow
     */
    public void initEnterpriseMember(Row loanRow, Row param) throws Exception {
        if (StringUtils.equals("1", RUNTIME)) {
            try {
                String idcard = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.idcard"));
                //个人名下企业
                JSONObject companyData = tianXingClinet.queryEnterpriseMember(idcard, false);
                JSONObject dataJson = companyData.getJSONObject("data");
                boolean b = true;
                if (StringUtils.equals("EXIST", dataJson.getString("status"))) {
                    JSONArray corporates = dataJson.getJSONArray("corporates");
                    for (int i = 0; i < corporates.size(); i++) {
                        JSONObject companyJson = corporates.getJSONObject(i);
                        //如果是在营状态 需要查询  企业涉诉
                        if (StringUtils.contains(companyJson.getString("entStatus"), "在营")) {
                            b = false;
                            param.put("loan_risk.is_enterprise", "01");//有企业
                            JSONObject jsonObject = tianXingClinet.queryHighCourtEnterprise(companyJson.getString("entName"), companyJson.getString("regNo"), false);
                            JSONObject data = jsonObject.getJSONObject("data");
                            String cpwsStatus = "";
                            String sxggStatus = "";
                            String zxggStatus = "";
                            JSONObject cpws = data.getJSONObject("cpws");
                            if (cpws != null) {
                                cpwsStatus = cpws.getString("checkStatus");
                            }
                            JSONObject sxgg = data.getJSONObject("sxgg");
                            if (sxgg != null) {
                                sxggStatus = sxgg.getString("checkStatus");
                            }
                            JSONObject zxgg = data.getJSONObject("zxgg");
                            if (zxgg != null) {
                                zxggStatus = zxgg.getString("checkStatus");
                            }
                            if (StringUtils.equals(cpwsStatus, "EXIST")
                                    || StringUtils.equals(sxggStatus, "EXIST")
                                    || StringUtils.equals(zxggStatus, "EXIST")) {
                                param.put("loan_risk.enterprise_complaint", "01");//企业有涉诉
                                return;
                            } else {
                                param.put("loan_risk.enterprise_complaint", "02");//企业无涉诉
                            }
                        }
                    }
                }
                if (b) {
                    param.put("loan_risk.is_enterprise", "02");//无企业
                }
            } catch (Exception e) {
                e.getStackTrace();
                param.put("loan_risk.is_enterprise", "02");//无企业
                param.put("loan_risk.enterprise_complaint", "02");//企业无涉诉
            }
        } else {
            param.put("loan_risk.is_enterprise", "02");//无企业
            param.put("loan_risk.enterprise_complaint", "02");//企业无涉诉
        }
    }

    /**
     * 获取手机在网时长
     *
     * @param loanRow
     * @param param
     * @throws Exception
     */
    public void initMobileWhenLong(Row loanRow, Row param) throws Exception {
        if (StringUtils.equals("1", RUNTIME)) {
            try {
                String cust_name = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_name"));
                String cust_phone = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_phone"));
                String idcard = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.idcard"));
                String resultString = jiAoClinet.queryMobileWhenLong(cust_name, idcard, cust_phone);
                String code = "";
                if (StringUtils.equals("(0,3]", resultString)) {
                    code = "01";
                } else if (StringUtils.equals("(3,6]", resultString)) {
                    code = "02";
                } else if (StringUtils.equals("(6,12]", resultString)) {
                    code = "03";
                } else if (StringUtils.equals("(12,24]", resultString)) {
                    code = "04";
                } else if (StringUtils.equals("(24,+)", resultString)) {
                    code = "05";
                } else {
                    code = "00";
                }
                param.put("loan_risk.mobileWhenLong", code);
            } catch (Exception e) {
                e.getStackTrace();
                param.put("loan_risk.mobileWhenLong", "00");
            }
        } else {
            param.put("loan_risk.mobileWhenLong", "00");
        }
    }

    /**
     * 通过同盾接口查询多头和分数
     *
     * @param loanRow
     * @throws Exception
     */
    public void initTongDun(Row loanRow, Row param) throws Exception {
        if (StringUtils.equals("1", RUNTIME)) {
            String cust_name = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_name"));
            String cust_phone = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_phone"));
            String idcard = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.idcard"));
            List<String> sqlList = new ArrayList<String>();
            String loan_id = ErpUtil.objectIsNullGetString(loanRow.get("loan_info.loan_id"));
            String loan_no = ErpUtil.objectIsNullGetString(loanRow.get("loan_info.loan_no"));
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("name", cust_name);// 姓名 必填
            paramMap.put("id_number", idcard);// 身份证 必填
            paramMap.put("mobile", cust_phone);// 手机号
            try {
                String reportId = tongDunClinet.queryReportNo(paramMap, false);
                Thread.sleep(2000);
                JSONObject jsonObject = tongDunClinet.queryReport(reportId, false);
                if (jsonObject.getBooleanValue("success")) {
                    int credit_score = ErpUtil.objectIsNullGetInteger(jsonObject.get("final_score"));
                    param.put("loan_risk.tongDunScore", credit_score);
                    JSONArray risk_items = jsonObject.getJSONArray("risk_items");
                    int num = 1;
                    for (Object o : risk_items) {
                        JSONObject risk_item = (JSONObject) o;
                        String item_name = risk_item.getString("item_name");
                        if (StringUtils.equals("1个月内申请人在多个平台申请借款", item_name)) {
                            JSONObject item_detail = risk_item.getJSONObject("item_detail");
                            int platform_count = ErpUtil.objectIsNullGetInteger(item_detail.get("platform_count"));
                            param.put("loan_risk.tongDunReport", platform_count);
                        }
                        String id = "0000" + num;
                        String tongdunId = "TD" + id.substring(id.length() - 3, id.length());
                        //将铜盾明细插入详细表
                        sqlList.add("insert into loan_riskcontrol_detailed "
                                + "(id,no,detailed_id,detailed_no,loan_id,loan_no,riskcontrol_dimension,condition_remark,riskcontrol_score,create_time,oper_time,status) "
                                + "values "
                                + "(uuid(),"
                                + " uuid(),"
                                + " '" + tongdunId + "',"
                                + " '" + tongdunId + "',"
                                + " '" + loan_id + "',"
                                + " '" + loan_no + "',"
                                + " '" + risk_item.getString("group") + "',"
                                + " '" + item_name + "',"
                                + " " + 0 + ","
                                + " '" + Util.format(new Date()) + "',"
                                + " '" + Util.format(new Date(), "YYYY-MM-dd HH:mm:ss") + "',"
                                + " '1')");
                        num++;
                    }
                    baseDao.execute(sqlList);
                } else {
                    throw new Exception(jsonObject.getString("reason_desc"));
                }
            } catch (Exception e) {
                e.printStackTrace();
                param.put("loan_risk.tongDunScore", 0);
                param.put("loan_risk.tongDunReport", 0);
            }
        } else {
            param.put("loan_risk.tongDunScore", 0);
            param.put("loan_risk.tongDunReport", 0);
        }
    }

    /**
     * 个人涉诉，失信
     *
     * @param loanRow
     * @throws Exception
     */
    public void initPersonalInvolvement(Row loanRow, Row param) throws Exception {
        if (StringUtils.equals("1", RUNTIME)) {
            try {
                String cust_name = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_name"));
                String idcard = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.idcard"));
                JSONObject jsonObject = tianXingClinet.queryHighCourtRersonal(cust_name, idcard, false);
                String success = jsonObject.getString("success");
                if (StringUtils.equals("true", success)) {
                    JSONObject data = jsonObject.getJSONObject("data");
                    String cpwsStatus = "";
                    String sxggStatus = "";
                    String zxggStatus = "";
                    JSONObject cpws = data.getJSONObject("cpws");
                    if (cpws != null) {
                        cpwsStatus = cpws.getString("checkStatus");
                    }
                    JSONObject sxgg = data.getJSONObject("sxgg");
                    if (sxgg != null) {
                        sxggStatus = sxgg.getString("checkStatus");
                    }
                    JSONObject zxgg = data.getJSONObject("zxgg");
                    if (zxgg != null) {
                        zxggStatus = zxgg.getString("checkStatus");
                    }
                    if (StringUtils.equals(cpwsStatus, "EXIST")
                            || StringUtils.equals(sxggStatus, "EXIST")
                            || StringUtils.equals(zxggStatus, "EXIST")) {
                        param.put("loan_risk.personal", "01");//个人有涉诉
                    } else {
                        param.put("loan_risk.personal", "02");//个人无涉诉
                    }
                    if (StringUtils.equals(sxggStatus, "EXIST")) {
                        param.put("loan_risk.discredit", "01");//个人有失信
                    } else {
                        param.put("loan_risk.discredit", "02");//个人无失信
                    }
                } else {
                    param.put("loan_risk.personal", "02");//个人无涉诉
                    param.put("loan_risk.discredit", "02");//个人无失信
                }
            } catch (Exception e) {
                e.printStackTrace();
                param.put("loan_risk.personal", "02");//个人无涉诉
                param.put("loan_risk.discredit", "02");//个人无失信
            }
        } else {
            param.put("loan_risk.personal", "02");//个人无涉诉
            param.put("loan_risk.discredit", "02");//个人无失信
        }
    }

    /**
     * 获取客户逾期情况
     *
     * @param loanRow
     * @param param
     * @throws Exception
     */
    public void initOverdue(Row loanRow, Row param) throws Exception {
        if (StringUtils.equals("1", RUNTIME)) {
            try {
                String cust_name = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_name"));
                String cust_phone = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_phone"));
                String idcard = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.idcard"));
                String resultString = jiAoClinet.queryOverdue(cust_name, idcard, cust_phone);
                JSONObject resultJson = JSONObject.parseObject(resultString);
                if (resultJson == null) {
                    throw new Exception("未查询到数据");
                }
                String beforeoverdue = resultJson.getString("result_YQ_DQJE");//当前逾期金额
                if (beforeoverdue != null) {
                    param.put("loan_risk.beforeoverdue", "01");
                } else {
                    param.put("loan_risk.beforeoverdue", "02");
                }
                String historyoverdue = resultJson.getString("result_YQ_ZDJE");//历史逾期金额
                if (historyoverdue != null) {
                    param.put("loan_risk.historyoverdue", "01");
                } else {
                    param.put("loan_risk.historyoverdue", "02");
                }
            } catch (Exception e) {
                param.put("loan_risk.beforeoverdue", "02");
                param.put("loan_risk.historyoverdue", "02");
            }
        } else {
            param.put("loan_risk.beforeoverdue", "02");
            param.put("loan_risk.historyoverdue", "02");
        }
    }

    /**
     * 不良信息
     *
     * @param loanRow
     * @throws Exception
     */
    public void initUnhealthy(Row loanRow, Row param) throws Exception {
        if (StringUtils.equals("1", RUNTIME)) {
            try {
                String cust_name = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_name"));
                String idcard = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.idcard"));
                JSONObject jsonObject = tianXingClinet.queryNegative(cust_name, idcard, false);
                String success = jsonObject.getString("success");
                if (StringUtils.equals("true", success)) {
                    JSONObject data = jsonObject.getJSONObject("data");
                    String status = data.getString("status");
                    if (StringUtils.equals("EXIST", status)) {
                        String crimeCompared = data.getString("crimeCompared");//前科
                        if (StringUtils.equals("一致", crimeCompared)) {
                            param.put("loan_risk.crime", "01");//有前科
                        } else {
                            param.put("loan_risk.crime", "02");//无
                        }
                        String drugCompared = data.getString("drugCompared");//吸毒
                        if (StringUtils.equals("一致", drugCompared)) {
                            param.put("loan_risk.drug", "01");//有吸毒
                        } else {
                            param.put("loan_risk.drug", "02");//无
                        }
                        String drugRelatedCompared = data.getString("drugRelatedCompared");//涉毒
                        if (StringUtils.equals("一致", drugRelatedCompared)) {
                            param.put("loan_risk.drugRelated", "01");//有涉毒
                        } else {
                            param.put("loan_risk.drugRelated", "02");//无
                        }
                        return;
                    }
                }
                param.put("loan_risk.crime", "02");//无
                param.put("loan_risk.drug", "02");//无
                param.put("loan_risk.drugRelated", "02");//无
            } catch (Exception e) {
                e.printStackTrace();
                param.put("loan_risk.crime", "02");//无
                param.put("loan_risk.drug", "02");//无
                param.put("loan_risk.drugRelated", "02");//无
            }
        } else {
            param.put("loan_risk.crime", "02");//无
            param.put("loan_risk.drug", "02");//无
            param.put("loan_risk.drugRelated", "02");//无
        }
    }

    /**
     * 获取通话详单内是否被催收（数据魔盒运营商数据获取）
     * 获取预留联系人是否有直亲（并匹配配偶，子女，父母）
     *
     * @param loanRow
     * @param param
     * @throws Exception
     */
    public void initYys(Row loanRow, Row param) throws Exception {
        if (StringUtils.equals("1", RUNTIME)) {
            try {
                String task_id = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.yys_message"));
                JSONObject jsonObject = shuJuMoHeClinet.queryBgzb(task_id);
                String data = jsonObject.getString("bgzb");
                String dataString = Util.gunzip(data);
                JSONObject dataJson = JSONObject.parseObject(dataString);
                //联系人催收风险分析
                JSONArray contact_suspect_collection_analysis = dataJson.getJSONArray("contact_suspect_collection_analysis");
                if (contact_suspect_collection_analysis.size() > 0) {
                    param.put("loan_risk.collection", "01");
                } else {
                    param.put("loan_risk.collection", "00");
                }
                //全部联系人明细
                JSONArray all_contact_detail = dataJson.getJSONArray("all_contact_detail");
                int num = 10;
                if (all_contact_detail.size() < 10) {
                    num = all_contact_detail.size();
                }
                String sql = "select * from loan_info_contacts where loan_no = '" + loanRow.getString("loan_info.loan_no") + "'";
                Store store = baseDao.query(sql);
                String directrelatives = "00";
                boolean b = true;
                for (Row row : store) {
                    String contacts_relation = ErpUtil.objectIsNullGetString(row.get("contacts_relation"));
                    if (StringUtils.equals("01", contacts_relation)
                            || StringUtils.equals("02", contacts_relation)
                            || StringUtils.equals("03", contacts_relation)
                            || StringUtils.equals("04", contacts_relation)) {
                        directrelatives = "01";
                        String contacts_mobile = ErpUtil.objectIsNullGetString(row.get("contacts_mobile"));
                        for (int i = 0; i < num; i++) {
                            JSONObject contactJSON = all_contact_detail.getJSONObject(i);
                            String contact_number = contactJSON.getString("contact_number");
                            if (StringUtils.equals(contacts_mobile, contact_number)) {
                                if (StringUtils.equals("01", contacts_relation)) {
                                    param.put("loan_risk.parents", "01");
                                    b = false;
                                } else if (StringUtils.equals("02", contacts_relation)) {
                                    param.put("loan_risk.spouse", "01");
                                    b = false;
                                } else {
                                    param.put("loan_risk.children", "01");
                                    b = false;
                                }
                            }
                        }
                    }
                }
                param.put("loan_risk.directrelatives", directrelatives);
                if (b && StringUtils.equals(directrelatives, "01")) {
                    for (Row row : store) {
                        String contacts_relation = ErpUtil.objectIsNullGetString(row.get("contacts_relation"));
                        if (StringUtils.equals("01", contacts_relation)) {
                            param.put("loan_risk.parents", "00");
                        } else if (StringUtils.equals("02", contacts_relation)) {
                            param.put("loan_risk.spouse", "00");
                        } else if (StringUtils.equals("03", contacts_relation) || StringUtils.equals("04", contacts_relation)) {
                            param.put("loan_risk.children", "00");
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                param.put("loan_risk.collection", "00");
                param.put("loan_risk.directrelatives", "00");
            }
        } else {
            param.put("loan_risk.collection", "00");
            param.put("loan_risk.directrelatives", "00");
        }
    }

    /**
     * 驾驶证状态
     *
     * @param loanRow
     * @param param
     * @throws Exception
     */
    public void initDriving(Row loanRow, Row param) throws Exception {
        if (StringUtils.equals("1", RUNTIME)) {
            try {
                String cust_name = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_name"));
                String idcard = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.idcard"));
                JSONObject jsonObject = acedataClinet.queryDrivingLicense(cust_name, idcard, false);
                String resCode = jsonObject.getString("resCode");
                if (StringUtils.equals("0000", resCode)) {
                    JSONObject dataJson = jsonObject.getJSONObject("data");
                    String statusCode = dataJson.getString("statusCode");
                    if (StringUtils.equals("2012", statusCode)) {
                        JSONObject resultJson = dataJson.getJSONObject("result");
                        String state = resultJson.getString("state");
                        if (StringUtils.equals("A", state)) {
                            param.put("loan_risk.driving", "01");
                            return;
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        param.put("loan_risk.driving", "02");
    }

    /**
     * 获取是否涉及敏感行业
     *
     * @param loanRow
     * @param param
     * @throws Exception
     */
    public void initSensitivesectors(Row loanRow, Row param) throws Exception {
        String sensitivesectors = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.sensitivesectors"));
        param.put("loan_risk.sensitivesectors", sensitivesectors);
    }

    /**
     * 获取分行评级
     *
     * @param loanRow
     */
    public void initBranchRating(Row loanRow, Row param) throws Exception {
        if (StringUtils.equals("1", RUNTIME)) {
            try {
                String sell_branch_no = ErpUtil.objectIsNullGetString(loanRow.get("loan_info.sell_branch_no"));
                Row branchDataRow = baseDao.queryRow("select * from loan_branch_data where branch_no='" + sell_branch_no + "'");
                String branchRating = "C";
                if (branchDataRow != null) {
                    branchRating = ErpUtil.objectIsNullGetString(branchDataRow.get("rating"));
                }
                param.put("loan_risk.branchRating", branchRating);
            } catch (Exception e) {
                e.printStackTrace();
                param.put("loan_risk.branchRating", "C");
            }
        } else {
            param.put("loan_risk.branchRating", "C");
        }
    }

    /**
     * 初始化分行运营时间
     *
     * @param loanRow
     */
    public void initBranchAge(Row loanRow, Row param) throws Exception {
        try {
            String sell_branch_no = ErpUtil.objectIsNullGetString(loanRow.get("loan_info.sell_branch_no"));
            Row branchRow = baseDao.queryRow("select * from join_branch where dept_id='" + sell_branch_no + "' and status='1'");

            String now = Util.format(new Date());
            String open_date = "";
            if (branchRow != null) {
                open_date = ErpUtil.objectIsNullGetString(branchRow.get("open_date"));
            }
            if (Util.isEmpty(open_date)) {
                open_date = now;
            }
            int branch_age = Util.monthsBetween(open_date, now);
            param.put("loan_risk.branch_age", branch_age);
        } catch (Exception e) {
            e.printStackTrace();
            param.put("loan_risk.branch_age", 1);
        }
    }

    /**
     * 初始化分行逾期坏账率
     *
     * @param loanRow
     */
    public void initBranchBadDebt(Row loanRow, Row param) throws Exception {
        try {
            String sell_branch_no = ErpUtil.objectIsNullGetString(loanRow.get("loan_info.sell_branch_no"));
            Row branchDataRow = baseDao.queryRow("select * from loan_branch_data where branch_no='" + sell_branch_no + "'");
            double bad_debt_1 = ErpUtil.objectIsNullGetDouble(branchDataRow.get("overdue_rate_10_30"));
            double bad_debt_2 = ErpUtil.objectIsNullGetDouble(branchDataRow.get("overdue_rate_10_60"));
            double bad_debt_3 = ErpUtil.objectIsNullGetDouble(branchDataRow.get("overdue_rate_10_90"));
            double bad_debt_4 = ErpUtil.objectIsNullGetDouble(branchDataRow.get("bad_debt_rate"));
            param.put("loan_risk.bad_debt_1", bad_debt_1);
            param.put("loan_risk.bad_debt_2", bad_debt_2);
            param.put("loan_risk.bad_debt_3", bad_debt_3);
            param.put("loan_risk.bad_debt_4", bad_debt_4);
        } catch (Exception e) {
            e.printStackTrace();
            param.put("loan_risk.bad_debt_1", 0);
            param.put("loan_risk.bad_debt_2", 0);
            param.put("loan_risk.bad_debt_3", 0);
            param.put("loan_risk.bad_debt_4", 0);
        }
    }


    /**
     * 初始化销售时间入职时间
     *
     * @param loanRow
     */
    public void initSellerWorkAge(Row loanRow, Row param) throws Exception {
        try {
            String seller_no = ErpUtil.objectIsNullGetString(loanRow.get("loan_info.seller_no"));
            Row userRow = baseDao.queryRow("select * from i_frame_sys_user where user_id='" + seller_no + "'");
            int work_age = 0;
            if (userRow != null) {
                String join_date = ErpUtil.objectIsNullGetString(userRow.get("join_date"));
                if (Util.isNotEmpty(join_date)) {
                    work_age = Util.monthsBetween(join_date, Util.format(new Date()));
                }
            }
            param.put("loan_risk.work_age", work_age);
        } catch (Exception e) {
            e.printStackTrace();
            param.put("loan_risk.work_age", 0);
        }
    }

    /**
     * 获取家庭住址匹配
     *
     * @param loanRow
     */
    public void initHomeAddressRange(Row loanRow, Row param) throws Exception {
        if (StringUtils.equals("1", RUNTIME)) {
            try {
                String cust_name = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_name"));
                String cust_phone = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_phone"));
                String idcard = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.idcard"));
                String address_p = loanRow.getString("loan_cust.address_p");
                String address = "";
                String address_add = erpUtil.getDetailedAddress(address,address_p);
                String cust_address = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_address"));
                String resultString = jiAoClinet.queryMobileHomeDeviate(cust_name, idcard, cust_phone,address_add+cust_address);
                String code = "";
                if (StringUtils.equals("(0,2]", resultString)) {
                    code = "01";
                } else if (StringUtils.equals("(2,5]", resultString)) {
                    code = "02";
                } else if (StringUtils.equals("(5,+)", resultString)) {
                    code = "03";
                } else {
                    code = "00";
                }
                param.put("loan_risk.home_address", code);
            } catch (Exception e) {
                e.printStackTrace();
                param.put("loan_risk.home_address", "00");
            }
        } else {
            param.put("loan_risk.home_address", "00");
        }
    }

    /**
     * 获取单位住址匹配
     *
     * @param loanRow
     */
    public void initWorkAddressRange(Row loanRow, Row param) throws Exception {
        if (StringUtils.equals("1", RUNTIME)) {
            try {
                String cust_name = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_name"));
                String cust_phone = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_phone"));
                String idcard = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.idcard"));
                String company_add_p = loanRow.getString("loan_cust.company_add_p");
                String company = "";
                String company_adress = erpUtil.getDetailedAddress(company,company_add_p);
                String company_add = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.company_add"));
                String resultString = jiAoClinet.queryMobileWorkDeviate(cust_name, idcard, cust_phone,company_adress+company_add);
                String code = "";
                if (StringUtils.equals("(0,2]", resultString)) {
                    code = "01";
                } else if (StringUtils.equals("(2,5]", resultString)) {
                    code = "02";
                } else if (StringUtils.equals("(5,+)", resultString)) {
                    code = "03";
                } else {
                    code = "00";
                }
                param.put("loan_risk.work_address", code);
            } catch (Exception e) {
                e.printStackTrace();
                param.put("loan_risk.work_address", "00");
            }
        } else {
            param.put("loan_risk.work_address", "00");
        }
    }

    /**
     * 有无共租人
     *
     * @param loanRow
     */
    public void initIsBorrowed(Row loanRow, Row param) throws Exception {
        try {
            String borrowedName = loanRow.getString("loan_info_et.borrowed_name");
            String borrowedIdcard = loanRow.getString("loan_info_et.borrowed_idcard");
            String borrowedMobile = loanRow.getString("loan_info_et.borrowed_mobile");
            if (StringUtils.isNotBlank(borrowedName) && StringUtils.isNotBlank(borrowedIdcard) && StringUtils.isNotBlank(borrowedMobile)) {
                param.put("loan_risk.is_borrowed", "01");//有
            } else {
                param.put("loan_risk.is_borrowed", "02");//无
            }
        } catch (Exception e) {
            e.printStackTrace();
            param.put("loan_risk.is_borrowed", "02");
        }
    }

    /**
     * 获取集奥分数
     *
     * @param loanRow
     */
    public void initJiAoScore(Row loanRow, Row param) throws Exception {
        if (StringUtils.equals("1", RUNTIME)) {
            try {
                String cust_name = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_name"));
                String cust_phone = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_phone"));
                String idcard = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.idcard"));
                String bank_accout = ErpUtil.objectIsNullGetString(loanRow.get("loan_info_et.bank_accout"));
                String resultString = jiAoClinet.queryBankCardCredit(cust_name, idcard, cust_phone, bank_accout);
                if (resultString == null) {
                    throw new Exception("未查询到数据");
                }
                JSONObject resultJson = JSONObject.parseObject(resultString);
                JSONArray data = resultJson.getJSONArray("data");
                JSONObject d = (JSONObject) data.get(0);
                Map<String, String> map = queryDictionaries();
                List<String> sqlList = new ArrayList<String>();
                String loan_id = ErpUtil.objectIsNullGetString(loanRow.get("loan_info.loan_id"));
                String loan_no = ErpUtil.objectIsNullGetString(loanRow.get("loan_info.loan_no"));
                int num = 1;
                for (Map.Entry<String, Object> entry : d.entrySet()) {
                    String id = "0000" + num;
                    String jiaoId = "JA" + id.substring(id.length() - 3, id.length());
                    //将集奥信用评分插出详细表
                    sqlList.add("insert into loan_riskcontrol_detailed "
                            + "(id,no,detailed_id,detailed_no,loan_id,loan_no,riskcontrol_dimension,condition_remark,riskcontrol_score,create_time,oper_time,status) "
                            + "values "
                            + "(uuid(),"
                            + " uuid(),"
                            + " '" + jiaoId + "',"
                            + " '" + jiaoId + "',"
                            + " '" + loan_id + "',"
                            + " '" + loan_no + "',"
                            + " '" + map.get(entry.getKey()) + "',"
                            + "'" + entry.getValue() + "',"
                            + " " + 0 + ","
                            + " '" + Util.format(new Date()) + "',"
                            + " '" + Util.format(new Date(), "YYYY-MM-dd HH:mm:ss") + "',"
                            + " '1')");
                    num++;

                    if (StringUtils.equals("CDCA005", entry.getKey())) {
                        baseDao.execute("update loan_info_et set bank_dep='" + entry.getValue() + "' "
                                + "where loan_no='" + loan_no + "' and (bank_dep is null or bank_dep='')");
                    }
                }
                baseDao.execute(sqlList);
                String score = d.getString("score");
                double count = Double.parseDouble(score);
                param.put("loan_risk.jiao_score", count);
            } catch (Exception e) {
                e.printStackTrace();
                param.put("loan_risk.jiao_score", 0);
            }
        } else {
            param.put("loan_risk.jiao_score", 0);
        }
    }

    /**
     * 获取人工评估价
     *
     * @param loanRow
     */
    public void initAssPrice(Row loanRow, Row param) throws Exception {
        double ass_price1 = ErpUtil.objectIsNullGetDouble(loanRow.get("loan_ass.ass_price1"));
        param.put("loan_risk.ass_price", ass_price1);
    }

    /**
     * 获取贷款成数
     *
     * @param loanRow
     */
    public void initApprovePercentage(Row loanRow, Row param) throws Exception {
        double approve_percentage = ErpUtil.objectIsNullGetDouble(loanRow.get("loan_info_et.approve_percentage"));
        param.put("loan_risk.approve_percentage", approve_percentage);
    }

    /**
     * 获取还款方式
     *
     * @param loanRow
     */
    public void initRepType(Row loanRow, Row param) throws Exception {
        String rep_type = ErpUtil.objectIsNullGetString(loanRow.get("loan_info_et.rep_type"));
        param.put("loan_risk.rep_type", rep_type);
    }

    /**
     * 获取贷款期数
     *
     * @param loanRow
     */
    public void initDuetime(Row loanRow, Row param) throws Exception {
        int duetime = ErpUtil.objectIsNullGetInteger(loanRow.get("loan_cost.duetime"));
        param.put("loan_risk.duetime", duetime);
    }

    /**
     * 获取自动评估分数
     *
     * @param loanRow
     */
    public void initAssScore(Row loanRow, Row param) throws Exception {
        double ass_score = ErpUtil.objectIsNullGetDouble(loanRow.get("loan_ass.ass_score"));
        param.put("loan_risk.ass_score", ass_score);
    }

    /**
     * 初始化风控入职时间
     *
     * @param loanRow
     */
    public void initRiskWorkAge(Row loanRow, Row param) throws Exception {
        String assessor_work_age = ErpUtil.objectIsNullGetString(loanRow.get("loan_ass.assessor_work_age"));
        if (StringUtils.equals(assessor_work_age, "")) {
            assessor_work_age = "0";
        }
        param.put("loan_risk.assessor_work_age", assessor_work_age);
    }

    /**
     * 初始化风控评级
     *
     * @param loanRow
     */
    public void initRiskRating(Row loanRow, Row param) throws Exception {
        String assessor_rating = ErpUtil.objectIsNullGetString(loanRow.get("loan_ass.assessor_rating"));
        param.put("loan_risk.assessor_rating", assessor_rating);
    }

    /**
     * 初始化婚姻状况
     *
     * @param loanRow
     */
    public void initMarriageState(Row loanRow, Row param) throws Exception {
        String marriage_state = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.marriage_state"));
        param.put("loan_risk.marriage_state", marriage_state);
    }

    /**
     * 初始化子女状况
     *
     * @param loanRow
     */
    public void initChildrenState(Row loanRow, Row param) throws Exception {
        String children_state = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.children_state"));
        param.put("loan_risk.children_state", children_state);
    }

    /**
     * 初始化户口状况
     *
     * @param loanRow
     */
    public void initResidenceState(Row loanRow, Row param) throws Exception {
        String residence_state = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.residence_state"));
        param.put("loan_risk.residence_state", residence_state);
    }

    /**
     * 获取集奥数据字典Z9
     *
     * @return
     * @throws Exception
     */
    public Map<String, String> queryDictionaries() throws Exception {
        Map<String, String> map = new HashMap<String, String>();
        String sql = "select no,dictionaries_name from dictionaries_jiao";
        Store store = baseDao.query(sql);
        for (Row row : store) {
            map.put(row.getString("no"), row.getString("dictionaries_name"));
        }
        return map;
    }
}

