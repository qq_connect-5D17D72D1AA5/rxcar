package com.rxcd.autofinace.model;

public class ClearBean {

    private String duetime;

    private String clear_type;

    private String demolition_period;

    private double contract_sum;

    private String duetime_unit;

    private String loan_date;

    private double pacc_fee;

    private double security_fee;

    private String accounts;

    public String getDuetime() {
        return duetime;
    }

    public void setDuetime(String duetime) {
        this.duetime = duetime;
    }

    public String getClear_type() {
        return clear_type;
    }

    public void setClear_type(String clear_type) {
        this.clear_type = clear_type;
    }

    public String getDemolition_period() {
        return demolition_period;
    }

    public void setDemolition_period(String demolition_period) {
        this.demolition_period = demolition_period;
    }

    public double getContract_sum() {
        return contract_sum;
    }

    public void setContract_sum(double contract_sum) {
        this.contract_sum = contract_sum;
    }

    public String getDuetime_unit() {
        return duetime_unit;
    }

    public void setDuetime_unit(String duetime_unit) {
        this.duetime_unit = duetime_unit;
    }

    public String getLoan_date() {
        return loan_date;
    }

    public void setLoan_date(String loan_date) {
        this.loan_date = loan_date;
    }

    public double getPacc_fee() {
        return pacc_fee;
    }

    public void setPacc_fee(double pacc_fee) {
        this.pacc_fee = pacc_fee;
    }

    public double getSecurity_fee() {
        return security_fee;
    }

    public void setSecurity_fee(double security_fee) {
        this.security_fee = security_fee;
    }

    public String getAccounts() {
        return accounts;
    }

    public void setAccounts(String accounts) {
        this.accounts = accounts;
    }
}
