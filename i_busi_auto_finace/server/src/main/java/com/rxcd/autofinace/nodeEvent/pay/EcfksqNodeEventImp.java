/**
 * 版权所有：版权所有：重庆易房网络科技股份有限公司
 * Chongqing YiFang Network Technology Co. LTD.  All Rights Reserved
 * 创建者：HeSenLin   创建时期：  2016年7月14日
 */
package com.rxcd.autofinace.nodeEvent.pay;


import com.rxcd.autofinace.service.MidService;
import com.rxcd.autofinace.common.BizErrorCodeEnum;
import com.rxcd.autofinace.util.RxErpUtil;
import org.apache.commons.codec.binary.StringUtils;
import org.appframe.wf.db.Row;
import org.appframe.wf.engine.AbstractWorkFlowNodeEvent;
import org.appframe.wf.util.ErpUtil;
import org.appframe.wf.util.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 二次付款申请流程节点事件
 */
@Component
public class EcfksqNodeEventImp extends AbstractWorkFlowNodeEvent {
    @Autowired
    private RxErpUtil erpUtil;
    @Autowired
    private MidService midService;

    /**
     * 流程节点启动时调用
     *
     * @param flow_node_no
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowNodeStart(String flow_node_no, Param p) throws Exception {
		
    }

    @Override
    public void workFlowNodeFinish(String flow_node_no, Param p) throws Exception {
        String mid_no = getTableKeyNo();

        Row payRow = baseDao.queryRow("select * from loan_mid a left join loan_mid_pay b on a.mid_no=b.mid_no where a.mid_no='" + mid_no + "'");
        String branch_success = ErpUtil.objectIsNullGetString(payRow.get("branch_success"));
        if (!StringUtils.equals("01", branch_success)) {
            throw new Exception(BizErrorCodeEnum.T1099.getCode());
        }
        midService.computeSecondAutoPay(mid_no);

    }

    /**
     * 流程节点驳回时回调方法
     *
     * @param flow_node_no
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowNodeOverrule(String flow_node_no, Param p) throws Exception {

    }

    @Override
    public void workFlowNodeVie(String flow_node_no, Param p) throws Exception {

    }

}
