package com.rxcd.autofinace.controller;

import org.apache.commons.lang.StringUtils;
import org.appframe.common.controller.BaseController;
import org.appframe.sys.bo.SysMenuBo;
import org.appframe.sys.po.SysMenuPo;
import org.appframe.sys.po.SysUserPo;
import org.appframe.sys.service.SysMenuService;
import org.appframe.sys.service.SysUserService;
import org.appframe.utils.SecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/")
public class RootController extends BaseController {

    @Autowired
    private SecurityConfig securityConfig;

    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private SysMenuService sysMenuService;

    /**
     * 默认首页
     *
     * @return 首页模板
     */
    @RequestMapping({"/", "/index"})
    public String index() {
        if (getSession().getAttribute(securityConfig.getSessionKeyUser()) == null) {
            return "redirect:/login";
        }
        return "/index";
    }

    /**
     * 注销用户
     *
     * @return 返回登录页面
     */
    @RequestMapping("logout")
    @ResponseBody
    public String logout() {
        String token = "";
        for (Cookie cookie : request.getCookies()) {
            if ("token".equals(cookie.getName())) {
                token = cookie.getValue();
            }
        }
        if (StringUtils.isNotEmpty(request.getHeader("token"))) {
            token = request.getHeader("token");
        }
        if (StringUtils.isNotEmpty(token)) {
            securityConfig.delUserToken(((SysUserPo) getSession().getAttribute(securityConfig.getSessionKeyUser())), token);
        }

        getSession().invalidate();
        return "success";
    }

    /**
     * 登录页面
     *
     * @return 登录模板
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login() {
        return "/login";
    }

    /**
     * 登录操作
     *
     * @param userLogin    用户名
     * @param userPassword 密码
     * @return 登录后续操作
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> login(String userLogin, String userPassword, HttpServletResponse response) {

        Map<String, Object> result = new HashMap<>();
        SysUserPo user = this.sysUserService.findByLoginAndPassword(userLogin, userPassword);

        if (user == null) {
            result.put("resultCode", "-1");
            result.put("msg", "用户名或密码错误！");
        } else if (!user.getUserStatus().equals(1)) {
            result.put("resultCode", "0");
            result.put("msg", "该用户禁止登录！");
        } else {
            String token = securityConfig.generateToken();
            user.setUserPassword(null);
            getSession().setAttribute(securityConfig.getSessionKeyUser(), user);
            getSession().setAttribute("token", token);
            securityConfig.updateUserToken(user, token);
            response.setHeader("token", token);
            result.put("resultCode", "1");
            result.put("user", user);
            result.put("msg", "登录成功！");
        }

        return result;
    }


    /**
     * 刷新用户
     *
     * @return
     */
    @RequestMapping(value = "/reloadUser", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> reloadUser() {
        SysUserPo sysUserPo = getCurrentUser();
        Map<String, Object> result = new HashMap<>();

        if (sysUserPo == null || sysUserPo.getUserId() == null) {
            result.put("resultCode", "-1");
            result.put("msg", "用户id不能为空！");
            return result;
        }
        result.put("resultCode", "1");
        result.put("user", sysUserService.reloadUser(sysUserPo.getUserId()));
        result.put("msg", "刷新成功！");
        return result;
    }

    /**
     * 查询菜单树
     *
     * @param menuType 1:pc  2:mb
     * @return
     */
    @ResponseBody
    @RequestMapping("/findMenu")
    public List<SysMenuPo> findMenu(@RequestParam(required = false) Integer menuType) {
        SysMenuBo sysMenuBo = new SysMenuBo();
        sysMenuBo.setMenuType(menuType);
        sysMenuBo.setNeedAuth(true);
        sysMenuBo.setIsUsed(1);
        sysMenuBo.setUserId(getCurrentUser().getUserId());
        return sysMenuService.findAllMenuTree(sysMenuBo);
    }
}
