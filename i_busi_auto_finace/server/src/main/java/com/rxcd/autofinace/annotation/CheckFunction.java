package com.rxcd.autofinace.annotation;

import java.lang.annotation.*;

@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CheckFunction {
	public String name() default "";
	public String key() default "";
	public String table() default "loan_info";
}
