/**
 * 创建者：ZhaoSheng 创建时期： 2013-07-15
 */
package com.rxcd.autofinace.controller;


import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.db.Store;
import org.appframe.wf.util.SafeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 匹配提示展现
 */
@RestController
@RequestMapping("/")
public class TipShowController {


    @Autowired
    private BaseDao baseDao;

    /**
     * 匹配提示
     *
     * @throws Exception
     */
    @RequestMapping("/loan_common_TipShow")
    public Row execute(@RequestParam("loan_no") String loan_no, @RequestParam("action") String action) throws Exception {
        Row ret = new Row();

        loan_no = loan_no.replaceAll("'", "");
        String query = "select loan_car.car_plate,loan_cust.cust_name,loan_cust.cust_phone,loan_cust.idcard "
                + " from loan_info "
                + " left join loan_car on loan_car.loan_no = loan_info.loan_no "
                + " left join loan_cust on loan_cust.loan_no = loan_info.loan_no "
                + " where loan_info.loan_no = '" + SafeUtil.safe(loan_no) + "'";
        Row row = baseDao.queryRow(query);

        String show_url = "wf_review_View";
        String listJson_url = "loan_common_TipJson?action=" + action;

//        ret.set("show_url", show_url);
//        ret.set("listJson_url", listJson_url);
//        ret.set("action", action);
//        ret.set("no", 1);
//        ret.set("car_plate", row.get("car_plate"));
//        ret.set("cust_name", row.get("cust_name"));
//        ret.set("cust_phone", row.get("cust_phone"));
//        ret.set("idcard", row.get("idcard"));
//        ret.set("loan_no", loan_no);
        Store store = new Store();

        store.add(row);

        for (int i = 0; i < store.size(); i++) {
            store.get(i).put("no", i + 1);
        }

        ret.put("report_name", "匹配提示");
        ret.put("type", "report");
        Row rowT = new Row();
        rowT.put("no", "序号");
        rowT.put("car_plate", "车牌");
        rowT.put("cust_name", "客户姓名");
        rowT.put("cust_phone", "手机");
        rowT.put("idcard", "身份证");
        ret.put("title", rowT);

        ret.put("list", store);

        return ret;

    }
}