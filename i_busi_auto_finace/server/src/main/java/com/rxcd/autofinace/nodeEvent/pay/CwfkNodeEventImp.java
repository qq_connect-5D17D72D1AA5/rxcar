/**
 * 版权所有：版权所有：重庆易房网络科技股份有限公司
 * Chongqing YiFang Network Technology Co. LTD.  All Rights Reserved
 * 创建者：HeSenLin   创建时期：  2016年7月14日
 */
package com.rxcd.autofinace.nodeEvent.pay;


import com.rxcd.autofinace.service.MidService;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.engine.AbstractWorkFlowNodeEvent;
import org.appframe.wf.service.ReviewService;
import org.appframe.wf.service.WfParamUtil;
import org.appframe.wf.util.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 财务放款节点事件
 */
@Component
public class CwfkNodeEventImp extends AbstractWorkFlowNodeEvent {
    @Autowired
    private WfParamUtil paramUtil;
    @Autowired
    private BaseDao baseDao;

    @Autowired
    private MidService midService;

    @Autowired
    private ReviewService reviewService;

    /**
     * 流程节点启动时调用
     *
     * @param flow_node_no
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowNodeStart(String flow_node_no, Param p) throws Exception {
		
    }

    @Override
    public void workFlowNodeFinish(String flow_node_no, Param p) throws Exception {

    }

    /**
     * 流程节点驳回时回调方法
     *
     * @param flow_node_no
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowNodeOverrule(String flow_node_no, Param p) throws Exception {

    }

    @Override
    public void workFlowNodeVie(String flow_node_no, Param p) throws Exception {

    }

}
