/**
 * 版权所有：版权所有：重庆易房网络科技股份有限公司
 * Chongqing YiFang Network Technology Co. LTD.  All Rights Reserved
 * 创建者：HeSenLin   创建时期：  2016年7月20日
 */
package com.rxcd.autofinace.util;


import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.db.Store;
import org.appframe.wf.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 贷款流程工具
 */
@Component
public class LoanReviewUtil {

    @Autowired
    private BaseDao baseDao;

    /**
     * 查询主合同及其所有子合同（续贷、追加贷、转等的追加贷）
     *
     * @param loan_no
     * @param fk      查询条件：是否放款;如果 false,可以查询到还在贷前审核的流程(未废弃)
     * @return
     * @throws Exception
     */
    public Row getSeriesLoan(String loan_no, boolean fk) throws Exception {

        //查询主合同
        Row mainRow = getMainLoan(loan_no);
        String main_loan_no = mainRow.getString("loan_no");

        Store additional = getAdditionalStore(main_loan_no, fk);
        mainRow.put("additional", additional);

        Store renawal = getRenawalStore(main_loan_no, fk);
        mainRow.put("renawal", renawal);

        if (renawal != null) {
            Row lastRow = renawal.get(renawal.size() - 1);
            String loan_type = lastRow.getString("loan_type");
            if ("03".equals(loan_type)) {//转等合同,查询其下的追加贷
                String item_loan_no = lastRow.getString("loan_no");
                Store renawalAdditional = getAdditionalStore(item_loan_no, fk);
                lastRow.put("additional", renawalAdditional);
            }
        }

        return mainRow;
    }


    /**
     * 根据主合同,查询下面所有续贷
     *
     * @param loan_no
     * @param fk      查询条件：是否放款
     * @return
     * @throws Exception
     */
    public Store getRenawalStore(String loan_no, boolean fk) throws Exception {

        Store renawalStore = new Store();
        getLoanListByAfter(renawalStore, loan_no, fk);
        if (renawalStore.size() == 0) {
            renawalStore = null;
        }


        return renawalStore;
    }


    /**
     * 根据主合同(或者转等合同),查询下面的追加贷
     *
     * @param loan_no
     * @param fk      查询条件：是否放款
     * @return
     * @throws Exception
     */
    public Store getAdditionalStore(String loan_no, boolean fk) throws Exception {

        String condition = "and f.review_no!='D' ";
        if (fk) {
            condition = "and d.fk_date is not null and d.fk_date!='' ";
        }
        String sql = "select a.*,b.con_code,c.cust_name,d.loan_type,d.rep_type,d.after_status "
                + "from loan_info a "
                + "left join loan_con b on a.loan_no=b.loan_no "
                + "left join loan_cust c on a.loan_no=c.loan_no "
                + "left join loan_info_et d on d.loan_no=a.loan_no "
                + "left join wf_review_biz e on e.table_key_no=a.loan_no "
                + "left join wf_review f on f.review_no=e.review_no "
                + "where a.renewal_loan_no='" + loan_no + "' and d.loan_type='05' "
                + condition;
        Store additionalStore = baseDao.query(sql);


        return additionalStore;
    }


    /**
     * 根据贷款查询到主合同
     *
     * @param loan_no
     * @return
     * @throws Exception
     */
    public Row getMainLoan(String loan_no) {


        String sql = "select a.*,b.con_code,c.cust_name,d.loan_type,d.rep_type,d.after_status,d.settle_date "
                + "from loan_info a "
                + "left join loan_con b on a.loan_no=b.loan_no "
                + "left join loan_cust c on a.loan_no=c.loan_no "
                + "left join loan_info_et d on d.loan_no=a.loan_no "
                + "where a.loan_no='" + loan_no + "'";
        Row loanRow = baseDao.queryRow(sql);

        String renewal_loan_no = loanRow.getString("renewal_loan_no");
        String loan_type = loanRow.getString("loan_type");
        if (Util.isNotEmpty(renewal_loan_no) && !"02".equals(loan_type)) {
            loanRow = getMainLoan(renewal_loan_no);
        }


        return loanRow;
    }

    /**
     * 向后获取续期转等系列贷款
     *
     * @param store
     * @param fk              查询条件：是否放款
     * @param renewal_loan_no
     * @throws Exception
     */
    private void getLoanListByAfter(Store store, String renewal_loan_no, boolean fk) throws Exception {

        String condition = "and f.review_no!='D' ";
        if (fk) {
            condition = "and d.fk_date is not null and d.fk_date!='' ";
        }

        String sql = "select a.*,b.con_code,c.cust_name,d.loan_type,d.rep_type,d.after_status "
                + "from loan_info a "
                + "left join loan_con b on a.loan_no=b.loan_no "
                + "left join loan_cust c on a.loan_no=c.loan_no "
                + "left join loan_info_et d on d.loan_no=a.loan_no "
                + "left join wf_review_biz e on e.table_key_no=a.loan_no "
                + "left join wf_review f on f.review_no=e.review_no "
                + "where a.renewal_loan_no='" + renewal_loan_no + "' "
                + condition
                + "and d.loan_type in ('03','04')";
        Row loan = baseDao.queryRow(sql);
        if (loan != null) {
            store.add(loan);
            String loan_no = loan.get("loan_no") == null ? "" : loan.get("loan_no").toString();
            getLoanListByAfter(store, loan_no, fk);
        }
    }

}
