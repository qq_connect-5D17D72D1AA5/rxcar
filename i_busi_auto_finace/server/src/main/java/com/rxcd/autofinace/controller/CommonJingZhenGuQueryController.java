package com.rxcd.autofinace.controller;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rxcd.autofinace.util.RxErpUtil;
import org.appframe.bigdata.JingZhenGuClinet2;
import org.appframe.wf.db.Row;
import org.appframe.wf.db.Store;
import org.appframe.wf.util.ErpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 精真估出险接口
 *
 * @author liuchang
 */
@RestController
@RequestMapping("/")
public class CommonJingZhenGuQueryController {


    @Autowired
    private RxErpUtil erpUtil;
    @Autowired
    private JingZhenGuClinet2 jingZhenGuClinet;



    @RequestMapping("/loan_common_CommonJingZhenGuQuery")
    public Row execute(@RequestParam("loan_no") String loan_no) throws Exception {
        loan_no = loan_no.replaceAll("'", "");
        Row loanRow = erpUtil.getLoanBasicDataInfo(loan_no);
        String cjh = ErpUtil.objectIsNullGetString(loanRow.get("cjh"));
        String bodyContent = jingZhenGuClinet.queryCarDanger(cjh);
        JSONObject jsonObject = JSONObject.parseObject(bodyContent);
        JSONObject summaryData = jsonObject.getJSONObject("summaryData");
        String claimCount = summaryData.getString("claimCount");//维修次数
        String renewCount = summaryData.getString("renewCount");//总换件件数
        String repairCount = summaryData.getString("repairCount");//总维修件数
        JSONArray carClaimRecords = jsonObject.getJSONArray("carClaimRecords");
        Store resultStore = new Store();
        for (Object o : carClaimRecords) {
            JSONObject carClaimRecord = (JSONObject) o;
            Row carClaimRecordRow = new Row();
            String damageMoney = getSubstring(carClaimRecord.getString("damageMoney"));//本次出险金额
            String dangerTime = carClaimRecord.getString("dangerTime");//本次出险时间
            JSONArray claimDetails = carClaimRecord.getJSONArray("claimDetails");//获取明细
            Store claimDetailStore = new Store();
            for (Object w : claimDetails) {
                JSONObject claimDetail = (JSONObject) w;
                Row claimDetailRow = new Row();
                String itemAmount = getSubstring(claimDetail.getString("itemAmount"));//费用
                String itemName = claimDetail.getString("itemName");//位置
                String itemType = claimDetail.getString("itemType");//类型
                claimDetailRow.put("itemAmount", itemAmount);
                claimDetailRow.put("itemName", itemName);
                claimDetailRow.put("itemType", itemType);
                claimDetailStore.add(claimDetailRow);
            }
            carClaimRecordRow.put("damageMoney", damageMoney);
            carClaimRecordRow.put("dangerTime", dangerTime);
            carClaimRecordRow.put("claimDetails", claimDetailStore);
            resultStore.add(carClaimRecordRow);
        }
        Row ret = new Row();

        ret.set("claimCount", claimCount);
        ret.set("renewCount", renewCount);
        ret.set("repairCount", repairCount);
        ret.set("list", resultStore);
        ret.set("path", "/CommonJingZhenGuQuery");
        return ret;
//        result.JSP();
    }

    /**
     * 通过substring方法去掉最后两位
     *
     * @param param
     * @return
     * @throws Exception
     */
    public static String getSubstring(String param) throws Exception {
        if (param.length() <= 2) {
            return param;
        }
        String result = param.substring(0, param.length() - 2);
        return result;
    }
}
