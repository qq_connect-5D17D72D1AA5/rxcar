/**
 * 创建者：ZhaoSheng 创建时期： 2013-07-15
 */
package com.rxcd.autofinace.controller;


import com.rxcd.autofinace.common.BizErrorCodeEnum;
import com.rxcd.autofinace.service.CostService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.appframe.wf.db.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 费用单展现
 */
@RestController
@RequestMapping("/")
public class CostShowController {

    private static final Log log = LogFactory.getLog(CostShowController.class);

    @Autowired
    private CostService costService;

    /**
     * 费用单 新
     *
     * @param loan_no
     * @param mid_no
     * @return
     * @throws Exception
     */
    @RequestMapping("/loan_common_CostShow")
    public Row costShow(@RequestParam String loan_no, @RequestParam String mid_no) throws Exception {
        Row ret = new Row();
        if (StringUtils.isEmpty(loan_no)) {
            throw new Exception(BizErrorCodeEnum.B0020.getCode());
        }
        Row map = costService.costSheet(loan_no);
        ret.put("type", "costInfo");
        ret.put("data", map);
        return ret;
    }


}