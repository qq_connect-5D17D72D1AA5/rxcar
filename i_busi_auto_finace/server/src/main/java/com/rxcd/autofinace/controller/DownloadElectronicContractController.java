package com.rxcd.autofinace.controller;


import org.appframe.contract.JunZiQianClient;
import org.appframe.wf.dao.BaseDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * 下载合同pdf
 */
@RestController
@RequestMapping("/")
public class DownloadElectronicContractController {

    @Autowired
    private BaseDao baseDao;

    @Autowired
    private JunZiQianClient JunZiQianClient;

    @RequestMapping("/loan_common_DownloadElectronicContract")
    public void execute(HttpServletRequest request, HttpServletResponse response, @RequestParam String apply_no) throws Exception {
        String applyNo = apply_no;
        InputStream is = null;
        OutputStream os = null;
        try {
            String fileUrl = JunZiQianClient.queryConFileUrl(applyNo);
            if (fileUrl == null) {
                throw new Exception("申请编号不存在");
            }
            // 构造URL
            URL url = new URL(fileUrl);
            // 打开连接
            URLConnection con = url.openConnection();
            // 设置请求超时为5s
            con.setConnectTimeout(5 * 1000);
            // 输入流
            is = con.getInputStream();
            os = response.getOutputStream();
            byte[] buffer = new byte[4 * 1024]; // 4k Buffer
            int read = 0;
            while ((read = is.read(buffer)) != -1) {
                os.write(buffer, 0, read);
            }
            os.flush();
        } catch (FileNotFoundException e) {
            throw new Exception(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        } finally {
            if (os != null) {
                os.close();
            }
            if (is != null) {
                is.close();
            }
        }
        return;
    }
}
