/**
 * 版权所有：版权所有：重庆易房网络科技股份有限公司
 * Chongqing YiFang Network Technology Co. LTD.  All Rights Reserved
 * 创建者：HeSenLin   创建时期：  2016年5月10日
 */
package com.rxcd.autofinace.flowEvent;


import com.rxcd.autofinace.service.client.ClearClient;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.engine.AbstractWorkFlowEvent;
import org.appframe.wf.service.ReviewService;
import org.appframe.wf.service.WfParamUtil;
import org.appframe.wf.util.Param;
import org.appframe.wf.util.SafeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 网点开设流程
 */
@Component
public class JoinBranchWorkFlowEventImp extends AbstractWorkFlowEvent {
    @Autowired
    private ReviewService reviewService;

    @Autowired
    private WfParamUtil paramUtil;

    @Autowired
    private BaseDao baseDao;

    @Autowired
    ClearClient clearClient;

    /**
     * 流程废弃时回调方法
     */
    public void workFlowDiscard(Param p) throws Exception {

    }

    /**
     * 流程開始时回调方法
     */
    @Override
    public void workFlowStart(Param p) throws Exception {
        // 流程NO
        Row reviewObject = (Row) paramUtil.get("reviewObject");
        String review_no = reviewObject.getString("review_no");
        String conCode = "JOIN-"+String.valueOf(new Date().getTime());
        String table_key_no = (String) baseDao.queryObject("select table_key_no from wf_review_biz where review_no='" + SafeUtil.safe(review_no) + "'");
        String sql = "update join_branch set join_branch.con_code = '"+conCode+"' where branch_join_no = '"+table_key_no+"'";
        baseDao.execute(sql);
    }

    /**
     * 流程结束时回调方法
     */
    @Override
    public void workFlowFinish(Param p) throws Exception {
        Row reviewObject = (Row) paramUtil.get("reviewObject");
        String review_no = reviewObject.getString("review_no");
        String branch_join_no=reviewService.getReviewBizByReviewNo(review_no).getString("table_key_no");
        clearClient.createBranchAccount(branch_join_no);
    }

}
