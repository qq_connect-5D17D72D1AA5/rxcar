package com.rxcd.autofinace.util;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.appframe.wf.util.Util;

import java.net.URI;
import java.net.URL;

/*****************
* @ClassName: SmsOpenUtils
* @Description: 短信工具类
* @author xx_erongdu
* @date 2013-7-22 下午1:53:11
*
*****************/ 
public class SmsOpenUtils {

	private static Log logger = LogFactory.getLog(SmsOpenUtils.class);

	/**
	 * 短信发送

	 * @param mobile
	 * @param content
	 * @return 发送响应结果
	 */
	public static String sendSms(String mobile, String content){
		String url = "http://www.ztsms.cn:8800/sendXSms.do";
		String username = "rxhl";
		String password = "JnvODO8S";
		String sendResult = null;
		try {
			HttpClient httpclient = Util.getDefaultHttpClient();
			StringBuffer sb = new StringBuffer(url);
			if(url.contains("?")){
				sb.append("&");
			}else{
				sb.append("?");
			}
			sb.append("username=");
			sb.append(username);
			sb.append("&password=");
			sb.append(password);
			sb.append("&mobile=");
			sb.append(mobile);
			sb.append("&content=");
			sb.append(content);
			sb.append("&productid=");
			sb.append("727727");
			URL url2 = new URL(sb.toString());
			URI uri = new URI(url2.getProtocol(), url2.getHost(), url2.getPath(), url2.getQuery(), null);
			HttpGet httpgets = new HttpGet(uri);
			HttpResponse response = httpclient.execute(httpgets);
			HttpEntity entity = response.getEntity();
			sendResult=EntityUtils.toString(entity, "utf-8");
			httpgets.abort();
		} catch (Exception e) {
			logger.error(e);
		} 
		return sendResult;
	}
}
