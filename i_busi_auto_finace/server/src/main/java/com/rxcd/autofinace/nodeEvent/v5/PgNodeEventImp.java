package com.rxcd.autofinace.nodeEvent.v5;


import com.rxcd.autofinace.service.LoanCheckServiceImpl;
import com.rxcd.autofinace.service.LoanService;
import com.rxcd.autofinace.util.RxErpUtil;
import org.appframe.wf.engine.AbstractWorkFlowNodeEvent;
import org.appframe.wf.service.WfParamUtil;
import org.appframe.wf.util.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 评估
 *
 * @author hubin
 */
@Component
public class PgNodeEventImp extends AbstractWorkFlowNodeEvent {
    @Autowired
    private WfParamUtil paramUtil;
    @Autowired
    private LoanCheckServiceImpl checkService;
    @Autowired
    private RxErpUtil erpUtil;
    @Autowired
    private LoanService loanService;

    @Override
    public void workFlowNodeStart(String flow_node_no, Param p) throws Exception {
        final String loanNo = getTableKeyNo();
    }

    @Override
    public void workFlowNodeOverrule(String flow_node_no, Param p) throws Exception {
        checkTempletFlowNo("0902");
    }

    @Override
    public void workFlowNodeVie(String flow_node_no, Param p) throws Exception {

    }

    @Override
    public void workFlowNodeFinish(String flow_node_no, Param p) throws Exception {
        checkTempletFlowNo("1");
        String loan_no = getTableKeyNo();
        loanService.updateMaxApproveSum(loan_no);
        loanService.updateApproveSumByMaxApproveSumAndCarPrice(loan_no);
        loanService.updateDepositSum(loan_no);
        loanService.updateConAmount(loan_no);
        loanService.updateAdvanceRateSum(loan_no);
        checkService.check_approve_sum(loan_no);
        loanService.updateEmployersV2(loan_no);
    }
}
