/**
 * 版权所有：版权所有：重庆易房网络科技股份有限公司
 * Chongqing YiFang Network Technology Co. LTD.  All Rights Reserved
 * 创建者：HeSenLin   创建时期：  2016年5月10日
 */
package com.rxcd.autofinace.flowEvent;


import com.rxcd.autofinace.service.LoanService;
import com.rxcd.autofinace.util.RxErpUtil;
import org.apache.commons.lang.StringUtils;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.engine.AbstractWorkFlowEvent;
import org.appframe.wf.engine.imp.DefaultWorkFlow;
import org.appframe.wf.service.ReviewService;
import org.appframe.wf.service.WfParamUtil;
import org.appframe.wf.util.ErpUtil;
import org.appframe.wf.util.Param;
import org.appframe.wf.util.SafeUtil;
import org.appframe.wf.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 贷前流程结束时创建资金账户及放款流程
 */
@Component
public class AddPayWorkFlowEventImp extends AbstractWorkFlowEvent {

    @Autowired
    private WfParamUtil paramUtil;

    @Autowired
    private BaseDao baseDao;

    @Autowired
    private ReviewService reviewService;

    @Autowired
    private RxErpUtil rxErpUtil;

    @Autowired
    private LoanService loanService;

    @Autowired
    private DefaultWorkFlow flow;

    /**
     * 流程废弃时回调方法
     */
    public void workFlowDiscard(Param p) throws Exception {

    }

    /**
     * 流程開始时回调方法
     */
    @Override
    public void workFlowStart(Param p) throws Exception {
        Row reviewObject = (Row) paramUtil.get("reviewObject");
        String review_no = reviewObject.getString("review_no");
        String templet_flow_no = reviewObject.getString("templet_flow_no");
        String loan_no = (String) baseDao.queryObject("select table_key_no from wf_review_biz where review_no='" + SafeUtil.safe(review_no) + "'");
        baseDao.execute("update loan_info set create_time= now()  where loan_no='"+loan_no+"'");
        if (StringUtils.equals("1", templet_flow_no)) {
            loanService.updateConsultInfo(loan_no);
        }
    }

    /**
     * 流程结束时回调方法
     */
    @Override
    public void workFlowFinish(Param p) throws Exception {
        Row reviewObject = (Row) paramUtil.get("reviewObject");
        String review_no = reviewObject.getString("review_no");
        String table_key_no = reviewService.getReviewBizByReviewNo(review_no).getString("table_key_no");
        Row loanRow = rxErpUtil.getLoanBasicDataInfoV2(table_key_no);
        final String loan_id = ErpUtil.objectIsNullGetString(loanRow.get("loan_info.loan_id"));
        final String loan_no = ErpUtil.objectIsNullGetString(loanRow.get("loan_info.loan_no"));
        //流程结束替换之前销售输入的公里数
        String sql = "select mileage from loan_ass where loan_no = '" + loan_no + "'";
        Row assRow = baseDao.queryRow(sql);
        String mileage = ErpUtil.objectIsNullGetString(assRow.get("mileage"));
        if (Util.isNotEmpty(mileage)) {
            sql = "update loan_car set mileage = '" + mileage + "' where loan_no = '" + loan_no + "'";
            baseDao.execute(sql);
        }
//        p = JSON.parseObject(JSONObject.toJSONString(p), Param.class);
//        Map<String, Object> map = flow.beforeInitAllSubFlow();
//        if (StringUtils.equals("1", reviewObject.getString("templet_flow_no"))) {
//            flow.initWorkFlow("0901", null, p);
//            Row retRow = flow.addWorkFlow();
//            // 执行流程扩展业务
//            workExtendBiz(retRow, loan_id, loan_no, "update recovery_info a,wf_review_biz b set a.loan_id='");
//            flow.reInitHandler();
//        }
//        flow.closeAllSubFlow(map);
    }


    public void workExtendBiz(Row retRow, String loan_id, String loan_no, String s) {
        String sql;
        // 新流程业务数据
        Row data = (Row) retRow.get("data");
        String review_no = data.getString("review_no");
        sql = s + SafeUtil.safe(loan_id) + "',a.loan_no='" + SafeUtil.safe(loan_no) + "' "
                + "where a.no=b.table_key_no and b.review_no='" + SafeUtil.safe(review_no) + "';";
        baseDao.execute(sql);

    }
}
