package com.rxcd.autofinace.check;

import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Component
public class CheckOperLog {

    @Autowired
    private BaseDao baseDao;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Row insert(String key, String name, String table, String table_key_no) throws Exception {
        String sql1 = "select * from wf_check where table_name='" + table + "' and table_key_no='" + table_key_no + "' and check_key='" + key + "'";
        Row checkRow = baseDao.queryRow(sql1);
        if (checkRow == null) {
            String sql2 = "insert into wf_check (check_no,table_name,table_key_no,check_name,check_key,check_flag,oper_time,status) "
                    + "values ("
                    + "'" + Util.uuid() + "',"
                    + "'" + table + "',"
                    + "'" + table_key_no + "',"
                    + "'" + name + "',"
                    + "'" + key + "',"
                    + "'1',"
                    + "'" + Util.format(new Date(), "yyyy-MM-dd HH:mm:ss") + "',"
                    + "'1')";
            baseDao.execute(sql2);
            checkRow = baseDao.queryRow(sql1);
        }
        return checkRow;
    }
}
