package com.rxcd.autofinace.controller;


import com.fida.clearing.AccountClient;
import com.fida.clearing.BillClient;
import com.rxcd.autofinace.util.StringUtil;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * 代扣服务
 *
 * @author liuchang
 */
@RestController
@RequestMapping("/")
public class WithholdController {

    private final static Logger log = LoggerFactory.getLogger(WithholdController.class);
    @Autowired
    private BaseDao baseDao;
    @Autowired
    private BillClient billClient;
    @Autowired
    private AccountClient accountclient;

    @RequestMapping("/loan_common_WithholdController")
    public Row execute(@RequestParam("cust_name") String cust_name,@RequestParam("idcard") String idcard,@RequestParam("con_code") String con_code,@RequestParam("amount") double amount) throws Exception {
        String account_id = "";
        Map<String, Object> map = accountclient.queryAccountByCertNo(idcard);
        log.info("账户信息:"+map);
        JSONObject josnObject = JSONObject.fromObject(map);
        if(josnObject!=null){
            JSONArray jsonArray = josnObject.getJSONArray("accountList");
            log.info("账户信息jsonArray:"+jsonArray);
            if(jsonArray!=null&&jsonArray.size()>0){
                JSONObject accountJosn = jsonArray.getJSONObject(0);
                account_id = accountJosn.getString("account_id");
            }
        }
        if(StringUtils.equals("",account_id)){
            throw new Exception("账户不存在");
        }
        log.info("clearing_ordinary:"+account_id);
        String loan_no = "";
        if(!StringUtils.equals("",con_code)){
            String sql = "select * from loan_con where con_code = '"+con_code+"'";
            Row custRow = baseDao.queryRow(sql);
            loan_no = custRow.getString("loan_no");
        }
        log.info("贷款编号:"+loan_no);
        log.info("代扣金额为:"+amount);
        Row row = new Row();
        //创建在线充值账单
        Map<String,Object> onlineRechargeBillResult = billClient.createOnlineRechargeBill(
                amount,null,
                account_id, null,null,
                "4bc199af-044f-497f-a6ea-c7657f2d0c08",null, null, loan_no);
        log.info("onlineRechargeBillResult:"+onlineRechargeBillResult);
        if(StringUtils.equals("1",String.valueOf(onlineRechargeBillResult.get("resultCode")))
                && onlineRechargeBillResult.get("rows") != null
                && !((List)onlineRechargeBillResult.get("rows")).isEmpty()){
            log.info("结算充值账单进入");
            Map<String, Object> bill = ((List<Map<String, Object>>)onlineRechargeBillResult.get("rows")).get(0);
            Map<String, Object> settleBillResult = billClient.settleBill((String)bill.get("billId"));
            //结算不成功时, resultCode == "1"表示结算成功，errCode == "800022"表示支付发起，等待支付结果返回
            log.info("settleBillResult:"+settleBillResult);
            for (Map.Entry<String, Object> entry : settleBillResult.entrySet()) {
                row.set(entry.getKey(), entry.getValue());
            }
            return row;
        }else{
            for (Map.Entry<String, Object> entry : onlineRechargeBillResult.entrySet()) {
                row.set(entry.getKey(), entry.getValue());
            }
            return row;
        }
    }
}
