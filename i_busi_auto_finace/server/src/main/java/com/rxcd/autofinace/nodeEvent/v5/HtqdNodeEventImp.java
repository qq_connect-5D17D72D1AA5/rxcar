package com.rxcd.autofinace.nodeEvent.v5;


import com.rxcd.autofinace.service.LoanCheckServiceImpl;
import com.rxcd.autofinace.service.LoanService;
import org.appframe.wf.engine.AbstractWorkFlowNodeEvent;
import org.appframe.wf.util.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * 签订合同
 */
@Component
public class HtqdNodeEventImp extends AbstractWorkFlowNodeEvent {

    @Autowired
    private LoanService loanService;

    @Autowired
    private LoanCheckServiceImpl checkService;

    @Override
    public void workFlowNodeOverrule(String flow_node_no, Param p) throws Exception {
        checkTempletFlowNo("1");
    }

    @Override
    public void workFlowNodeVie(String flow_node_no, Param p) throws Exception {

    }

    /**
     * 流程节点启动时调用
     *
     * @param flow_node_no
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowNodeStart(String flow_node_no, Param p) throws Exception {

    }

    @Override
    public void workFlowNodeFinish(String flow_node_no, Param p) throws Exception {
        checkTempletFlowNo("1");
        String loanNo = getTableKeyNo();
        checkService.check_sign_successful(loanNo);
        loanService.createUserAccount(loanNo);
    }
}
