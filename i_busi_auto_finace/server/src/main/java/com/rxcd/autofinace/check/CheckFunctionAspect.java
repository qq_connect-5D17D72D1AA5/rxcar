package com.rxcd.autofinace.check;

import com.rxcd.autofinace.annotation.CheckFunction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.DB;
import org.appframe.wf.db.Row;
import org.appframe.wf.util.Util;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Method;
import java.util.Date;

@Aspect
@Component
public class CheckFunctionAspect {

    private final static Log log = LogFactory.getLog(CheckFunctionAspect.class);
    @Autowired
    private BaseDao baseDao;

    @Autowired
    private CheckOperLog checkOperLog;

    @Pointcut("execution(* com.rxcd.autofinace.service.LoanCheckServiceImpl.*(..))")
    public void annotationPointCut() {
    }

    @Around("annotationPointCut()")
    public void around(ProceedingJoinPoint joinPoint) throws Exception {
        MethodSignature sign = (MethodSignature) joinPoint.getSignature();
        Method method = sign.getMethod();
        Object[] args = joinPoint.getArgs();
        try {
            /*原对象方法调用前处理日志信息*/
//            joinPoint.getTarget().getClass().getInterfaces()[0].getMethod(method.getName(), String.class).getAnnotation(CheckFunction.class);
            boolean check_flag = true;
            //如果有CheckFunction注解认为是开关方法
            Class[] classes = new Class[args.length];
            for (int i = 0; i < args.length; i++) {
                classes[i] = args[i].getClass();
            }

            CheckFunction checkFunction = joinPoint.getTarget().getClass().getInterfaces()[0].getMethod(method.getName(), classes).getAnnotation(CheckFunction.class);

            if (checkFunction == null) {
                joinPoint.proceed();
                return;
            }
            String key = checkFunction.key();
            String name = checkFunction.name();
            String table = checkFunction.table();
            log.info("table=" + table + ",key=" + key + ",name=" + name);

            String table_key_no = args[0].toString();

            Row checkRow = checkOperLog.insert(key, name, table, table_key_no);
             if (!"1".equals(checkRow.getString("check_flag"))) {
                check_flag = false;
            }


            if (check_flag) {
                //TODO 全局验证开关
                //调用目标方法
                joinPoint.proceed();
            }
            /*原对象方法调用后处理日志信息*/
            log.info("代理方法执行成功");
        } catch (Throwable e) {
            Exception exception = new Exception(e.getMessage());
            log.info("代理方法异常");
            e.printStackTrace();
            throw exception;
        }
        return;
    }



}
