package com.rxcd.autofinace.formula;


import com.rxcd.autofinace.util.RxErpUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.service.ReviewService;
import org.appframe.wf.util.ErpUtil;
import org.appframe.wf.util.Param;
import org.appframe.wf.util.SafeUtil;
import org.appframe.wf.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LoanRelyonRule {
    private final static Log log = LogFactory.getLog(LoanRelyonRule.class);
    @Autowired
    private BaseDao baseDao;
    @Autowired
    private RxErpUtil erpUtil;
    @Autowired
    private ReviewService reviewService;

    public boolean yfk(String review_no, Param p) {
        return false;
    }


    /**
     * 考察节点自动过（合同金额20W以下 自动过）
     *
     * @param review_no
     * @param p
     * @return
     * @throws Exception
     */
    public boolean ycInvestigatePass(String review_no, Param p) throws Exception {
        boolean cross = false;
        String sql = "select a.* from loan_cost a left join wf_review_biz b on a.loan_no=b.table_key_no where b.review_no='" + review_no + "'";
        Row costRow = baseDao.queryRow(sql);
        double contract_sum = ErpUtil.objectIsNullGetDouble(costRow.get("contract_sum"));
        if (contract_sum >= 200000) {
            return true;
        }
        return cross;
    }

    /**
     * 远程车信息节点自动过
     *
     * @param review_no
     * @param p
     * @return
     * @throws Exception
     */
    public boolean ycCarInfo(String review_no, Param p) throws Exception {
        boolean cross = false;
        String sql = "select a.car_no,a.cjh from loan_car a left join wf_review_biz b on a.loan_no=b.table_key_no where b.review_no='" + review_no + "'";
        Row car = baseDao.queryRow(sql);
        if (car != null && car.get("car_no") != null && car.get("cjh") == null) {
            cross = true;
        }
        return cross;
    }

    /**
     * 远程全款GPS系统风险报告
     *
     * @param review_no
     * @param p
     * @return
     */
    public boolean ycQkGpsXtFxBgAuto(String review_no, Param p) {
        return false;
    }


    /**
     * 远程进件全款GPS查档自动通过
     *
     * @param review_no
     * @param p
     * @return
     */
    public boolean ycQkGpsCdAuto(String review_no, Param p) {
        return false;
    }

    /**
     * 远程进件全款GPS合同修正
     *
     * @param review_no
     * @param p
     * @return
     */
    public boolean ycQkGpsHtXzAuto(String review_no, Param p) {
        return false;
    }

    /**
     * 检查是否自动通过财务审核
     *
     * @param review_no
     * @param p
     * @return
     * @throws Exception
     */
    public boolean isPassFkfhNode(String review_no, Param p) throws Exception {
        if (getIsRenewalRule(review_no, p)) {
            return false;
        } else {
            return false;
        }

    }

    /**
     * 贷款是否为续期，如果是就直接通过此节点
     *
     * @param review_no
     * @param p
     * @return
     * @throws Exception
     */
    public boolean getIsRenewalRule(String review_no, Param p) throws Exception {
        boolean flag = true;
        String sql = "select loan_info_et.loan_type from loan_info_et join wf_review_biz on wf_review_biz.status='1' "
                + "and loan_info_et.loan_no=wf_review_biz.table_key_no and wf_review_biz.review_no='" + SafeUtil.safe(review_no) + "' "
                + "where "
                + "loan_info_et.status='1' ";
        String loan_type = (String) baseDao.queryObject(sql);
        //04为续期，03位转等
        if ("04".equals(loan_type) || "03".equals(loan_type)) {
            flag = false;
        }
        return flag;
    }

    /**
     * 判断贷款是否过评估节点
     *
     * @param review_no
     * @param p
     * @return
     * @throws Exception
     */
    public boolean getEvaluateRule(String review_no, Param p) throws Exception {
        boolean flag = false;
        String loan_no = reviewService.getReviewBizByReviewNo(review_no).getString("table_key_no");
        log.info("loan_no:"+loan_no);
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String loan_type = erpUtil.objectIsNullGetString(loanRow.get("loan_info_et.loan_type"));
        if ("01".equals(loan_type) || "02".equals(loan_type)) {
            int car_age=erpUtil.objectIsNullGetInteger(loanRow.get("loan_ass.car_age"));
            double mileage=erpUtil.objectIsNullGetDouble(loanRow.get("loan_ass.mileage"));
            String  car_audi_text=erpUtil.objectIsNullGetString(loanRow.get("loan_ass.car_audi_text"));
            double insurance_compensation=erpUtil.objectIsNullGetDouble(loanRow.get("loan_ass.insurance_compensation"));
            double mlieage_per_year=12*mileage/car_age;
            double ass_score=erpUtil.objectIsNullGetDouble(loanRow.get("loan_ass.ass_score"));
            if(ass_score>50//汽车终评50分以上
                    ||mlieage_per_year<5000//年里程低于5000
                    ||(Util.isNotEmpty(car_audi_text)&&!"无".equals(car_audi_text))//自填车系
                    ||insurance_compensation>10000){//出险10000元以上
                flag = true;
            }
        }
        return flag;
    }

    public boolean defaultTrue(String review_no, Param p) {
        return true;
    }


    public boolean defaultFalse(String review_no, Param p) {
        return false;
    }

    /**
     * 跨年评估需要通过规则
     *
     * @param review_no
     * @param transaction
     * @param
     * @return
     * @throws Exception
     */
//	public boolean getEvaluateByDateRule(String review_no, Param p) throws Exception{
//		boolean flag=getIsRenewalRule(review_no, transaction, p);
//		if(flag){//如果不是续期，转等，返回true,此节点要继续过
//			return flag;
//		}
//		//统一去掉年份比较
//		String start="12-15";//12.15开始
//		String end="01-15";//1.15截止
//		
//		long s_long=Util.parseDate(start,"MM-dd").getTime();
//		long e_long=Util.parseDate(end,"MM-dd").getTime();
//		
//		String now=Util.date2StringDay(new Date()).substring(5);
//		long n_long=Util.parseDate(now,"MM-dd").getTime();
//		
//		if(s_long<=e_long){
//			if(n_long>=s_long&&n_long<=e_long){
//				return true;
//			}else{
//				return false;
//			}
//		}else{
//			if(n_long>=s_long||n_long<=e_long){
//				return true;
//			}else{
//				return false;
//			}
//		}
//	}
//    private static String getTableKeyNo(String review_no, Transaction transaction) throws Exception {
//        String table_key_no = (String) baseDao.queryObject("select table_key_no from wf_review_biz where review_no='" + review_no + "' ");
//        return table_key_no;
//    }
}
