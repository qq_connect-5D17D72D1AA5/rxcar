package com.rxcd.autofinace.controller;

import com.rxcd.autofinace.util.RxErpUtil;
import org.apache.commons.lang.StringUtils;
import org.appframe.wf.db.Row;
import org.appframe.wf.db.Store;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/")
public class IncomeMainController {

    @Autowired
    private RxErpUtil erpUtil;

    @RequestMapping("/income_main")
    public Row execute(@RequestParam("loan_no") String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String cust_name = erpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_name"));
        String idcard = erpUtil.objectIsNullGetString(loanRow.get("loan_cust.idcard"));
        String cust_phone = erpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_phone"));
        String bank_accout = erpUtil.objectIsNullGetString(loanRow.get("loan_info_et.bank_accout"));
        String bank_accout_often1 = erpUtil.objectIsNullGetString(loanRow.get("loan_info_et.bank_accout_often1"));
        String bank_accout_often2 = erpUtil.objectIsNullGetString(loanRow.get("loan_info_et.bank_accout_often2"));
        Store store = new Store();
        Row carRow = new Row();
        carRow.put("card",bank_accout);
        store.add(carRow);
        if(StringUtils.isNotEmpty(bank_accout_often1)){
            Row carRowoften = new Row();
            carRowoften.put("card",bank_accout_often1);
            store.add(carRowoften);
        }
        if(StringUtils.isNotEmpty(bank_accout_often2)){
            Row carRowoften = new Row();
            carRowoften.put("card",bank_accout_often2);
            store.add(carRowoften);
        }
        Row ret = new Row();
        ret.put("name", cust_name);
        ret.put("idcard", idcard);
        ret.put("mobile", cust_phone);
        ret.put("cards", store);
        ret.put("path", "/getIncomeReporting");
        ret.put("type", "income");
        return ret;
    }


}
