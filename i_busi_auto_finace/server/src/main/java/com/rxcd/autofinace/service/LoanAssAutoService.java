/**
 * 版权所有：版权所有：重庆易房网络科技股份有限公司
 * Chongqing YiFang Network Technology Co. LTD.  All Rights Reserved
 * 创建者：HeSenLin   创建时期：  2016年6月3日
 */
package com.rxcd.autofinace.service;


import com.alibaba.fastjson.JSONObject;
import com.rxcd.autofinace.util.RxErpUtil;
import org.apache.commons.lang.StringUtils;
import org.appframe.common.BigDecimalUtil;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.db.Store;
import org.appframe.wf.util.Formula;
import org.appframe.wf.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;

/**
 * 车辆自动评估
 *
 * @author zhoukuanding
 */
@Component
public class LoanAssAutoService {

//	private final static Log log = LogFactory.getLog(LoanAssAutoService.class);

    /**
     * 分行得分
     */
    public static final String BRANCH_SCORE = "01";
    /**
     * 真实分
     */
    public static final String TRUE_SCORE = "02";
    /**
     * 分行初价
     */
    public static final String BRANCH_INIT_PRICE = "03";
    /**
     * 真实初价
     */
    public static final String TURE_INIT_PRICE = "04";
    /**
     * 分行终价
     */
    public static final String BRANCH_FINAL_PRICE = "05";
    /**
     * 真实终价
     */
    public static final String TRUE_FINAL_PRICE = "06";

    @Autowired
    private RxErpUtil erpUtil;

    @Autowired
    private BaseDao baseDao;


    /**
     * 自动评估
     *
     * @param loan_no
     * @param assType 评估类型（01:分行得分；02:真实得分；03:分行初价；04:真实初价；05:分行终价；06:真实终价）
     * @throws Exception
     */
    public void autoAss(String loan_no, String assType) throws Exception {
        if (!(BRANCH_SCORE.equals(assType) || TRUE_SCORE.equals(assType) || BRANCH_INIT_PRICE.equals(assType)
                || TURE_INIT_PRICE.equals(assType) || BRANCH_FINAL_PRICE.equals(assType) || TRUE_FINAL_PRICE.equals(assType))) {
            throw new Exception("不支持当前评估类型：" + assType);
        }
        init(loan_no, assType);
        autoAssItem(loan_no, assType);
        updateResult(loan_no, assType);
    }


    /**
     * 汇总明细结果出最后结果并更新
     *
     * @param loan_no
     * @param assType 评估类型（01:分行得分；02:真实得分；03:分行初价；04:真实初价；05:分行终价；06:真实终价）
     */
    public void updateResult(String loan_no, String assType) {

        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        //接口初价
        double interface_price = erpUtil.objectIsNullGetDouble(loanRow.get("loan_ass.interface_price"));
        //接口终价
        double final_interface_price = erpUtil.objectIsNullGetDouble(loanRow.get("loan_ass.final_interface_price"));
        double system_ass_price, system_ass_price2;
        //汇总所有得分并更新
        Store results = baseDao.query("select * from loan_ass_auto where loan_no='" + loan_no + "' and ass_type='" + assType + "'");
        double result = 1;
        if (StringUtils.equals(BRANCH_SCORE, assType) || StringUtils.equals(TRUE_SCORE, assType)) {
            result = 0;
        }

        for (Row row : results) {
            if (StringUtils.equals(BRANCH_SCORE, assType) || StringUtils.equals(TRUE_SCORE, assType)) {
                result += erpUtil.objectIsNullGetDouble(row.get("result"));
            } else {
                result *= (1 + erpUtil.objectIsNullGetDouble(row.get("result")));
            }
        }
        result = BigDecimalUtil.round(result, 4);
        String sql = "";
        switch (assType) {
            case BRANCH_SCORE:
                sql = "update loan_ass set ass_score_branch=" + result + " where loan_no='" + loan_no + "'";
                break;
            case TRUE_SCORE:
                sql = "update loan_ass set ass_score=" + result + " where loan_no='" + loan_no + "'";
                break;
            case BRANCH_INIT_PRICE:
                system_ass_price = BigDecimalUtil.round(BigDecimalUtil.mul(interface_price, result), 0);
                sql = "update loan_ass set system_ass_price=" + system_ass_price + " where loan_no='" + loan_no + "'";
                break;
            case TURE_INIT_PRICE:
                system_ass_price = BigDecimalUtil.round(BigDecimalUtil.mul(interface_price, result), 0);
                sql = "update loan_ass set ass_price=" + system_ass_price + " where loan_no='" + loan_no + "'";
                break;
            case BRANCH_FINAL_PRICE:
                system_ass_price2 = BigDecimalUtil.round(BigDecimalUtil.mul(final_interface_price, result), 0);
                sql = "update loan_ass set system_ass_price2=" + system_ass_price2 + " where loan_no='" + loan_no + "'";
                break;
            case TRUE_FINAL_PRICE:
                system_ass_price2 = BigDecimalUtil.round(BigDecimalUtil.mul(final_interface_price, result), 0);
                sql = "update loan_ass set ass_price1=" + system_ass_price2 + " where loan_no='" + loan_no + "'";
                break;
        }
        if (Util.isNotEmpty(sql)) {
            baseDao.execute(sql);
        }
    }

    /**
     * 评估出明细结果
     *
     * @param loan_no
     * @param assType 评估类型（01:分行得分；02:真实得分；03:分行初价；04:真实初价；05:分行终价；06:真实终价）
     * @throws Exception
     */
    public void autoAssItem(String loan_no, String assType) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        baseDao.execute("delete from loan_ass_auto where loan_no='" + loan_no + "' and ass_type='" + assType + "'");
        String loan_id = erpUtil.objectIsNullGetString(loanRow.get("loan_info.loan_id"));
        Store rules = baseDao.query("select * from loan_ass_rule where find_in_set('" + assType + "',ass_type) and status='1' order by sort_no");

        //计算单项结果
        String assdimension, condition, condition_remark,
                score, true_score, initial_price_influence, true_initial_price_influence, final_price_influence, true_final_price_influence;
        Formula conFormula = null;
        double result = 0;
        Map<String, Object> param = new HashMap<String, Object>();
        List<String> sqlList = new ArrayList<String>();
        for (Row rule : rules) {

            assdimension = erpUtil.objectIsNullGetString(rule.get("ass_dimension"));
            condition = erpUtil.objectIsNullGetString(rule.get("condition_exp"));
            condition_remark = erpUtil.objectIsNullGetString(rule.get("condition_remark"));

            score = erpUtil.objectIsNullGetString(rule.get("score"));
            true_score = erpUtil.objectIsNullGetString(rule.get("true_score"));
            initial_price_influence = erpUtil.objectIsNullGetString(rule.get("initial_price_influence"));
            true_initial_price_influence = erpUtil.objectIsNullGetString(rule.get("true_initial_price_influence"));
            final_price_influence = erpUtil.objectIsNullGetString(rule.get("final_price_influence"));
            true_final_price_influence = erpUtil.objectIsNullGetString(rule.get("true_final_price_influence"));

            switch (assType) {
                case BRANCH_SCORE:
                    result = Util.isEmpty(score) ? 0.0 : Double.valueOf(score);
                    break;
                case TRUE_SCORE:
                    result = Util.isEmpty(true_score) ? 0.0 : Double.valueOf(true_score);
                    break;
                case BRANCH_INIT_PRICE:
                    result = Util.isEmpty(initial_price_influence) ? 0.0 : Double.valueOf(initial_price_influence);
                    break;
                case TURE_INIT_PRICE:
                    result = Util.isEmpty(true_initial_price_influence) ? 0.0 : Double.valueOf(true_initial_price_influence);
                    break;
                case BRANCH_FINAL_PRICE:
                    result = Util.isEmpty(final_price_influence) ? 0.0 : Double.valueOf(final_price_influence);
                    break;
                case TRUE_FINAL_PRICE:
                    result = Util.isEmpty(true_final_price_influence) ? 0.0 : Double.valueOf(true_final_price_influence);
                    break;
            }
            result = BigDecimalUtil.round(result, 6);
            // 条件公式
            conFormula = new Formula(condition);
            String[] keys = assdimension.split(",");
            param.clear();
            for (int i = 0; i < keys.length; i++) {
                param.put(keys[i], loanRow.get(keys[i]));
                conFormula.setVariable(keys[i], loanRow.get(keys[i]));
            }
            // 条件是否成立
            if (conFormula.bool()) {
                sqlList.add("insert into loan_ass_auto "
                        + "(auto_id,auto_no,loan_id,loan_no,ass_type,dimension,condition_exp,condition_remark,param,result,create_time,oper_time,status) "
                        + "values "
                        + "(uuid(),"
                        + " uuid(),"
                        + " '" + loan_id + "',"
                        + " '" + loan_no + "',"
                        + " '" + assType + "',"
                        + " '" + assdimension + "',"
                        + " '" + condition + "',"
                        + " '" + condition_remark + "',"
                        + "'" + JSONObject.toJSONString(param) + "',"
                        + " " + result + ","
                        + " '" + Util.format(new Date()) + "',"
                        + " '" + Util.format(new Date(), "YYYY-MM-dd HH:mm:ss") + "',"
                        + " '1')");
            }
        }
        baseDao.execute(sqlList);
    }

    /**
     * 初始化基础数据
     *
     * @param loan_no
     * @throws Exception
     */
    public void init(String loan_no, String assType) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        switch (assType) {
            case BRANCH_SCORE:
                initCarAge_First(loanRow);
                initCarRetention_First(loanRow);
                initBranchRating(loanRow);
                initBranchAge(loanRow);
                initBranchBadDebt(loanRow);
                initAssessorRating(loanRow);
                initAssessorWorkAge(loanRow);
                initSellerWorkAge(loanRow);
                break;
            case TRUE_SCORE:
                initCarAge_Final(loanRow);
                initCarRetention_Final(loanRow);
                break;
            case BRANCH_INIT_PRICE:
                initCarAge_First(loanRow);
                initCarRetention_First(loanRow);
                initBranchRating(loanRow);
                initBranchAge(loanRow);
                initBranchBadDebt(loanRow);
                initAssessorRating(loanRow);
                initAssessorWorkAge(loanRow);
                initSellerWorkAge(loanRow);
                break;
            case TURE_INIT_PRICE:
                initCarAge_First(loanRow);
                initCarRetention_First(loanRow);
                break;
            case BRANCH_FINAL_PRICE:
                initCarAge_Final(loanRow);
                initCarRetention_Final(loanRow);
                initBranchRating(loanRow);
                initBranchAge(loanRow);
                initBranchBadDebt(loanRow);
                initAssessorRating(loanRow);
                initAssessorWorkAge(loanRow);
                initSellerWorkAge(loanRow);
                break;
            case TRUE_FINAL_PRICE:
                initCarAge_Final(loanRow);
                initCarRetention_Final(loanRow);
                break;
        }
        initInsurance(loanRow);
    }

    /**
     * 初始化车龄(初)(当前日期-初始登记日期)
     * 由销售录入资料查接口得来
     *
     * @param loanRow
     */
    public void initCarAge_First(Row loanRow) throws Exception {
        String loan_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info.loan_no"));
        String inireg_date = erpUtil.objectIsNullGetString(loanRow.get("loan_car.inireg_date"));
        if (Util.isNotEmpty(inireg_date)) {
            int age = Util.monthsBetween(inireg_date, Util.format(new Date()));
            baseDao.execute("update loan_car set car_age=" + age + " where loan_no='" + loan_no + "'");
        }
    }

    /**
     * 初始化车龄(终)(当前日期-初始登记日期)
     * 由评估师录入资料查接口得来
     *
     * @param loanRow
     */
    public void initCarAge_Final(Row loanRow) throws Exception {
        String loan_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info.loan_no"));
        String inireg_date = erpUtil.objectIsNullGetString(loanRow.get("loan_ass.inireg_date"));
        if (Util.isNotEmpty(inireg_date)) {
            int age = Util.monthsBetween(inireg_date, Util.format(new Date()));
            baseDao.execute("update loan_ass set car_age=" + age + " where loan_no='" + loan_no + "'");
        }
    }

    /**
     * 初始化滞留时间(初)(初始登记日期-出厂日期)
     * 由销售录入资料查接口得来
     *
     * @param loanRow
     */
    public void initCarRetention_First(Row loanRow) throws Exception {
        String loan_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info.loan_no"));
        String inireg_date = erpUtil.objectIsNullGetString(loanRow.get("loan_car.inireg_date"));
        String production_date = erpUtil.objectIsNullGetString(loanRow.get("loan_car.production_date"));
        if (Util.isNotEmpty(inireg_date) && Util.isNotEmpty(production_date)) {
            int retentionMonth = Util.monthsBetween(production_date, inireg_date);
            baseDao.execute("update loan_car set car_retention=" + retentionMonth + " where loan_no='" + loan_no + "'");
        }
    }

    /**
     * 初始化滞留时间(终)(初始登记日期-出厂日期)
     * 由评估师录入资料查接口得来
     *
     * @param loanRow
     */
    public void initCarRetention_Final(Row loanRow) throws Exception {
        String loan_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info.loan_no"));
        String inireg_date = erpUtil.objectIsNullGetString(loanRow.get("loan_ass.inireg_date"));
        String production_date = erpUtil.objectIsNullGetString(loanRow.get("loan_ass.production_date"));
        if (Util.isNotEmpty(inireg_date) && Util.isNotEmpty(production_date)) {
            int retentionMonth = Util.monthsBetween(production_date, inireg_date);
            baseDao.execute("update loan_ass set car_retention=" + retentionMonth + " where loan_no='" + loan_no + "'");
        }
    }

    /**
     * 初始化分行评级
     *
     * @param loanRow
     */
    public void initBranchRating(Row loanRow) throws Exception {
        String loan_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info.loan_no"));
        String sell_branch_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info.sell_branch_no"));
        Row branchDataRow = baseDao.queryRow("select * from loan_branch_data where branch_no='" + sell_branch_no + "'");
        String branchRating = "C";
        if (branchDataRow != null) {
            branchRating = erpUtil.objectIsNullGetString(branchDataRow.get("rating"));
        }
        baseDao.execute("update loan_ass set branch_rating='" + branchRating + "' where loan_no='" + loan_no + "'");
    }

    /**
     * 初始化分行运营时间
     *
     * @param loanRow
     */
    public void initBranchAge(Row loanRow) throws Exception {
        String loan_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info.loan_no"));
        String sell_branch_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info.sell_branch_no"));
        Row branchRow = baseDao.queryRow("select * from join_branch where dept_id='" + sell_branch_no + "' and status='1'");

        String now = Util.format(new Date());
        String open_date = "";
        if (branchRow != null) {
            open_date = erpUtil.objectIsNullGetString(branchRow.get("open_date"));
        }
        if (Util.isEmpty(open_date)) {
            open_date = now;
        }
        int branch_age = Util.monthsBetween(open_date, now);
        baseDao.execute("update loan_ass set branch_age=" + branch_age + " where loan_no='" + loan_no + "'");
    }

    /**
     * 初始化分行逾期坏账率
     *
     * @param loanRow
     */
    public void initBranchBadDebt(Row loanRow) throws Exception {
        String loan_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info.loan_no"));
        String sell_branch_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info.sell_branch_no"));
        Row branchDataRow = baseDao.queryRow("select * from loan_branch_data where branch_no='" + sell_branch_no + "'");
        if (branchDataRow != null) {
            double bad_debt_1 = erpUtil.objectIsNullGetDouble(branchDataRow.get("overdue_rate_10_30"));
            double bad_debt_2 = erpUtil.objectIsNullGetDouble(branchDataRow.get("overdue_rate_10_60"));
            double bad_debt_3 = erpUtil.objectIsNullGetDouble(branchDataRow.get("overdue_rate_10_90"));
            double bad_debt_4 = erpUtil.objectIsNullGetDouble(branchDataRow.get("bad_debt_rate"));
            baseDao.execute("update loan_ass set "
                    + "bad_debt_1=" + bad_debt_1 + ","
                    + "bad_debt_2=" + bad_debt_2 + ","
                    + "bad_debt_3=" + bad_debt_3 + ","
                    + "bad_debt_4=" + bad_debt_4 + " "
                    + "where loan_no='" + loan_no + "'");
        }
    }

    /**
     * 初始化评估师评级
     *
     * @param loanRow
     */
    public void initAssessorRating(Row loanRow) throws Exception {
        String loan_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info.loan_no"));
        String review_no = erpUtil.objectIsNullGetString(loanRow.get("wf_review.review_no"));
        String sql = "select d.user_no "
                + "from wf_review a "
                + "left join wf_review_process b on a.review_no=b.review_no "
                + "left join wf_templet_flow_node c on c.templet_flow_no=a.templet_flow_no and c.flow_node_no=b.flow_node_no "
                + "left join wf_review_process_op d on d.process_no=b.process_no "
                + "where a.review_no='" + review_no + "' and b.process_status in('Y','N') and c.node_short_name='照' and d.op_status in('Y','N') ";
        String user_no = erpUtil.objectIsNullGetString(baseDao.queryObject(sql));
        String assessor_rating = erpUtil.objectIsNullGetString(baseDao.queryObject("select assess_rating from loan_riskcontrol_rating where user_no='" + user_no + "'"));
        if (Util.isEmpty(assessor_rating)) {
            assessor_rating = "01";
        }
        baseDao.execute("update loan_ass set assessor_rating='" + assessor_rating + "' where loan_no='" + loan_no + "'");
    }

    /**
     * 初始化评估师入职时间
     *
     * @param loanRow
     */
    public void initAssessorWorkAge(Row loanRow) throws Exception {
        String loan_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info.loan_no"));
        String review_no = erpUtil.objectIsNullGetString(loanRow.get("wf_review.review_no"));
        String sql = "select d.user_no "
                + "from wf_review a "
                + "left join wf_review_process b on a.review_no=b.review_no "
                + "left join wf_templet_flow_node c on c.templet_flow_no=a.templet_flow_no and c.flow_node_no=b.flow_node_no "
                + "left join wf_review_process_op d on d.process_no=b.process_no "
                + "where a.review_no='" + review_no + "' and b.process_status in('Y','N') and c.node_short_name='照' and d.op_status in('Y','N') ";
        String user_no = erpUtil.objectIsNullGetString(baseDao.queryObject(sql));
        Row userRow = baseDao.queryRow("select * from i_frame_sys_user where user_id='" + user_no + "'");
        int work_age = 0;
        if (userRow != null) {
            String join_date = erpUtil.objectIsNullGetString(userRow.get("join_date"));
            if (Util.isNotEmpty(join_date)) {
                work_age = Util.monthsBetween(join_date, Util.format(new Date()));
            }
        }
        baseDao.execute("update loan_ass set assessor_work_age='" + work_age + "' where loan_no='" + loan_no + "'");
    }

    /**
     * 初始化业务员入职时间
     *
     * @param loanRow
     */
    public void initSellerWorkAge(Row loanRow) throws Exception {
        String loan_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info.loan_no"));
        String seller_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info.seller_no"));

        Row userRow = baseDao.queryRow("select * from i_frame_sys_user where user_id='" + seller_no + "'");
        int work_age = 0;
        if (userRow != null) {
            String join_date = erpUtil.objectIsNullGetString(userRow.get("join_date"));
            if (Util.isNotEmpty(join_date)) {
                work_age = Util.monthsBetween(join_date, Util.format(new Date()));
            }
        }
        baseDao.execute("update loan_ass set assessor_work_age='" + work_age + "' where loan_no='" + loan_no + "'");
    }

    /**
     * 初始化保险理赔
     *
     * @param loanRow
     */
    public void initInsurance(Row loanRow) throws Exception {

    }
}

