/**
 * 版权所有：版权所有：重庆易房网络科技股份有限公司
 * Chongqing YiFang Network Technology Co. LTD.  All Rights Reserved
 * 创建者：HeSenLin   创建时期：  2016年6月2日
 */
package com.rxcd.autofinace.flowEvent;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.rxcd.autofinace.util.ClassUtil;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.engine.AbstractWorkFlowEvent;
import org.appframe.wf.engine.common.ErrorCodeEnum;
import org.appframe.wf.engine.imp.DefaultWorkFlow;
import org.appframe.wf.service.WfParamUtil;
import org.appframe.wf.util.Param;
import org.appframe.wf.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.regex.Pattern;

/**
 * 续期贷中放款流程结束后回调结清老合同，传入的参数为续期合同的参数。
 */
@Component
public class RenewalWorkFlowEventImp extends AbstractWorkFlowEvent {
    @Autowired
    private WfParamUtil paramUtil;
    @Autowired
    private BaseDao baseDao;

    @Autowired
    private DefaultWorkFlow flow;
    @Autowired
    private ClassUtil classUtil;

    /**
     * 流程启动时调用
     *
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowStart(Param p) throws Exception {

    }

    @Override
    public void workFlowFinish(final Param p) throws Exception {
        String sql;
        // 流程NO
        String review_no = ((Row) paramUtil.get("reviewObject")).getString("review_no");

        // 取贷款
        sql = "select a.mid_no, a.loan_id, a.loan_no,e.con_code,b.fk_date,c.renewal_date,d.renewal_repay_duetime,d.duetime "
                + "  from loan_mid a"
                + "  left join loan_mid_pay b on(b.mid_no=a.mid_no) "
                + "  left join loan_info_et c on(c.loan_no=a.loan_no) "
                + "  left join loan_cost d on(d.loan_no=a.loan_no)"
                + "  left join loan_con e on(e.loan_no=a.loan_no)"
                + " where a.mid_no=(select table_key_no from wf_review_biz where status='1' and review_no='" + review_no + "')";
        final Row fkRow = baseDao.queryRow(sql);
        if (fkRow == null) {
            throw new Exception(ErrorCodeEnum.W4003.getCode());
        }
        // 续期贷款NO
        final String final_renewal_date;
        String loan_no = fkRow.getString("loan_no");
        String fk_date = fkRow.getString("fk_date");
        String renewal_date = fkRow.get("renewal_date") == null ? fk_date : fkRow.getString("renewal_date");
        if (Util.daysBetween(renewal_date, fk_date) > 0) {
            final_renewal_date = fk_date;
        } else {
            final_renewal_date = renewal_date;
        }

        //创建并通过续期还款流程
        //查询原合同信息
        sql = "select b.loan_id,b.loan_no,c.con_code,d.after_status "
                + "from loan_info a "
                + "left join loan_info b on a.renewal_loan_no=b.loan_no "
                + "left join loan_con c on c.loan_no=b.loan_no "
                + "left join loan_info_et d on d.loan_no=a.loan_no "
                + "where a.loan_no='" + loan_no + "'";

        final Row oldRow = this.baseDao.queryRow(sql);
        if (oldRow == null) {
            throw new Exception(ErrorCodeEnum.B0013.getCode());
        }

        //创建还款流程（续期结清）
//		String after_status=oldRow.getString("after_status");
//		if("-1".equals(after_status)){

        Map<String, Object> map = flow.beforeInitAllSubFlow();
        Param param = JSON.parseObject(JSONObject.toJSONString(p), Param.class);
        flow.initWorkFlow("18", null, param);
        Row retRow = flow.addWorkFlow();

//		WorkFactory flow = (WorkFactory)WorkFactory.getInstanceByTempletNo("18",transaction,p);
//		Row retRow = flow.addWorkFlow();
        // 执行流程扩展业务
//		String sql;
        // 新流程业务数据
        Row data = (Row) retRow.get("data");
//		String review_no = data.getString("review_no");
        sql = "select a.mid_no,b.repayment_id,b.repayment_no "
                + "from loan_mid a "
                + "left join loan_mid_repayment b on a.mid_no=b.mid_no "
                + "left join wf_review_biz c on c.table_key_no=a.mid_no "
                + "where c.review_no='" + data.getString("review_no") + "'";
        Row midRow = this.baseDao.queryRow(sql);
        //贷中no
        String mid_no = midRow.getString("mid_no");
        String repayment_no = midRow.getString("repayment_no");

        //绑定贷前NO,同步合同号
        sql = "update loan_mid "
                + "   set loan_id='" + oldRow.getString("loan_id") + "',"
                + "       loan_no='" + oldRow.getString("loan_no") + "',"
                + "       con_code='" + oldRow.getString("con_code") + "'"
                + " where mid_no='" + mid_no + "'";
        this.baseDao.execute(sql);


        sql = "update loan_mid_repayment set repayment_date='" + final_renewal_date + "',repayment_type='03',repay_type='07' where repayment_no='" + repayment_no + "'";
        this.baseDao.execute(sql);

//        classUtil.updateRepaymentFeeDetails(review_no, p);

        sql = "update loan_mid_repay_fee set pay_amount=fee_amount,status=0 where repayment_no='" + repayment_no + "'";
        this.baseDao.execute(sql);
        sql = "update loan_mid_repay_payable set pay_amount=fee_amount where repayment_no='" + repayment_no + "'";
        this.baseDao.execute(sql);

        flow.reviewPrivilegePass();
        flow.closeAllSubFlow(map);
//		}


        //根据0期还款期数，做月供还款
        int renewal_repay_duetime = 1;
        Object obj = fkRow.get("renewal_repay_duetime");
        if (obj != null) {
            Pattern pattern = Pattern.compile("[0-9]*");
            if (pattern.matcher(obj.toString()).matches()) {
                renewal_repay_duetime = Integer.valueOf(obj.toString());
            }
        }

        int duetime = fkRow.get("duetime") == null ? 1 : Integer.valueOf(fkRow.get("duetime").toString());

        if (renewal_repay_duetime > 1 && duetime > 1) {

            String duetimes = "";
            for (int i = 1; i < renewal_repay_duetime && i < duetime; i++) {
                duetimes += "'" + i + "',";
            }

            Map<String, Object> map2 = flow.beforeInitAllSubFlow();
            Param param2 = JSON.parseObject(JSONObject.toJSONString(p), Param.class);
            flow.initWorkFlow("18", null, param2);
            Row retRow2 = flow.addWorkFlow();

            final String duetimes_str = duetimes.substring(0, duetimes.length() - 1);
//			WorkFactory flow_2 = (WorkFactory)WorkFactory.getInstanceByTempletNo("18",transaction,p);
//            Row  retRow_2= flow.addWorkFlow();

            initNewFlow(retRow2, final_renewal_date, duetimes_str, param2, fkRow);
            // 执行流程扩展业务

            flow.reviewPrivilegePass();
            flow.closeAllSubFlow(map2);


        }

        //添加到续期咨询费表中
        sql = "insert into loan_renewal_consult(id,`no`,loan_id,loan_no,CONSULT_USER,CONSULT_BANK,CONSULT_CARD,CONSULT_FEE,IS_PAY,`DISCARD`,CREATE_USER_ID,CREATE_USER_NO,CREATE_TIME,STATUS)"
                + " select UUID(),UUID(),a.loan_id,a.loan_no,d.CONSULT_USER,d.CONSULT_BANK,d.CONSULT_CARD,round(d.CONTRACT_SUM*d.CONSULT_FEE/100.0,2) as  CONSULT_FEE,'0','0','SYS','SYS',DATE_FORMAT(NOW(),'%y-%m-%d'),'1'  "
                + " from loan_info a "
                + " left join wf_review_biz b on a.loan_no=b.TABLE_KEY_NO "
                + " left join wf_review c on c.review_no=b.review_no  "
                + " left join loan_cost d on d.loan_no=a.loan_no "
                + " left join loan_info_et e on e.loan_no=a.loan_no "
                + " left join loan_con f on f.loan_no=a.loan_no "
                + " left join loan_cust g on g.loan_no=a.loan_no "
                + " where a.loan_no = '" + loan_no + "'";
        this.baseDao.execute(sql);
    }

    private void initNewFlow(Row bizData, String final_renewal_date, String duetimes_str, Param p, Row fkRow) throws Exception {
        String sql;
        // 新流程业务数据
        Row data = (Row) bizData.get("data");
        String review_no = data.getString("review_no");
        sql = "select a.mid_no,b.repayment_id,b.repayment_no "
                + "from loan_mid a "
                + "left join loan_mid_repayment b on a.mid_no=b.mid_no "
                + "left join wf_review_biz c on c.table_key_no=a.mid_no "
                + "where c.review_no='" + review_no + "'";
        Row midRow = this.baseDao.queryRow(sql);

        //贷中no
        String mid_no = midRow.getString("mid_no");
        String repayment_no = midRow.getString("repayment_no");

        //绑定贷前NO,同步合同号
        sql = "update loan_mid "
                + "   set loan_id='" + fkRow.getString("loan_id") + "',"
                + "       loan_no='" + fkRow.getString("loan_no") + "',"
                + "       con_code='" + fkRow.getString("con_code") + "'"
                + " where mid_no='" + mid_no + "'";
        this.baseDao.execute(sql);


        sql = "update loan_mid_repayment set repayment_date='" + final_renewal_date + "',repayment_type='01',repay_type='11' where repayment_no='" + repayment_no + "'";
        this.baseDao.execute(sql);

//        classUtil.updateRepaymentFeeDetails(review_no, p);

        //删除非当前合同的费用
        sql = "delete from loan_mid_repay_payable where fee_no in "
                + "(select fee_no from loan_mid_repay_fee where repayment_no='" + repayment_no + "' and "
                + "	(con_code!='" + fkRow.getString("con_code") + "' "
                + "	or (con_code='" + fkRow.getString("con_code") + "' and duetime not in (" + duetimes_str + "))))";
        this.baseDao.execute(sql);
        sql = "delete from loan_mid_repay_fee where repayment_no='" + repayment_no + "' and "
                + "(con_code!='" + fkRow.getString("con_code") + "' "
                + "or (con_code='" + fkRow.getString("con_code") + "' and duetime not in (" + duetimes_str + ")))";
        this.baseDao.execute(sql);

        sql = "update loan_mid_repay_payable set pay_amount=fee_amount where fee_no in"
                + "(select fee_no from loan_mid_repay_fee where repayment_no='" + repayment_no + "' and duetime in (" + duetimes_str + ") and repay_fee_type!='PENALTY_FEE')";
        this.baseDao.execute(sql);
        sql = "update loan_mid_repay_fee set pay_amount=fee_amount,status=0 where repayment_no='" + repayment_no + "' and duetime in (" + duetimes_str + ") and repay_fee_type!='PENALTY_FEE'";
        this.baseDao.execute(sql);
    }

    /**
     * 流程废弃时回调方法
     *
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowDiscard(Param p) throws Exception {

    }
}
