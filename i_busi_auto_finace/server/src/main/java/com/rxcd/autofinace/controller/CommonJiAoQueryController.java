package com.rxcd.autofinace.controller;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rxcd.autofinace.util.RxErpUtil;
import org.apache.commons.lang.StringUtils;
import org.appframe.bigdata.JiAoClinet2;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.db.Store;
import org.appframe.wf.util.ErpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * 集奥接口公共返回类
 *
 * @author liuchang
 */
@RestController
@RequestMapping("/")
public class CommonJiAoQueryController {

    @Autowired
    private RxErpUtil erpUtil;
    @Autowired
    private JiAoClinet2 jiAoClinet;
    @Autowired
    private BaseDao baseDao;

    @Transactional(rollbackFor = Exception.class)
    @RequestMapping("/loan_common_CommonJiAoQuery")
    public Row execute(@RequestParam String oper_type, @RequestParam String loan_no) throws Exception {
        Store resultStore = new Store();
        loan_no = loan_no.replaceAll("'", "");
        String operType = oper_type.replaceAll("'", "");
        Row loanRow = erpUtil.getLoanBasicDataInfo(loan_no);
        String cust_name = ErpUtil.objectIsNullGetString(loanRow.get("cust_name"));
        String idcard = ErpUtil.objectIsNullGetString(loanRow.get("idcard"));
        String cust_phone = ErpUtil.objectIsNullGetString(loanRow.get("cust_phone"));
        String bank_accout = ErpUtil.objectIsNullGetString(loanRow.get("bank_accout"));
        String borrowed_name = ErpUtil.objectIsNullGetString(loanRow.get("borrowed_name"));
        String borrowed_mobile = ErpUtil.objectIsNullGetString(loanRow.get("borrowed_mobile"));
        String borrowed_idcard = ErpUtil.objectIsNullGetString(loanRow.get("borrowed_idcard"));
        try {
            if (StringUtils.equals("A2", oper_type)) {
                Store store = queryContacts(loan_no);
                if (store != null && store.size() > 0) {
                    for (Row row : store) {
                        Row resultRow = new Row();
                        String contacts_name = ErpUtil.objectIsNullGetString(row.get("contacts_name"));
                        String contacts_mobile = ErpUtil.objectIsNullGetString(row.get("contacts_mobile"));
                        if (StringUtils.equals(contacts_name, "") || StringUtils.equals(contacts_mobile, "")) {
                            continue;
                        }
                        boolean lxrboolean = jiAoClinet.queryMobile2Element(contacts_name, contacts_mobile);
                        if (lxrboolean) {
                            resultRow.put("result", contacts_name);
                            resultRow.put("message", "结果一致");
                        } else {
                            resultRow.put("result", contacts_name);
                            resultRow.put("message", "结果不一致");
                        }
                        resultStore.add(resultRow);
                    }
                } else {
                    throw new Exception("还未输入联系人资料！");
                }
            } else if (StringUtils.equals("A3", operType)) {
                Row resultRow = new Row();
                String resultString = jiAoClinet.queryMobileWhenLong(cust_name, idcard, cust_phone);
                resultRow.put("result", cust_name);
                resultRow.put("message", resultString);
                resultStore.add(resultRow);
                if (!StringUtils.equals(borrowed_name, "")) {
                    Row bresultRow = new Row();
                    String borrowedresultString = jiAoClinet.queryMobileWhenLong(borrowed_name, borrowed_idcard, borrowed_mobile);
                    bresultRow.put("result", borrowed_name);
                    bresultRow.put("message", borrowedresultString);
                    resultStore.add(bresultRow);
                }
            } else if (StringUtils.equals("A4", operType)) {
                Row resultRow = new Row();
                String resultString = jiAoClinet.queryMobileState(cust_name, idcard, cust_phone);
                resultRow.put("result", cust_name);
                resultRow.put("message", resultString);
                resultStore.add(resultRow);
                if (!StringUtils.equals(borrowed_name, "")) {
                    Row bresultRow = new Row();
                    String borrowedresultString = jiAoClinet.queryMobileState(borrowed_name, borrowed_idcard, borrowed_mobile);
                    bresultRow.put("result", borrowed_name);
                    bresultRow.put("message", borrowedresultString);
                    resultStore.add(bresultRow);
                }
            } else if (StringUtils.equals("B7", operType)) {
                Row resultRow = new Row();
                boolean resultboolean = jiAoClinet.queryMobile3Element(cust_name, idcard, cust_phone);
                if (resultboolean) {
                    resultRow.put("result", cust_name);
                    resultRow.put("message", "结果一致");
                } else {
                    resultRow.put("result", cust_name);
                    resultRow.put("message", "结果不一致");
                }
                resultStore.add(resultRow);
                if (!StringUtils.equals(borrowed_name, "")) {
                    Row bresultRow = new Row();
                    boolean borrowedresultboolean = jiAoClinet.queryMobile3Element(borrowed_name, borrowed_idcard, borrowed_mobile);
                    if (borrowedresultboolean) {
                        bresultRow.put("result", borrowed_name);
                        bresultRow.put("message", "结果一致");
                    } else {
                        bresultRow.put("result", borrowed_name);
                        bresultRow.put("message", "结果不一致");
                    }
                    resultStore.add(bresultRow);
                }
            } else if (StringUtils.equals("C7", operType)) {
                Store store = queryContacts(loan_no);
                if (store != null && store.size() > 0) {
                    for (Row row : store) {
                        Row resultRow = new Row();
                        String resultString = jiAoClinet.queryMobileCallNum(cust_name, idcard, cust_phone, row.getString("contacts_mobile"));
                        resultRow.put("result", cust_name + "与" + row.getString("contacts_name") + "联系次数");
                        resultRow.put("message", resultString);
                        resultStore.add(resultRow);
                    }
                } else {
                    throw new Exception("还未输入联系人资料！");
                }
            } else if (StringUtils.equals("B18", operType)) {
                Row resultRow = new Row();
                String company_add = ErpUtil.objectIsNullGetString(loanRow.get("company_add"));
                String resultString = jiAoClinet.queryMobileWorkDeviate(cust_name, idcard, cust_phone, company_add);
                resultRow.put("result", cust_name + "偏离工作地");
                resultRow.put("message", resultString);
                resultStore.add(resultRow);
            } else if (StringUtils.equals("B19", operType)) {
                Row resultRow = new Row();
                String cust_address = ErpUtil.objectIsNullGetString(loanRow.get("cust_address"));
                String resultString = jiAoClinet.queryMobileHomeDeviate(cust_name, idcard, cust_phone, cust_address);
                resultRow.put("result", cust_name + "偏离居住地");
                resultRow.put("message", resultString);
                resultStore.add(resultRow);
            } else if (StringUtils.equals("Z4", operType)) {
                Row resultRow = new Row();
                boolean resultboolean = jiAoClinet.queryUnionpay4Element(cust_name, idcard, cust_phone, bank_accout);
                if (resultboolean) {
                    resultRow.put("result", cust_name);
                    resultRow.put("message", "结果一致");
                } else {
                    resultRow.put("result", cust_name);
                    resultRow.put("message", "结果不一致");
                }
                resultStore.add(resultRow);
                if (!StringUtils.equals(borrowed_name, "")) {
                    Row bresultRow = new Row();
                    boolean borrowedresultboolean = jiAoClinet.queryUnionpay4Element(borrowed_name, borrowed_idcard, borrowed_mobile, bank_accout);
                    if (borrowedresultboolean) {
                        bresultRow.put("result", borrowed_name);
                        bresultRow.put("message", "结果一致");
                    } else {
                        bresultRow.put("result", borrowed_name);
                        bresultRow.put("message", "结果不一致");
                    }
                    resultStore.add(bresultRow);
                }
            } else if (StringUtils.equals("Z5", operType)) {
                Row resultRow = new Row();
                boolean resultboolean = jiAoClinet.queryUnionpay3Element(cust_name, idcard, bank_accout);
                if (resultboolean) {
                    resultRow.put("result", cust_name);
                    resultRow.put("message", "结果一致");
                } else {
                    resultRow.put("result", cust_name);
                    resultRow.put("message", "结果不一致");
                }
                resultStore.add(resultRow);
                if (!StringUtils.equals(borrowed_name, "")) {
                    Row bresultRow = new Row();
                    boolean borrowedresultboolean = jiAoClinet.queryUnionpay3Element(borrowed_name, borrowed_idcard, bank_accout);
                    if (borrowedresultboolean) {
                        bresultRow.put("result", borrowed_name);
                        bresultRow.put("message", "结果一致");
                    } else {
                        bresultRow.put("result", borrowed_name);
                        bresultRow.put("message", "结果不一致");
                    }
                    resultStore.add(bresultRow);
                }
            } else if (StringUtils.equals("Y4", operType)) {
                Row resultRow = new Row();
                String resultString = jiAoClinet.housePropertyValidate(cust_name, idcard, cust_phone);
                JSONObject resultJson = JSONObject.parseObject(resultString);
                String identify_result = resultJson.getString("identify_result");
                resultRow.put("result", cust_name);
                if (StringUtils.equals("1", identify_result)) {
                    resultRow.put("message", "有房");
                } else {
                    resultRow.put("message", "查无记录");
                }
                resultStore.add(resultRow);
                if (!StringUtils.equals(borrowed_name, "")) {
                    Row bresultRow = new Row();
                    String borrowedresultString = jiAoClinet.housePropertyValidate(borrowed_name, borrowed_idcard, borrowed_mobile);
                    JSONObject bresultJson = JSONObject.parseObject(borrowedresultString);
                    String bidentify_result = bresultJson.getString("identify_result");
                    bresultRow.put("result", borrowed_name);
                    if (StringUtils.equals("1", bidentify_result)) {
                        bresultRow.put("message", "有房");
                    } else {
                        bresultRow.put("message", "查无记录");
                    }
                    resultStore.add(bresultRow);
                }
            } else if (StringUtils.equals("T20103", operType)) {
                String resultString = jiAoClinet.queryOverdue(cust_name, idcard, cust_phone);
                JSONObject resultJson = JSONObject.parseObject(resultString);
                if (resultJson == null) {
                    throw new Exception("未查询到数据");
                }
                for (Map.Entry<String, Object> entry : resultJson.entrySet()) {
                    Row resultRow = new Row();
                    if (StringUtils.equals("result_YQ_ZZSJ", entry.getKey())) {
                        resultRow.put("result", "逾期最早出现时间");
                    } else if (StringUtils.equals("result_YQ_ZJSJ", entry.getKey())) {
                        resultRow.put("result", "逾期最近出现时间");
                    } else if (StringUtils.equals("result_YQ_LJCS", entry.getKey())) {
                        resultRow.put("result", "逾期累计出现次数");
                    } else if (StringUtils.equals("result_YQ_DQJE", entry.getKey())) {
                        resultRow.put("result", "当前逾期金额");
                    } else if (StringUtils.equals("result_YQ_DQSC", entry.getKey())) {
                        resultRow.put("result", "当前逾期时长");
                    } else if (StringUtils.equals("result_YQ_ZDJE", entry.getKey())) {
                        resultRow.put("result", "历史最大逾期金额");
                    } else if (StringUtils.equals("result_YQ_ZDSC", entry.getKey())) {
                        resultRow.put("result", "历史最大逾期时长");
                    } else if (StringUtils.equals("result_QZ_ZZSJ", entry.getKey())) {
                        resultRow.put("result", "欺诈最早出现时间");
                    } else if (StringUtils.equals("result_QZ_ZJSJ", entry.getKey())) {
                        resultRow.put("result", "欺诈最近出现时间");
                    } else if (StringUtils.equals("result_QZ_LJCS", entry.getKey())) {
                        resultRow.put("result", "欺诈累计出现次数");
                    } else if (StringUtils.equals("result_SX_ZZSJ", entry.getKey())) {
                        resultRow.put("result", "失信最早出现时间");
                    } else if (StringUtils.equals("result_SX_ZJSJ", entry.getKey())) {
                        resultRow.put("result", "失信最近出现时间");
                    } else if (StringUtils.equals("result_SX_LJCS", entry.getKey())) {
                        resultRow.put("result", "失信累计出现次数");
                    }
                    resultRow.put("message", entry.getValue());
                    resultStore.add(resultRow);
                }
            } else if (StringUtils.equals("T20105", operType)) {
                String resultString = jiAoClinet.queryLawsuitInformation(cust_name, idcard);
                JSONArray resultJsonArray = JSONArray.parseArray(resultString);
                for (Object o : resultJsonArray) {
                    Row resultRow = new Row();
                    JSONObject resultJson = (JSONObject) o;
                    String dataType = resultJson.getString("dataType");
                    if (StringUtils.equals("cpws", dataType)) {
                        resultRow.put("result", "裁判文书");
                    } else if (StringUtils.equals("shixin", dataType)) {
                        resultRow.put("result", "失信公告");
                    } else if (StringUtils.equals("zxgg", dataType)) {
                        resultRow.put("result", "执行公告");
                    } else if (StringUtils.equals("bgt", dataType)) {
                        resultRow.put("result", "曝光台");
                    } else if (StringUtils.equals("ktgg", dataType)) {
                        resultRow.put("result", "开庭公告");
                    } else if (StringUtils.equals("fygg", dataType)) {
                        resultRow.put("result", "法院公告");
                    }
                    resultRow.put("message", "<table><tr><td>当事人</td><td>法院名称</td><td>案号</td><td>审结时间</td><td>具体情形</td><td>案件状态</td><td>执行标的</td></tr>"
                            + "<tr><td>" + resultJson.getString("pname") + "</td><td>" + resultJson.getString("court") + "</td><td>" + resultJson.getString("caseNo") + "</td><td>" + resultJson.getString("sortTimeString") + "</td><td>" + resultJson.getString("jtqx") + "</td><td>" + resultJson.getString("caseState") + "</td><td>" + resultJson.getString("execMoney") + "</td></tr></table>");
                    resultStore.add(resultRow);
                }
            } else if (StringUtils.equals("T40301", operType)) {
                String resultString = jiAoClinet.queryMultiplatformLoan(cust_name, idcard, cust_phone);
                JSONObject resultJson = JSONObject.parseObject(resultString);
                if (resultJson == null) {
                    throw new Exception("未查询到数据");
                }
                JSONObject tjxx = resultJson.getJSONObject("TJXX");
                for (Map.Entry<String, Object> entry : tjxx.entrySet()) {
                    Row resultRow = new Row();
                    if (StringUtils.equals("regtimes", entry.getKey())) {
                        resultRow.put("result", "注册次数");
                    } else if (StringUtils.equals("regplatforms", entry.getKey())) {
                        resultRow.put("result", "注册平台数");
                    } else if (StringUtils.equals("reglasttime", entry.getKey())) {
                        resultRow.put("result", "最近注册时间");
                    } else if (StringUtils.equals("regfirsttime", entry.getKey())) {
                        resultRow.put("result", "最早注册时间");
                    } else if (StringUtils.equals("apptimes", entry.getKey())) {
                        resultRow.put("result", "申请次数");
                    } else if (StringUtils.equals("appplatforms", entry.getKey())) {
                        resultRow.put("result", "申请平台数");
                    } else if (StringUtils.equals("appmoney", entry.getKey())) {
                        resultRow.put("result", "最大申请金额区间");
                    } else if (StringUtils.equals("applasttime", entry.getKey())) {
                        resultRow.put("result", "最近申请时间");
                    } else if (StringUtils.equals("appfirsttime", entry.getKey())) {
                        resultRow.put("result", "最早申请时间");
                    } else if (StringUtils.equals("loantimes", entry.getKey())) {
                        resultRow.put("result", "放款次数");
                    } else if (StringUtils.equals("loanplatforms", entry.getKey())) {
                        resultRow.put("result", "放款平台数");
                    } else if (StringUtils.equals("loanmoney", entry.getKey())) {
                        resultRow.put("result", "最大放款金额区间");
                    } else if (StringUtils.equals("loanfirsttime", entry.getKey())) {
                        resultRow.put("result", "最早放款时间");
                    } else if (StringUtils.equals("loanlasttime", entry.getKey())) {
                        resultRow.put("result", "最近放款时间");
                    } else if (StringUtils.equals("rejtimes", entry.getKey())) {
                        resultRow.put("result", "驳回次数");
                    } else if (StringUtils.equals("rejplatforms", entry.getKey())) {
                        resultRow.put("result", "驳回平台数");
                    } else if (StringUtils.equals("rejfirsttime", entry.getKey())) {
                        resultRow.put("result", "最早驳回时间");
                    } else if (StringUtils.equals("rejlasttime", entry.getKey())) {
                        resultRow.put("result", "最近驳回时间");
                    } else if (StringUtils.equals("regtimes_bank", entry.getKey())) {
                        resultRow.put("result", "注册次数-银行");
                    } else if (StringUtils.equals("regplatforms_bank", entry.getKey())) {
                        resultRow.put("result", "注册平台数-银行");
                    } else if (StringUtils.equals("reglasttime_bank", entry.getKey())) {
                        resultRow.put("result", "最近注册时间-银行");
                    } else if (StringUtils.equals("regfirsttime_bank", entry.getKey())) {
                        resultRow.put("result", "最早注册时间-银行");
                    } else if (StringUtils.equals("apptimes_bank", entry.getKey())) {
                        resultRow.put("result", "申请次数-银行");
                    } else if (StringUtils.equals("appplatforms_bank", entry.getKey())) {
                        resultRow.put("result", "申请平台数-银行");
                    } else if (StringUtils.equals("appmoney_bank", entry.getKey())) {
                        resultRow.put("result", "最大申请金额区间-银行");
                    } else if (StringUtils.equals("applasttime_bank", entry.getKey())) {
                        resultRow.put("result", "最近申请时间-银行");
                    } else if (StringUtils.equals("appfirsttime_bank", entry.getKey())) {
                        resultRow.put("result", "最早申请时间-银行");
                    } else if (StringUtils.equals("loantimes_bank", entry.getKey())) {
                        resultRow.put("result", "放款次数-银行");
                    } else if (StringUtils.equals("loanplatforms_bank", entry.getKey())) {
                        resultRow.put("result", "放款平台数-银行");
                    } else if (StringUtils.equals("loanmoney_bank", entry.getKey())) {
                        resultRow.put("result", "最大放款金额区间-银行");
                    } else if (StringUtils.equals("loanfirsttime_bank", entry.getKey())) {
                        resultRow.put("result", "最早放款时间-银行");
                    } else if (StringUtils.equals("loanlasttime_bank", entry.getKey())) {
                        resultRow.put("result", "最近放款时间-银行");
                    } else if (StringUtils.equals("rejtimes_bank", entry.getKey())) {
                        resultRow.put("result", "驳回次数-银行");
                    } else if (StringUtils.equals("rejplatforms_bank", entry.getKey())) {
                        resultRow.put("result", "驳回平台数-银行");
                    } else if (StringUtils.equals("rejfirsttime_bank", entry.getKey())) {
                        resultRow.put("result", "最早驳回时间-银行");
                    } else if (StringUtils.equals("rejlasttime_bank", entry.getKey())) {
                        resultRow.put("result", "最近驳回时间-银行");
                    } else if (StringUtils.equals("regtimes_nonbank", entry.getKey())) {
                        resultRow.put("result", "注册次数-非银");
                    } else if (StringUtils.equals("regplatforms_nonbank", entry.getKey())) {
                        resultRow.put("result", "注册平台数-非银");
                    } else if (StringUtils.equals("reglasttime_nonbank", entry.getKey())) {
                        resultRow.put("result", "最近注册时间-非银");
                    } else if (StringUtils.equals("regfirsttime_nonbank", entry.getKey())) {
                        resultRow.put("result", "最早注册时间-非银");
                    } else if (StringUtils.equals("apptimes_nonbank", entry.getKey())) {
                        resultRow.put("result", "申请次数-非银");
                    } else if (StringUtils.equals("appplatforms_nonbank", entry.getKey())) {
                        resultRow.put("result", "申请平台数-非银");
                    } else if (StringUtils.equals("appmoney_nonbank", entry.getKey())) {
                        resultRow.put("result", "最大申请金额区间-非银");
                    } else if (StringUtils.equals("applasttime_nonbank", entry.getKey())) {
                        resultRow.put("result", "最近申请时间-非银");
                    } else if (StringUtils.equals("appfirsttime_nonbank", entry.getKey())) {
                        resultRow.put("result", "最早申请时间-非银");
                    } else if (StringUtils.equals("loantimes_nonbank", entry.getKey())) {
                        resultRow.put("result", "放款次数-非银");
                    } else if (StringUtils.equals("loanplatforms_nonbank", entry.getKey())) {
                        resultRow.put("result", "放款平台数-非银");
                    } else if (StringUtils.equals("loanmoney_nonbank", entry.getKey())) {
                        resultRow.put("result", "最大放款金额区间-非银");
                    } else if (StringUtils.equals("loanfirsttime_nonbank", entry.getKey())) {
                        resultRow.put("result", "最早放款时间-非银");
                    } else if (StringUtils.equals("loanlasttime_nonbank", entry.getKey())) {
                        resultRow.put("result", "最近放款时间-非银");
                    } else if (StringUtils.equals("rejtimes_nonbank", entry.getKey())) {
                        resultRow.put("result", "驳回次数-非银");
                    } else if (StringUtils.equals("rejplatforms_nonbank", entry.getKey())) {
                        resultRow.put("result", "驳回平台数-非银");
                    } else if (StringUtils.equals("rejfirsttime_nonbank", entry.getKey())) {
                        resultRow.put("result", "最早驳回时间-非银");
                    } else if (StringUtils.equals("rejlasttime_nonbank", entry.getKey())) {
                        resultRow.put("result", "最近驳回时间-非银");
                    }
                    resultRow.put("message", entry.getValue());
                    resultStore.add(resultRow);
                }
            } else if (StringUtils.equals("T40303", operType)) {
                String resultString = jiAoClinet.queryMobileRisk(cust_name, idcard, cust_phone);
                JSONObject resultJson = JSONObject.parseObject(resultString);
                if (resultJson == null) {
                    throw new Exception("未查询到数据");
                }
                JSONObject callertag = resultJson.getJSONObject("callertag");
                String callertag_tagType = callertag.getString("tagType");
                String callertag_updateTime = callertag.getString("updateTime");
                JSONObject black = resultJson.getJSONObject("black");
                String black_updateTime = black.getString("updateTime");
                JSONObject alt = resultJson.getJSONObject("alt");
                String alt_updateTime = alt.getString("updateTime");
                Row resultRow1 = new Row();
                resultRow1.put("result", "标记类型");
                resultRow1.put("message", callertag_tagType);
                resultStore.add(resultRow1);
                Row resultRow2 = new Row();
                resultRow2.put("result", "标记更新时间");
                resultRow2.put("message", callertag_updateTime);
                resultStore.add(resultRow2);
                Row resultRow3 = new Row();
                resultRow3.put("result", "虚假手机号更新时间");
                resultRow3.put("message", black_updateTime);
                resultStore.add(resultRow3);
                Row resultRow4 = new Row();
                resultRow4.put("result", "小号更新时间");
                resultRow4.put("message", alt_updateTime);
                resultStore.add(resultRow4);
            } else if (StringUtils.equals("Z9", operType)) {
                String resultString = jiAoClinet.queryBankCardCredit(cust_name, idcard, cust_phone, bank_accout);
                JSONObject resultJson = JSONObject.parseObject(resultString);
                if (resultJson == null) {
                    throw new Exception("未查询到数据");
                }
                JSONArray data = resultJson.getJSONArray("data");
                JSONObject d = (JSONObject) data.get(0);
                Map<String, String> map = queryDictionaries();
                for (Map.Entry<String, Object> entry : d.entrySet()) {
                    Row resultRow = new Row();
                    resultRow.put("result", map.get(entry.getKey()));
                    resultRow.put("message", entry.getValue());
                    resultStore.add(resultRow);
                }
            } else {
                throw new Exception("操作类型不存在");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Row resultRow = new Row();
            resultRow.put("result", false);
            resultRow.put("message", e.getMessage());
            resultStore.add(resultRow);
        }
        Row data = new Row();
        Row rowT = new Row();

        rowT.put("result", "描述");
        rowT.put("message", "接口返回信息");
        data.put("title", rowT);

        data.put("list", resultStore);

        return data;
    }

    /**
     * 返回所有联系人
     *
     * @param loan_no
     * @return
     * @throws Exception
     */
    private Store queryContacts(String loan_no) throws Exception {
        String sql = "select * from loan_info_contacts where loan_no = '" + loan_no + "'";
        return baseDao.query(sql);
    }

    /**
     * 获取集奥数据字典Z9
     *
     * @return
     * @throws Exception
     */
    private Map<String, String> queryDictionaries() throws Exception {
        Map<String, String> map = new HashMap<String, String>();
        String sql = "select no,dictionaries_name from dictionaries_jiao";
        Store store = baseDao.query(sql);
        for (Row row : store) {
            map.put(row.getString("no"), row.getString("dictionaries_name"));
        }
        return map;
    }
}
