/**
 * 版权所有：版权所有：重庆易房网络科技股份有限公司
 * Chongqing YiFang Network Technology Co. LTD.  All Rights Reserved
 * 创建者：HeSenLin   创建时期：  2016年3月16日
 */
package com.rxcd.autofinace.service;

import com.alibaba.fastjson.JSONObject;
import com.rxcd.autofinace.common.BizErrorCodeEnum;
import com.rxcd.autofinace.common.CarColorEnum;
import com.rxcd.autofinace.common.CarPlateCityEnum;
import com.rxcd.autofinace.common.LoanConst;
import com.rxcd.autofinace.service.client.ClearClient;
import com.rxcd.autofinace.util.ContractContentUtil;
import com.rxcd.autofinace.util.DateUtil;
import com.rxcd.autofinace.util.RxErpUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.appframe.bigdata.AcedataClinet2;
import org.appframe.bigdata.ChaboshiClinet2;
import org.appframe.bigdata.JingZhenGuClinet2;
import org.appframe.bigdata.TongDunClinet2;
import org.appframe.common.BigDecimalUtil;
import org.appframe.contract.ContractClient;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.db.Store;
import org.appframe.wf.util.ErpUtil;
import org.appframe.wf.util.Formula;
import org.appframe.wf.util.SafeUtil;
import org.appframe.wf.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.Map.Entry;

/**
 * 租前业务
 *
 * @author hubin
 */
@Component
@Transactional(rollbackFor = Exception.class)
public class LoanService {
    private final static Log log = LogFactory.getLog(LoanService.class);
    @Value("runtime")
    private String runtime;
    @Autowired
    private BaseDao baseDao;
    @Autowired
    private RxErpUtil erpUtil;
    @Autowired
    private ChaboshiClinet2 chaboshiClinet;
    @Autowired
    private JingZhenGuClinet2 jingZhenGuClinet;
    @Autowired
    private AcedataClinet2 acedataClinet;
    @Autowired
    private TongDunClinet2 tongDunClinet;
    @Autowired
    private ContractContentUtil contractContentUtil;
    @Autowired
    private ContractClient contractClient;
    @Autowired
    private ClearClient clearClient;

    /**
     * 查询查博士信息（初）
     *
     * @param loan_no
     * @throws Exception
     */
    public void queryCarCBSFirst(String loan_no) throws Exception {
        if (StringUtils.equals("0", runtime)) {
            String[] ratings = {"A+", "A", "B", "C"};
            baseDao.execute("update loan_car set chaboshi_rating='" + ratings[(int) (4 * Math.random())] + "' where loan_no='" + loan_no + "'");
            return;
        }
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String cjh = erpUtil.objectIsNullGetString(loanRow.get("loan_car.cjh"));
        String fdjh = erpUtil.objectIsNullGetString(loanRow.get("loan_car.fdjh"));
        String report_id;
        try {
            report_id = chaboshiClinet.getReportId(cjh, fdjh, false);
            baseDao.execute("update loan_car set cbs_report_id='" + report_id + "' where loan_no='" + loan_no + "'");
        } catch (Exception e) {
            baseDao.execute("update loan_ass set cbs_error_msg='" + e.getMessage() + "' where loan_no='" + loan_no + "'");
            return;
        }
        for (int i = 0; i < 5; i++) {
            Thread.sleep(500);
            try {
                chaboshiClinet.checkReportStatus(report_id);
                JSONObject resultJson = chaboshiClinet.getNewReportJson(report_id);

                //泡水
                String carWaterFlag = resultJson.getString("carWaterFlag");
                //火烧
                String carFireFlag = resultJson.getString("carFireFlag");
                //结构
                String carConstructRecordsFlag = resultJson.getString("carConstructRecordsFlag");
                //重要部件
                String carComponentRecordsFlag = resultJson.getString("carComponentRecordsFlag");
                String componentAnalyzeRepairRecords = resultJson.getString("componentAnalyzeRepairRecords");

                //平均保养次数
                String mainTainTimes = resultJson.getString("mainTainTimes");
                int mainTainTimesCount = erpUtil.objectIsNullGetInteger(mainTainTimes);

                //有泡水、火烧或者结构件维修记录
                if (StringUtils.equals("1", carWaterFlag) || StringUtils.equals("1", carFireFlag) || StringUtils.equals("1", carConstructRecordsFlag)) {
                    baseDao.execute("update loan_car set chaboshi_rating='C' where loan_no='" + loan_no + "'");
                }//有重要件维修记录，且为发动机、变速箱、气囊或者安全带
                else if (StringUtils.equals("1", carComponentRecordsFlag)
                        && (componentAnalyzeRepairRecords.indexOf("发动机") > -1
                        || componentAnalyzeRepairRecords.indexOf("变速箱") > -1
                        || componentAnalyzeRepairRecords.indexOf("气囊") > -1
                        || componentAnalyzeRepairRecords.indexOf("安全带") > -1)) {
                    baseDao.execute("update loan_car set chaboshi_rating='B' where loan_no='" + loan_no + "'");
                }//年平保养次数小于2
                else if (mainTainTimesCount < 2) {
                    baseDao.execute("update loan_car set chaboshi_rating='A' where loan_no='" + loan_no + "'");
                }//年平保养次数大于等于2
                else if (mainTainTimesCount >= 2) {
                    baseDao.execute("update loan_car set chaboshi_rating='A+' where loan_no='" + loan_no + "'");
                }//其他
                else {
                    baseDao.execute("update loan_car set chaboshi_rating='' where loan_no='" + loan_no + "'");
                }
                return;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 查询查博士信息（终）
     *
     * @param loan_no
     * @throws Exception
     */
    public void queryCarCBSFinal(String loan_no) throws Exception {
        if (StringUtils.equals("0", runtime)) {
            String[] ratings = {"A+", "A", "B", "C"};
            baseDao.execute("update loan_ass set chaboshi_rating='" + ratings[(int) (4 * Math.random())] + "' where loan_no='" + loan_no + "'");
            return;
        }

        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String vin = erpUtil.objectIsNullGetString(loanRow.get("loan_ass.vin_code"));
        String engine_code = erpUtil.objectIsNullGetString(loanRow.get("loan_ass.engine_code"));
        String report_id;
        try {
            report_id = chaboshiClinet.getReportId(vin, engine_code, false);
            baseDao.execute("update loan_ass set cbs_report_id='" + report_id + "' where loan_no='" + loan_no + "'");
        } catch (Exception e) {
            baseDao.execute("update loan_ass set cbs_error_msg='" + e.getMessage() + "' where loan_no='" + loan_no + "'");
            return;
        }

        for (int i = 0; i < 5; i++) {
            Thread.sleep(500);
            try {
                chaboshiClinet.checkReportStatus(report_id);
                JSONObject resultJson = chaboshiClinet.getNewReportJson(report_id);

                //泡水
                String carWaterFlag = resultJson.getString("carWaterFlag");
                //火烧
                String carFireFlag = resultJson.getString("carFireFlag");
                //结构
                String carConstructRecordsFlag = resultJson.getString("carConstructRecordsFlag");
                //重要部件
                String carComponentRecordsFlag = resultJson.getString("carComponentRecordsFlag");
                String componentAnalyzeRepairRecords = resultJson.getString("componentAnalyzeRepairRecords");

                //平均保养次数
                String mainTainTimes = resultJson.getString("mainTainTimes");
                int mainTainTimesCount = erpUtil.objectIsNullGetInteger(mainTainTimes);

                //有泡水、火烧或者结构件维修记录
                if (StringUtils.equals("1", carWaterFlag) || StringUtils.equals("1", carFireFlag) || StringUtils.equals("1", carConstructRecordsFlag)) {
                    baseDao.execute("update loan_ass set chaboshi_rating='C' where loan_no='" + loan_no + "'");
                }//有重要件维修记录，且为发动机、变速箱、气囊或者安全带
                else if (StringUtils.equals("1", carComponentRecordsFlag)
                        && (componentAnalyzeRepairRecords.indexOf("发动机") > -1
                        || componentAnalyzeRepairRecords.indexOf("变速箱") > -1
                        || componentAnalyzeRepairRecords.indexOf("气囊") > -1
                        || componentAnalyzeRepairRecords.indexOf("安全带") > -1)) {
                    baseDao.execute("update loan_ass set chaboshi_rating='B' where loan_no='" + loan_no + "'");
                }//年平保养次数小于2
                else if (mainTainTimesCount < 2) {
                    baseDao.execute("update loan_ass set chaboshi_rating='A' where loan_no='" + loan_no + "'");
                }//年平保养次数大于等于2
                else if (mainTainTimesCount >= 2) {
                    baseDao.execute("update loan_ass set chaboshi_rating='A+' where loan_no='" + loan_no + "'");
                }//其他
                else {
                    baseDao.execute("update loan_ass set chaboshi_rating='' where loan_no='" + loan_no + "'");
                }
                return;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 通过车牌(销售录入)查询当前业务汽车注册信息
     *
     * @param loan_no
     */
    public String queryCarRegisterInfoFirst(String loan_no) throws Exception {
        String biz_msg = "";
        loan_no = SafeUtil.safe(loan_no);
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String carPlate = loanRow.getString("loan_car.car_plate");
        String cust_name = loanRow.getString("loan_cust.cust_name");
        String cjh, fdjh, inireg_date, color, brand, car_type, car_syr, vehicle_usage, fuel_type, brand_country,retirement_date;
        if ("0".equals(runtime)) {
            cjh = "LVAV2JBB0FE018781";fdjh = "123456";inireg_date = "2016-10-10";color = CarColorEnum.OTHER.name();brand = "奥迪";car_type = "SVW71612GS";car_syr = cust_name;
            baseDao.execute( "update loan_car set car_syr='" + car_syr + "',cjh='" + cjh + "',fdjh='" + fdjh + "',inireg_date='" + DateUtil.date2StringDay(DateUtil.toDate(inireg_date)) +
                    "',car_color='" + color + "',brand='" + brand + "',car_type='" + car_type + "' where loan_no = '"+loan_no+"'");
        } else {
            try {
                JSONObject jsonObject = acedataClinet.queryCarRegister(carPlate,cust_name, false);
                JSONObject resultObject =  jsonObject.getJSONObject("data").getJSONObject("result");
                cjh = resultObject.getString("vin");//车架号
                fdjh = resultObject.getString("engineNo");//发动机号
                inireg_date = resultObject.getString("initialRegDate");//注册日期
                color = resultObject.getString("vehicleColor");//车身颜色
                brand = resultObject.getString("vehicleBrand");//车辆型号
                car_type = resultObject.getString("vehicleModel");//车辆型号
                vehicle_usage = resultObject.getString("useCharacter");//营运类型
                fuel_type = resultObject.getString("fuelType");//燃料种类
                retirement_date = resultObject.getString("compulsoryRetirementDate");//强制报废期
                car_syr = resultObject.getString("owner");//所有人
                if(StringUtils.equals("一致", car_syr)){
                    car_syr=cust_name;
                }
                brand_country=erpUtil.getCarCountryByBrand(brand,cjh);
                if(vehicle_usage.matches(".*(非营运).*")){
                    vehicle_usage="01";
                }else if(vehicle_usage.matches(".*(预约).*")){
                    vehicle_usage="04";
                }else if(vehicle_usage.matches(".*[(客运)|(货运)|(租赁)].*")){
                    vehicle_usage="02";
                }else if(vehicle_usage.matches(".*(转非).*")){
                    vehicle_usage="03";
                }else{
                    vehicle_usage="05";
                }
                if(fuel_type.matches("[A]")){
                    fuel_type="01";
                }else if(fuel_type.matches("[B]")){
                    fuel_type="02";
                }else if(fuel_type.matches("[EF]")){
                    fuel_type="03";
                }else if(fuel_type.matches("[O]")){
                    fuel_type="04";
                }else if(fuel_type.matches("[C]")){
                    fuel_type="05";
                }else{
                    fuel_type="06";
                }
                baseDao.execute("update loan_ass set fuel_type='"+fuel_type+"',vehicle_usage='"+vehicle_usage+"',brand_country='"+brand_country+"',retirement_date='"+retirement_date+"' where loan_no='"+loan_no+"'");
                CarColorEnum carColorEnum = CarColorEnum.getCarColorEnumByCode(color,CarColorEnum.ACEDATA);//调哪个公司接口，就传哪个公司
                if(carColorEnum!=null){
                    color = carColorEnum.name();
                }else{
                    color = CarColorEnum.OTHER.name();//没得的话默认其他
                }
                baseDao.execute("update loan_info_et set car_info_explain = ''  where loan_info_et.loan_no = '" + loan_no + "'");
                baseDao.execute("update loan_car set car_syr='"+car_syr+"',cjh='"+cjh+"',fdjh='"+fdjh+ "',inireg_date='"+DateUtil.date2StringDay(DateUtil.toDate(inireg_date))+
                        "',car_color='"+color+"',brand='"+brand+"',car_type='"+car_type+"' where loan_no = '"+loan_no+"'");
            } catch (Exception e) {
                e.printStackTrace();
                biz_msg = "该车在大数据接口未能查到相关车辆信息，请在车辆大数据初审节点手动填写车辆信息，有疑问请联系审核部门处理!";
                baseDao.execute("update loan_info_et set car_info_explain = '"+biz_msg+"'  where loan_info_et.loan_no = '" + loan_no + "'");
                baseDao.execute("update loan_car set car_syr='"+cust_name+"' where loan_no = '"+loan_no+"'");
            }
        }
        return biz_msg;
    }

    /**
     * 通过车架号(初)查询汽车品牌类型
     *
     * @param loan_no
     * @throws Exception
     */
    public void queryCarBrandFirst(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String cjh = erpUtil.objectIsNullGetString(loanRow.get("loan_car.cjh"));
        if (Util.isEmpty(cjh)) {
            return;
        }

        baseDao.execute("delete from loan_car_brand_type where loan_no='" + SafeUtil.safe(loan_no) + "' and query_type='01'");

        Map<String, String> catTypes = new HashMap<String, String>();
        if (StringUtils.equals("0", runtime)) {
            catTypes.put("0001", "奥迪A4L");
            catTypes.put("0002", "奥迪A6L");
            catTypes.put("0003", "奥迪A7L");
            catTypes.put("0004", "奥迪A8L");
        } else {
            if (Util.isNotEmpty(cjh)) {
                //默认5次  如果一次通过 则循环一次
                for (int i = 0; i < 5; i++) {
                    try {
                        catTypes = jingZhenGuClinet.queryCarModel(cjh);
                        break;
                    } catch (Exception e) {
                        e.printStackTrace();
                        Thread.sleep(800);
                    }
                }
            }
        }
        List<String> sqls = new ArrayList<>();
        if (catTypes.size() > 0) {
            for (Entry<String, String> entry : catTypes.entrySet()) {
                String tempSql = "insert into loan_car_brand_type(no,loan_no,query_type,type_no,type_name,status) values "
                        + "('" + Util.uuid() + "','" + SafeUtil.safe(loan_no) + "','01','" + entry.getKey() + "','" + entry.getValue() + "','1')";
                sqls.add(tempSql);
            }
            baseDao.execute(sqls);
        }
    }

    /**
     * 通过车架号(终)查询汽车品牌类型
     *
     * @param loan_no
     * @throws Exception
     */
    public void queryCarBrandFinal(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String cjh = erpUtil.objectIsNullGetString(loanRow.get("loan_ass.vin_code"));
        if (Util.isEmpty(cjh)) {
            return;
        }
        baseDao.execute("delete from loan_car_brand_type where loan_no='" + SafeUtil.safe(loan_no) + "' and query_type='02'");
        Map<String, String> catTypes = new HashMap<String, String>();
        if (StringUtils.equals("0", runtime)) {
            catTypes.put("0001", "奥迪A4L");
            catTypes.put("0002", "奥迪A6L");
            catTypes.put("0003", "奥迪A7L");
            catTypes.put("0004", "奥迪A8L");
        } else {
            if (Util.isNotEmpty(cjh)) {
                //默认5次  如果一次通过 则循环一次
                for (int i = 0; i < 5; i++) {
                    try {
                        catTypes = jingZhenGuClinet.queryCarModel(cjh);
                        break;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        if (catTypes.size() > 0) {
            List<String> sqls = new ArrayList<String>();
            for (Entry<String, String> entry : catTypes.entrySet()) {
                sqls.add("insert into loan_car_brand_type(no,loan_no,query_type,type_no,type_name,status) values ('" + Util.uuid() + "','" + SafeUtil.safe(loan_no) + "','02','" + entry.getKey() + "','" + entry.getValue() + "','1')");
            }
            baseDao.execute(sqls);
        }
    }

    /**
     * 查询汽车接口初价
     *
     * @param loan_no
     * @throws Exception
     */
    public void queryCarPriceFirst(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String carPlate = erpUtil.objectIsNullGetString(loanRow.get("loan_car.car_plate"));
        String brandType = erpUtil.objectIsNullGetString(loanRow.get("loan_car.brand_type"));
        String mileage = erpUtil.objectIsNullGetString(loanRow.get("loan_car.mileage"));
        String iniregDate = erpUtil.objectIsNullGetString(loanRow.get("loan_car.inireg_date"));
        String colorKey = erpUtil.objectIsNullGetString(loanRow.get("loan_car.car_color"));//颜色对应key
        String colorId = CarColorEnum.getCarColorEnum(colorKey) == null ? "0" : CarColorEnum.getCarColorEnum(colorKey).getColor_code_2();//精针估对应颜色id
        double assessPrice = 0d;
        if (StringUtils.equals("0", runtime)) {
            while (true) {
                assessPrice = BigDecimalUtil.round(Math.random() * 500000, 0);
                if (assessPrice > 10000) {
                    break;
                }
            }
        } else {
            if (Util.isNotEmpty(carPlate) && Util.isNotEmpty(brandType) && Util.isNotEmpty(iniregDate)) {
                //默认5次  如果一次通过 则循环一次
                for (int i = 0; i < 5; i++) {
                    try {
                        CarPlateCityEnum carPlateCityEnum = CarPlateCityEnum.getCarPlateCityEnum(carPlate);
                        if (carPlateCityEnum == null) {
                            throw new Exception(BizErrorCodeEnum.T1018.getCode());
                        }
                        //调用真实接口获取数据
                        assessPrice = jingZhenGuClinet.queryCarAssessPrice(brandType, mileage, iniregDate, carPlateCityEnum.getCityId(), colorId);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            if (assessPrice == 0) {
                assessPrice = LoanConst.PGJ_DEFAULT;
            }
        }
        String sql = "update loan_ass set interface_price='" + assessPrice + "' where loan_no='" + SafeUtil.safe(loan_no) + "'";
        baseDao.execute(sql);
    }

    /**
     * 评估照初始化车基本信息,包括通过车架号查询车系
     *
     * @throws Exception
     */
    public void initAssInfo(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String prod_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info.prod_no"));
        String car_plate = erpUtil.objectIsNullGetString(loanRow.get("loan_car.car_plate"));
        String cjh = erpUtil.objectIsNullGetString(loanRow.get("loan_car.cjh"));
        String fdjh = erpUtil.objectIsNullGetString(loanRow.get("loan_car.fdjh"));
        String brand = erpUtil.objectIsNullGetString(loanRow.get("loan_car.brand"));
        String color = erpUtil.objectIsNullGetString(loanRow.get("loan_car.car_color"));
        String car_inireg_date = erpUtil.objectIsNullGetString(loanRow.get("loan_car.inireg_date"));
        String car_plate_ass = erpUtil.objectIsNullGetString(loanRow.get("loan_ass.car_plate_ass"));
        String vin_code = erpUtil.objectIsNullGetString(loanRow.get("loan_ass.vin_code"));
        String engine_code = erpUtil.objectIsNullGetString(loanRow.get("loan_ass.engine_code"));
        String car_brand = erpUtil.objectIsNullGetString(loanRow.get("loan_ass.car_brand"));
        String inireg_date = erpUtil.objectIsNullGetString(loanRow.get("loan_ass.inireg_date"));
        String car_color = erpUtil.objectIsNullGetString(loanRow.get("loan_ass.car_color"));
        if (Util.isEmpty(car_plate_ass)) {
            baseDao.execute("update loan_ass set car_plate_ass='" + car_plate + "' where loan_no='" + loan_no + "'");
        }
        if (Util.isEmpty(vin_code)) {
            baseDao.execute("update loan_ass set vin_code='" + cjh + "' where loan_no='" + loan_no + "'");
            queryCarBrandFinal(loan_no);
        }
        if (Util.isEmpty(engine_code)) {
            baseDao.execute("update loan_ass set engine_code='" + fdjh + "' where loan_no='" + loan_no + "'");
        }
        if (Util.isEmpty(car_brand)) {
            baseDao.execute("update loan_ass set car_brand='" + brand + "' where loan_no='" + loan_no + "'");
        }
        if (Util.isEmpty(inireg_date)) {
            baseDao.execute("update loan_ass set inireg_date='" + car_inireg_date + "' where loan_no='" + loan_no + "'");
        }
        if (Util.isEmpty(car_color)) {
            baseDao.execute("update loan_ass set car_color='" + color + "' where loan_no='" + loan_no + "'");
        }
    }

    /**
     * 查询汽车接口终价
     *
     * @param loan_no
     */
    public void queryCarPriceFinal(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String carPlate = erpUtil.objectIsNullGetString(loanRow.get("loan_ass.car_plate_ass"));
        String car_audi = erpUtil.objectIsNullGetString(loanRow.get("loan_ass.car_audi"));
        String mileage = erpUtil.objectIsNullGetString(loanRow.get("loan_ass.mileage"));
        String inireg_date = erpUtil.objectIsNullGetString(loanRow.get("loan_ass.inireg_date"));
        String colorKey = erpUtil.objectIsNullGetString(loanRow.get("loan_ass.car_color"));
        String colorId = CarColorEnum.getCarColorEnum(colorKey) == null ? "0" : CarColorEnum.getCarColorEnum(colorKey).getColor_code_2();

        Double assessPrice = 0d;
        //开发环境 直接获取假数据
        if (StringUtils.equals("0", runtime)) {
            while (true) {
                assessPrice = BigDecimalUtil.round(Math.random() * 500000, 0);
                if (assessPrice > 10000) {
                    break;
                }
            }
        } else {
            if (Util.isNotEmpty(carPlate) && Util.isNotEmpty(car_audi) && Util.isNotEmpty(mileage) && Util.isNotEmpty(inireg_date) && Util.isNotEmpty(colorId)) {
                //默认5次  如果一次通过 则循环一次
                for (int i = 0; i < 5; i++) {
                    try {
                        CarPlateCityEnum carPlateCityEnum = CarPlateCityEnum.getCarPlateCityEnum(carPlate);
                        if (carPlateCityEnum == null) {
                            throw new Exception(BizErrorCodeEnum.T1018.getCode());
                        }
                        //调用真实接口获取数据
                        assessPrice = jingZhenGuClinet.queryCarAssessPrice(car_audi, mileage, inireg_date, carPlateCityEnum.getCityId(), colorId);
                        break;
                    } catch (Exception e) {
                        e.printStackTrace();//随眠800毫秒后继续
                        Thread.sleep(800);
                    }
                }
            }
            assessPrice = assessPrice == 0 ? (int) (LoanConst.PGJ_DEFAULT) : assessPrice;
        }
        String sql = "update loan_ass set final_interface_price='" + assessPrice + "' where loan_no='" + SafeUtil.safe(loan_no) + "'";
        baseDao.execute(sql);
    }

    /**
     * 通过车牌、(销售录入)查询当前业务汽车违章
     *
     * @param loan_no
     */
    public void queryCarOffencesMoney(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String driveNum = erpUtil.objectIsNullGetString(loanRow.get("loan_car.fdjh"));
        String vin = erpUtil.objectIsNullGetString(loanRow.get("loan_car.cjh"));
        String car_plate = erpUtil.objectIsNullGetString(loanRow.get("loan_car.car_plate"));
        CarPlateCityEnum carPlateCityEnum = CarPlateCityEnum.getCarPlateCityEnum(car_plate);

        long offencesMoney = 0;
        try {
            if (carPlateCityEnum == null) {
                throw new Exception(BizErrorCodeEnum.T1018.getCode());
            }
            if (StringUtils.equals("0", runtime)) {
                offencesMoney = 1000;
            } else {
                offencesMoney = erpUtil.getOffencesMoney(car_plate, vin, driveNum, erpUtil.getCityCode(carPlateCityEnum.getCityName()));
            }
        } catch (Exception e) {
            Util.getStackTrace(e);
            baseDao.execute("update loan_info_et set car_info_explain = concat(ifnull(car_info_explain,''),'违章查询异常，请人工填写违章押金')  where loan_info_et.loan_no = '" + loan_no + "'");
        }
        String sql = "update loan_cost set pacc_fee='" + offencesMoney + "' where loan_no='" + SafeUtil.safe(loan_no) + "'";
        baseDao.execute(sql);
    }

    /**
     * 重新计算大数据终价
     *
     * @param loan_no
     * @throws Exception
     */
    public void updateSystemAssPrice2(String loan_no) {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String loan_type = erpUtil.objectIsNullGetString(loanRow.get("loan_info_et.loan_type"));
        double ass_price1 = erpUtil.objectIsNullGetDouble(loanRow.get("loan_ass.ass_price1"));

        double system_ass_price2;
        if (StringUtils.equals("01", loan_type) || StringUtils.equals("02", loan_type) || StringUtils.equals("05", loan_type)) {
            //车辆终价、大数据终价影像率
            double percent1 = 1, percent2 = 1;
            Store results1 = baseDao.query("select * from loan_ass_auto where loan_no='" + loan_no + "' and ass_type='" + LoanAssAutoService.TRUE_FINAL_PRICE + "'");
            Store results2 = baseDao.query("select * from loan_ass_auto where loan_no='" + loan_no + "' and ass_type='" + LoanAssAutoService.BRANCH_FINAL_PRICE + "'");
            for (Row row : results1) {
                percent1 *= (1 + erpUtil.objectIsNullGetDouble(row.get("result")));
            }
            for (Row row : results2) {
                percent2 *= (1 + erpUtil.objectIsNullGetDouble(row.get("result")));
            }
            percent1 = percent1 - 1;
            percent2 = percent2 - 1;
            system_ass_price2 = BigDecimalUtil.round(BigDecimalUtil.mul(BigDecimalUtil.div(ass_price1, BigDecimalUtil.add(1, percent1)), BigDecimalUtil.add(1, percent2)), 0);
        } else {
            system_ass_price2 = BigDecimalUtil.round(BigDecimalUtil.mul(ass_price1, 0.88), 0);
        }
        String sql = "update loan_ass set system_ass_price2=" + system_ass_price2 + " where loan_no='" + loan_no + "'";
        baseDao.execute(sql);
    }


    /**
     * 计算人工价(分行)
     *
     * @param loan_no
     * @throws Exception
     */
    public void updateArtificialPrice(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String loan_type = erpUtil.objectIsNullGetString(loanRow.get("loan_info_et.loan_type"));
        double artificial_price_true = erpUtil.objectIsNullGetDouble(loanRow.get("loan_ass.artificial_price_true"));

        double artificial_price = BigDecimalUtil.round(BigDecimalUtil.mul(artificial_price_true, 0.8), 0);
        String sql = "update loan_ass set artificial_price=" + artificial_price + " where loan_no='" + loan_no + "'";
        baseDao.execute(sql);
    }

    /**
     * 更新最大审批金额
     *
     * @param loan_no
     * @throws Exception
     */
    public void updateMaxApprovePercent(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String prod_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info.prod_no"));
        String prod_version = erpUtil.objectIsNullGetString(loanRow.get("loan_info.prod_version"));

        double max_approve_percentage = 9;
        if (StringUtils.equals("09", prod_no)) {
            if (StringUtils.equals("01", prod_version) || StringUtils.equals("03", prod_version)) {
                max_approve_percentage = 9;
            } else {
                max_approve_percentage = 6;
            }
        } else if (StringUtils.equals("51", prod_no)) {
            max_approve_percentage = 11;
        } else if (StringUtils.equals("52", prod_no) || StringUtils.equals("53", prod_no)) {
            max_approve_percentage = 3;
        }
        String sql;
        sql = "update loan_info_et set max_approve_percentage=" + max_approve_percentage + " where loan_no='" + loan_no + "'";
        baseDao.execute(sql);
    }

    /**
     * 根据最大审批金额更新最大成数
     *
     * @param loan_no
     * @throws Exception
     */
    public void updateMaxApprovePercentByMaxApproveSum(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        double final_price = getFinalAssPrice(loan_no);

        double max_approve_sum = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.max_approve_sum"));

        if (final_price != 0) {
            double max_approve_percentage = BigDecimalUtil.round(BigDecimalUtil.mul(BigDecimalUtil.div(max_approve_sum, final_price), 10), 2);
            String sql = "update loan_info_et set max_approve_percentage=" + max_approve_percentage + " where loan_no='" + loan_no + "'";
            baseDao.execute(sql);
        }
    }


    /**
     * 根据产品更新产品版本
     *
     * @param loan_no
     * @throws Exception
     */
    public void updateProdVersion(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String prod_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info.prod_no"));
        String prod_version;
        if (StringUtils.equals("09", prod_no) || StringUtils.equals("01", prod_no)) {
            prod_version = "01";
        } else {
            prod_version = "04";
        }
        String sql = "update loan_info set prod_version='" + prod_version + "' where loan_no='" + loan_no + "'";
        baseDao.execute(sql);
    }


    /**
     * 根据产品版本更新产品
     *
     * @param loan_no
     * @throws Exception
     */
    public void updateProdNo(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String templet_flow_no = erpUtil.objectIsNullGetString(loanRow.get("wf_review.templet_flow_no"));
        String prod_version = erpUtil.objectIsNullGetString(loanRow.get("loan_info.prod_version"));
        String prod_no;
        if (RxErpUtil.checkYcFlow(templet_flow_no)) {
            if (StringUtils.equals("04", prod_version)) {
                prod_no = "52";
            } else {
                prod_no = "09";
            }
        } else {
            if (StringUtils.equals("04", prod_version)) {
                prod_no = "02";
            } else {
                prod_no = "01";
            }
        }
        String sql = "update loan_info set prod_no='" + prod_no + "' where loan_no='" + loan_no + "'";
        baseDao.execute(sql);
    }

    /**
     * 更新最大审批金额
     *
     * @param loan_no
     * @throws Exception
     */
    public void updateMaxApproveSum(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        double final_price = getFinalAssPrice(loan_no);
        double max_approve_percentage = erpUtil.objectIsNullGetDouble(loanRow.get("loan_info_et.max_approve_percentage"));
        double max_approve_sum = BigDecimalUtil.round(BigDecimalUtil.mul(final_price, BigDecimalUtil.div(max_approve_percentage, 10)), 0);
        String sql;
        sql = "update loan_cost set max_approve_sum=" + max_approve_sum + " where loan_no='" + loan_no + "'";
        baseDao.execute(sql);
    }

    /**
     * 更新实际审批金额
     *
     * @param loan_no
     * @throws Exception
     */
    public void updateApproveSum(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        double final_price = getFinalAssPrice(loan_no);
        double approve_percentage = erpUtil.objectIsNullGetDouble(loanRow.get("loan_info_et.approve_percentage"));
        double approve_sum = BigDecimalUtil.round(BigDecimalUtil.mul(final_price, BigDecimalUtil.div(approve_percentage, 10)), 0);
        String sql = "update loan_cost set approve_sum='" + approve_sum + "' where loan_no='" + loan_no + "'";
        baseDao.execute(sql);
    }

    /**
     * 更新实际审批成数
     *
     * @param loan_no
     * @throws Exception
     */
    public void updateApprovePercent(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        double final_price = getFinalAssPrice(loan_no);
        double approve_sum = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.approve_sum"));
        if (final_price != 0) {
            double approve_percentage = BigDecimalUtil.round(BigDecimalUtil.mul(BigDecimalUtil.div(approve_sum, final_price), 10), 2);
            String sql = "update loan_info_et set approve_percentage='" + approve_percentage + "' where loan_no='" + loan_no + "'";
            baseDao.execute(sql);
        }
    }

    /**
     * 更新临时担保额度
     *
     * @param loan_no
     * @throws Exception
     */
    public void updateTempSum(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String prod_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info.prod_no"));

        double approve_sum = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.approve_sum"));
        double temp_sum = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.temp_sum"));
        double car_price = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.car_price"));

        if (StringUtils.equals("51", prod_no)) {
            if (temp_sum != 0) {
                if (approve_sum + temp_sum > car_price) {
                    temp_sum = BigDecimalUtil.sub(car_price, approve_sum);
                    String sql = "update loan_cost set temp_sum= " + temp_sum + " where loan_no = '" + loan_no + "'";
                    baseDao.execute(sql);
                }
            }
        }
    }


    /**
     * 更新实际审批金额/成数
     *
     * @param loan_no
     * @throws Exception
     */
    public void updateApproveSumByMaxApproveSumAndCarPrice(String loan_no) throws Exception {
        String sql;
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String prod_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info.prod_no"));
        double final_price = getFinalAssPrice(loan_no);
        double approve_sum = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.approve_sum"));
        double max_approve_sum = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.max_approve_sum"));
        double car_price = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.car_price"));
        if (approve_sum == 0 || approve_sum > max_approve_sum) {
            approve_sum = max_approve_sum;
            sql = "update loan_cost set approve_sum=" + approve_sum + " where loan_no='" + loan_no + "'";
            baseDao.execute(sql);
        }
        sql = "update loan_info_et set approve_percentage='" + BigDecimalUtil.round(BigDecimalUtil.mul(BigDecimalUtil.div(approve_sum, final_price), 10), 2) + "' where loan_no='" + loan_no + "'";
        baseDao.execute(sql);
    }


    /**
     * 更新保证金提额
     *
     * @param loan_no
     * @throws Exception
     */
    public void updateDepositSum(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        double risk_margin = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.risk_margin"));
        double approve_sum = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.approve_sum"));
        double temp_sum = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.temp_sum"));
        double assurance_sum = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.assurance_sum"));
        double sum = BigDecimalUtil.add(BigDecimalUtil.add(approve_sum, temp_sum), assurance_sum);
        double deposit_sum = BigDecimalUtil.round(BigDecimalUtil.mul(sum, risk_margin / 100), 2);
        String sql = "update loan_cost set deposit_sum= " + deposit_sum + " where loan_no = '" + loan_no + "'";
        baseDao.execute(sql);
    }

    /**
     * 更新合同金额
     *
     * @param loan_no
     * @throws Exception
     */
    public void updateConAmount(String loan_no) throws Exception {
        synchronized (loan_no) {
            Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
            String prod_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info.prod_no"));
            String loan_type = erpUtil.objectIsNullGetString(loanRow.get("loan_info_et.loan_type"));
            double deposit_sum = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.deposit_sum"));
            double approve_sum = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.approve_sum"));
            double temp_sum = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.temp_sum"));
            double assurance_sum = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.assurance_sum"));
            double renewal_principal_sum = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.renewal_principal_sum"));
            double insurance_premium = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.insurance_premium"));
            double sum = BigDecimalUtil.add(BigDecimalUtil.add(approve_sum, deposit_sum), BigDecimalUtil.add(temp_sum, assurance_sum));
            double contract_sum = 0D;
            double finetune_sum = 0D;
            if (StringUtils.equals("03", loan_type) || StringUtils.equals("04", loan_type)) {//续期或转等 不在这里计算
                String renewal_loan_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info.renewal_loan_no"));
                double principal_fee = getPrincipalFee(renewal_loan_no);
                contract_sum = BigDecimalUtil.round(BigDecimalUtil.sub(principal_fee, renewal_principal_sum), 2);
                baseDao.execute("update loan_cost set approve_sum =" + (int) contract_sum + " where loan_no = '" + loan_no + "'");
            } else {
                contract_sum = BigDecimalUtil.mul(BigDecimalUtil.decimal(BigDecimalUtil.div(sum, 100), 0), 100);
                finetune_sum = Math.abs(BigDecimalUtil.sub(sum, contract_sum));
            }
            String sql = "update loan_cost set contract_sum =" + (int) contract_sum + ",finetune_sum= " + finetune_sum + " where loan_no = '" + loan_no + "'";
            baseDao.execute(sql);
        }
    }

    /**
     * 得到剩余本金
     *
     * @param loan_no
     * @return
     */
    public double getPrincipalFee(String loan_no) {
        String sql = "select sum(fee_amount) "
                + "	from loan_mid_payable "
                + "	where table_key_no='" + loan_no + "' and fee_type='PRINCIPAL_FEE' and is_writeoff='N'";
        double principal_fee = erpUtil.objectIsNullGetDouble(baseDao.queryObject(sql));
        return principal_fee;
    }


    /**
     * 更新待预收利息
     *
     * @param loan_no
     * @throws Exception
     */
    public void updateAdvanceRateSum(String loan_no) {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        double duetime = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.duetime"));
        double contract_sum = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.contract_sum"));
        double advance_rate_percent = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.advance_rate_percent"));
        double advance_rate_sum = BigDecimalUtil.round(BigDecimalUtil.mul(BigDecimalUtil.mul(contract_sum, Double.valueOf(advance_rate_percent) / 100), duetime), 2);
        String sql = "update loan_cost set advance_rate_sum=" + advance_rate_sum + " where loan_no = '" + loan_no + "'";
        baseDao.execute(sql);
    }

    /**
     * 更新首付租金
     *
     * @param loan_no
     * @throws Exception
     */
    public void updateInitialPaymentFee(String loan_no) {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String prod_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info.prod_no"));
        if (StringUtils.equals("51", prod_no)) {
            double contract_sum = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.contract_sum"));
            double initial_payment_fee_percent = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.initial_payment_fee_percent"));
            double car_price = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.car_price"));
            double insurance_premium = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.insurance_premium"));
            double initial_payment_fee = BigDecimalUtil.add(
                    BigDecimalUtil.mul(contract_sum, BigDecimalUtil.div(initial_payment_fee_percent, 100)),
                    BigDecimalUtil.sub(BigDecimalUtil.add(car_price, insurance_premium), contract_sum)
            );
            String sql = "update loan_cost set initial_payment_fee=" + initial_payment_fee + " where loan_no = '" + loan_no + "'";
            baseDao.execute(sql);
        }
    }


    /**
     * 更新放款方（2018-09-15集金版），固定为2次放款，分别5成
     *
     * @param loan_no
     * @throws Exception
     */
    public void updateEmployersV2(String loan_no) throws Exception {
        synchronized (loan_no) {
            Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);

            String loan_id = ErpUtil.objectIsNullGetString(loanRow.get("loan_info.loan_id"));
            String loan_type = ErpUtil.objectIsNullGetString(loanRow.get("loan_info_et.loan_type"));
            String prod_no = ErpUtil.objectIsNullGetString(loanRow.get("loan_info.prod_no"));
            String duetime = ErpUtil.objectIsNullGetString(loanRow.get("loan_cost.duetime"));
            double contract_sum = ErpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.contract_sum"));
            String is_prepay = ErpUtil.objectIsNullGetString(loanRow.get("loan_info_et.is_prepay"));
            double prepay_sum = ErpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.prepay_sum"));

            if (StringUtils.equals("01", is_prepay) || prepay_sum > 0) {//5.0没有预付款流程
                throw new Exception(BizErrorCodeEnum.T1098.getCode());
            }


            String sql = "select * from loan_financing_rules where prod_no = '" + prod_no + "' and loan_type = '" + loan_type + "'";
            Store store = baseDao.query(sql);
            String lenders_no = "";
            for (Row row : store) {
                String duetime_condition = row.getString("duetime_condition");
                // 条件公式
                Formula conFormula = new Formula(duetime_condition);
                conFormula.setVariable("loan_cost.duetime", duetime);
                conFormula.setVariable("loan_cost.contract_sum", contract_sum);
                if (conFormula.bool()) {
                    lenders_no = ErpUtil.objectIsNullGetString(row.get("lenders_no"));
                }
            }

            Row employersinforow;

            if (StringUtils.equals("jc", lenders_no)) {// 万微融资租赁（上海）有限公司
                employersinforow = erpUtil.getEmployersInfo("jc");
            } else if (StringUtils.equals("wz", lenders_no)) {
                employersinforow = erpUtil.getEmployersInfo("wz");//王湛
            } else {
                employersinforow = erpUtil.getEmployersInfo("rzzl");//深圳任性融资租赁有限公司 账户
            }

            String pay_card = employersinforow.getString("pay_card");
            String pay_bank = employersinforow.getString("pay_bank");
            String receive_card = employersinforow.getString("receive_card");
            String receive_bank = employersinforow.getString("receive_bank");
            String employerNo = employersinforow.getString("employerNo");
            double contribution_amount = BigDecimalUtil.div(contract_sum, 2);


            sql = "select * from loan_info_employers where loan_no='" + loan_no + "'";
            Store employersInfo = baseDao.query(sql);

            sql = "INSERT INTO loan_info_employers (ID, NO, EMPLOYERS_NO, LOAN_ID, LOAN_NO, CONTRIBUTION_AMOUNT, PAY_TYPE, PAY_MODE, PAY_CARD, PAY_BANK, RECEIVE_CARD, RECEIVE_BANK, OPER_USER_ID, OPER_USER_NO, OPER_TIME, STATUS) VALUES ("
                    + "uuid(),uuid(), " + "'" + employerNo + "', " + "'" + SafeUtil.safe(loan_id) + "', " + "'" + SafeUtil.safe(loan_no)
                    + "', " + contribution_amount + ", " + "'02', " + "'01', " + "'" + pay_card + "'," + "'" + pay_bank + "'," + "'" + receive_card + "'," + "'" + receive_bank
                    + "'," + "'SYS', " + "'SYS', " + "now(), " + "'1');";
            if (employersInfo == null || employersInfo.size() == 0) {//插入两条
                baseDao.execute(sql);
                baseDao.execute(sql);
            } else if (employersInfo.size() == 1) {//插入一条
                baseDao.execute(sql);
            }
            //全部状态改为3
            sql = "update loan_info_employers set status='3' where loan_no='" + loan_no + "' ";
            baseDao.execute(sql);

            //最新两条更新为1
            sql = "update loan_info_employers set status='1' where loan_no='" + loan_no + "' order by oper_time desc limit 2";
            baseDao.execute(sql);
            //统一更新两条放款方信息
            sql = "update loan_info_employers set "
                    + "employers_no='" + employerNo + "',"
                    + "pay_card='" + pay_card + "',"
                    + "pay_bank='" + pay_bank + "',"
                    + "receive_card='" + receive_card + "',"
                    + "receive_bank='" + receive_bank + "',"
                    + "contribution_amount=" + contribution_amount + ","
                    + "pay_type='02',"
                    + "pay_mode='01' "
                    + "where loan_no ='" + loan_no + "' and status='1'";
            baseDao.execute(sql);
            //修改后一条放款方放款类型为二次放款到卡
            sql = "update loan_info_employers set pay_type='03' where loan_no='" + loan_no + "' and status='1' limit 1";
            baseDao.execute(sql);
        }
    }

    /**
     * 得到远程全款GPS模板编号
     *
     * @param loan_no
     * @return
     * @throws Exception
     */
    public String getYcQkGpsContractModelId(String loan_no) throws Exception{
        String sql = "select * from loan_cost where loan_no='" + SafeUtil.safe(loan_no) + "'";
        Row loanCostRow = baseDao.queryRow(sql);
        // 合同金额
        double contractSum = loanCostRow.getNumber("contract_sum").doubleValue();
        if (contractSum <= 0) {
            throw new Exception(BizErrorCodeEnum.T1015.getCode());
        }
        sql = "select * from loan_info_et where loan_no='" + SafeUtil.safe(loan_no) + "' ";
        Row loanEtRow = baseDao.queryRow(sql);
        String borrowedName = loanEtRow.getString("borrowed_name");
        String borrowedIdcard = loanEtRow.getString("borrowed_idcard");
        String borrowedMobile = loanEtRow.getString("borrowed_mobile");
        String modelId;
        //是否有共借人
        boolean isUseBorrowed = false;
        if (StringUtils.isNotBlank(borrowedName) || StringUtils.isNotBlank(borrowedIdcard) || StringUtils.isNotBlank(borrowedMobile)) {
            if (StringUtils.isBlank(borrowedName) || StringUtils.isBlank(borrowedIdcard) || StringUtils.isBlank(borrowedMobile)) {
                throw new Exception(BizErrorCodeEnum.T1038.getCode());
            }
            isUseBorrowed = true;
        }
        if (isUseBorrowed) {
            modelId = "0504";
        } else {
            modelId = "0503";
        }
        return modelId;
    }


    /**
     * 创建合同
     *
     * @param loan_no
     * @throws Exception
     */
    public void creatCon(String loan_no) throws Exception {


        Row loanRow = erpUtil.getLoanConData(loan_no);
        String prod_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info.prod_no"));
        String sell_branch_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info.sell_branch_no"));
        if (StringUtils.equals("09", prod_no)) {
            String modelId = getYcQkGpsContractModelId(loan_no);
            loanRow.put("modelId",modelId);
            loanRow.put("branchRow",getBranchZhhr(sell_branch_no));
            loanRow.put("qyzRow",getBranchZqz(sell_branch_no));
            loanRow.put("guarantorStore",getGuarantorList(sell_branch_no));
            loanRow.put("generalStore",getGeneralList());
            loanRow = contractContentUtil.createQkqpsContent(loanRow);
        }else{
            throw new Exception(BizErrorCodeEnum.T1041.getCode());
        }
        contractClient.createContract(loanRow);
    }

    /**
     * 创建网点入伙流程合同
     *
     * @throws Exception
     */
    public void creatJoinBranchCon(String branch_join_no) throws Exception {

        Row joinRow = new Row();
        String modelId = "0401";
        joinRow.put("modelId",modelId);
        joinRow.put("branch_join_no",branch_join_no);
        joinRow.put("partner_store",getBranchPartner(branch_join_no));
        joinRow.put("guarantee_store",getBranchGuarantee(branch_join_no));
        joinRow.put("loanuncleared0cust",getBranchStockCustomer(branch_join_no));
        joinRow = contractContentUtil.createJoinBranchContent(joinRow);
        contractClient.createContract(joinRow);
    }

    /**
     * 创建区域入伙流程合同
     *
     * @throws Exception
     */
    public void creatJoinAreaCon(String area_join_no) throws Exception {

        Row joinRow = new Row();
        String modelId = "0402";
        joinRow.put("modelId",modelId);
        joinRow.put("area_join_no",area_join_no);
        joinRow.put("partner_store",getAreaPartner(area_join_no));
        joinRow.put("guarantee_store",getAreaGuarantee(area_join_no));
        joinRow.put("loanuncleared0cust",getAreaStockCustomer(area_join_no));
        joinRow = contractContentUtil.createJoinAreaContent(joinRow);
        contractClient.createContract(joinRow);
    }

    /**
     * 获取网点下所有存量客户
     * @param branch_join_no
     * @return
     * @throws Exception
     */
    public Store getBranchStockCustomer(String branch_join_no)throws Exception{
        String sql = "select dept_id from join_branch where branch_join_no = '"+branch_join_no+"'";
        String branch_dept_id = (String)baseDao.queryObject(sql);
        sql = "select * from stock_customer where dept_id = '"+branch_dept_id+"'";
        return baseDao.query(sql);
    }

    /**
     * 获取区域下所有存量客户
     * @param area_join_no
     * @return
     * @throws Exception
     */
    public Store getAreaStockCustomer(String area_join_no)throws Exception{
        String sql = "select dept_id from join_area where area_join_no = '"+area_join_no+"'";
        String area_dept_id = (String)baseDao.queryObject(sql);
        sql = "select * from join_branch where area_dept_id = '"+area_dept_id+"'";
        Store branchStore = baseDao.query(sql);
        Store stockcustomer = new Store();
        for(Row branchRow:branchStore){
            String branch_dept_id =  branchRow.getString("dept_id");
            sql = "select * from stock_customer where dept_id = '"+branch_dept_id+"'";
            stockcustomer.addAll(baseDao.query(sql));
        }
        return stockcustomer;
    }

    /**
     * 获取区域所有合伙人
     * @param area_join_no
     * @return
     * @throws Exception
     */
    public Store getAreaPartner(String area_join_no)throws Exception{
        String sql = "select join_area_partner.partner_name,join_area_partner.idcard,join_area_partner.phone_1,join_area_partner.residence_address "
                +"from join_area "
                +"left join join_area_partner on join_area_partner.area_join_no = join_area.area_join_no "
                +"where join_area.area_join_no = '"+area_join_no+"' and join_area_partner.status != '3' and partner_identity !='03' and join_area.status = '1'";
        Store partner_store  = baseDao.query(sql);
        return partner_store;
    }

    /**
     * 获取区域所有担保人
     * @param area_join_no
     * @return
     * @throws Exception
     */
    public Store getAreaGuarantee(String area_join_no)throws Exception{
        String sql = "select join_area_partner.partner_name,join_area_partner.idcard,join_area_partner.phone_1,join_area_partner.residence_address "
                +"from join_area "
                +"left join join_area_partner on join_area_partner.area_join_no = join_area.area_join_no "
                +"where join_area.area_join_no = '"+area_join_no+"' and join_area_partner.status != '3' and partner_identity ='03' and join_area.status = '1'";
        Store guarantee_store  = baseDao.query(sql);
        return guarantee_store;
    }

    /**
     * 获取网点所有合伙人
     * @param branch_join_no
     * @return
     * @throws Exception
     */
    public Store getBranchPartner(String branch_join_no)throws Exception{
        String sql = "select join_branch_partner.partner_name,join_branch_partner.idcard,join_branch_partner.phone_1,join_branch_partner.residence_address "
                + "from join_branch "
                + "left join join_branch_partner on join_branch_partner.branch_join_no = join_branch.branch_join_no "
                + "where join_branch.branch_join_no = '"+branch_join_no+"' and join_branch_partner.status != '3' and partner_identity !='03' and join_branch.status = '1'";
        Store partner_store  = baseDao.query(sql);
        return partner_store;
    }

    /**
     * 获取网点所有担保人
     * @param branch_join_no
     * @return
     * @throws Exception
     */
    public Store getBranchGuarantee(String branch_join_no)throws Exception{
        String sql = "select join_branch_partner.partner_name,join_branch_partner.idcard,join_branch_partner.phone_1,join_branch_partner.residence_address "
                + "from join_branch "
                + "left join join_branch_partner on join_branch_partner.branch_join_no = join_branch.branch_join_no "
                + "where join_branch.branch_join_no = '"+branch_join_no+"' and join_branch_partner.status != '3' and partner_identity ='03' and join_branch.status = '1'";
        Store guarantee_store  = baseDao.query(sql);
        return guarantee_store;
    }

    /**
     * 得到分行主合伙人
     * @param sell_branch_no 分行编号
     * @return
     * @throws Exception
     */
    public Row getBranchZhhr (String sell_branch_no)throws Exception{
        String sql = "select a.partner_name,a.idcard,a.phone_1 "
                + "from join_branch t "
                + "left join join_branch_partner a on t.branch_join_no = a.branch_join_no "
                + "where t.dept_id = '" + sell_branch_no + "' and a.partner_identity = '01' and a.status !='3' and t.status = '1'";
        Row branchRow = baseDao.queryRow(sql);
        return branchRow;
    }

    /**
     * 得到网点主区域总
     * @param sell_branch_no
     * @return
     * @throws Exception
     */
    public Row getBranchZqz(String sell_branch_no)throws Exception{
        String sql = "select b.partner_name,b.idcard,b.phone_1 "
                + "from join_branch t "
                + "left join join_area a on t.area_dept_id = a.dept_id "
                + "left join join_area_partner b on a.area_join_no = b.area_join_no "
                + "where t.dept_id = '" + sell_branch_no + "' and b.partner_identity = '01' and b.status !='3' and t.status = '1'";
        Row qzRow = baseDao.queryRow(sql);
        return qzRow;
    }

    /**
     * 得到所有区域总
     * @return
     * @throws Exception
     */
    public Store getGeneralList()throws Exception{
        String sql = "select * from join_area_partner where partner_identity = '01' and status !='3'";
        Store generalStore = baseDao.query(sql);

        for(Row row:generalStore){
            row.put("general0qz_name", row.getString("partner_name"));
            row.put("general0qz_idcard", row.getString("idcard"));
            row.put("general0qz_address", row.getString("residence_address"));
            row.put("general0qz_phone", row.getString("phone_1"));
        }
        return generalStore;
    }

    /**
     * 得到分行担保人
     * @param sell_branch_no
     * @return
     * @throws Exception
     */
    public Store getGuarantorList(String sell_branch_no) throws Exception{
        //找出部门下的担保人
        String sql = "select partner_no,partner_name,idcard,phone_1,residence_address "
                + "from join_branch t "
                +"left join join_branch_partner a on t.branch_join_no = a.branch_join_no "
                +"where t.dept_id = '" + sell_branch_no + "' and t.status = '1' and a.status != '3'" ;
        Store partnerList = baseDao.query(sql);

        if (partnerList!=null&&partnerList.size()>0) {
            for (Row partner_row : partnerList) {
                if (StringUtils.isBlank(partner_row.getString("partner_name"))
                        ||StringUtils.isBlank(partner_row.getString("idcard"))
                        ||StringUtils.isBlank(partner_row.getString("phone_1"))) {
                    throw new Exception("分行担保人员信息存在空信息，请检查分行担保人信息");
                }
            }
        }
        for(Row row:partnerList){
            row.put("join_branch_partner0partner_name", row.getString("partner_name"));
            row.put("join_branch_partner0idcard", row.getString("idcard"));
            row.put("join_branch_partner0residence_address", row.getString("residence_address"));
            row.put("join_branch_partner0phone_1", row.getString("phone_1"));
        }
        return partnerList;
    }

    /**
     * 获取同盾评分
     *
     * @param loan_no
     * @throws Exception
     */
    public void updateTongDunScore(String loan_no) throws Exception {

        int credit_score = 20;
        if (StringUtils.equals("0", runtime)) {
            credit_score = (int) (60 * Math.random());
        } else {
            Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
            String cust_name = erpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_name"));
            String id_number = erpUtil.objectIsNullGetString(loanRow.get("loan_cust.idcard"));
            String mobile = erpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_phone"));

            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("name", cust_name);// 姓名 必填
            paramMap.put("id_number", id_number);// 身份证 必填
            paramMap.put("mobile", mobile);// 手机号

            try {
                String reportId = tongDunClinet.queryReportNo(paramMap, false);
                Thread.sleep(2000);
                JSONObject jsonObject = tongDunClinet.queryReport(reportId, false);
                if (jsonObject.getBooleanValue("success")) {
                    credit_score = erpUtil.objectIsNullGetInteger(jsonObject.get("final_score"));
                } else {
                    //表示没有生成报告
                    if (!StringUtils.equals("204", jsonObject.getString("reason_code"))) {
                        throw new Exception(jsonObject.getString("reason_desc"));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        baseDao.execute("update loan_cust set credit_score='" + credit_score + "' where loan_no='" + loan_no + "'");
    }

    /**
     * 初始化新增贷款执行人员和分行信息
     *
     * @param loan_no
     * @return
     * @throws Exception
     */
    public void updateConsultInfo(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String loan_type = erpUtil.objectIsNullGetString(loanRow.get("loan_info_et.loan_type"));
        String create_user_id = erpUtil.objectIsNullGetString(loanRow.get("loan_info.oper_user_no"));
        String sql;
        if (StringUtils.isEmpty(loan_type)) {
            loan_type = "01";
            sql = "update loan_info_et set loan_type='" + loan_type + "' where loan_no='" + loan_no + "'";
            baseDao.execute(sql);
        }
        //目前只管新增，其他类型需要重写
        if (StringUtils.equals("01", loan_type)||StringUtils.equals("02", loan_type)) {
            sql = "select e.dept_id,e.leader_id,a.id_card,e.company_no " +
                    "from i_frame_sys_user a " +
                    "left join i_frame_sys_user_position c on a.user_id=c.user_id " +
                    "left join i_frame_sys_position d on c.position_id=d.position_id " +
                    "left join i_frame_sys_dept e on d.dept_id=e.dept_id " +
                    "where a.user_id='" + create_user_id + "' and a.user_status=1 and c.is_primary = '1' limit 1";
            Row dept_user_row = baseDao.queryRow(sql);
            String dept_id = dept_user_row.getString("dept_id");
            String leader_no = dept_user_row.getString("leader_id");
            String sell_branch_no = erpUtil.getBelongBranch(dept_id);
            if(sell_branch_no==null){
                throw new Exception("所属网点未完成网点入伙流程！");
            }
            sql = "select * from join_branch where dept_id = '"+sell_branch_no+"' limit 1";
            Row branchRow = baseDao.queryRow(sql);
            String area_dept_id = erpUtil.objectIsNullGetString(branchRow.get("area_dept_id"));
            sql = "update loan_info "
                    + "set seller_no='" + create_user_id + "',"
                    + "	   sell_dept_no='" + dept_id + "',"
                    + "	   seller_leader_no='" + leader_no + "',"
                    + "	   sell_branch_no='" + sell_branch_no + "',"
                    + "	   branch_no='" + sell_branch_no + "',"
                    + "    dept_no = '" + dept_id + "',"
                    + "    dept_id = '" + dept_id + "',"
                    + "    dept_leader_no= '" + leader_no + "',"
                    + "    area_no= '" + area_dept_id + "',"
                    + "    user_no = '" + create_user_id + "' "
                    + "where loan_no='" + loan_no + "'";
            baseDao.execute(sql);
        }
    }



    /**
     * 生成随机数
     */
    public static String getStringRandom(int length) {
        StringBuffer val = new StringBuffer();
        Random random = new Random();
        //参数length，表示生成几位随机数
        int ischar = 0;
        int isnum = 0;
        for (int i = 0; i < length; i++) {
            String charOrNum = random.nextInt(2) % 2 == 0 ? "char" : "num";
            if (ischar >= 2) {
                charOrNum = "num";
            }
            if (isnum >= 4) {
                charOrNum = "char";
            }
            //输出字母还是数字
            if ("char".equalsIgnoreCase(charOrNum)) {
                val.append((char) (random.nextInt(26) + 65));
                ischar++;
            } else if ("num".equalsIgnoreCase(charOrNum)) {
                val.append(random.nextInt(10));
                isnum++;
            }
        }
        return val.toString();
    }

    /**
     * 初始化贷款类型
     * @param loan_no
     * @throws Exception
     */
    public void updateLoanType(String loan_no) throws Exception{
        Row loanRow= erpUtil.getLoanBasicDataInfoV2(loan_no);
        String carPlate = loanRow.getString("loan_car.car_plate");
        String sql="select * "
                + "from loan_info_et "
                + "where loan_no in (select loan_no from loan_car where car_plate='"+carPlate+"') "
                + "and fk_date is not null and fk_date!='' "
                + "and loan_type in ('01','02','05') "
                + "order by fk_date";
        Store loans= baseDao.query(sql);
        for(Row loan:loans){
            String settle_date=ErpUtil.objectIsNullGetString(loan.get("settle_date"));
            if(Util.isEmpty(settle_date)){
                throw new Exception(BizErrorCodeEnum.Z0041.getCode());
            }
        }
        if(loans.size()>0){
            Row loan=loans.get(loans.size()-1);
            String renewal_loan_no=loan.getString("loan_no");
            baseDao.execute("update loan_info set renewal_loan_no='"+renewal_loan_no+"' where loan_no='"+loan_no+"'");
            baseDao.execute("update loan_info_et set loan_type='02' where loan_no='"+loan_no+"'");
        }
    }

    /**
     * 得到最终价格
     * @param loan_no
     * @return
     * @throws Exception
     */
    public double getFinalAssPrice(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        double system_ass_price = erpUtil.objectIsNullGetDouble(loanRow.get("loan_ass.system_ass_price"));
        double system_ass_price2 = erpUtil.objectIsNullGetDouble(loanRow.get("loan_ass.system_ass_price2"));
        double artificial_price = erpUtil.objectIsNullGetDouble(loanRow.get("loan_ass.artificial_price"));
        double final_price = artificial_price;
        if (final_price == 0) {
            final_price = system_ass_price2;
            if (final_price == 0) {
                final_price = system_ass_price;
            }
        }
        return final_price;
    }

    /**
     * 创建清算系统个人账户
     */
    public void createUserAccount(String loanNo) throws Exception {
        clearClient.createCustAccount(loanNo);

        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loanNo);

        String clearing_ordinary = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.clearing_ordinary"));
        String clearing_frozen = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.clearing_frozen"));
        //如果已存在清算系统账户ID ，则不再创建新账户
        if (Util.isNotEmpty(clearing_ordinary)||Util.isNotEmpty(clearing_frozen)) {
            return;
        }
        clearClient.createCustAccount(loanNo);
    }
}