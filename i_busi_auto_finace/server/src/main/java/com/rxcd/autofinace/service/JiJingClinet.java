package com.rxcd.autofinace.service;

import com.alibaba.druid.support.logging.Log;
import com.alibaba.druid.support.logging.LogFactory;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rxcd.autofinace.common.CarPlateCityEnum;
import com.rxcd.autofinace.util.RxErpUtil;
import com.rxcd.autofinace.util.jijing.SignatureAlgorithm;
import com.rxcd.autofinace.util.jijing.SignatureUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.appframe.bigdata.*;
import org.appframe.common.BigDecimalUtil;
import org.appframe.contract.ContractClient;
import org.appframe.contract.JunZiQianClient;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.db.Store;
import org.appframe.wf.util.Const;
import org.appframe.wf.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.sql.SQLException;
import java.util.*;
import java.util.Map.Entry;


/**
 * 集金调用接口
 * 
 */
@Component
public class JiJingClinet {
	
	private final Log log = LogFactory.getLog(JiJingClinet.class);
	
	/**
     * 指定算法为MD5的MessageDigest
     */
    private static MessageDigest messageDigest = null;
    
    /** 16进制字符集 */
    private final char[] HEX_DIGITS = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
    
	/** 合作方标识，由集金分配 */
	private String partnerCode = "renxingcar";
	
	/** 商户编号，由集金分配 */
	//正式
	private String platformNo = "renxingcar201153734088218558";
	//测试
//	private String platformNo = "renxingcar201153614327611910";
	/** ip地址:端口号 */
	//正式
	private String IPCODE = "https://www.fanglbb.com";
	// 测试
//	private String IPCODE = "http://devp2p.worldunion.com.cn:9000";

	/** 私钥*/
	//正式
	private String privateStr = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCXS5hYS4aL9J+MJdpl4jr+gbq9tSB1zf5H/oBgJax94DCrtOFRKidaJ0zkUgfgaCWXsuaNVASaFz/rb9JyBXfxWNuMnY0KDTaWRf4p0hH+HZWAKqcEt5+Jp/xuRrfQyrLYjZWwMZz1VSAS8zeCZpvCslhCktqBS+snnoC7IAeQb6o/Gf3ScnkFrocJ4BbpIcvIwGxvSMvCn6fhF33EGrYkFdDc03iH0i38czr6340Sc38arPucKPrBSj1AZHJyBBD/o9t2A7Q/dLMpvnEp/gagw4i6z4Bc1yHt9hFUQCgVeT12+Xqb9rZ6LOXNoasasie8VYV/EeCDAI0SBu0eF6XTAgMBAAECggEAPEDK5z8M0mgVskbQ94plXD5mFG8yAjiLcDiW/7OBQpAqUfezVBOpoGnOYY+Zu3E+p2+ECfzP6F4iAq2B7g7neaKdrR6BsOMaPjaZxiX3LHCzvVs/MM3AJ55TFcfsJKbVGxEn8AeSLtI799AM503d8mf8Zmb9xkhp2llxgxR6brasy00gD8CBc3JOVMx1xyMQNJw90wtSq0m52feEwgh5ei4L3dz/aEQ5lHBhJKhNMRr3Uz8hBiU6L4aA5KdHMlQJIiBr57KiErUCiCxN/yyezh98dLiXW8MhkV6GAyHlH68bEoUR1skr4HL3qpXbGWpSV0e0GxPJFNVDw1GEtq90AQKBgQDSGGAvde6BVksj2X74G88TK+8mItyWm60FhK4xGb+NSfvnMg8rzrS46UUM9SCXg8w520NbWlkPOaZ1UWWhniIuMP7BhDcET+lISpH8hJYKPyD6Jk7GY5oM85i60lYLlOBDu7UZ/9T0xRgHSmozYIdn1NbGVUS/Lw2xa6BsNLkcgQKBgQC4WkKUwffkL7lezbfrVNQAsCfC1VZMZGJIPoB1ulnyuP4dfuUgODxlBZYYKLpt341BG0EYTLx/AIMYXvd7Af1QzB5kXqXyOkEs1Ofkn88wu0L1VMbqMDhISnCRibkREeC0+mSLppPmkjbOIwo29aGhU1QbD4QJRYbhh4LKbv9oUwKBgAy3mwLkcZZ0ocG+xZfBHyy66RP24kFflP6ThHPGajjkNJJPDmoJbhkPTbW2FM33FWKfKhmag2+Pfsr1LWC2EL2ZC6KtHSNuruFIWkPdxrNCFP3vWa9akhWcnS8Z0kmLLnQLJbA4MyzJzWWLlJgdjLS1Rnbk7G9iKsMFj8Vc/6OBAoGBAINbRP/L6f78hl/nwzn6mojW+63W5wPNrz380vmPy9GoQODxbv9zMo1k2priQlxRosuWSN6wpsrzqXVvDXXnaADvQGSSEDo1igUQux7rQ+1cMQyv2IA71Y6lA+qfsG9aRruNgqEDE3WCD2z4CKo6gW16+33Ao3sbwEzoiqgFSRWjAoGAZIkTxA1tYpl3xBjCT4sAlP4WviaTb+1/BiLjESqscHTkcS2W75ck++SEA34QrQW96LMga5yX/B+iMUccxVw2b5V+eW+MMRWaP801sDiIxmxbvOA7XnSgEj24vuaFhFWL7eYJWHmaSWwtZhmctfj6X8Dw2n4MCsxurzKALC0K+aY=";
	//测试
//	private String privateStr = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCESU1gQfDwCQWzm3Lc267xyfuGm3gIVjAo55Bu7Xyj3W+q+A3lz8Zoe3ptuSPkSraC7YgPWCwTRk5CwpIp9f7/fgwFTMVo5HedN0svHoyBMsyxnwxR8MynQRL0hR5uJQ3W8Ag+5D1znPKFLauvOmx+l3mfuG8jq/2x1DXhrPLZurAC6scfv+NJjOSjWLA6RRMQfvb2Dd3ftnjx50XdmKjzwY37B+nVlF6WfY1et3VSnbILuwk0VU2998gvqo4DswUaU+4RgYUnwxgYwji0AnUT4f3q5escLOQqCzaJuOLc8ZmbITmLmWB6IjSONfbK/cUaMzuiZhat8hCDRGPUAxG7AgMBAAECggEAOrVBZq+9GvoWErCxWD0StfG68/E5YxHFC9JA8knpX9vdPBYcLNp6JlJuchWRTTavKwn5sT2jSeaQwuYQ8hhfU8n/0bZRggyq3RX2AvJeztPZrhZ05ESmvq+NlwEhESbWroYWUrr+XrnPk7VCFWErVz8oVe8HO+CPlYUlaINVv7SJBeLUDpPdSBqWGZvMWC0wHA2U2cOKEcvZ7W5pcTpR/Ikj+Gfp44eRvAwwzLS7Xe4F7X6mVZwqJxp7jWUoqELDyqahF/dn08mXOw+VB5rjkgPVdKkJQSKGX24DlZAbxD+UR1dybgz0x4Ggwz/rCLbyYYa/VqhypEzMEaxgC4inaQKBgQDfpMSBC2TnHgc/fCb4XlJTChDzsxC7HaT9pu+qlLqBGlug6xhXrrkst0zaSgfEEklqFghfILW2isqRdDdYOQR+a4uA5P2mVoRlJJf1DFatD069XrbgtTglQZtFoJdFEjPBmlL2PB0giuK5OfWy9T6Q3uCXVwKL42GTzzo8ctr2RwKBgQCXbOBPEAROZ8e+qLIgvdLnGBj/QztZKLKlEEqgS8iE2+WQqkIzEisB0UQ25bTbv9HXAHmal47MhM6/SwS/SiC/FvmK7W/mo/S/CqxEEScBOvd5VRbK3o6CLQ+4RB47zGtv0SxAHG58OQVety+Zx2HuPprRT+NwNAqJy9O1XKle7QKBgQCQZ+bOYFoV9ci+UF8lbln/7ncFtLDv8XNQqZMFdSV8KVXS8Rp1u4cr/WutY7HiA+DNlE5xhkBvDu0/wGfgGmjxbGXWo4W4+xJf8KICA6aP9ltkYZz6OWl4fQVSjyA95EPxFcHN5wDzu1osOeWRWVO93XvtnKwzQW4HL/6te4gxDwKBgGdmChkSt84OIZ/4M4IV/MCefNs5PnpYRvxdjoe8l9TZDv3ktMWI32MjUXAnFIOFjTVB9sdwPCs9eKu4/ynb5VZ2yrlp/qJATKxzDkSAbeDyMfBhAOpQZIFRYc2Sfg6ZPLei3pN8lpkumpMdhVx9gEqd5zuMUKLadmO3kf3ioyjVAoGAeNZNYM25Q+eJloQ5gW6QB+KByuHaOef2KynZNbw08121Pr+1ikuQNnxFwsPIjGGWKZqV75Gq0oQ9yKWEmm5FK/VoKobnengH2+8auiUHA4V13KSaKFCcvQ0PdgFBbHDovWiZVBcMLQcuGgPeZAa+fC3NaEdZRVN9Z/VYh4IDXvY=";

	@Autowired
	private RxErpUtil erpUtil;

	@Autowired
    private BaseDao baseDao;

	@Autowired
	private JunZiQianClient JunZiQianClient;

	@Autowired
	private TianXingClinet2 tianXingClinet;

	@Autowired
	private TongDunClinet2 tongDunClinet;

	@Autowired
	private JiAoClinet2 jiAoClinet;

	@Autowired
	private AcedataClinet2 acedataClinet;

	@Autowired
	private JingZhenGuClinet2 jingZhenGuClinet;

	@Autowired
	private ChaboshiClinet2 chaboshiClinet;

	@Autowired
	private ContractClient contractClient;
	 /**
     * 初始化messageDigest的加密算法为MD5
     */
    static {
        try {
            messageDigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

	/**
	 * 预审信息接口
	 * @return
	 * @throws Exception
	 */
	public JSONObject preApplyLoanAndProduct(Row loanRow) throws Exception{
		String url = IPCODE+"/p2passet/fanglbb/loanAction/preApplyLoanAndProduct";
		JSONObject jsonObject = new JSONObject();//业务数据JSON
		jsonObject.put("requestNo", loanRow.getString("loan_info.requestNo"));//资产资产订单流水号(唯一标识)
		jsonObject.put("platformNo", platformNo);//商户编号，由集金分配
		jsonObject.put("realName", loanRow.getString("loan_cust.cust_name"));
		jsonObject.put("idCardNo", loanRow.getString("loan_cust.idcard"));
		jsonObject.put("mobile", loanRow.getString("loan_cust.cust_phone"));
		jsonObject.put("amount", erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.contract_sum")));
		jsonObject.put("period", erpUtil.objectIsNullGetInteger(loanRow.get("loan_cost.duetime")));
		jsonObject.put("periodUnit", 1);//借款期限单位(0:天;1:月;2:年)
		jsonObject.put("userType", "01");//用户类型(01:个人;02:企业)，仅支持个人
		jsonObject.put("workJob", loanRow.getString("loan_cust.work_job"));//个人职业
		jsonObject.put("industry", loanRow.getString("loan_cust.industry"));//所属行业
		jsonObject.put("loanContact1", getloanContact(loanRow.getString("loan_info.loan_no")));//联系人(loanRow未获取，需要重新获取)
		jsonObject.put("sceneCode", "RXQC_CREDIT");//场景code (RXQC_CREDIT)
		jsonObject.put("yearRate", "12");//年利率
		String rep_type = erpUtil.objectIsNullGetString(loanRow.get("loan_info_et.rep_type"));
		String payType = "";
		if(StringUtils.equals("01",rep_type)){
			payType = "38";
		}else if(StringUtils.equals("02",rep_type)){
			payType = "37";
		}
		jsonObject.put("payType", payType);//还款方式(37：先息后本；38：等额等息)
		jsonObject.put("source", loanRow.getString("loan_cust.source"));//还款来源（1工资；2房租；3企业经营收入）
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("partnerCode", partnerCode);
		paramMap.put("reqData", jsonObject.toString());//业务数据报文，JSON格式
		paramMap.put("sign", getSign(jsonObject));//对reqData参数的签名，加密
		return getData(url, paramMap);
	}
	
	/**
	 * 获取联系人号码
	 * @param loan_no
	 * @return
	 * @throws Exception
	 */
	private String getloanContact(String loan_no) throws Exception{
		String sql = "select * from loan_info_contacts where loan_no = '"+loan_no+"' and (contacts_relation = '01' or contacts_relation = '02')";
		Store store = baseDao.query(sql);
		if(store!=null&&store.size()>0){
			return store.get(0).getString("contacts_mobile");
		}else{
			sql = "select * from loan_info_contacts where loan_no = '"+loan_no+"' limit 1";
			Row row = baseDao.queryRow(sql);
			return row.getString("contacts_mobile");
		}
	}
	
	/**
	 * 申请贷款H5 URL
	 * @param loanRow
	 * @throws Exception
	 */
	public String sqdkUrl(Row loanRow) throws Exception{
		String url = IPCODE+"/p2passet/index.html#/login?"
				+ "relateUserId="+loanRow.getString("loan_info.loan_no")
				+"&partnerCode="+partnerCode
				+"&requestNo="+loanRow.getString("loan_info.requestNo")
				+"&partnerPlatformNo="+platformNo
				+"&originType=apply";
		return url;
	}
	
	/**
	 * 查询开户结果接口
	 * @param idcard 身份证
	 * @param mobile 电话号码
	 * @return
	 * @throws Exception
	 */
	public JSONObject queryAccountStatus(String mobile,String idcard) throws Exception{
		String url = IPCODE+"/p2passet/fanglbb/loanAction/queryAccountStatus";
		JSONObject jsonObject = new JSONObject();//业务数据JSON
		jsonObject.put("platformNo", platformNo);//商户编号，由世联集金分配
		jsonObject.put("idcard", idcard);
		jsonObject.put("mobile", mobile);
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("partnerCode", partnerCode);
		paramMap.put("reqData", jsonObject.toString());//业务数据报文，JSON格式
		paramMap.put("sign", getSign(jsonObject));//对reqData参数的签名，加密
		return getData(url, paramMap);
	}
	
	/**
	 * 协议影像上传接口(单个上传)
	 * @return
	 * @throws Exception
	 */
	public JSONObject uploadContract(String filePath,String fileType,String requestNo) throws Exception{
		String url = IPCODE+"/p2passet/fanglbb/loanAction/uploadContract";
		JSONObject jsonObject = new JSONObject();//业务数据JSON
		jsonObject.put("requestNo", requestNo);//资产资产订单流水号(唯一标识)
		jsonObject.put("platformNo", platformNo);//商户编号，由集金分配
		jsonObject.put("fileName", getFileName(getFileNameAndFileSuffix(filePath)));//文件名称
		jsonObject.put("fileType", fileType);//文件类型(《集金-任性汽车对接字段1.0.xlsx》文件上传编码)
		jsonObject.put("fileSuffix", getFileSuffix(getFileNameAndFileSuffix(filePath)));//文件后缀名
		jsonObject.put("fileMD5", getFileMD5String(new File(filePath)));//文件MD5校验字符串(调用协议上传接口则必输)
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("partnerCode", partnerCode);
		paramMap.put("reqData", jsonObject.toString());//业务数据报文，JSON格式
		paramMap.put("sign", getSign(jsonObject));//对reqData参数的签名，加密
		return getFileData(url, paramMap, filePath);
	}
	
	/**
	 * 获取所有需要上传文件的Map集合
	 * @return
	 * @throws Exception
	 */
	public Map<String, String> getUploadMap(Row loanRow) throws Exception{
		String sfzzmz = getFilePath(erpUtil.objectIsNullGetString(loanRow.get("loan_cust.sfzzmz")));//身份证正面
		String sfzfmz = getFilePath(erpUtil.objectIsNullGetString(loanRow.get("loan_cust.sfzfmz")));//身份证反面
		String yhkz1 = getFilePath(erpUtil.objectIsNullGetString(loanRow.get("loan_cust.yhkz1")));//银行卡正面照
		String xszz1 = getFilePath(erpUtil.objectIsNullGetString(loanRow.get("loan_car.xszz1")));//行驶证照1
		String xszz2 = getFilePath(erpUtil.objectIsNullGetString(loanRow.get("loan_car.xszz2")));//行驶证照2
		String djz1 = getFilePath(erpUtil.objectIsNullGetString(loanRow.get("loan_car.djz1")));//登记证1
		String djz2 = getFilePath(erpUtil.objectIsNullGetString(loanRow.get("loan_car.djz2")));//登记证2
		String force_insurance = getFilePath(erpUtil.objectIsNullGetString(loanRow.get("loan_car.force_insurance")));//交强险照
		String cqz = getFilePath(erpUtil.objectIsNullGetString(loanRow.get("loan_car.cqz")));//车前照
		String cpgsz = getFilePath(erpUtil.objectIsNullGetString(loanRow.get("loan_car.cpgsz")));//车+评估师
		String ybpz = getFilePath(erpUtil.objectIsNullGetString(loanRow.get("loan_car.ybpz")));//仪表盘照
		String car_key = getFilePath(erpUtil.objectIsNullGetString(loanRow.get("loan_car.car_key")));//车钥匙照
		String cnz = getFilePath(erpUtil.objectIsNullGetString(loanRow.get("loan_car.cnz")));//中控台
		String chz = getFilePath(erpUtil.objectIsNullGetString(loanRow.get("loan_car.chz")));//车后照
		String cjhz = getFilePath(erpUtil.objectIsNullGetString(loanRow.get("loan_car.cjhz")));//出厂牌
		String yqz = getFilePath(erpUtil.objectIsNullGetString(loanRow.get("loan_car.yqz")));//引擎舱
		String hzz = getFilePath(erpUtil.objectIsNullGetString(loanRow.get("loan_info_et.hzz")));//抵押登记回执照
		String gpsjtz = getFilePath(erpUtil.objectIsNullGetString(loanRow.get("loan_car.gpsjtz")));//GPS截图照(有线)
		String gpswx1 = getFilePath(erpUtil.objectIsNullGetString(loanRow.get("loan_car.gpswx1")));//GPS截图照(无线1)
		String gpswx2 = getFilePath(erpUtil.objectIsNullGetString(loanRow.get("loan_car.gpswx2")));//GPS截图照(无线2)
		String rzzlCon = getConFilePath(loanRow, "0600");//车辆融资租赁合同
		String sksjCon = getConFilePath(loanRow, "0611");//收款收据
		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put(sfzzmz, "1004");
		paramMap.put(sfzfmz, "1005");
		paramMap.put(yhkz1, "1006");
		paramMap.put(xszz1, "1007");
		paramMap.put(xszz2, "1007");
		paramMap.put(djz1, "1008");
		paramMap.put(djz2, "1008");
		paramMap.put(force_insurance, "1009");
		paramMap.put(cqz, "1011");
		paramMap.put(cpgsz, "1012");
		paramMap.put(ybpz, "1013");
		paramMap.put(car_key, "1014");
		paramMap.put(cnz, "1015");
		paramMap.put(chz, "1016");
		paramMap.put(cjhz, "1017");
		paramMap.put(yqz, "1018");
		paramMap.put(hzz, "1020");
		paramMap.put(gpsjtz, "1021");
		paramMap.put(gpswx1, "1021");
		paramMap.put(gpswx2, "1021");
		paramMap.put(rzzlCon, "1022");
		paramMap.put(sksjCon, "1024");

		return paramMap;
	}
	
	/**
	 * 获取合同文件位置（需要下载到本地）
	 * @param loanRow
	 * @param model_detail_id 所属合同模板号
	 * @return
	 * @throws Exception
	 */
	private String getConFilePath(Row loanRow,String model_detail_id) throws Exception{
		String loan_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info.loan_no"));
		String con_code = erpUtil.objectIsNullGetString(loanRow.get("loan_con.con_code"));
		JSONArray conArray = contractClient.getContract(loan_no);
		String applyNo = "";
		String instance_no = "";
		if (conArray != null && conArray.size() > 0) {
			for (Object instance : conArray) {
				JSONObject instanceJson = (JSONObject) instance;
				if(StringUtils.equals(model_detail_id,instanceJson.getString("model_detail_id"))){
					applyNo = instanceJson.getString("apply_no");
					instance_no = instanceJson.getString("instance_no");
				}
			}
		}
		String fileUrl=JunZiQianClient.queryConFileUrl(applyNo);
		InputStream input = null;
		FileOutputStream output = null;
		String jiJingPushPath = Const.SYS_FILE_UPLOAD_URL_PATH + "jijing_push/" + con_code + "/";
		// 目录不存在则创建目录
		File dir_push = new File(jiJingPushPath);
		if (!dir_push.exists()) {
			dir_push.mkdirs();
		}
		URL url = new URL(fileUrl);
		URLConnection con = url.openConnection();
		con.setConnectTimeout(5 * 1000);
		String outputFile = jiJingPushPath+instance_no+".pdf";
		try {
			input = con.getInputStream();
			output = new FileOutputStream(outputFile);
			byte[] buffer = new byte[4 * 1024]; // 4k Buffer
			int read = 0;
			while ((read = input.read(buffer)) != -1) {
				output.write(buffer, 0, read);
			}
		} catch (Exception e) {
			throw new Exception("下载合同失败:" + instance_no);
		}finally {
			if(input!=null){
				input.close();
			}
			if(output!=null){
				output.close();
			}
		}
		return outputFile;
	}
	
	/**
	 * 资产信息补充更新接口
	 * @return
	 * @throws Exception
	 */
	public JSONObject updateApplyLoan(Row loanRow) throws Exception{
		String url = IPCODE+"/p2passet/fanglbb/loanAction/updateApplyLoan";
		JSONObject jsonObject = new JSONObject();//业务数据JSON
		jsonObject.put("partnerCode", partnerCode);//合作方标识，由集金分配
		jsonObject.put("requestNo", loanRow.getString("loan_info.requestNo"));//资产资产订单流水号(唯一标识)
		jsonObject.put("platformNo", platformNo);//商户编号，由集金分配
		jsonObject.put("education",loanRow.getString("loan_cust.education"));//最高学历（1:硕士及以;2:本科;3:专科;4:高中;5:中专;6:初中及以下）
		jsonObject.put("maritalStatus",loanRow.getString("loan_cust.marriage_state"));//婚姻状态(1:未婚;2:已婚;3:离异;4:其他)
		String address_p = loanRow.getString("loan_cust.address_p");
		String cust_address="";
		jsonObject.put("homeAddress",erpUtil.getDetailedAddress(cust_address,address_p));//居住所在地(广东省-深圳市-福田区-车公庙)
		jsonObject.put("homeDetailAddress",loanRow.getString("loan_cust.cust_address"));//居住详细地址
		jsonObject.put("workType",loanRow.getString("loan_cust.work_nature"));//工作性质(1:企业主/个体户;2:普通工薪;3:精英工薪;4:自雇/无业)
		jsonObject.put("workName",loanRow.getString("loan_cust.company_name"));//单位名称
		jsonObject.put("workProperty",loanRow.getString("loan_cust.work_property"));//单位性质(1:私营企业;2:个体户;3:国有企业;4:外资企业;5:合资企业;6:事业单位;7:国家行政机关;8:自雇/无业)
		String company_add_p = loanRow.getString("loan_cust.company_add_p");
		String company_add="";
		jsonObject.put("workAddress",erpUtil.getDetailedAddress(company_add,company_add_p));//单位地址(广东省-深圳市-罗湖区-湖贝)
		jsonObject.put("workDetailAddres",loanRow.getString("loan_cust.company_add"));//单位详细地址
		jsonObject.put("workPhone",loanRow.getString("loan_cust.company_telephone"));//单位电话
		jsonObject.put("icStatus",loanRow.getString("loan_cust.residence_state"));//户口状态
		jsonObject.put("function", "16");//借款用途
		jsonObject.put("carNo", loanRow.getString("loan_car.car_plate"));//车牌号
		jsonObject.put("carShelfNo", loanRow.getString("loan_ass.vin_code"));//车架号
		jsonObject.put("km", loanRow.getString("loan_ass.mileage"));//里程
		jsonObject.put("engineNo", loanRow.getString("loan_ass.engine_code"));//发动机号
		
		String car_audi = erpUtil.objectIsNullGetString(loanRow.get("loan_ass.car_audi"));
		String carBrand = "";
		if(StringUtils.equals("", car_audi)){
			carBrand = erpUtil.objectIsNullGetString(loanRow.get("loan_ass.car_audi_text"));
		}else{
			String sql = "select type_name from loan_car_brand_type where loan_no='"+loanRow.getString("loan_info.loan_no")+"' and type_no = '"+car_audi+"' and query_type='02'";
			carBrand = (String)baseDao.queryObject(sql);
		}
		jsonObject.put("carBrand", carBrand);//车辆品牌类型
		jsonObject.put("firstTime", loanRow.getString("loan_ass.inireg_date"));//初始登记日期（2018-08-18）
		String car_color = erpUtil.objectIsNullGetString(loanRow.get("loan_ass.car_color"));
		String sql = "select code_val from SYS_STD_CODE_VAL where CODE_NO = 'CAR_COLOR' and no = '"+car_color+"'";
		String color = (String)baseDao.queryObject(sql);
		jsonObject.put("color", color);//车辆品牌类型
		jsonObject.put("keyNum", loanRow.getString("loan_ass.key_count"));//车辆交付钥匙数
		jsonObject.put("changeNum",erpUtil.objectIsNullGetString(loanRow.get("loan_ass.transfer_count")));//过户次数
		jsonObject.put("comment", erpUtil.objectIsNullGetString(loanRow.get("loan_ass.ass_price1")));//车辆评估价
		
		jsonObject.put("report", getReport(loanRow));//融租报告
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("partnerCode", partnerCode);
		paramMap.put("reqData", jsonObject.toString());//业务数据报文，JSON格式
		paramMap.put("sign", getSign(jsonObject));//对reqData参数的签名，加密
		return getData(url, paramMap);
	}
	
	/**
	 * 获取综合报告（JSON）
	 * @param loanRow
	 * @return
	 * @throws Exception
	 */
	private JSONObject getReport(Row loanRow) throws Exception{
		String loan_no = loanRow.getString("loan_info.loan_no");
		
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("third_report", getThirdReport(loanRow));//第三方报告
		
		jsonObject.put("income_report", getIncomeReport(loanRow));//收入报告
		
		JSONObject operator_json = new JSONObject();
		String shuju_url = Const.SYS_WEB_URL+"/loan_common_YysShow?loan_no="+loan_no;
		operator_json.put("url", shuju_url);
		jsonObject.put("operator_report", operator_json);//运营商报告
		
		jsonObject.put("danger_report", getDangerReport(loanRow));//出险报告
		
		jsonObject.put("carass_report", getCarassReport(loanRow));//车辆评估报告
		
		JSONObject repair_json = new JSONObject();
		String repair_url = Const.SYS_WEB_URL+"/loan_common_RepairReport?loan_no="+loan_no;
		repair_json.put("url", repair_url);
		jsonObject.put("repair_report", repair_json);//维修报告
		
		jsonObject.put("carcon_report", getcarconReport(loanRow));//车况评估报告（真实）
		
		return jsonObject;
	}
	
	/**
	 * 获取3方报告
	 * @param loanRow
	 * @return
	 * @throws Exception
	 */
	private JSONObject getThirdReport(Row loanRow) throws Exception{
		String cust_name = loanRow.getString("loan_cust.cust_name");
		String idcard = loanRow.getString("loan_cust.idcard");
		String cust_phone = loanRow.getString("loan_cust.cust_phone");
		String driveNum = loanRow.getString("loan_car.fdjh");
		String vin = loanRow.getString("loan_car.cjh");
		String car_plate = loanRow.getString("loan_car.car_plate");
		JSONObject third_json = new JSONObject();
		try {
			JSONObject queryNegativeJosn = tianXingClinet.queryNegative(cust_name, idcard,false);
			String success = queryNegativeJosn.getString("success");
			if(StringUtils.equals("true", success)){
				third_json.put("badInfo", queryNegativeJosn.getJSONObject("data"));//个人不良
			}else{
				third_json.put("badInfo", new JSONObject());//个人不良
			}
			
			JSONObject queryHighCourtRersonalJosn = tianXingClinet.queryHighCourtRersonal(cust_name,idcard,false);
			String highCourtRersonalsuccess = queryHighCourtRersonalJosn.getString("success");
			if(StringUtils.equals("true", highCourtRersonalsuccess)){
				third_json.put("personalLitigation", queryHighCourtRersonalJosn.getJSONObject("data"));//个人涉诉
			}else{
				third_json.put("personalLitigation", new JSONObject());//个人涉诉
			}
			
			try {
				//个人名下企业
				JSONObject companyData = tianXingClinet.queryEnterpriseMember(idcard, false);
				JSONObject dataJson = companyData.getJSONObject("data");
				if(StringUtils.equals("EXIST", dataJson.getString("status"))){
					JSONArray corporates = dataJson.getJSONArray("corporates");
					for (int i = 0; i < corporates.size(); i++) {
						JSONObject companyJson = corporates.getJSONObject(i);
						//如果是在营状态 需要查询  企业涉诉
						if(StringUtils.contains(companyJson.getString("entStatus"),"在营")){
							JSONObject highCourt = tianXingClinet.queryHighCourtEnterprise(companyJson.getString("entName"), companyJson.getString("regNo"), false);
							try {
								companyJson.put("companyLitigation", highCourt.getJSONObject("data"));
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				}
				third_json.put("companyData", dataJson);
			} catch (Exception e) {
				third_json.put("companyData", "");
			}
			
			try {
				CarPlateCityEnum carPlateCityEnum = CarPlateCityEnum.getCarPlateCityEnum(car_plate);
				String cityCode = erpUtil.getCityCode(carPlateCityEnum.getCityName());
				JSONObject queryTrafficOffencesJosn = tianXingClinet.queryTrafficOffences(car_plate, vin, driveNum,cityCode, false);
				String trafficOffencessuccess = queryTrafficOffencesJosn.getString("success");
				if(StringUtils.equals("true", trafficOffencessuccess)){
					third_json.put("offencesData",queryTrafficOffencesJosn.getJSONObject("data"));
				}else{
					third_json.put("offencesData", new JSONObject());
				}
			} catch (Exception e) {
				third_json.put("offencesData","");
			}
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("name", cust_name);// 姓名 必填
			paramMap.put("id_number", idcard);// 身份证 必填
			paramMap.put("mobile", cust_phone);// 手机号
			String reportId = tongDunClinet.queryReportNo(paramMap,false);
			Thread.sleep(1000);
			JSONObject tondunJson = tongDunClinet.queryReport(reportId,false);
			if(tondunJson.getBooleanValue("success")){
				third_json.put("tongDunData", tondunJson);
			}
		} catch (Exception e) {
			third_json.put("error", "查询失败");
		}
		return third_json;
	}
	
	/**
	 * 获取收入报告
	 * @param loanRow
	 * @return
	 * @throws Exception
	 */
	private JSONObject getIncomeReport(Row loanRow) throws Exception{
		String cust_name = loanRow.getString("loan_cust.cust_name");
		String idcard = loanRow.getString("loan_cust.idcard");
		String cust_phone = loanRow.getString("loan_cust.cust_phone");
		String bank_accout = loanRow.getString("loan_info_et.bank_accout");
		String bank_accout_often1 = erpUtil.objectIsNullGetString(loanRow.get("loan_info_et.bank_accout_often1"));
		String bank_accout_often2 = erpUtil.objectIsNullGetString(loanRow.get("loan_info_et.bank_accout_often2"));
		JSONObject income_json = new JSONObject();
		List<Map<Object, Object>> cards = new ArrayList<>();
		Map<Object, Object> resultMap = new HashMap<>();
		resultMap.put("name", cust_name);
		resultMap.put("idcard", idcard);
		resultMap.put("mobile", cust_phone);
		List<Map<Object, Object>> feeList = new ArrayList<>();
		Map<Object, Object> feeMap = new HashMap<>();
		tongDunMessage(resultMap, feeMap, cust_name, idcard, cust_phone);
		List<String> cardList = new ArrayList<>();
		cardList.add(bank_accout);
		if(StringUtils.isNotEmpty(bank_accout_often1)){
			cardList.add(bank_accout_often1);
		}
		if(StringUtils.isNotEmpty(bank_accout_often2)){
			cardList.add(bank_accout_often2);
		}
		for(String card:cardList){
			Map<Object, Object> cardsMap = new HashMap<>();
			if(StringUtils.equals(card, "")){
				continue;
			}
			cardsMap.put("card", card);
			Map<Object, Object> cardMessage = new HashMap<>();
			cardMessage = aceBankCard(cardMessage,feeMap,cardsMap,cust_name, card);
			if(cardMessage.size()==0){
				cardsMap.put("aceError", "该卡未查得大数据信息，不能计算还贷能力");
			}
			cardMessage = jiaoBankCard(cardMessage,cardsMap,cust_name, idcard, cust_phone, card);
			cardsMap.put("cardMessage", cardMessage);
			cards.add(cardsMap);
			feeList.add(feeMap);
		}
		double repayLoanMoney = repayLoan(feeList);

		income_json.put("name", cust_name);
		income_json.put("idcard", idcard);
		income_json.put("mobile", cust_phone);
		income_json.put("repayLoanMoney", repayLoanMoney);
		income_json.put("cards", cards);
		
		return income_json;
	}
	
	/**
	 * 获取多头数据
	 * @param resultMap
	 * @param name
	 * @param idcard
	 * @param mobile
	 * @return
	 * @throws Exception
	 */
	private Map<Object, Object> tongDunMessage(Map<Object, Object> resultMap,Map<Object, Object> feeMap,String name,String idcard,String mobile)throws Exception{
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("name", name);// 姓名 必填
		paramMap.put("id_number", idcard);// 身份证 必填
		paramMap.put("mobile",mobile);// 手机号
		String reportId =tongDunClinet.queryReportNo(paramMap,false);
		Thread.sleep(2000);
		JSONObject jsonObject =tongDunClinet.queryReport(reportId,false);
		List<Object> list = new ArrayList<>();
		if(jsonObject.getBooleanValue("success")){
			JSONArray risk_items = jsonObject.getJSONArray("risk_items");
			for(Object o:risk_items){
				JSONObject risk_item = (JSONObject)o;
				String group = risk_item.getString("group");
				if(!StringUtils.equals("多平台借贷申请检测", group)){
					continue;
				}
				String item_name = risk_item.getString("item_name");
				if(StringUtils.equals("1个月内申请人在多个平台申请借款", item_name)){
					JSONObject item_detail = risk_item.getJSONObject("item_detail");
					String platform_count = item_detail.getString("platform_count");
					feeMap.put("duotouCount", platform_count);
				}
				list.add(risk_item);
			}
			resultMap.put("tongdun", list);
			return resultMap;
		}
		throw new Exception(jsonObject.getString("errorDesc"));
	}
	
	/**
	 * 获取集奥银行卡信息
	 * @param cardMessage
	 * @param name
	 * @param idcard
	 * @param mobile
	 * @param card
	 * @return
	 * @throws Exception
	 */
	private Map<Object,Object> jiaoBankCard(Map<Object, Object> cardMessage,Map<Object, Object> cardsMap,String name,String idcard,String mobile,String card) throws Exception{
		String jiaoString = jiAoClinet.queryBankCardCredit(name, idcard, mobile,card);
		if(jiaoString!=null){
			JSONObject jiaoJson = JSONObject.parseObject(jiaoString);
			JSONArray data = jiaoJson.getJSONArray("data");
			JSONObject d = (JSONObject)data.get(0);
			Map<String, String> jiaodictionMap = queryJiaoDictionaries();
			for (Map.Entry<String, Object> entry : d.entrySet()) {
				if(StringUtils.equals(null, jiaodictionMap.get(entry.getKey()))){
					continue;
				}
				if(StringUtils.equals(entry.getKey(), "CDCA005")){
					cardsMap.put("openCard", entry.getValue());
				}
				cardMessage.put(jiaodictionMap.get(entry.getKey()), entry.getValue());
			}
		}
		return cardMessage;
	}
	
	/**
	 * 获取优分银行卡信息
	 * @param cardMessage
	 * @param name
	 * @param card
	 * @return
	 * @throws Exception
	 */
	private Map<Object,Object> aceBankCard(Map<Object, Object> cardMessage,Map<Object, Object> feeMap,Map<Object, Object> cardsMap,String name,String card) throws Exception{
		JSONObject aceJson = acedataClinet.queryOpenBankCard(name, card, false);
		String resCode = aceJson.getString("resCode");
		if(StringUtils.equals(resCode, "0000")){
			JSONObject aceData = aceJson.getJSONObject("data");
			String statusCode = aceData.getString("statusCode");
			if(StringUtils.equals(statusCode, "2012")){
				JSONObject aceResult = aceData.getJSONObject("result");
				computeRepayLoan(aceResult,feeMap);
				initBaseData(aceResult, cardsMap);
				Map<String, String> acedictionMap = queryAceDictionaries();
				for (Map.Entry<String, Object> entry : aceResult.entrySet()) {
					if(StringUtils.equals(null, acedictionMap.get(entry.getKey()))){
						continue;
					}
					cardMessage.put(acedictionMap.get(entry.getKey()), entry.getValue());
				}
			}
		}
		return cardMessage;
	}
	
	/**
	 * 初始化基本数据
	 * @param aceResult
	 * @throws Exception
	 */
	private void initBaseData(JSONObject aceResult,Map<Object, Object> cardsMap) throws Exception{
		String bp1001 = erpUtil.objectIsNullGetString(aceResult.get("BP1001"));
		cardsMap.put("卡种", bp1001);
		String bp1002 =  erpUtil.objectIsNullGetString(aceResult.get("BP1002"));
		cardsMap.put("卡性质", bp1002);
		String bp1003 =  erpUtil.objectIsNullGetString(aceResult.get("BP1003"));
		cardsMap.put("卡品牌", bp1003);
	}
	
	/**
	 * 获取每张银行卡还贷能力
	 * @throws Exception
	 */
	private void computeRepayLoan(JSONObject aceResult,Map<Object, Object> feeMap) throws Exception{
		double proportion = getDuotouProportion(feeMap);
		double rule = 1;
		double cp6014 = erpUtil.objectIsNullGetDouble(aceResult.get("CP6014"));//月还贷能力评估
		feeMap.put("hdzb", cp6014);
		double yjxf;
		double cp5001 = erpUtil.objectIsNullGetDouble(aceResult.get("CP5001"));//该卡近12月的交易总金额
		double cp5005 = erpUtil.objectIsNullGetDouble(aceResult.get("CP5005"));//近12月该卡有交易的月数
		if(cp5005==0){
			cp5005 = 12;
		}
		yjxf = BigDecimalUtil.mul(BigDecimalUtil.mul(BigDecimalUtil.div(cp5001, cp5005), proportion), rule);
		feeMap.put("yjxf", yjxf);
	}
	
	/**
	 * 获取总还贷能力
	 * @param feeList
	 * @throws Exception
	 */
	private double repayLoan(List<Map<Object, Object>> feeList) throws Exception{
		double repayLoanMoney = 0.0;
		if(feeList.size()>0){
			for(Map<Object, Object> map:feeList){
				double hdzb = erpUtil.objectIsNullGetDouble(map.get("hdzb"));
				repayLoanMoney = BigDecimalUtil.add(repayLoanMoney, hdzb);
			}
			repayLoanMoney = BigDecimalUtil.div(BigDecimalUtil.div(repayLoanMoney, feeList.size()), 2);
			for(Map<Object, Object> map:feeList){
				double yjxf = erpUtil.objectIsNullGetDouble(map.get("yjxf"));
				repayLoanMoney = BigDecimalUtil.add(repayLoanMoney, BigDecimalUtil.div(yjxf, 2));
			}
		}
		return BigDecimalUtil.round(repayLoanMoney, 2);
	}
	
	/**
	 * 通过多头次数返回占比
	 * @param feeMap
	 * @return
	 */
	private double getDuotouProportion(Map<Object, Object> feeMap) throws Exception{
		int duotouCount = erpUtil.objectIsNullGetInteger(feeMap.get("duotouCount"));
		double proportion = 1.0;
		if(duotouCount>=1&&duotouCount<=4){
			proportion = 0.9;
		}else if(duotouCount>=5&&duotouCount<=9){
			proportion = 0.7;
		}else if(duotouCount>=10){
			proportion = 0.5;
		}
		return proportion;
	}
	
	/**
	 * 获取集奥数据字典Z9
	 * @return
	 * @throws Exception
	 */
	private Map<String,String> queryJiaoDictionaries() throws Exception{
		Map<String,String> map= new HashMap<String, String>();
		String sql = "select no,dictionaries_name from dictionaries_jiao";
		Store store = baseDao.query(sql);
		for(Row row:store){
			map.put(row.getString("no"), row.getString("dictionaries_name"));
		}
		return map;
	}
	
	/**
	 * 获取消费画像数据字典
	 * @return
	 * @throws Exception
	 */
	private Map<String,String> queryAceDictionaries() throws Exception{
		Map<String,String> map= new HashMap<String, String>();
		String sql = "select no,dictionaries_name from dictionaries_ace";
		Store store = baseDao.query(sql);
		for(Row row:store){
			map.put(row.getString("no"), row.getString("dictionaries_name"));
		}
		return map;
	}
	
	/**
	 * 获取出险报告
	 * @param loanRow
	 * @return
	 * @throws Exception
	 */
	private JSONObject getDangerReport(Row loanRow) throws Exception{
		String loan_no = loanRow.getString("loan_info.loan_no");
		String vin = loanRow.getString("loan_car.cjh");
		JSONObject jsonObject;
		try {
			String bodyContent = jingZhenGuClinet.queryCarDanger(vin);
			jsonObject = JSONObject.parseObject(bodyContent);
		} catch (Exception e) {
			jsonObject = new JSONObject();
			jsonObject.put("error", "查询失败");
		}
		return jsonObject;
		
	}
	
	/**
	 * 获取车况评估报告
	 * @param loanRow
	 * @return
	 * @throws Exception
	 */
	private JSONObject getCarassReport(Row loanRow) throws Exception{
		String loan_no = loanRow.getString("loan_info.loan_no");
		String sql="select ifnull(b.rule_id,'') as rule_id,ifnull(b.ass_dimension_cn,'') as dimension_cn,a.condition_remark,a.param,a.result as score,a.oper_time " 
				+  "from loan_ass_auto a "
				+  "left join loan_ass_rule b on a.dimension=b.ass_dimension and a.condition_exp=b.condition_exp " 
				+  "where a.loan_no='"+loan_no+"' and a.ass_type='01' ";
		Store store=baseDao.query(sql);
		
		sql="select ass_score_branch from loan_ass where loan_no='"+loan_no+"'";
		String result_score=erpUtil.objectIsNullGetString(baseDao.queryObject(sql));
		String report_name="车况评估报告";
		try{
			String oper_time= Util.format(new Date());
			if(store.size()>0){
				oper_time=store.get(0).getString("oper_time");
			}
			String report_id=erpUtil.objectIsNullGetString(loanRow.get("loan_car.cbs_report_id"));
			
			if(Util.isNotEmpty(report_id)){
				JSONObject resultJson=chaboshiClinet.getNewReportJson(report_id);
				Store cbs=baseDao.query("select no,dictionaries_name as dimension_cn from dictionaries_cbs where status='1'");
				
				int index=0;
				for(Row item:cbs){
					if(StringUtils.endsWith(item.getString("no"), "Records")){
						JSONArray arr=resultJson.getJSONArray(item.getString("no"));
						if(arr!=null){
							for(int i=0;i<arr.size();i++){
								JSONObject j_item=(JSONObject) arr.get(i);
								Row itemRow=new Row();
								itemRow.put("rule_id","cbs"+String.format("%03d", index));
								itemRow.put("dimension_cn",item.get("dimension_cn")+"-"+erpUtil.objectIsNullGetString(j_item.getString("type")));
								itemRow.put("condition_remark",erpUtil.objectIsNullGetString(j_item.getString("content"))+erpUtil.objectIsNullGetString(j_item.getString("materal")));
								itemRow.put("param",j_item.toJSONString());
								itemRow.put("score","0");
								itemRow.put("oper_time",oper_time);
								store.add(itemRow);
								index++;
							}
						}
					}else{
						String condition_remark=resultJson.getString(item.getString("no"));
						if(Util.isEmpty(condition_remark)){
							condition_remark="无";
						}
						item.put("rule_id","cbs"+String.format("%03d", index));
						item.put("condition_remark",condition_remark);
						item.put("param",resultJson.getString(item.getString("no")));
						item.put("score","0");
						item.put("oper_time",oper_time);
						store.add(item);
						index++;
					}
					
				}
				
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		JSONObject carass_json = new JSONObject();
		carass_json.put("report_name", report_name);
		carass_json.put("report_score", result_score);
		JSONObject data = new JSONObject();
		for(Row row:store){
			data.put(row.getString("dimension_cn"), row.getString("condition_remark"));
		}
		carass_json.put("data", data);
		return carass_json;
	}
	
	/**
	 * 车况终价报告
	 * @param loanRow
	 * @return
	 * @throws Exception
	 */
	private JSONObject getcarconReport(Row loanRow) throws Exception{
		String loan_no = loanRow.getString("loan_info.loan_no");
		
		String sql="select ifnull(b.rule_id,'') as rule_id,ifnull(b.ass_dimension_cn,'') as dimension_cn,a.condition_remark,a.param,a.result as score,a.oper_time " 
				+  "from loan_ass_auto a "
				+  "left join loan_ass_rule b on a.dimension=b.ass_dimension and a.condition_exp=b.condition_exp " 
				+  "where a.loan_no='"+loan_no+"' and a.ass_type='06' ";
		Store store=baseDao.query(sql);
		
		String report_name="车况终价报告(真实)";
		double result_double=1;
		sql="select result from loan_ass_auto where loan_no='"+loan_no+"' and ass_type='06'";
		Store results=baseDao.query(sql);
		for(Row row:results){
			result_double*=(1+erpUtil.objectIsNullGetDouble(row.get("result")));
		}
		String result_score=String.valueOf(BigDecimalUtil.round(result_double-1,4));
		JSONObject carcon_json = new JSONObject();
		carcon_json.put("report_name", report_name);
		carcon_json.put("report_score", result_score);
		JSONObject data = new JSONObject();
		for(Row row:store){
			data.put(row.getString("dimension_cn"), row.getString("condition_remark"));
		}
		carcon_json.put("data", data);
		return carcon_json;
	}
	
	/**
	 * 查询借款信息接口
	 * @return
	 * @throws Exception
	 */
	public JSONObject queryLoanInformation(Row loanRow) throws Exception{
		String url = IPCODE+"/p2passet/fanglbb/loanAction/queryLoanInformation";
		JSONObject jsonObject = new JSONObject();//业务数据JSON
		jsonObject.put("requestNo", loanRow.getString("loan_info.requestNo"));//资产资产订单流水号(唯一标识)
		jsonObject.put("platformNo", platformNo);//商户编号，由集金分配
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("partnerCode", partnerCode);
		paramMap.put("reqData", jsonObject.toString());//业务数据报文，JSON格式
		paramMap.put("sign", getSign(jsonObject));//对reqData参数的签名，加密
		return getData(url, paramMap);
	}
	
	/**
	 * 充值H5 URL
	 * @param loanRow
	 * @throws Exception
	 */
	public String czUrl(Row loanRow) throws Exception{
		String url = IPCODE+"/p2passet/index.html#/login?"
				+ "relateUserId="+loanRow.getString("loan_info.loan_no")
				+"&partnerCode="+partnerCode
				+"&requestNo="+loanRow.getString("loan_info.requestNo")
				+"&partnerPlatformNo="+platformNo
				+"&originType=recharge";
		return url;
	}
	
	/**
	 * 查询用户账户余额
	 * @param idcard 身份证号
	 * @param mobile 手机号（账号）
	 * @return
	 * @throws Exception
	 */
	public JSONObject queryUserAccount(String idcard,String mobile) throws Exception{
		String url = IPCODE+"/p2passet/fanglbb/loanAction/queryUserAccount";
		JSONObject jsonObject = new JSONObject();//业务数据JSON
		jsonObject.put("idcard", idcard);
		jsonObject.put("mobile", mobile);
		jsonObject.put("platformNo", platformNo);//商户编号，由集金分配
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("partnerCode", partnerCode);
		paramMap.put("reqData", jsonObject.toString());//业务数据报文，JSON格式
		paramMap.put("sign", getSign(jsonObject));//对reqData参数的签名，加密
		return getData(url, paramMap);
	}
	
	/**
	 * 借款作废接口
	 * @return
	 * @throws Exception
	 */
	public JSONObject cancelOrder(String requestNo) throws Exception{
		String url = IPCODE+"/p2passet/fanglbb/loanAction/cancelOrder";
		JSONObject jsonObject = new JSONObject();//业务数据JSON
		jsonObject.put("requestNo", requestNo);
		jsonObject.put("platformNo", platformNo);//商户编号，由集金分配
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("partnerCode", partnerCode);
		paramMap.put("reqData", jsonObject.toString());//业务数据报文，JSON格式
		paramMap.put("sign", getSign(jsonObject));//对reqData参数的签名，加密
		return getData(url, paramMap);
	}
	
	/**
	 * 获取文件路径
	 * 
	 * @param no
	 * @return
	 * @throws SQLException
	 */
	private String getFilePath(String no) throws SQLException {
		String sql = "SELECT file_path from i_frame_sys_attachment where id='" + no + "'";
		String filePath = (String) baseDao.queryObject(sql);
		if (StringUtils.isBlank(filePath)) {
			return null;
		}
		return Const.SYS_FILE_UPLOAD_URL_PATH + "/" + filePath;
	}
	
	/**
	 * 获取文件全称
	 * 
	 * @param filePath
	 * @return
	 */
	private static String getFileNameAndFileSuffix(String filePath) {
		if (StringUtils.isBlank(filePath)) {
			return null;
		}
		return filePath.substring(filePath.lastIndexOf("/") + 1, filePath.length());
	}
	
	/**
	 * 获取文件名称
	 * 
	 * @param fileNameSuffix
	 * @return
	 */
	private static String getFileName(String fileNameSuffix) {
		if (StringUtils.isBlank(fileNameSuffix)) {
			return null;
		}
		String[] ss = fileNameSuffix.split("\\.");
		String fileName = ss[0];
		return fileName;
	}
	
	/**
	 * 获取文件后缀
	 * 
	 * @param fileNameSuffix
	 * @return
	 */
	private static String getFileSuffix(String fileNameSuffix) {
		if (StringUtils.isBlank(fileNameSuffix)) {
			return null;
		}
		String[] ss = fileNameSuffix.split("\\.");
		String fileSuffix = "."+ss[1];
		return fileSuffix;
	}
	
	/**
     * 获取文件的MD5值
     * 
     * @param file
     *            目标文件
     * @return MD5字符串
     */
    public String getFileMD5String(File file) {
        String ret = "";
        FileInputStream in = null;
        FileChannel ch = null;

        try {
            in = new FileInputStream(file);
            ch = in.getChannel();
            ByteBuffer byteBuffer = ch.map(FileChannel.MapMode.READ_ONLY, 0, file.length());
            messageDigest.update(byteBuffer);
            ret = bytesToHex(messageDigest.digest());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(ch);
            IOUtils.closeQuietly(in);
        }

        return ret;
    }
	
    /**
     * 
     * 将字节数组转换成16进制字符串
     * 
     * @param bytes
     *            目标字节数组
     * 
     * @return 转换结果
     * 
     */
    public String bytesToHex(byte bytes[]) {
        return bytesToHex(bytes, 0, bytes.length);
    }
    
    /**
     * 
     * 将字节数组中指定区间的子数组转换成16进制字符串
     * 
     * @param bytes
     *            目标字节数组
     * 
     * @param start
     *            起始位置（包括该位置）
     * 
     * @param end
     *            结束位置（不包括该位置）
     * @return 转换结果
     * 
     */
    public String bytesToHex(byte bytes[], int start, int end) {
        StringBuilder sb = new StringBuilder();
        
        for (int i = start; i < start + end; i++) {
            sb.append(byteToHex(bytes[i]));
        }

        return sb.toString();
    }
    
    /**
     * 将单个字节码转换成16进制字符串
     * @param bt
     *            目标字节
     * @return 转换结果
     */
    public String byteToHex(byte bt) {
        return HEX_DIGITS[(bt & 0xf0) >> 4] + "" + HEX_DIGITS[bt & 0xf];
    }
    
    /**
     * reqData返回签名信息
     * @param jsonObject reqData参数
     * @return
     * @throws Exception
     */
    private String getSign(JSONObject jsonObject) throws Exception {
    	PrivateKey privateKey = SignatureUtils.getRsaPkcs8PrivateKey(Base64
				.decodeBase64(privateStr));
		byte[] sign = SignatureUtils.sign(SignatureAlgorithm.SHA1WithRSA,
				privateKey, jsonObject.toString());
		
		return Base64.encodeBase64String(sign);
    }
    
	/**
	 * 公共调用接口返回JSON
	 * @param url
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	private JSONObject getData(String url, Map<String, Object> paramMap)throws Exception {
		JSONObject params = new JSONObject();
		if (paramMap != null && paramMap.size() > 0) {
			for (Entry<String, Object> entry : paramMap.entrySet()) {
				Object obj = entry.getValue();
				if (obj != null) {
					params.put(entry.getKey(), obj);
				}
			}
		}
		CloseableHttpClient httpClient = null;
		CloseableHttpResponse httpResponse = null;
		try{
			httpClient = HttpClientBuilder.create().build();
			RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(200000).setSocketTimeout(200000000).build();
			HttpPost httpPost = new HttpPost(url);
			httpPost.setConfig(requestConfig);
			String param = JSONObject.toJSONString(params);
			StringEntity se = new StringEntity(param, "utf-8");
			se.setContentType("application/json");
			httpPost.setEntity(se);
			httpResponse = httpClient.execute(httpPost);
			// 发送Post,并返回一个HttpResponse对象
			if (httpResponse.getStatusLine().getStatusCode() == 200) {// 如果状态码为200,就是正常返回
				String result = EntityUtils.toString(httpResponse.getEntity());
				JSONObject jsonObject = JSONObject.parseObject(result);
				String code = jsonObject.getString("code");
				if(StringUtils.equals("200", code)){
					return jsonObject;
				}
//				log.info("调用接口返回code异常:jsonObject=" + jsonObject);
				throw new Exception("调用接口返回code异常:jsonObject=" + jsonObject);
			}
//			log.info("调用接口异常:url=" + url + ",param=" + paramMap);
			throw new Exception("调用接口异常,返回状态:" + httpResponse.getStatusLine().getStatusCode());
		}finally {
			if(httpClient !=null){
				httpClient.close();
			}
			if (httpResponse != null) {
				httpResponse.close();
			}
		}
	}

	/**
	 * 文件上传
	 * @param url
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	private JSONObject getFileData(String url, Map<String, Object> paramMap,String filePath)throws Exception {
		JSONObject params = new JSONObject();
		if (paramMap != null && paramMap.size() > 0) {
			for (Entry<String, Object> entry : paramMap.entrySet()) {
				Object obj = entry.getValue();
				if (obj != null) {
					params.put(entry.getKey(), obj);
				}
			}
		}
		CloseableHttpClient httpClient = null;
		CloseableHttpResponse httpResponse = null;
		try{
			httpClient = HttpClientBuilder.create().build();
			RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(200000).setSocketTimeout(200000000).build();
			HttpPost httpPost = new HttpPost(url);
			httpPost.setConfig(requestConfig);
			MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create();

			File file = new File(filePath);
			multipartEntityBuilder.addTextBody("obj",params.toJSONString());
			multipartEntityBuilder.addBinaryBody("file", file);
			HttpEntity httpEntity = multipartEntityBuilder.build();
			httpPost.setEntity(httpEntity);

			httpResponse = httpClient.execute(httpPost);

			if (200 == httpResponse.getStatusLine().getStatusCode()) {
				String result = EntityUtils.toString(httpResponse.getEntity());
				JSONObject jsonObject = JSONObject.parseObject(result);
				String code = jsonObject.getString("code");
				if(StringUtils.equals("200", code)){
					return jsonObject;
				}
				log.info("调用接口返回code异常:jsonObject=" + jsonObject);
				throw new Exception("调用接口返回code异常:jsonObject=" + jsonObject);
			}
			log.info("调用接口异常:url=" + url + ",param=" + paramMap);
			throw new Exception("调用接口异常,返回状态:" + httpResponse.getStatusLine().getStatusCode());
		}finally {
			if(httpClient !=null){
				httpClient.close();
			}
			if (httpResponse != null) {
				httpResponse.close();
			}
		}
	}
}
