package com.rxcd.autofinace.formula;

import org.apache.commons.lang.StringUtils;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.util.ErpUtil;
import org.appframe.wf.util.Param;
import org.appframe.wf.util.SafeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.SQLException;

@Component
public class MidRelyonRule {

    @Autowired
    private BaseDao baseDao;

    public boolean defaultFalse(String review_no, Param p) {
        return false;
    }


    /**
     * 二次付款是否过审核
     *
     * @param review_no
     * @param transaction
     * @param p
     * @return
     * @throws Exception
     */
    public boolean ecfksh(String review_no, Param p) throws Exception {
        boolean flag = false;
        String sql = "select is_pay from loan_mid_pay where mid_no="
                + "(select table_key_no from wf_review_biz where review_no='" + review_no + "')";
        String is_pay = ErpUtil.objectIsNullGetString(baseDao.queryObject(sql));
        if (!"YES".equals(is_pay)) {
            flag = true;
        }
        return flag;
    }


    /**
     * 还款流程是否过分总审批节点
     *
     * @param review_no
     * @param transaction
     * @param p
     * @return
     * @throws Exception
     */
    public boolean getRepaymentApprovalRule(String review_no, Param p) throws Exception {
        boolean flag = false;
        String sql = "select is_approval from loan_mid_repayment where mid_no="
                + "(select table_key_no from wf_review_biz where review_no='" + review_no + "')";
        Object obj = baseDao.queryObject(sql);
        String is_approval = obj == null ? "0" : obj.toString();

        if (!"0".equals(is_approval)) {
            flag = true;
        }

        return flag;
    }

    /**
     * 流程节点是否执行判断
     *
     * @param review_no
     * @param transaction
     * @return
     * @throws Exception
     */
    public boolean getRemitNodeRule(String review_no, Param p) throws Exception {
        boolean flag = false;

        // 查询是否有减免
        String sql = "select ifnull(sum(remit_amount), 0) remit_amount "
                + "  from loan_mid_repay_fee "
                + " where status='1' and mid_no=(select table_key_no from wf_review_biz where review_no='" + SafeUtil.safe(review_no) + "' ) ";
        Row feeRow = baseDao.queryRow(sql);
        double remit_amount = 0;

        if (feeRow != null) {
            remit_amount = feeRow.getNumber("remit_amount").doubleValue();
        }

        if (remit_amount > 0) {
            flag = true;
        }

        return flag;
    }

    /**
     * 贷中是否执行二次放款流程
     *
     * @param review_no
     * @param transaction
     * @param p
     * @return
     * @throws Exception
     */
    public boolean getPayNodeRule(String review_no, Param p) throws Exception {
        boolean flag = getIsRenewalRule(review_no, p);
        if (flag) {
            return flag;
        }

        // 查询是否二次放款流程
        String sql = "select cast(count(1) as char) "
                + "from loan_mid_pay t "
                + "join loan_mid_employers a "
                + "on t.mid_no=a.mid_no and a.status='1' "
                + "where t.status='1' "
                + "and a.pay_type='03' "
                + "and t.mid_no=(select table_key_no from wf_review_biz where review_no='" + SafeUtil.safe(review_no) + "' ) ";
        String check = (String) baseDao.queryObject(sql);

        if (!"0".equals(check)) {
            flag = true;
        }

        return flag;
    }


    /**
     * 贷中结清是否执行客户取车流程    追加贷款流程直接通过
     *
     * @param review_no
     * @param transaction
     * @param p
     * @return false:直接通过,true:进入节点
     * @throws Exception
     */
    public boolean getGetCarNodeRule(String review_no, Param p) throws Exception {
        boolean flag = true;
        if (!getIsRenewalRule(review_no, p)) {
            return false;
        }
        // 查询产品类型
        String sql = "select prod_no from loan_info where loan_no=(select loan_no from loan_mid where mid_no=(select table_key_no from wf_review_biz where review_no='" + review_no + "'))";
        String prod_no = baseDao.queryObject(sql).toString();
        //如果不是押车就直接通过取车流程
        if (!("03".equals(prod_no) || "04".equals(prod_no))) {
            flag = false;
        }
        return flag;
    }

    /**
     * 结清是否续期流程或则是追加贷款流程 ，如果是就直接通过此节点
     *
     * @param review_no
     * @param transaction
     * @param p
     * @return false:直接通过,true:进入节点
     * @throws Exception
     */
    public boolean getIsRenewalRule(String review_no, Param p) throws Exception {
        //如果是追加贷款  直接过掉
        if (checkFlowIsAdditionalLoan( review_no)) {
            return false;
        }
        boolean flag = true;
        //查询到是否有续期流程
        String sql = "select count(1) "
                + "from "
                + "loan_mid a "
                + "left join wf_review_biz b on a.mid_no=b.table_key_no "
                + "left join loan_info_et c on c.loan_no=a.loan_no "
                + "left join wf_review_biz d on c.loan_no=d.table_key_no "
                + "left join wf_review e on e.review_no=d.review_no "
                + "where b.review_no='" + SafeUtil.safe(review_no) + "' "
                + "and c.fk_date is not null and c.fk_date!='' "
                + "and c.loan_type in ('03','04') "
                + "and e.review_status='P' ";
        int count = Integer.parseInt(baseDao.queryObject(sql).toString());
        if (count > 1) {//是续期结清流程
            flag = false;
        }
        return flag;
    }

    /**
     * 检查当前流程节点对应的贷款是否是  追加贷款
     *
     * @param transaction
     * @param review_no
     * @return
     * @throws SQLException
     */
    public boolean checkFlowIsAdditionalLoan(String review_no) throws SQLException {
        String sql = "SELECT r.* from wf_review r "
                + "LEFT JOIN wf_review_biz b on b.review_no=r.review_no "
                + "LEFT JOIN loan_mid m on b.table_key_no=m.mid_no "
                + "LEFT JOIN loan_info_et e on m.loan_no=e.loan_no "
                + "where r.review_no='" + review_no + "' AND e.loan_type='05'";
        Row row = baseDao.queryRow(sql);
        //如果可以查询到数据 表示当前是一个 追加贷  直接过掉该流程节点
        if (row != null) {
            return true;
        }
        return false;
    }

    /**
     * 还款流程 是否需要分总审批
     *
     * @param review_no
     * @param transaction
     * @param p
     * @return
     * @throws Exception
     */
    public boolean isApprove(String review_no, Param p) throws Exception {
        boolean flag = false;
        String sql = "select after_status from loan_mid a "
                + "left join wf_review_biz b on b.table_key_no=a.mid_no "
                + "left join loan_info_et c on a.loan_no=c.loan_no "
                + "where b.review_no='" + review_no + "'";
        String after_status = baseDao.queryObject(sql).toString();
        if (!"-1".equals(after_status)) {
//			flag=true;
            return flag;
        }

        return flag;
    }

    /**
     * 退款流程 是否要过财务执行(是否提现)
     *
     * @param review_no
     * @param transaction
     * @param p
     * @return
     * @throws Exception
     */
    public boolean isRefundWithdraw(String review_no, Param p) throws Exception {
        boolean flag = false;
        String sql = "select refund_type from loan_mid_refund where mid_no=(select table_key_no from wf_review_biz where review_no='" + review_no + "')";
        String refund_type = baseDao.queryObject(sql).toString();
        //退款提现
        if ("02".equals(refund_type)) {
            flag = true;
        } else if ("03".equals(refund_type)) {
            flag = true;
        }
        return flag;
    }

    /**
     * 判断是否需要邮寄
     *
     * @param review_no
     * @param transaction
     * @param p
     * @return
     * @throws Exception
     */
    public boolean isNeedMail(String review_no, Param p) throws Exception {
        String sql = "select table_key_no from wf_review_biz where review_no='" + review_no + "'";
        String mid_no = baseDao.queryObject(sql).toString();
        sql = "select * from loan_mid_settle where mid_no = '" + mid_no + "'";
        Row row = baseDao.queryRow(sql);
        String is_need_mail = ErpUtil.objectIsNullGetString(row.get("is_need_mail"));
        if (StringUtils.equals("02", is_need_mail)) {
            return false;
        }
        return true;
    }
}