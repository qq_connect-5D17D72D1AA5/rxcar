package com.rxcd.autofinace.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.appframe.utils.Utils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用来执行数据导入相关操作
 * @author 王钰博
 */
@RestController
@RequestMapping("/data/import")
public class DataImportController {
    private Log log = LogFactory.getLog(DataImportController.class);

    @RequestMapping("/generateUUID")
    public String generateUUID(Integer count){
        StringBuffer result = new StringBuffer();
        if(count == null || count <= 0) return result.toString();

        for(int i = 0; i < count; i++){
            result.append(Utils.getUUID());
            result.append(";");
        }
        return result.toString();
    }
}
