package com.rxcd.autofinace.flowEvent;

import com.fida.clearing.impl.BillClientImpl;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.engine.AbstractWorkFlowEvent;
import org.appframe.wf.engine.common.ErrorCodeEnum;
import org.appframe.wf.service.WfParamUtil;
import org.appframe.wf.util.Param;
import org.appframe.wf.util.SafeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Component
public class RechargeWorkFlowEventImp extends AbstractWorkFlowEvent {

    @Autowired
    private BaseDao baseDao;
    @Autowired
    private BillClientImpl billClientImpl;
    @Autowired
    private WfParamUtil paramUtil;

    @Override
    public void workFlowStart(Param p) throws Exception {

    }

    @Override
    public void workFlowFinish(Param p) throws Exception {
        Row reviewObject = (Row) paramUtil.get("reviewObject");
        String review_no = reviewObject.getString("review_no");
        String table_key_no = (String) baseDao.queryObject("select table_key_no from wf_review_biz where review_no='" + SafeUtil.safe(review_no) + "'");
        String sql;
        sql="select * from loan_mid_recharge left join loan_cust on loan_cust.idcard = loan_mid_recharge.recharge_paper_number " +
                "where loan_mid_recharge.recharge_no='"+table_key_no+"' and loan_mid_recharge.status='1'";
        Row recharge=baseDao.queryRow(sql);
        if (recharge == null) {
            throw new Exception(ErrorCodeEnum.W4003.getCode());
        }
        //判断充值状态是否成功
        if(recharge.getString("recharge_status").equals("01")){
            //充值金额
            BigDecimal amount = (BigDecimal)recharge.get("recharge_payment_amount");
            String feeSubject = "";
            //付款方的账户ID
            String payer = recharge.getString("clearing_ordinary");
            String payerBank = "";
            //付款方的账户卡号
            String payerCardNo = recharge.getString("recharge_payment_bank");
            //收款方银行卡号
            String payee = recharge.getString("recharge_goal_no");
            String payeeBank = "";
            String payeeCardNo = "";
            String busiId = table_key_no;
            String notifyUrl = "";
            Map<String,Object> bill = billClientImpl.createUnderLineRechargeBill(amount.doubleValue(),feeSubject,payer,payerBank,payerCardNo,payee,payeeBank,payeeCardNo,busiId,notifyUrl);
            List<Object> list = (List<Object>)bill.get("rows");
            Map<String, Object> bill_1 = (Map<String, Object>)list.get(0);
            billClientImpl.settleBill((String)bill_1.get("billId"));
        }
    }

    @Override
    public void workFlowDiscard(Param p) throws Exception {
    }
}
