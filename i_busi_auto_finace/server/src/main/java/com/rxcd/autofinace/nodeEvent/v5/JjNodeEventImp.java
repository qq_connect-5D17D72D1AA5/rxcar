/**
 * 版权所有：版权所有：重庆易房网络科技股份有限公司
 * Chongqing YiFang Network Technology Co. LTD.  All Rights Reserved
 * 创建者：HeSenLin   创建时期：  2016年7月14日
 */
package com.rxcd.autofinace.nodeEvent.v5;


import com.rxcd.autofinace.service.LoanCheckServiceImpl;
import com.rxcd.autofinace.service.LoanService;
import com.rxcd.autofinace.util.RxErpUtil;
import com.rxcd.autofinace.util.StringUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.appframe.wf.db.Row;
import org.appframe.wf.engine.AbstractWorkFlowNodeEvent;
import org.appframe.wf.engine.common.FormUtil;
import org.appframe.wf.engine.common.ReviewUtil;
import org.appframe.wf.form.WorkView;
import org.appframe.wf.service.WfParamUtil;
import org.appframe.wf.util.Param;
import org.appframe.wf.util.SafeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 进件节点事件
 */
@Component
public class JjNodeEventImp extends AbstractWorkFlowNodeEvent {

    private final static Log log = LogFactory.getLog(JjNodeEventImp.class);

    @Autowired
    private WfParamUtil paramUtil;
    @Autowired
    private LoanCheckServiceImpl checkService;
    @Autowired
    private RxErpUtil erpUtil;
    @Autowired
    private ReviewUtil reviewUtil;
    @Autowired
    private LoanService loanService;
    @Autowired
    private FormUtil formUtil;
    @Autowired
    private WorkView defaultWorkView;


    /**
     * 流程节点启动时调用
     *
     * @param flow_node_no
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowNodeStart(String flow_node_no, Param p) throws Exception {

    }

    @Override
    public void workFlowNodeFinish(String flow_node_no, Param p) throws Exception {
        checkTempletFlowNo("1");
        String biz_msg = "";
        String loanNo = SafeUtil.safe(getTableKeyNo());
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loanNo);
        String cust_phone = SafeUtil.safe(StringUtil.replaceBlank(erpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_phone"))));
        String idcard = SafeUtil.safe(StringUtil.replaceBlank(erpUtil.objectIsNullGetString(loanRow.get("loan_cust.idcard"))));
        String car_plate = SafeUtil.safe(StringUtil.replaceBlank(erpUtil.objectIsNullGetString(loanRow.get("loan_car.car_plate"))));
        String cust_name = SafeUtil.safe(StringUtil.replaceBlank(erpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_name"))));
        checkService.check_idcard(idcard);
        checkService.check_cust_phone(cust_phone);
        List<String> init_sqls = new ArrayList<String>();
        init_sqls.add("update loan_cust set idcard='"+idcard+"',cust_phone='"+cust_phone+"',cust_name='"+cust_name+"' where loan_no='" + loanNo + "'");
        init_sqls.add("update loan_car set car_plate='"+car_plate+"', wxgps='2',yxgps='1' where loan_no='" + loanNo + "'");
        init_sqls.add("update loan_info set selectable_prod = '01',prod_version = '01' where loan_no = '"+loanNo+"'");
        init_sqls.add("update loan_info_et set rep_type = '01' where loan_no = '"+loanNo+"'");
        init_sqls.add("update loan_cost set duetime = '24' where loan_no = '"+loanNo+"'");
        baseDao.execute(init_sqls);
        erpUtil.updateClearType(loanNo);
        checkService.check_carPlate(car_plate);
        checkService.check_unclear_car_plate(car_plate);
        checkService.check_otherUnfinishedFk(loanNo);
        checkService.check_otherNewFk(loanNo);
        checkService.check_lockBranch(loanNo);
        checkService.checkValidatedIdcard(loanNo, idcard, cust_name);
        loanService.updateLoanType(loanNo);
        biz_msg = loanService.queryCarRegisterInfoFirst(loanNo);
        checkService.check_car_syr(loanNo);
        Row pageObject = (Row) paramUtil.get("pageObject");
        String form_no = pageObject.getString("form_no");
        String view_no = pageObject.getString("view_no");
        String biz_no = paramUtil.getString("biz_no");
        Row fieldRow = formUtil.getField(view_no, form_no, "loan_info.prod_no", null);
        formUtil.calculateFieldsRule(biz_no, form_no, fieldRow, loanRow, this.workForm, p, null);
        p.getParam().put("biz_msg",biz_msg);
    }

    /**
     * 流程节点驳回时回调方法
     *
     * @param flow_node_no
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowNodeOverrule(String flow_node_no, Param p) throws Exception {

    }

    @Override
    public void workFlowNodeVie(String flow_node_no, Param p) throws Exception {

    }


}
