package com.rxcd.autofinace.service;


import com.alibaba.fastjson.JSONObject;
import com.fida.clearing.impl.ChannelClientImpl;
import com.fida.http.HttpResult;
import com.fida.http.HttpService;
import com.rxcd.autofinace.common.BizErrorCodeEnum;
import com.rxcd.autofinace.util.RxErpUtil;
import com.rxcd.autofinace.util.StringUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.appframe.bigdata.ShuJuMoHeClinet2;
import org.appframe.bigdata.TianXingClinet2;
import org.appframe.common.BigDecimalUtil;
import org.appframe.contract.ContractClient;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.db.Store;
import org.appframe.wf.util.SafeUtil;
import org.appframe.wf.util.Util;
import org.appframe.wf.util.ValidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class LoanCheckServiceImpl implements LoanCheckService {

    private final static Log log = LogFactory.getLog(CostService.class);
    @Value("runtime")
    private String runtime;
    @Autowired
    private RxErpUtil erpUtil;
    @Autowired
    private BaseDao baseDao;
    @Autowired
    private TianXingClinet2 tianXingClinet;
    @Autowired
    private LoanService loanService;
    @Autowired
    private ShuJuMoHeClinet2 shuJuMoHeClinet;
    @Autowired
    private ContractClient contractClient;
    @Autowired
    private ChannelClientImpl channelClientImpl;
    @Autowired
    private HttpService httpService;

    /**
     * 验证身份证
     * @param idcard
     */
    public void check_idcard(String idcard)throws Exception {
        if( !Pattern.matches("^\\d{17}(\\d{1}|[X|x])$", idcard)){
            throw new Exception(BizErrorCodeEnum.T1100.getCode());
        }
    }

    /**
     * 验证电话号码
     * @param cust_phone
     */
    public void check_cust_phone(String cust_phone)throws Exception {
        if( !Pattern.matches("^\\d{11}$", cust_phone)){
            throw new Exception(BizErrorCodeEnum.T1101.getCode());
        }
    }

    /**
     * 验证车架号
     * @param cjh
     */
    public void check_cjh(String cjh)throws Exception {
        if(cjh.length() != 17){
            throw new Exception(BizErrorCodeEnum.T1102.getCode());
        }
    }


    /**
     * 检查合同签订
     * @param loan_no
     * @throws Exception
     */
    public void check_sign_successful(String loan_no) throws Exception{
        //修改签约验证
        if(!contractClient.isSignSuccessful(loan_no)){
            throw new Exception(BizErrorCodeEnum.T1024.getCode());
        }
    }

    /**
     * 检查放款上限
     */
    public void checkFkMax(String loan_no) throws Exception {
        Row contract_sum_max_row = baseDao.queryRow("select * from application_control where application_control.application_key = 'CONTRACT_SUM_MAX'");
        double contract_sum_max_valve = erpUtil.objectIsNullGetDouble(contract_sum_max_row.get("control_valve"));
        Row prepay_sum_max_row = baseDao.queryRow("select * from application_control where application_control.application_key = 'PREPAY_SUM_MAX'");
        double prepay_sum_max_valve = erpUtil.objectIsNullGetDouble(prepay_sum_max_row.get("control_valve"));
        Row loan_row = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String prod_no = erpUtil.objectIsNullGetString(loan_row.get("loan_info.prod_no"));
        double contract_sum = erpUtil.objectIsNullGetDouble(loan_row.get("loan_cost.contract_sum"));
        double prepay_sum = erpUtil.objectIsNullGetDouble(loan_row.get("loan_cost.prepay_sum"));
        if (contract_sum <= 0 || contract_sum > contract_sum_max_valve) {
            throw new Exception(BizErrorCodeEnum.T1050.getCode());
        }
        if (prepay_sum < 0 || prepay_sum > prepay_sum_max_valve) {
            throw new Exception(BizErrorCodeEnum.T1050.getCode());
        }
    }

    /**
     * 检查分行逾期率、临时担保额度
     *
     * @param loan_no
     * @return
     * @throws Exception
     */
    public void checkLoanQuota(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String fk_date = erpUtil.objectIsNullGetString(loanRow.get("loan_info_et.fk_date"));
        if (Util.isNotEmpty(fk_date)) {
            return;
        }
        double final_price = loanService.getFinalAssPrice(loan_no);
        double temp_sum = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.temp_sum"));
        if(temp_sum < 0){
            throw new Exception(BizErrorCodeEnum.F0044.getCode());
        }
        if(temp_sum >= final_price * 0.1 ) {
            throw new Exception(BizErrorCodeEnum.F0030.getCode());
        }
    }

    /**
     * 分行放款检查(新增、再贷、追加贷)
     *
     * @throws Exception
     */
    public void check_branch_fk(String loan_no) throws Exception {
        Row loan_row = erpUtil.getLoanBasicDataInfo(loan_no);
        String sql = "select * from application_control where application_control.control_switch = 1 and application_control.application_key = '" + loan_row.getString("sell_branch_no") + "'";
        Row row = baseDao.queryRow(sql);
        if (null == row) {
            throw new Exception(BizErrorCodeEnum.B0087.getCode());
        }
    }

    /**
     * 验证车辆所有人
     *
     * @param loan_no
     * @throws Exception
     */
    public void check_car_syr(String loan_no) throws Exception {
        Row check_car_syr_row = baseDao.queryRow("select * from application_control where application_control.application_key = 'CHECK_CAR_SYR'");
        if (StringUtils.equals(check_car_syr_row.getString("control_switch"), "1")) {
            Row loan_row = erpUtil.getLoanBasicDataInfo(loan_no);
            if (!StringUtils.equals(loan_row.getString("cust_name"), loan_row.getString("car_syr"))) {
                throw new Exception(BizErrorCodeEnum.T1053.getCode());
            }
        }
    }

    /**
     * 验证身份证
     *
     * @param loanNo
     * @param idcard
     * @param custName
     * @throws Exception
     */
    public void checkValidatedIdcard(String loanNo, String idcard, String custName) throws Exception {
        if (StringUtils.equals("0", runtime)) {
            String sql = "update loan_info_et set idcard_verify = '03',bankacct_verify='03' where loan_no='" + SafeUtil.safe(loanNo) + "'";
            baseDao.execute(sql);
        } else {
            if (!ValidUtil.DCard.checkLegitimate(idcard)) {
                throw new Exception(BizErrorCodeEnum.T1016.getCode());
            }
            boolean result = tianXingClinet.queryIdentity(custName, idcard, false);
            if (!result) {
                throw new Exception(BizErrorCodeEnum.T1016.getCode());
            }
            String sql = "update loan_info_et set idcard_verify = '03' where loan_no='" + SafeUtil.safe(loanNo) + "'";
            baseDao.execute(sql);
        }
    }

    /**
     * 检查基本数据
     *
     * @param loan_no
     * @throws Exception
     */
    public void checkBasicData(String loan_no) throws Exception {
        Row loan_row = erpUtil.getLoanBasicDataInfoV2(loan_no);
        if (StringUtils.isBlank(erpUtil.objectIsNullGetString(loan_row.get("loan_info.sell_branch_no")))) {
            throw new Exception(BizErrorCodeEnum.B0031.getCode());
        }
        String bank_accout = SafeUtil.safe(StringUtil.replaceBlank(erpUtil.objectIsNullGetString(loan_row.get("loan_info_et.bank_accout"))));
        if (!bank_accout.matches("[0-9]+")) {
            throw new Exception(BizErrorCodeEnum.T1046.getCode());
        }
        String borrowed_name = SafeUtil.safe(StringUtil.replaceBlank(erpUtil.objectIsNullGetString(loan_row.get("loan_info_et.borrowed_name"))));
        String borrowed_mobile = SafeUtil.safe(StringUtil.replaceBlank(erpUtil.objectIsNullGetString(loan_row.get("loan_info_et.borrowed_mobile"))));
        String borrowed_idcard = SafeUtil.safe(StringUtil.replaceBlank(erpUtil.objectIsNullGetString(loan_row.get("loan_info_et.borrowed_idcard"))));
        List<String> init_sqls = new ArrayList<String>();
        //主要是为替换空格保存，应该做到表单字段的保存操作，保存查询保存规则，然后进行保存
        init_sqls.add("update loan_info_et set bank_accout='" + bank_accout + "',borrowed_name='" + borrowed_name + "',borrowed_mobile='" + borrowed_mobile + "',borrowed_idcard='" + borrowed_idcard + "' where loan_no='" + loan_no + "'");

    }

    /**
     * 验证资方数据
     *
     * @param loan_no
     * @throws Exception
     */
    public void checkEmployerInfo(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String loan_type = erpUtil.objectIsNullGetString(loanRow.get("loan_info_et.loan_type"));
        double contract_sum = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.contract_sum"));
        String is_prepay = erpUtil.objectIsNullGetString(loanRow.get("loan_info_et.is_prepay"));
        double prepay_percentage = erpUtil.objectIsNullGetDouble(loanRow.get("loan_info_et.prepay_percentage"));
        String sql = "select sum(pay_type_01) as pay_type_01,"
                + "		   sum(pay_type_02) as pay_type_02,"
                + "		   sum(pay_type_03) as pay_type_03,"
                + "		   sum(pay_type_04) as pay_type_04,"
                + "		   sum(CONTRIBUTION_AMOUNT) as CONTRIBUTION_AMOUNT,"
                + "		   sum(err_count) as err_count "
                + "from (select loan_no,"
                + "				case when pay_type='01' then 1 else 0 end as pay_type_01,"
                + "				case when pay_type='02' then 1 else 0 end as pay_type_02,"
                + "				case when pay_type='03' then 1 else 0 end as pay_type_03,"
                + "				case when pay_type='04' then 1 else 0 end as pay_type_04,"
                + "				case when contribution_amount<=0 then 1 else 0 end as err_count,"
                + "				contribution_amount "
                + "		 from loan_info_employers "
                + "		 where loan_no='" + loan_no + "' and status='1'"
                + ")x group by x.loan_no";
        Row employerRow = baseDao.queryRow(sql);
        int pay_type_01 = erpUtil.objectIsNullGetInteger(employerRow.get("pay_type_01"));
        int pay_type_02 = erpUtil.objectIsNullGetInteger(employerRow.get("pay_type_02"));
        int pay_type_03 = erpUtil.objectIsNullGetInteger(employerRow.get("pay_type_03"));
        int pay_type_04 = erpUtil.objectIsNullGetInteger(employerRow.get("pay_type_04"));
        //付款金额为0或者0以下的资方数量
        int err_count = erpUtil.objectIsNullGetInteger(employerRow.get("err_count"));
        double contribution_amount = erpUtil.objectIsNullGetDouble(employerRow.get("contribution_amount"));
        //新增、再租
        if (StringUtils.equals("01", loan_type) || StringUtils.equals("02", loan_type)) {
            if (pay_type_01 != 0 || pay_type_02 != 1) {
                throw new Exception(BizErrorCodeEnum.B0081.getCode());
            } else {
                //预付款的
                if (StringUtils.equals(is_prepay, "01") && prepay_percentage > 0) {
                    if (pay_type_03 != 0 || pay_type_04 != 1) {
                        throw new Exception(BizErrorCodeEnum.B0081.getCode());
                    }
                    //非预付款
                } else {
                    if (pay_type_03 > 1 || pay_type_04 != 0) {
                        throw new Exception(BizErrorCodeEnum.B0081.getCode());
                    }
                }
            }
            //续租
        } else if (StringUtils.equals("03", loan_type) || StringUtils.equals("04", loan_type)) {
            if (pay_type_01 != 1 || pay_type_02 != 0 || pay_type_03 != 0 || pay_type_04 != 0) {
                throw new Exception(BizErrorCodeEnum.B0081.getCode());
            }
            //追加
        } else if (StringUtils.equals("05", loan_type)) {
            if (pay_type_01 != 0 || pay_type_02 != 1 || pay_type_03 != 0 || pay_type_04 != 0) {
                throw new Exception(BizErrorCodeEnum.B0081.getCode());
            }
        }
        //总付款金额 不等于 合同金额
        if (contribution_amount != contract_sum) {
            throw new Exception(BizErrorCodeEnum.B0047.getCode());
        }
        //有付款金额为负数或者0的资方信息存在
        if (err_count > 0) {
            throw new Exception(BizErrorCodeEnum.B0047.getCode());
        }
    }

    /**
     * 验证放款金额是否等于合同金额
     *
     * @param loan_no
     * @throws Exception
     */
    public void check_contribution_amount(String loan_no) throws Exception {
        double fk_amount = 0;
        String sql = "select *  from loan_info_employers where loan_no='" + SafeUtil.safe(loan_no) + "' and status='1' ";
        Store loanEmployers = baseDao.query(sql);
        Row loan_row = erpUtil.getLoanBasicDataInfo(loan_no);
        double contract_sum = Double.parseDouble(loan_row.get("contract_sum") == null ? "0" : loan_row.get("contract_sum").toString());
        for (Row emp : loanEmployers) {
            double contribution_amount = erpUtil.objectIsNullGetDouble(emp.get("contribution_amount"));
            fk_amount = BigDecimalUtil.add(fk_amount, contribution_amount);
        }
        if (fk_amount != contract_sum) {//放款金额不等于合同金额
            throw new Exception(BizErrorCodeEnum.B0047.getCode());
        }
    }

    /**
     * 检查审批金额
     *
     * @throws Exception
     */
    public void check_approve_sum(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String loan_type = erpUtil.objectIsNullGetString(loanRow.get("loan_info_et.loan_type"));
        if (StringUtils.equals("01", loan_type) || StringUtils.equals("02", loan_type) || StringUtils.equals("05", loan_type)) {
            double approve_sum = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.approve_sum"));
            double max_approve_sum = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.max_approve_sum"));
            if (approve_sum > max_approve_sum) {
                throw new Exception(BizErrorCodeEnum.T1067.getCode());
            }
        }
    }

    /**
     * 车牌号验证
     *
     * @throws Exception
     */
    public void check_carPlate(String car_plate) throws Exception {
        String regex = ".*[a-z]+.*";
        if (StringUtils.isNotEmpty(car_plate) && car_plate.length() != 7 && car_plate.length() != 8) {
            throw new Exception(BizErrorCodeEnum.T1042.getCode());
        }
        Matcher m = Pattern.compile(regex).matcher(car_plate);
        if (m.matches()) {
            throw new Exception(BizErrorCodeEnum.B0090.getCode());
        }
    }

    /**
     * 车牌号验证
     *
     * @throws Exception
     */
    public void check_unclear_car_plate(String car_plate) throws Exception {
        Map<String, Object> map = new HashMap<String,Object>();
        map.put("car_plate",car_plate);
        map.put("type","checkUnclearByCarPlate");
        HttpResult httpResult = httpService.doPost("https://erp.renxingchedai.com/loan_common_CheckLoanData",map);
        if (httpResult.getCode() == 200) {
            JSONObject resultJson   = JSONObject.parseObject(httpResult.getBody());
            log.info("resultJson:"+resultJson);
            String resultCode = resultJson.getString("resultCode");
            String resultMsg = resultJson.getString("resultMsg");
            if("faild".equals(resultCode)){
                throw new Exception(resultMsg);
            }
        }else{
            throw new Exception("车牌号远程验证失败");
        }
    }

    /**
     * 检查预付款没有完成
     *
     * @param loan_no
     * @throws Exception
     */
    public void check_unfinishAdvancePayWorkFlow(String loan_no) throws Exception {
        String sql = "select loan_mid_pay.pay_no  "
                + "from loan_mid_pay "
                + "left join loan_mid "
                + "on loan_mid_pay.mid_no=loan_mid.mid_no and loan_mid.status='1' "
                + "left join loan_mid_employers "
                + "on loan_mid_employers.MID_NO = loan_mid_pay.mid_no "
                + "left join wf_review_biz "
                + "on wf_review_biz.TABLE_KEY_NO=loan_mid.mid_no and wf_review_biz.status='1' "
                + "left join wf_review "
                + "on wf_review.review_no=wf_review_biz.review_no and (wf_review.REVIEW_STATUS='E' OR wf_review.REVIEW_STATUS='R') "
                + "where loan_mid_employers.pay_type = '04' and loan_mid_pay.status='1' and loan_mid.loan_no='" + loan_no + "'  and wf_review.review_no is not null ";
        if (baseDao.query(sql).size() > 0) {
            throw new Exception(BizErrorCodeEnum.B0004.getCode());
        }
    }

    /**
     * 检查分行锁定
     *
     * @param loan_no
     * @throws SQLException
     */
    public void check_lockBranch(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String sell_branch_no = erpUtil.objectIsNullGetString(loanRow.get("sell_branch_no"));
        String car_plate = erpUtil.objectIsNullGetString(loanRow.get("car_plate"));
        String sql = "select loan_info.sell_branch_no,loan_info_et.fk_date,i_frame_sys_dept.dept_name from loan_info "
                + "left join loan_info_et on loan_info.loan_no = loan_info_et.loan_no "
                + "left join loan_car on loan_info.loan_no = loan_car.loan_no "
                + "left join i_frame_sys_dept on loan_info.dept_id = i_frame_sys_dept.dept_id  "
                + " where trim(loan_car.car_plate)='" + SafeUtil.safe(car_plate) + "' and loan_info.loan_no != '" + loan_no + "' "
                + "and loan_info_et.fk_date is not null order by fk_date asc limit 1";
        Row fk_loan_row = baseDao.queryRow(sql);
        if (fk_loan_row != null && !sell_branch_no.equalsIgnoreCase(erpUtil.objectIsNullGetString(fk_loan_row.get("sell_branch_no")))) {
            throw new Exception("当前客户已被" + erpUtil.objectIsNullGetString(fk_loan_row.get("dept_name")) + "锁定,请确认并自主协调");
        }
    }

    /**
     * 同一车架号，在未结清的情况下，不能新增（车架号验证）
     *
     * @param loan_no
     * @throws Exception
     */
    public void check_otherNewFkCjh(String loan_no) throws Exception {
        Row row = baseDao.queryRow("select loan_type,cjh from loan_info_et left join loan_car on loan_car.loan_no=loan_info_et.loan_no where loan_info_et.loan_no = '" + loan_no + "'");
        String loan_type = row.getString("loan_type");
        String cjh = row.getString("cjh");
        if ("01".equals(loan_type)) {
            Store store = baseDao.query(
                    "select i_frame_sys_dept.DEPT_NAME,i_frame_sys_user.USER_NAME from loan_info_et "
                            + "left join loan_car on loan_info_et.loan_no = loan_car.loan_no "
                            + "left join wf_review_biz on wf_review_biz.table_key_no=loan_info_et.loan_no "
                            + "left join wf_review on wf_review.review_no=wf_review_biz.review_no "
                            + "left join loan_info on loan_info.loan_no=loan_info_et.loan_no "
                            + "left join i_frame_sys_dept on i_frame_sys_dept.dept_id=loan_info.dept_id "
                            + "left join i_frame_sys_user on i_frame_sys_user.user_id=loan_info.SELLER_NO "
                            + "where (loan_info_et.settle_date is null or loan_info_et.settle_date='') "//未结清(包括在审核中)
                            + "and trim(cjh)='" + SafeUtil.safe(StringUtil.replaceBlank(cjh)) + "' "
                            + "and loan_info_et.loan_no <> '" + loan_no + "' "//不是当前流程
                            + "and wf_review.review_status!='D' ");//不是废弃流程
            if (store != null && store.size() > 0) {
                Row loan = store.get(0);
                throw new Exception("该车架号已经贷过【由" + loan.getString("dept_name") + loan.getString("user_name") + "进件】，不能再次新增贷款只能结清再贷！");
            }
        }
    }

    /**
     * 已经存在新增放款（车牌号验证）
     *
     * @param loan_no
     * @throws Exception
     */
    public void check_otherNewFk(String loan_no) throws Exception {
        Row row = baseDao.queryRow("select loan_type,car_plate from loan_info_et left join loan_car on loan_car.loan_no=loan_info_et.loan_no where loan_info_et.loan_no = '" + loan_no + "'");
        String loan_type = row.getString("loan_type");
        String car_plate = row.getString("car_plate");
        if ("01".equals(loan_type)) {
            String sql = "select i_frame_sys_dept.DEPT_NAME,i_frame_sys_user.USER_NAME "
                    + "from loan_info_et "
                    + "left join loan_car on loan_info_et.loan_no = loan_car.loan_no "
                    + "left join loan_info on loan_info.LOAN_NO=loan_info_et.loan_no "
                    + "left join i_frame_sys_dept on i_frame_sys_dept.dept_id=loan_info.dept_id "
                    + "left join i_frame_sys_user on i_frame_sys_user.user_id=loan_info.SELLER_NO "
                    + "where loan_info_et.fk_date is not null "
                    + "and trim(car_plate)='" + SafeUtil.safe(StringUtil.replaceBlank(car_plate)) + "' "
                    + "and loan_info_et.loan_no <> '" + loan_no + "'"
                    + "order by loan_info.CREATE_TIME desc";
            Store store = baseDao.query(sql);
            if (store.size() > 0) {
                Row loan = store.get(0);
                throw new Exception("该车牌已经贷过【由" + loan.getString("dept_name") + loan.getString("user_name") + "进件】，不能再次新增贷款只能结清再贷！");
            }
        }
    }


    /**
     * 存在未放款的贷款（车牌号查询）
     *
     * @param loan_no
     * @throws Exception
     */
    public void check_otherUnfinishedFk(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfo(loan_no);
        String car_plate = loanRow.getString("car_plate");
        String sql = "select i_frame_sys_dept.dept_name,i_frame_sys_user.user_name,loan_info.create_time  "
                + "from loan_info "
                + "left join loan_info_et on loan_info.loan_no = loan_info_et.loan_no "
                + "left join loan_car on loan_info.loan_no = loan_car.loan_no  "
                + "left join wf_review_biz on wf_review_biz.table_key_no =  loan_info.loan_no "
                + "left join wf_review on wf_review.review_no = wf_review_biz.review_no "
                + "left join wf_review_process on wf_review_process.review_no = wf_review.review_no "
                + "left join wf_templet_flow_node on wf_templet_flow_node.templet_flow_no=wf_review.templet_flow_no "
                + "and wf_templet_flow_node.flow_node_no=wf_review_process.flow_node_no "
                + "left join i_frame_sys_dept on i_frame_sys_dept.`dept_id`=loan_info.dept_id "
                + "left join i_frame_sys_user on i_frame_sys_user.user_id=loan_info.seller_no "
                + "where "
                + "wf_review.`status` = 1 and (wf_review.review_status <> 'D') "
                + "and (loan_info_et.fk_date is null or loan_info_et.fk_date = '') and wf_templet_flow_node.flow_node_no IN ('101')  and wf_review_process.process_status = 'Y' "
                + "and loan_info.loan_no <> '" + SafeUtil.safe(loan_no) + "' and trim(loan_car.car_plate) = '" + SafeUtil.safe(car_plate) + "' ";
        Store loans = baseDao.query(sql);
        if (loans != null && loans.size() > 0) {
            Row loan = loans.get(0);
            throw new Exception("该车牌号存在其他未完成的放款流程【由" + loan.getString("dept_name") + loan.getString("user_name") + "进件】，本次贷款不能发起，请确认自主协调其他未完成的放款流程是否废弃！");
        }
    }

    /**
     * 检查银行卡代扣代付支持
     */
    public void check_bank(String loan_no) throws Exception {
        Row loan_row = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String bank_accout = SafeUtil.safe(erpUtil.objectIsNullGetString(loan_row.get("loan_info_et.bank_accout")));
        String bank_short = erpUtil.getBankShortByCard(bank_accout);
        String sql = "select count(1) from loan_bank where bank_short_name='" + bank_short + "' and status='1'";
        int count = Integer.valueOf(baseDao.queryObject(sql).toString());
        if (count == 0) {
            String str = baseDao.queryObject("select group_concat(bank_name) from loan_bank where status='1'").toString();
            throw new Exception("当前银行卡银行不支持代付代扣,请更换为以下银行(" + str + ")");
        }
    }

    /**
     * 存在一次放款到卡的流程
     *
     * @param loan_no
     * @throws Exception
     */
    public void check_exist_on_pay_card_fk_review(String loan_no) throws Exception {
        String sql = "select loan_mid_pay.pay_no  "
                + "from loan_mid_pay "
                + "left join loan_mid "
                + "on loan_mid_pay.mid_no=loan_mid.mid_no and loan_mid.status='1' "
                + "left join loan_mid_employers "
                + "on loan_mid_employers.mid_no = loan_mid_pay.mid_no "
                + "left join wf_review_biz "
                + "on wf_review_biz.table_key_no=loan_mid.mid_no and wf_review_biz.status='1' "
                + "left join wf_review "
                + "on wf_review.review_no=wf_review_biz.review_no "
                + "where loan_mid_employers.pay_type = '02' and loan_mid_pay.status='1' and loan_mid.loan_no='" + loan_no + "' "
                + "and wf_review.review_no is not null ";
        if (baseDao.query(sql).size() > 0) {
            throw new Exception(BizErrorCodeEnum.B0004.getCode());
        }
    }

    /**
     * 检查3要素
     *
     * @param loan_no
     * @throws Exception
     */
    public void check_3_element(String loan_no) throws Exception {
        Row loan_row = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String cust_name = erpUtil.objectIsNullGetString(loan_row.get("loan_cust.cust_name"));
        String idcard = erpUtil.objectIsNullGetString(loan_row.get("loan_cust.idcard"));
        String bank_dep = erpUtil.objectIsNullGetString(loan_row.get("loan_info_et.bank_dep"));
        String bank_account = erpUtil.objectIsNullGetString(loan_row.get("loan_info_et.bank_accout"));
        String cust_phone = erpUtil.objectIsNullGetString(loan_row.get("loan_cust.cust_phone"));
        String borrowed_name = erpUtil.objectIsNullGetString(loan_row.get("loan_info_et.borrowed_name"));
        String borrowed_idcard = erpUtil.objectIsNullGetString(loan_row.get("loan_info_et.borrowed_idcard"));
        String borrowed_mobile = erpUtil.objectIsNullGetString(loan_row.get("loan_info_et.borrowed_mobile"));
        if (!tianXingClinet.queryUnionpay3Element(cust_name, idcard, bank_account, false)) {
            if (StringUtils.isNotBlank(borrowed_name) && StringUtils.isNotBlank(borrowed_idcard)) {
                if (StringUtils.isBlank(borrowed_mobile)) {
                    throw new Exception(BizErrorCodeEnum.T1026.getCode());
                } else if (!tianXingClinet.queryUnionpay3Element(cust_name, idcard, bank_account, false)) {
                    throw new Exception(BizErrorCodeEnum.T1017.getCode());
                }
            } else {
                throw new Exception(BizErrorCodeEnum.T1017.getCode());
            }
            erpUtil.updatePayeeInfo(borrowed_name, borrowed_idcard, bank_account, borrowed_mobile, loan_no, bank_dep);
        } else {
            erpUtil.updatePayeeInfo(cust_name, idcard, bank_account, cust_phone, loan_no, bank_dep);
        }
    }

    /**
     * 检查4要素
     * @param loan_no
     * @throws Exception
     */
    public void check_4_element(String loan_no) throws Exception {
        Row loan_row = erpUtil.getLoanBasicDataInfoV2(loan_no);

        String cust_name =erpUtil.objectIsNullGetString(loan_row.get("loan_cust.cust_name"));
        String idcard = erpUtil.objectIsNullGetString(loan_row.get("loan_cust.idcard"));
        String bank_dep = erpUtil.objectIsNullGetString(loan_row.get("loan_info_et.bank_dep"));
        String bank_account = erpUtil.objectIsNullGetString(loan_row.get("loan_info_et.bank_accout"));
        String cust_phone = erpUtil.objectIsNullGetString(loan_row.get("loan_cust.cust_phone"));
        String borrowed_name= erpUtil.objectIsNullGetString(loan_row.get("loan_info_et.borrowed_name"));
        String borrowed_idcard= erpUtil.objectIsNullGetString(loan_row.get("loan_info_et.borrowed_idcard"));
        String borrowed_mobile=erpUtil.objectIsNullGetString(loan_row.get("loan_info_et.borrowed_mobile"));

        if(!tianXingClinet.queryUnionpay4Element(cust_name, idcard, bank_account,cust_phone, false)){
            if(StringUtils.isNotBlank(borrowed_name) && StringUtils.isNotBlank(borrowed_idcard)){
                if(StringUtils.isBlank(borrowed_mobile)){
                    throw new Exception(BizErrorCodeEnum.T1026.getCode());
                }else if(!tianXingClinet.queryUnionpay4Element(borrowed_name, borrowed_idcard, bank_account,borrowed_mobile, false)){
                    throw new Exception(BizErrorCodeEnum.Z0045.getCode());
                }
            }else{
                throw new Exception(BizErrorCodeEnum.Z0045.getCode());
            }
            erpUtil.updatePayeeInfo(borrowed_name,borrowed_idcard,bank_account,borrowed_mobile,loan_no,bank_dep);
        }else{
            erpUtil.updatePayeeInfo(cust_name,idcard,bank_account,cust_phone,loan_no,bank_dep);
        }
    }

    /**
     * 检查预付款额度
     *
     * @param loan_no
     * @throws Exception
     */
    public void check_prepaySumQuota(String loan_no) throws Exception {
        String sql = "select * from loan_info where loan_no='" + loan_no + "' LIMIT 1;";
        Row loanInfoRow = baseDao.queryRow(sql);
        sql = "select * from loan_cost where loan_no='" + loan_no + "' LIMIT 1;";
        Row loanCostRow = baseDao.queryRow(sql);
        sql = "select * from loan_branch_data where loan_branch_data.branch_no = '" + loanInfoRow.getString("sell_branch_no") + "' and status != 3";
        Row loan_branch_data_row = baseDao.queryRow(sql);
        if (null == loan_branch_data_row) {
            throw new Exception(BizErrorCodeEnum.T1055.getCode());
        }
        double prepay_sum_quota = loan_branch_data_row.getNumber("prepay_sum_quota").doubleValue();//预付款额度
        double useprepaysum = erpUtil.getUsePrepaySumQuota(loanInfoRow.getString("sell_branch_no"));
        if (loanCostRow.getNumber("prepay_sum").doubleValue() + useprepaysum > prepay_sum_quota) {
            throw new Exception(BizErrorCodeEnum.T1054.getCode());
        }
    }

    /**
     * 检查预付款金额
     *
     * @param loan_no
     * @throws Exception
     */
    public void check_yfk_prepay_sum(String loan_no) throws Exception {
//        Row loan_row = erpUtil.getLoanBasicDataInfoV2(loan_no);
//        double contract_sum = ErpUtil.objectIsNullGetDouble(loan_row.get("loan_cost.contract_sum"));
//        String is_prepay = ErpUtil.objectIsNullGetString(loan_row.get("loan_info_et.is_prepay"));
//        double prepay_sum = ErpUtil.objectIsNullGetDouble(loan_row.get("loan_cost.prepay_sum"));
//        Row yfk_employersInfo_row = baseDao.queryRow("select * from loan_info_employers where pay_type = '04' and status = '1' and loan_no='" + loan_no + "'");
//        //已经存在预付款数据不正确
//        if (null != yfk_employersInfo_row) {
//            if (prepay_sum != yfk_employersInfo_row.getNumber("contribution_amount").doubleValue() || !StringUtils.equals("01", is_prepay)) {
//                throw new Exception(BizErrorCodeEnum.B0092.getCode());
//            }
//        }
//        if (prepay_sum <= 0 || prepay_sum > contract_sum * 0.5) {
//            throw new Exception(BizErrorCodeEnum.T1044.getCode());
//        }
//        Map<String, Object> map = costService.getBasicsCostInfo(loan_no, null);
//        if (prepay_sum > Double.parseDouble(String.valueOf(map.get("totle_price")))) {
//            throw new Exception(BizErrorCodeEnum.T1079.getCode());
//        }
    }

    /**
     * 预付款验证
     *
     * @param loan_no
     * @throws Exception
     */
    @Override
    public void check_prepay_percent(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String is_prepay = erpUtil.objectIsNullGetString(loanRow.get("loan_info_et.is_prepay"));
        double prepay_percentage = erpUtil.objectIsNullGetDouble(loanRow.get("loan_info_et.prepay_percentage"));

        if (StringUtils.equals("01", is_prepay)) {
            if (prepay_percentage <= 0 || prepay_percentage > 5) {
                throw new Exception(BizErrorCodeEnum.T1044.getCode());
            }
        } else {
            if (prepay_percentage != 0) {
                throw new Exception(BizErrorCodeEnum.T1096.getCode());
            }
        }
    }

    /**
     * 检查合同金额
     *
     * @param loan_no
     * @throws Exception
     */
    @Override
    public void check_contract_sum(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String prod_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info.prod_no"));
        String loan_type = erpUtil.objectIsNullGetString(loanRow.get("loan_info_et.loan_type"));
        double deposit_sum = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.deposit_sum"));
        double approve_sum = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.approve_sum"));
        double temp_sum = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.temp_sum"));
        double assurance_sum = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.assurance_sum"));
        double contract_sum = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.contract_sum"));
        if (StringUtils.equals("01", loan_type) || StringUtils.equals("02", loan_type)) {
            double sum = BigDecimalUtil.add(BigDecimalUtil.add(approve_sum, deposit_sum), BigDecimalUtil.add(temp_sum, assurance_sum));
            double contract_sum_compute = BigDecimalUtil.mul(BigDecimalUtil.decimal(BigDecimalUtil.div(sum, 100), 0), 100);
            if (contract_sum_compute != contract_sum) {
                throw new Exception(BizErrorCodeEnum.T1097.getCode());
            }
        }
    }

    /**
     * 检查可选资方
     *
     * @param loan_no
     * @throws Exception
     */
    @Override
    public void check_select_capital(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String selectable_capital_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info_et.selectable_capital_no"));
        String intentional_capital_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info_et.intentional_capital_no"));
        if (!StringUtils.contains(selectable_capital_no, intentional_capital_no)) {
            throw new Exception(BizErrorCodeEnum.Z0039.getCode());
        }

    }

    /**
     * 运营商报告
     *
     * @param cust_phone
     * @throws Exception
     */
    @Override
    public void check_yys_message(String cust_phone) throws Exception {

        String task_id = shuJuMoHeClinet.queryCarrier(cust_phone);
        if (StringUtils.isNotEmpty(task_id)) {
            return;
        }
        throw new Exception(BizErrorCodeEnum.Z0036.getCode());

    }

    /**
     * 检查反欺诈
     *
     * @param loan_no
     * @throws Exception
     */
    @Override
    public void check_fraud(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        double fraud_first_score = erpUtil.objectIsNullGetDouble(loanRow.get("loan_ass.fraud_first_score"));
        if (fraud_first_score > 100) {
            throw new Exception(BizErrorCodeEnum.Z0040.getCode());
        }
    }

}
