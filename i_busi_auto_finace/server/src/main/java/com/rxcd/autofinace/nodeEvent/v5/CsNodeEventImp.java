package com.rxcd.autofinace.nodeEvent.v5;


import org.appframe.wf.engine.AbstractWorkFlowNodeEvent;
import org.appframe.wf.service.WfParamUtil;
import org.appframe.wf.util.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 初审
 *
 * @author Yuan_Song, HuBin
 */
@Component
public class CsNodeEventImp extends AbstractWorkFlowNodeEvent {

    @Autowired
    private WfParamUtil paramUtil;

    @Override
    public void workFlowNodeStart(String flow_node_no, Param p) throws Exception {
        checkTempletFlowNo("1");
    }

    @Override
    public void workFlowNodeOverrule(String flow_node_no, Param p) throws Exception {
        checkTempletFlowNo("1");
    }

    @Override
    public void workFlowNodeVie(String flow_node_no, Param p) throws Exception {
        checkTempletFlowNo("1");
    }

    @Override
    public void workFlowNodeFinish(String flow_node_no, Param p) throws Exception {
        checkTempletFlowNo("1");
    }

}
