package com.rxcd.autofinace.service;

import com.rxcd.autofinace.service.client.ClearClient;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.util.Const;
import org.appframe.wf.util.ErpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 租前业务
 *
 * @author hubin
 */
@Component
public class MidService {

    private final static Log log = LogFactory.getLog(MidService.class);

    @Autowired
    private BaseDao baseDao;

    @Autowired
    private ClearClient clearClient;

    /**
     * 计算二次放款是否可以自动放款了
     *
     * @param mid_no
     */
    public void computeSecondAutoPay(String mid_no) throws Exception {
        Row payRow = baseDao.queryRow("select * from loan_mid_pay where mid_no='" + mid_no + "'");

        if (payRow != null) {
            String zero_success = ErpUtil.objectIsNullGetString(payRow.get("zero_success"));
            String branch_success = ErpUtil.objectIsNullGetString(payRow.get("branch_success"));
            if (StringUtils.equals("01", zero_success) && StringUtils.equals("01", branch_success)) {
                baseDao.execute("update loan_mid_pay set is_pay='YES' where mid_no='" + mid_no + "'");
            }
        }
    }

    /**
     * 生成应收应付(放款计划、还款计划)
     *
     * @param mid_no
     * @throws Exception
     */
    public void computePayable(String mid_no) throws Exception {
        Row empRow=baseDao.queryRow("select * from loan_mid_employers where mid_no='"+mid_no+"'");
        clearClient.createPayPayable(mid_no);
        if(StringUtils.equals("02",empRow.getStringNotNull("pay_type"))){//一次放款
            Row midRow=baseDao.queryRow("select * from loan_mid where mid_no='"+mid_no+"'");
            String loan_no=midRow.getStringNotNull("loan_no");
//          还款应付记录
            clearClient.createRepayPayable(loan_no);
        }
    }


    /**
     * 放款结算
     *
     * @param mid_no
     * @throws Exception
     */
    public void excutePay(String mid_no) throws Exception {
        Row empRow = baseDao.queryRow("select * from loan_mid_employers where mid_no='"+mid_no+"'");
        Row loan_mid_row = baseDao.queryRow("select * from loan_mid where mid_no='"+mid_no+"'");
        String loan_no = loan_mid_row.getString("loan_no");
        Row loan_cust_row = baseDao.queryRow("select * from loan_cust where loan_no='"+loan_no+"'");
        if(null == loan_cust_row.get("clearing_ordinary") || "".equals(loan_cust_row.getString("clearing_ordinary"))){
            throw new Exception("客户账户不存在");
        }
        //生成账单
        List<Map> billList=null;
        if(StringUtils.equals("02",empRow.getStringNotNull("pay_type"))){//一次放款
            billList=clearClient.createPay02Bill(mid_no);
        }else if(StringUtils.equals("03",empRow.getStringNotNull("pay_type"))){//二次放款
            billList=clearClient.createPay03Bill(mid_no);
        }
        if(billList!=null){
            Map<String,Object> settle_result = null;
            Map<String,Object> cash_settle_result = null;
            List<Map> withdrawCashBillList = null;
            for(Map map:billList){
                //结算放款账单
                settle_result = clearClient.settleBill((String)map.get("billId"));
                if("1".equals(settle_result.get("resultCode")) && String.valueOf(map.get("payeeAccountId")).equals(loan_cust_row.getString("clearing_ordinary"))){
                    map.put("notifyUrl", Const.SYS_WEB_URL+"/loan_comm_paycallback");
                    withdrawCashBillList =  clearClient.createWithdrawCashBillWithBill(map);
                    if(null != withdrawCashBillList){
                        for(Map cashMap:withdrawCashBillList){
                            //结算提现账单
                            cash_settle_result = clearClient.settleBill((String)cashMap.get("billId"));
                            log.info("提现结果:"+cash_settle_result.toString());
                        }
                    }
                }
            }
        }
        Row loan_info_et_row = baseDao.queryRow("select * from loan_info_et where loan_no='"+loan_no+"'");
        Row loan_mid_pay_row = baseDao.queryRow("select * from loan_mid_pay where mid_no='"+mid_no+"'");
        if(StringUtils.isBlank(loan_info_et_row.getStringNotNull("fk_date"))){
            baseDao.execute("update loan_info_et set loan_info_et.fk_date = '"+loan_mid_pay_row.getStringNotNull("fk_date")+"' where loan_no = '"+loan_no+"'");
        }
    }
}
