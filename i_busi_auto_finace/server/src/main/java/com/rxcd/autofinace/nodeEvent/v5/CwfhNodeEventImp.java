package com.rxcd.autofinace.nodeEvent.v5;


import com.rxcd.autofinace.service.FkService;
import com.rxcd.autofinace.service.LoanCheckServiceImpl;
import com.rxcd.autofinace.service.LoanService;
import org.appframe.wf.db.Row;
import org.appframe.wf.engine.AbstractWorkFlowNodeEvent;
import org.appframe.wf.service.WfParamUtil;
import org.appframe.wf.util.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 财务复核
 *
 * @author hubin
 */
@Component
public class CwfhNodeEventImp extends AbstractWorkFlowNodeEvent {

    @Autowired
    private WfParamUtil paramUtil;
    @Autowired
    private LoanCheckServiceImpl checkService;
    @Autowired
    private FkService fkService;
    @Autowired
    private LoanService loanService;
    /**
     * 流程节点启动时调用
     *
     * @param flow_node_no
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowNodeStart(String flow_node_no, Param p) throws Exception {
        
    }

    @Override
    public void workFlowNodeFinish(String flow_node_no, Param p) throws Exception {
        checkTempletFlowNo("1");
        String loan_no = getTableKeyNo();
        checkService.check_unfinishAdvancePayWorkFlow(loan_no);
        checkService.check_otherNewFk(loan_no);
        checkService.check_otherUnfinishedFk(loan_no);
        loanService.createUserAccount(loan_no);
        Row reviewObject = (Row) paramUtil.get("reviewObject");
        String review_no = reviewObject.getString("review_no");
        fkService.execute_fk(p,review_no);
    }

    /**
     * 流程节点驳回时回调方法
     *
     * @param flow_node_no
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowNodeOverrule(String flow_node_no, Param p) throws Exception {

    }

    @Override
    public void workFlowNodeVie(String flow_node_no, Param p) throws Exception {

    }

}
