package com.rxcd.autofinace.controller;


import com.alibaba.fastjson.JSONArray;
import com.rxcd.autofinace.util.RxErpUtil;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.appframe.file.FileServiceClient;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.db.Store;
import org.appframe.wf.util.Const;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileInputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 远程对象上传
 *
 * @author liuchang
 */
@RestController
@RequestMapping("/")
public class OSSController {

    private final static Logger log = LoggerFactory.getLogger(OSSController.class);
    @Autowired
    private BaseDao baseDao;
    @Autowired
    private RxErpUtil erpUtil;
    @Autowired
    private FileServiceClient fileServiceClient;

    @RequestMapping("/loan_common_OSSController")
    public Row execute(@RequestParam("loan_no") String loan_no,@RequestParam("type") String type) throws Exception {
        String sql = "";
        if(StringUtils.equals("IMG",type)){
            sql = "select field,table_code,field_type from wf_data_fields where field_type like 'OSS_IMG%'";
        }else if(StringUtils.equals("ALL",type)){
            sql = "select field,table_code,field_type from wf_data_fields where field_type not like 'OSS_IMG%' and field_type like 'OSS%'";
        }
        Store store = baseDao.query(sql);
        Store custStore = new Store();
        if(StringUtils.equals("",loan_no)||loan_no==null){
            String custSql = "select * from loan_info_et where fk_date is not null";
            custStore = baseDao.query(custSql);
        }else{
            Row custRow = new Row();
            custRow.put("loan_no",loan_no);
            custStore.add(custRow);
        }
        for(Row custRow:custStore){
            loan_no = erpUtil.objectIsNullGetString(custRow.get("loan_no"));
            log.info("融租编号:"+loan_no+"-----------上传开始");
            Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
            for(Row fieldRow:store){
                List fileList = new ArrayList();
                String field = erpUtil.objectIsNullGetString(fieldRow.get("field"));
                log.info("执行:"+field+"-----------文件开始");
                String field_type = erpUtil.objectIsNullGetString(fieldRow.get("field_type"));
                if(StringUtils.equals("OSS_ATTS",field_type)
                        || StringUtils.equals("OSS_IMGS",field_type)
                        || StringUtils.equals("OSS_VIDEOS",field_type)
                        ){
                    String table_code = erpUtil.objectIsNullGetString(fieldRow.get("table_code"));
                    try{
                        String attsSql = "select att_no from "+table_code+" where loan_no = '"+loan_no+"'";
                        Store attStore = baseDao.query(attsSql);
                        for(Row attRow:attStore){
                            String filePath = getFilePath(erpUtil.objectIsNullGetString(attRow.get("att_no")));
                            initOSSList(filePath,fileList);
                        }
                    }catch (Exception e){
                        log.info("表在数据库中不存在："+table_code);
                    }
                }else{
                    String filePath = getFilePath(erpUtil.objectIsNullGetString(loanRow.get(field)));
                    initOSSList(filePath,fileList);
                }
                if(fileList!=null&&fileList.size()>0){
                    JSONArray data = fileServiceClient.uploadFile(loan_no, field, fileList, "", "");
                    log.info(field+"文件上传返回："+data);
                }
                log.info("执行:"+field+"-----------文件结束");
            }
            log.info("融租编号:"+loan_no+"-----------上传结束");
        }
        return null;
    }

    /**
     * 初始化传输对象
     * @param filePath
     * @param fileList
     * @throws Exception
     */
    private void initOSSList(String filePath,List fileList)throws Exception {
        if(filePath!=null){
            File file = new File(filePath);
            FileInputStream input = new FileInputStream(file);
            byte[] bytes = new byte[input.available()];
            input.read(bytes);
            HashMap hashMap = new HashMap();
            hashMap.put("fileName", getFileNameAndFileSuffix(filePath));
            hashMap.put("fileBody", new String(Base64.encodeBase64(bytes), "utf-8"));
            fileList.add(hashMap);
        }
    }

    /**
     * 获取文件路径
     *
     * @param no
     * @return
     * @throws SQLException
     */
    private String getFilePath(String no) throws SQLException {
        String sql = "select file_path from i_frame_sys_attachment where id='" + no + "'";
        String filePath = (String) baseDao.queryObject(sql);
        if (StringUtils.isBlank(filePath)) {
            return null;
        }
        return Const.SYS_FILE_UPLOAD_URL_PATH + "/" + filePath;
    }

    /**
     * 获取文件全称
     *
     * @param filePath
     * @return
     */
    private static String getFileNameAndFileSuffix(String filePath) {
        if (StringUtils.isBlank(filePath)) {
            return null;
        }
        return filePath.substring(filePath.lastIndexOf("/") + 1, filePath.length());
    }
}
