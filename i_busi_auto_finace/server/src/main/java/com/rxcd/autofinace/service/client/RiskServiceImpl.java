package com.rxcd.autofinace.service.client;

import com.alibaba.fastjson.JSONObject;
import com.rxcd.autofinace.common.CarPlateCityEnum;
import com.rxcd.autofinace.util.RxErpUtil;
import org.appframe.risk.RiskClient;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.db.Store;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * 风控client
 * @author yeqingxin
 * @date 2018/12/03
 */
@Service
public class RiskServiceImpl implements RiskService {
    @Autowired
    RiskClient riskClient;
    @Autowired
    private BaseDao baseDao;
    @Autowired
    RxErpUtil erpUtil;

    /**
     * 创建一个返回结果的ROW对象
     * @param html
     * @return
     */
    private Row buildResultRow(String html){
        Row row = new Row();

        row.put("type", "jsonJsp");
        row.put("html", html);

        return row;
    }

    @Override
    public Row getFirstRiskControl(String loanNo, String pageType) throws Exception {
        String querySql = "select cust_name, cust_phone, idcard from loan_cust where loan_no = '"+ loanNo +"'";
        Store store = baseDao.query(querySql);

        StringBuffer html = new StringBuffer();

        if(store != null && store.size() > 0){
            Map<String, Object> paramMap = new HashMap<String, Object>();

            paramMap.put("pageType", pageType);
            paramMap.put("isForceQuery", 0);
            paramMap.put("name", store.get(0).get("cust_name"));
            paramMap.put("phone", store.get(0).get("cust_phone"));
            paramMap.put("idCard", store.get(0).get("idcard"));

            JSONObject jsonObject = riskClient.getFirstRiskControl(paramMap);

            if(jsonObject.get("html") != null){
                html.append(jsonObject.get("html"));
            }
        }

        return buildResultRow(html.toString());
    }

    @Override
    public Row getMidRiskControl(String loanNo, String pageType) throws Exception {
        String querySql = "select a.cust_name, a.cust_phone, a.idcard, a.address_p, a.cust_address, a.company_add_p, "
                   + "a.company_add, a.marital_status, a.children_state, a.residence_state, b.borrowed_name, b.borrowed_idcard, "
                   + "b.borrowed_mobile, b.bank_accout, b.approve_percentage, b.rep_type, c.ass_price, c.ass_score, "
                   + "c.assessor_work_age, d.duetime from loan_cust a "
                   + "left join loan_info_et b on a.loan_no = b.loan_no "
                   + "left join loan_ass c on a.loan_no = c.loan_no "
                   + "left join loan_cost d on a.loan_no = d.loan_no "
                   + "where a.loan_no = '"+ loanNo +"'";

        Store store = baseDao.query(querySql);

        StringBuffer html = new StringBuffer();

        if(store != null && store.size() > 0){
            String addressP = store.get(0).getString("address_p");
            String companyAddP = store.get(0).getString("company_add_p");

            String custAddress = "";
            String companyAddress = "";

            custAddress = erpUtil.getDetailedAddress(custAddress,addressP);
            companyAddress = erpUtil.getDetailedAddress(companyAddress,companyAddP);

            String contactQuerySql = "select contacts_relation, contacts_mobile from loan_info_contacts where loan_no = '"+ loanNo +"'";

            Store contactStore = baseDao.query(contactQuerySql);

            Map<String, Object> paramMap = new HashMap<String, Object>();

            paramMap.put("pageType", pageType);
            paramMap.put("isForceQuery", 0);
            paramMap.put("custName", store.get(0).get("cust_name"));
            paramMap.put("custPhone", store.get(0).get("cust_phone"));
            paramMap.put("idCard", store.get(0).get("idcard"));
            paramMap.put("custAddress", custAddress + store.get(0).get("cust_address"));
            paramMap.put("companyAdd", companyAddress + store.get(0).get("company_add"));
            paramMap.put("borrowedName", store.get(0).get("borrowed_name"));
            paramMap.put("borrowedIdcard", store.get(0).get("borrowed_idcard"));
            paramMap.put("borrowedMobile", store.get(0).get("borrowed_mobile"));
            paramMap.put("bankAccout", store.get(0).get("bank_accout"));
            paramMap.put("assPrice1", store.get(0).get("ass_price"));
            paramMap.put("approvePercentage", store.get(0).get("approve_percentage"));
            paramMap.put("repType", store.get(0).get("rep_type"));
            paramMap.put("dueTime", store.get(0).get("duetime"));
            paramMap.put("assScore", store.get(0).get("ass_score"));
            paramMap.put("assessorWorkAge", store.get(0).get("assessor_work_age"));
            paramMap.put("marriageState", store.get(0).get("marital_status"));
            paramMap.put("childrenState", store.get(0).get("children_state"));
            paramMap.put("residenceState", store.get(0).get("residence_state"));
            paramMap.put("localContactDetail", contactStore);

            JSONObject jsonObject = riskClient.getMidRiskControl(paramMap);

            if(jsonObject.get("html") != null){
                html.append(jsonObject.get("html"));
            }
        }

        return buildResultRow(html.toString());
    }

    @Override
    public Row getFinalRiskControl(String loanNo, String pageType) throws Exception {
        String querySql = "select sensitivesectors from loan_cust where loan_no = '"+ loanNo +"'";
        Store store = baseDao.query(querySql);

        StringBuffer html = new StringBuffer();

        if(store != null && store.size() > 0){
            Map<String, Object> paramMap = new HashMap<String, Object>();

            paramMap.put("pageType", pageType);
            paramMap.put("isForceQuery", 0);
            paramMap.put("sensitivesectors", store.get(0).get("sensitivesectors"));

            JSONObject jsonObject = riskClient.getFinalRiskControl(paramMap);

            if(jsonObject.get("html") != null){
                html.append(jsonObject.get("html"));
            }
        }

        return buildResultRow(html.toString());
    }

    @Override
    public Row getScore(String loanNo, String pageType) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loanNo);

        StringBuffer html = new StringBuffer();

        if(loanRow != null){
            Map<String, Object> paramMap = new HashMap<String, Object>();

            paramMap.put("pageType", pageType);
            paramMap.put("isForceQuery", 0);
            paramMap.put("carColor", loanRow.get("loan_car.car_color"));
            paramMap.put("interfacePrice", loanRow.get("loan_ass.interface_price"));
            paramMap.put("carAge", loanRow.get("loan_car.car_age"));
            paramMap.put("carRetention", loanRow.get("loan_car.car_retention"));
            paramMap.put("mileage", loanRow.get("loan_car.mileage"));
            paramMap.put("vehicleUsage", loanRow.get("loan_car.vehicle_usage"));
            paramMap.put("assessorRating", loanRow.get("loan_ass.assessor_rating"));
            paramMap.put("assessorWorkAge", loanRow.get("loan_ass.assessor_work_age"));
            paramMap.put("sellerWorkAge", loanRow.get("loan_ass.seller_work_age"));
            paramMap.put("fuelType", loanRow.get("loan_car.fuel_type"));

            JSONObject jsonObject = riskClient.getScore(paramMap);

            if(jsonObject.get("html") != null){
                html.append(jsonObject.get("html"));
            }
        }

        return buildResultRow(html.toString());
    }

    @Override
    public Row getTrueScore(String loanNo, String pageType) throws Exception {
        {
            Row loanRow = erpUtil.getLoanBasicDataInfoV2(loanNo);

            StringBuffer html = new StringBuffer();

            if(loanRow != null){
                Map<String, Object> paramMap = new HashMap<String, Object>();

                paramMap.put("pageType", pageType);
                paramMap.put("isForceQuery", 0);
                paramMap.put("carRetention", loanRow.get("loan_ass.car_retention"));
                paramMap.put("mileage", loanRow.get("loan_ass.mileage"));
                paramMap.put("mileageAbnormal", loanRow.get("loan_ass.mileage_abnormal"));
                paramMap.put("vehicleUsage", loanRow.get("loan_ass.vehicle_usage"));
                paramMap.put("fuelType", loanRow.get("loan_ass.fuel_type"));
                paramMap.put("transferCount", loanRow.get("loan_ass.transfer_count"));
                paramMap.put("engineLid", loanRow.get("loan_ass.engine_lid"));
                paramMap.put("trunkLid", loanRow.get("loan_ass.trunk_lid"));
                paramMap.put("rearBumper", loanRow.get("loan_ass.rear_bumper"));
                paramMap.put("frontBumper", loanRow.get("loan_ass.front_bumper"));
                paramMap.put("panelFrontFenderRh", loanRow.get("loan_ass.panel_front_fender_rh"));
                paramMap.put("panelFrontFenderLh", loanRow.get("loan_ass.panel_front_fender_lh"));
                paramMap.put("panelRearFenderRh", loanRow.get("loan_ass.panel_rear_fender_rh"));
                paramMap.put("panelRearFenderLh", loanRow.get("loan_ass.panel_rear_fender_lh"));
                paramMap.put("frontDoorRh", loanRow.get("loan_ass.front_door_rh"));
                paramMap.put("frontDoorLh", loanRow.get("loan_ass.front_door_lh"));
                paramMap.put("rearDoorRh", loanRow.get("loan_ass.rear_door_rh"));
                paramMap.put("rearDoorLh", loanRow.get("loan_ass.rear_door_lh"));
                paramMap.put("aPillarRh", loanRow.get("loan_ass.a_pillar_rh"));
                paramMap.put("aPillarLh", loanRow.get("loan_ass.a_pillar_lh"));
                paramMap.put("bPillarRh", loanRow.get("loan_ass.b_pillar_rh"));
                paramMap.put("bPillarLh", loanRow.get("loan_ass.b_pillar_lh"));
                paramMap.put("cPillarRh", loanRow.get("loan_ass.c_pillar_rh"));
                paramMap.put("cPillarLh", loanRow.get("loan_ass.c_pillar_lh"));
                paramMap.put("carRoof", loanRow.get("loan_ass.car_roof"));
                paramMap.put("frontWindshield", loanRow.get("loan_ass.front_windshield"));
                paramMap.put("rearWindshield", loanRow.get("loan_ass.rear_windshield"));
                paramMap.put("frontHeadlightLh", loanRow.get("loan_ass.front_headlight_lh"));
                paramMap.put("frontHeadlightRh", loanRow.get("loan_ass.front_headlight_rh"));
                paramMap.put("rearTaillampLh", loanRow.get("loan_ass.rear_taillamp_lh"));
                paramMap.put("rearTaillampRh", loanRow.get("loan_ass.rear_taillamp_rh"));
                paramMap.put("cisternFrame", loanRow.get("loan_ass.cistern_frame"));
                paramMap.put("anticollisionBeam", loanRow.get("loan_ass.anticollision_beam"));
                paramMap.put("frontLongeronLh", loanRow.get("loan_ass.front_longeron_lh"));
                paramMap.put("frontLongeronRh", loanRow.get("loan_ass.front_longeron_rh"));
                paramMap.put("rearLongeronLh", loanRow.get("loan_ass.rear_longeron_lh"));
                paramMap.put("rearLongeronRh", loanRow.get("loan_ass.rear_longeron_rh"));
                paramMap.put("frontPanelLh", loanRow.get("loan_ass.front_panel_lh"));
                paramMap.put("frontPanelRh", loanRow.get("loan_ass.front_panel_rh"));
                paramMap.put("frontAbsorberBearingLh", loanRow.get("loan_ass.front_absorber_bearing_lh"));
                paramMap.put("frontAbsorberBearingRh", loanRow.get("loan_ass.front_absorber_bearing_rh"));
                paramMap.put("firewall", loanRow.get("loan_ass.firewall"));
                paramMap.put("anticollisionBeamRear", loanRow.get("loan_ass.anticollision_beam_rear"));
                paramMap.put("spareWheelMulde", loanRow.get("loan_ass.spare_wheel_mulde"));
                paramMap.put("rearWaterChannelLh", loanRow.get("loan_ass.rear_water_channel_lh"));
                paramMap.put("rearWaterChannelRh", loanRow.get("loan_ass.rear_water_channel_rh"));
                paramMap.put("backPanel", loanRow.get("loan_ass.back_panel"));
                paramMap.put("aPillarRh2", loanRow.get("loan_ass.a_pillar_rh2"));
                paramMap.put("aPillarLh2", loanRow.get("loan_ass.a_pillar_lh2"));
                paramMap.put("bPillarRh2", loanRow.get("loan_ass.b_pillar_rh2"));
                paramMap.put("bPillarLh2", loanRow.get("loan_ass.b_pillar_lh2"));
                paramMap.put("cPillarRh2", loanRow.get("loan_ass.c_pillar_rh2"));
                paramMap.put("cPillarLh2", loanRow.get("loan_ass.c_pillar_lh2"));
                paramMap.put("dPillarRh2", loanRow.get("loan_ass.d_pillar_rh2"));
                paramMap.put("dPillarLh2", loanRow.get("loan_ass.d_pillar_lh2"));
                paramMap.put("topBeamRh", loanRow.get("loan_ass.top_beam_rh"));
                paramMap.put("topBeamLh", loanRow.get("loan_ass.top_beam_lh"));
                paramMap.put("bottomBeamRh", loanRow.get("loan_ass.bottom_beam_rh"));
                paramMap.put("bottomBeamLh", loanRow.get("loan_ass.bottom_beam_lh"));
                paramMap.put("rearPanelRh", loanRow.get("loan_ass.rear_panel_rh"));
                paramMap.put("rearPanelLh", loanRow.get("loan_ass.rear_panel_lh"));
                paramMap.put("frontHingeBracketRh", loanRow.get("loan_ass.front_hinge_bracket_rh"));
                paramMap.put("frontHingeBracketLh", loanRow.get("loan_ass.front_hinge_bracket_lh"));
                paramMap.put("rearHingeBracketRh", loanRow.get("loan_ass.rear_hinge_bracket_rh"));
                paramMap.put("rearHingeBracketLh", loanRow.get("loan_ass.rear_hinge_bracket_lh"));
                paramMap.put("mainEngineBundle", loanRow.get("loan_ass.main_engine_bundle"));
                paramMap.put("seatSlideRail", loanRow.get("loan_ass.seat_slide_rail"));
                paramMap.put("inCarWire", loanRow.get("loan_ass.in_car_wire"));
                paramMap.put("seatBelt", loanRow.get("loan_ass.seat_belt"));
                paramMap.put("trunkCorner", loanRow.get("loan_ass.trunk_corner"));
                paramMap.put("seatCushion", loanRow.get("loan_ass.seat_cushion"));
                paramMap.put("fuseBoxEcu", loanRow.get("loan_ass.fuse_box_ecu"));
                paramMap.put("rubberCarpets", loanRow.get("loan_ass.rubber_carpets"));
                paramMap.put("ashtrayBottom", loanRow.get("loan_ass.ashtray_bottom"));
                paramMap.put("insulationCotton", loanRow.get("loan_ass.insulation_cotton"));
                paramMap.put("rubberProduct", loanRow.get("loan_ass.rubber_product"));
                paramMap.put("vehicleCoverCockpit", loanRow.get("loan_ass.vehicle_cover_cockpit"));
                paramMap.put("valveCoverGasket", loanRow.get("loan_ass.valve_cover_gasket"));
                paramMap.put("engineHead", loanRow.get("loan_ass.engine_head"));
                paramMap.put("powerSteeringPump", loanRow.get("loan_ass.power_steering_pump"));
                paramMap.put("engineAssy", loanRow.get("loan_ass.engine_assy"));
                paramMap.put("cistern", loanRow.get("loan_ass.cistern"));
                paramMap.put("transmissionAssembly", loanRow.get("loan_ass.transmission_assembly"));
                paramMap.put("passengersAirbag", loanRow.get("loan_ass.passengers_airbag"));
                paramMap.put("driversAirbag", loanRow.get("loan_ass.drivers_airbag"));
                paramMap.put("frontSideAirbags", loanRow.get("loan_ass.front_side_airbags"));
                paramMap.put("rearSideAirbags", loanRow.get("loan_ass.rear_side_airbags"));
                paramMap.put("frontHeadBag", loanRow.get("loan_ass.front_head_bag"));
                paramMap.put("bearHeadBag", loanRow.get("loan_ass.bear_head_bag"));
                paramMap.put("rearSeat", loanRow.get("loan_ass.rear_seat"));
                paramMap.put("centerConsole", loanRow.get("loan_ass.center_console"));
                paramMap.put("armrestBox", loanRow.get("loan_ass.armrest_box"));
                paramMap.put("carStorageBox", loanRow.get("loan_ass.car_storage_box"));
                paramMap.put("shield", loanRow.get("loan_ass.shield"));
                paramMap.put("steeringWheel", loanRow.get("loan_ass.steering_wheel"));
                paramMap.put("mainDrivingSeat", loanRow.get("loan_ass.main_driving_seat"));
                paramMap.put("assistantSeat", loanRow.get("loan_ass.assistant_seat"));
                paramMap.put("carCarpet", loanRow.get("loan_ass.car_carpet"));
                paramMap.put("instrumentDesk", loanRow.get("loan_ass.instrument_desk"));
                paramMap.put("chaboshiRating", loanRow.get("loan_ass.chaboshi_rating"));
                paramMap.put("insuranceCompensation", loanRow.get("loan_ass.insurance_compensation"));
                paramMap.put("brandCountry", loanRow.get("loan_ass.brand_country"));
                paramMap.put("carColor", loanRow.get("loan_ass.car_color"));
                paramMap.put("invoicePrice", loanRow.get("loan_ass.invoice_price"));
                paramMap.put("finalInterfacePrice", loanRow.get("loan_ass.final_interface_price"));
                paramMap.put("carAge", loanRow.get("loan_ass.car_age"));

                JSONObject jsonObject = riskClient.getTrueScore(paramMap);

                if(jsonObject.get("html") != null){
                    html.append(jsonObject.get("html"));
                }
            }

            return buildResultRow(html.toString());
        }
    }

    @Override
    public Row getInitialPrice(String loanNo, String pageType) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loanNo);

        StringBuffer html = new StringBuffer();

        if(loanRow != null){
            Map<String, Object> paramMap = new HashMap<String, Object>();

            paramMap.put("pageType", pageType);
            paramMap.put("isForceQuery", 0);
            paramMap.put("carColor", loanRow.get("loan_car.car_color"));
            paramMap.put("interfacePrice", loanRow.get("loan_ass.interface_price"));
            paramMap.put("carAge", loanRow.get("loan_car.car_age"));
            paramMap.put("carRetention", loanRow.get("loan_car.car_retention"));
            paramMap.put("mileage", loanRow.get("loan_car.mileage"));
            paramMap.put("vehicleUsage", loanRow.get("loan_car.vehicle_usage"));
            paramMap.put("assessorRating", loanRow.get("loan_ass.assessor_rating"));
            paramMap.put("assessorWorkAge", loanRow.get("loan_ass.assessor_work_age"));
            paramMap.put("sellerWorkAge", loanRow.get("loan_ass.seller_work_age"));
            paramMap.put("chaboshiRating", loanRow.get("loan_car.chaboshi_rating"));
            paramMap.put("insuranceCompensation", loanRow.get("loan_ass.insurance_compensation"));
            paramMap.put("fuelType", loanRow.get("loan_car.fuel_type"));

            JSONObject jsonObject = riskClient.getInitialPrice(paramMap);

            if(jsonObject.get("html") != null){
                html.append(jsonObject.get("html"));
            }
        }

        return buildResultRow(html.toString());
    }

    @Override
    public Row getTrueInitialPrice(String loanNo, String pageType) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loanNo);

        StringBuffer html = new StringBuffer();

        if(loanRow != null){
            Map<String, Object> paramMap = new HashMap<String, Object>();

            paramMap.put("pageType", pageType);
            paramMap.put("isForceQuery", 0);
            paramMap.put("carColor", loanRow.get("loan_car.car_color"));
            paramMap.put("interfacePrice", loanRow.get("loan_ass.interface_price"));
            paramMap.put("carAge", loanRow.get("loan_car.car_age"));
            paramMap.put("carRetention", loanRow.get("loan_car.car_retention"));
            paramMap.put("mileage", loanRow.get("loan_car.mileage"));
            paramMap.put("vehicleUsage", loanRow.get("loan_car.vehicle_usage"));
            paramMap.put("chaboshiRating", loanRow.get("loan_car.chaboshi_rating"));
            paramMap.put("insuranceCompensation", loanRow.get("loan_ass.insurance_compensation"));
            paramMap.put("fuelType", loanRow.get("loan_car.fuel_type"));

            JSONObject jsonObject = riskClient.getTrueInitialPrice(paramMap);

            if(jsonObject.get("html") != null){
                html.append(jsonObject.get("html"));
            }
        }

        return buildResultRow(html.toString());
    }

    @Override
    public Row getFinalPrice(String loanNo, String pageType) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loanNo);

        StringBuffer html = new StringBuffer();

        if(loanRow != null){
            Map<String, Object> paramMap = new HashMap<String, Object>();

            paramMap.put("pageType", pageType);
            paramMap.put("isForceQuery", 0);
            paramMap.put("carRetention", loanRow.get("loan_ass.car_retention"));
            paramMap.put("mileage", loanRow.get("loan_ass.mileage"));
            paramMap.put("mileageAbnormal", loanRow.get("loan_ass.mileage_abnormal"));
            paramMap.put("vehicleUsage", loanRow.get("loan_ass.vehicle_usage"));
            paramMap.put("retirementYear", loanRow.get("loan_ass.retirement_year"));
            paramMap.put("fuelType", loanRow.get("loan_ass.fuel_type"));
            paramMap.put("transferCount", loanRow.get("loan_ass.transfer_count"));
            paramMap.put("engineLid", loanRow.get("loan_ass.engine_lid"));
            paramMap.put("trunkLid", loanRow.get("loan_ass.trunk_lid"));
            paramMap.put("rearBumper", loanRow.get("loan_ass.rear_bumper"));
            paramMap.put("frontBumper", loanRow.get("loan_ass.front_bumper"));
            paramMap.put("panelFrontFenderRh", loanRow.get("loan_ass.panel_front_fender_rh"));
            paramMap.put("panelFrontFenderLh", loanRow.get("loan_ass.panel_front_fender_lh"));
            paramMap.put("panelRearFenderRh", loanRow.get("loan_ass.panel_rear_fender_rh"));
            paramMap.put("panelRearFenderLh", loanRow.get("loan_ass.panel_rear_fender_lh"));
            paramMap.put("frontDoorRh", loanRow.get("loan_ass.front_door_rh"));
            paramMap.put("frontDoorLh", loanRow.get("loan_ass.front_door_lh"));
            paramMap.put("rearDoorRh", loanRow.get("loan_ass.rear_door_rh"));
            paramMap.put("rearDoorLh", loanRow.get("loan_ass.rear_door_lh"));
            paramMap.put("aPillarRh", loanRow.get("loan_ass.a_pillar_rh"));
            paramMap.put("aPillarLh", loanRow.get("loan_ass.a_pillar_lh"));
            paramMap.put("bPillarRh", loanRow.get("loan_ass.b_pillar_rh"));
            paramMap.put("bPillarLh", loanRow.get("loan_ass.b_pillar_lh"));
            paramMap.put("cPillarRh", loanRow.get("loan_ass.c_pillar_rh"));
            paramMap.put("cPillarLh", loanRow.get("loan_ass.c_pillar_lh"));
            paramMap.put("carRoof", loanRow.get("loan_ass.car_roof"));
            paramMap.put("frontWindshield", loanRow.get("loan_ass.front_windshield"));
            paramMap.put("rearWindshield", loanRow.get("loan_ass.rear_windshield"));
            paramMap.put("frontHeadlightLh", loanRow.get("loan_ass.front_headlight_lh"));
            paramMap.put("frontHeadlightRh", loanRow.get("loan_ass.front_headlight_rh"));
            paramMap.put("rearTaillampLh", loanRow.get("loan_ass.rear_taillamp_lh"));
            paramMap.put("rearTaillampRh", loanRow.get("loan_ass.rear_taillamp_rh"));
            paramMap.put("cisternFrame", loanRow.get("loan_ass.cistern_frame"));
            paramMap.put("anticollisionBeam", loanRow.get("loan_ass.anticollision_beam"));
            paramMap.put("frontLongeronLh", loanRow.get("loan_ass.front_longeron_lh"));
            paramMap.put("frontLongeronRh", loanRow.get("loan_ass.front_longeron_rh"));
            paramMap.put("rearLongeronLh", loanRow.get("loan_ass.rear_longeron_lh"));
            paramMap.put("rearLongeronRh", loanRow.get("loan_ass.rear_longeron_rh"));
            paramMap.put("frontPanelLh", loanRow.get("loan_ass.front_panel_lh"));
            paramMap.put("frontPanelRh", loanRow.get("loan_ass.front_panel_rh"));
            paramMap.put("frontAbsorberBearingLh", loanRow.get("loan_ass.front_absorber_bearing_lh"));
            paramMap.put("frontAbsorberBearingRh", loanRow.get("loan_ass.front_absorber_bearing_rh"));
            paramMap.put("firewall", loanRow.get("loan_ass.firewall"));
            paramMap.put("anticollisionBeamRear", loanRow.get("loan_ass.anticollision_beam_rear"));
            paramMap.put("spareWheelMulde", loanRow.get("loan_ass.spare_wheel_mulde"));
            paramMap.put("rearWaterChannelLh", loanRow.get("loan_ass.rear_water_channel_lh"));
            paramMap.put("rearWaterChannelRh", loanRow.get("loan_ass.rear_water_channel_rh"));
            paramMap.put("backPanel", loanRow.get("loan_ass.back_panel"));
            paramMap.put("aPillarRh2", loanRow.get("loan_ass.a_pillar_rh2"));
            paramMap.put("aPillarLh2", loanRow.get("loan_ass.a_pillar_lh2"));
            paramMap.put("bPillarRh2", loanRow.get("loan_ass.b_pillar_rh2"));
            paramMap.put("bPillarLh2", loanRow.get("loan_ass.b_pillar_lh2"));
            paramMap.put("cPillarRh2", loanRow.get("loan_ass.c_pillar_rh2"));
            paramMap.put("cPillarLh2", loanRow.get("loan_ass.c_pillar_lh2"));
            paramMap.put("dPillarRh2", loanRow.get("loan_ass.d_pillar_rh2"));
            paramMap.put("dPillarLh2", loanRow.get("loan_ass.d_pillar_lh2"));
            paramMap.put("topBeamRh", loanRow.get("loan_ass.top_beam_rh"));
            paramMap.put("topBeamLh", loanRow.get("loan_ass.top_beam_lh"));
            paramMap.put("bottomBeamRh", loanRow.get("loan_ass.bottom_beam_rh"));
            paramMap.put("bottomBeamLh", loanRow.get("loan_ass.bottom_beam_lh"));
            paramMap.put("rearPanelRh", loanRow.get("loan_ass.rear_panel_rh"));
            paramMap.put("rearPanelLh", loanRow.get("loan_ass.rear_panel_lh"));
            paramMap.put("frontHingeBracketRh", loanRow.get("loan_ass.front_hinge_bracket_rh"));
            paramMap.put("frontHingeBracketLh", loanRow.get("loan_ass.front_hinge_bracket_lh"));
            paramMap.put("rearHingeBracketRh", loanRow.get("loan_ass.rear_hinge_bracket_rh"));
            paramMap.put("rearHingeBracketLh", loanRow.get("loan_ass.rear_hinge_bracket_lh"));
            paramMap.put("mainEngineBundle", loanRow.get("loan_ass.main_engine_bundle"));
            paramMap.put("seatSlideRail", loanRow.get("loan_ass.seat_slide_rail"));
            paramMap.put("inCarWire", loanRow.get("loan_ass.in_car_wire"));
            paramMap.put("seatBelt", loanRow.get("loan_ass.seat_belt"));
            paramMap.put("trunkCorner", loanRow.get("loan_ass.trunk_corner"));
            paramMap.put("seatCushion", loanRow.get("loan_ass.seat_cushion"));
            paramMap.put("fuseBoxEcu", loanRow.get("loan_ass.fuse_box_ecu"));
            paramMap.put("rubberCarpets", loanRow.get("loan_ass.rubber_carpets"));
            paramMap.put("ashtrayBottom", loanRow.get("loan_ass.ashtray_bottom"));
            paramMap.put("insulationCotton", loanRow.get("loan_ass.insulation_cotton"));
            paramMap.put("rubberProduct", loanRow.get("loan_ass.rubber_product"));
            paramMap.put("vehicleCoverCockpit", loanRow.get("loan_ass.vehicle_cover_cockpit"));
            paramMap.put("valveCoverGasket", loanRow.get("loan_ass.valve_cover_gasket"));
            paramMap.put("engineHead", loanRow.get("loan_ass.engine_head"));
            paramMap.put("powerSteeringPump", loanRow.get("loan_ass.power_steering_pump"));
            paramMap.put("engineAssy", loanRow.get("loan_ass.engine_assy"));
            paramMap.put("cistern", loanRow.get("loan_ass.cistern"));
            paramMap.put("transmissionAssembly", loanRow.get("loan_ass.transmission_assembly"));
            paramMap.put("passengersAirbag", loanRow.get("loan_ass.passengers_airbag"));
            paramMap.put("driversAirbag", loanRow.get("loan_ass.drivers_airbag"));
            paramMap.put("frontSideAirbags", loanRow.get("loan_ass.front_side_airbags"));
            paramMap.put("rearSideAirbags", loanRow.get("loan_ass.rear_side_airbags"));
            paramMap.put("frontHeadBag", loanRow.get("loan_ass.front_head_bag"));
            paramMap.put("bearHeadBag", loanRow.get("loan_ass.bear_head_bag"));
            paramMap.put("rearSeat", loanRow.get("loan_ass.rear_seat"));
            paramMap.put("centerConsole", loanRow.get("loan_ass.center_console"));
            paramMap.put("armrestBox", loanRow.get("loan_ass.armrest_box"));
            paramMap.put("carStorageBox", loanRow.get("loan_ass.car_storage_box"));
            paramMap.put("shield", loanRow.get("loan_ass.shield"));
            paramMap.put("steeringWheel", loanRow.get("loan_ass.steering_wheel"));
            paramMap.put("mainDrivingSeat", loanRow.get("loan_ass.main_driving_seat"));
            paramMap.put("assistantSeat", loanRow.get("loan_ass.assistant_seat"));
            paramMap.put("carCarpet", loanRow.get("loan_ass.car_carpet"));
            paramMap.put("instrumentDesk", loanRow.get("loan_ass.instrument_desk"));
            paramMap.put("assessorRating", loanRow.get("loan_ass.assessor_rating"));
            paramMap.put("assessorWorkAge", loanRow.get("loan_ass.assessor_work_age"));
            paramMap.put("sellerWorkAge", loanRow.get("loan_ass.seller_work_age"));
            paramMap.put("chaboshiRating", loanRow.get("loan_ass.chaboshi_rating"));
            paramMap.put("insuranceCompensation", loanRow.get("loan_ass.insurance_compensation"));
            paramMap.put("brandCountry", loanRow.get("loan_ass.brand_country"));
            paramMap.put("carColor", loanRow.get("loan_ass.car_color"));
            paramMap.put("invoicePrice", loanRow.get("loan_ass.invoice_price"));
            paramMap.put("finalInterfacePrice", loanRow.get("loan_ass.final_interface_price"));
            paramMap.put("carAge", loanRow.get("loan_ass.car_age"));

            JSONObject jsonObject = riskClient.getFinalPrice(paramMap);

            if(jsonObject.get("html") != null){
                html.append(jsonObject.get("html"));
            }
        }

        return buildResultRow(html.toString());
    }

    @Override
    public Row getTrueFinalPrice(String loanNo, String pageType) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loanNo);

        StringBuffer html = new StringBuffer();

        if(loanRow != null){
            Map<String, Object> paramMap = new HashMap<String, Object>();

            paramMap.put("pageType", pageType);
            paramMap.put("isForceQuery", 0);
            paramMap.put("carRetention", loanRow.get("loan_ass.car_retention"));
            paramMap.put("mileageAbnormal", loanRow.get("loan_ass.mileage_abnormal"));
            paramMap.put("mileage", loanRow.get("loan_ass.mileage"));
            paramMap.put("vehicleUsage", loanRow.get("loan_ass.vehicle_usage"));
            paramMap.put("retirementYear", loanRow.get("loan_ass.retirement_year"));
            paramMap.put("fuelType", loanRow.get("loan_ass.fuel_type"));
            paramMap.put("transferCount", loanRow.get("loan_ass.transfer_count"));
            paramMap.put("engineLid", loanRow.get("loan_ass.engine_lid"));
            paramMap.put("trunkLid", loanRow.get("loan_ass.trunk_lid"));
            paramMap.put("rearBumper", loanRow.get("loan_ass.rear_bumper"));
            paramMap.put("frontBumper", loanRow.get("loan_ass.front_bumper"));
            paramMap.put("panelFrontFenderRh", loanRow.get("loan_ass.panel_front_fender_rh"));
            paramMap.put("panelFrontFenderLh", loanRow.get("loan_ass.panel_front_fender_lh"));
            paramMap.put("panelRearFenderRh", loanRow.get("loan_ass.panel_rear_fender_rh"));
            paramMap.put("panelRearFenderLh", loanRow.get("loan_ass.panel_rear_fender_lh"));
            paramMap.put("frontDoorRh", loanRow.get("loan_ass.front_door_rh"));
            paramMap.put("frontDoorLh", loanRow.get("loan_ass.front_door_lh"));
            paramMap.put("rearDoorRh", loanRow.get("loan_ass.rear_door_rh"));
            paramMap.put("rearDoorLh", loanRow.get("loan_ass.rear_door_lh"));
            paramMap.put("aPillarRh", loanRow.get("loan_ass.a_pillar_rh"));
            paramMap.put("aPillarLh", loanRow.get("loan_ass.a_pillar_lh"));
            paramMap.put("bPillarRh", loanRow.get("loan_ass.b_pillar_rh"));
            paramMap.put("bPillarLh", loanRow.get("loan_ass.b_pillar_lh"));
            paramMap.put("cPillarRh", loanRow.get("loan_ass.c_pillar_rh"));
            paramMap.put("cPillarLh", loanRow.get("loan_ass.c_pillar_lh"));
            paramMap.put("carRoof", loanRow.get("loan_ass.car_roof"));
            paramMap.put("frontWindshield", loanRow.get("loan_ass.front_windshield"));
            paramMap.put("rearWindshield", loanRow.get("loan_ass.rear_windshield"));
            paramMap.put("frontHeadlightLh", loanRow.get("loan_ass.front_headlight_lh"));
            paramMap.put("frontHeadlightRh", loanRow.get("loan_ass.front_headlight_rh"));
            paramMap.put("rearTaillampLh", loanRow.get("loan_ass.rear_taillamp_lh"));
            paramMap.put("rearTaillampRh", loanRow.get("loan_ass.rear_taillamp_rh"));
            paramMap.put("cisternFrame", loanRow.get("loan_ass.cistern_frame"));
            paramMap.put("anticollisionBeam", loanRow.get("loan_ass.anticollision_beam"));
            paramMap.put("frontLongeronLh", loanRow.get("loan_ass.front_longeron_lh"));
            paramMap.put("frontLongeronRh", loanRow.get("loan_ass.front_longeron_rh"));
            paramMap.put("rearLongeronLh", loanRow.get("loan_ass.rear_longeron_lh"));
            paramMap.put("rearLongeronRh", loanRow.get("loan_ass.rear_longeron_rh"));
            paramMap.put("frontPanelLh", loanRow.get("loan_ass.front_panel_lh"));
            paramMap.put("frontPanelRh", loanRow.get("loan_ass.front_panel_rh"));
            paramMap.put("frontAbsorberBearingLh", loanRow.get("loan_ass.front_absorber_bearing_lh"));
            paramMap.put("frontAbsorberBearingRh", loanRow.get("loan_ass.front_absorber_bearing_rh"));
            paramMap.put("firewall", loanRow.get("loan_ass.firewall"));
            paramMap.put("anticollisionBeamRear", loanRow.get("loan_ass.anticollision_beam_rear"));
            paramMap.put("spareWheelMulde", loanRow.get("loan_ass.spare_wheel_mulde"));
            paramMap.put("rearWaterChannelLh", loanRow.get("loan_ass.rear_water_channel_lh"));
            paramMap.put("rearWaterChannelRh", loanRow.get("loan_ass.rear_water_channel_rh"));
            paramMap.put("backPanel", loanRow.get("loan_ass.back_panel"));
            paramMap.put("aPillarRh2", loanRow.get("loan_ass.a_pillar_rh2"));
            paramMap.put("aPillarLh2", loanRow.get("loan_ass.a_pillar_lh2"));
            paramMap.put("bPillarRh2", loanRow.get("loan_ass.b_pillar_rh2"));
            paramMap.put("bPillarLh2", loanRow.get("loan_ass.b_pillar_lh2"));
            paramMap.put("cPillarRh2", loanRow.get("loan_ass.c_pillar_rh2"));
            paramMap.put("cPillarLh2", loanRow.get("loan_ass.c_pillar_lh2"));
            paramMap.put("dPillarRh2", loanRow.get("loan_ass.d_pillar_rh2"));
            paramMap.put("dPillarLh2", loanRow.get("loan_ass.d_pillar_lh2"));
            paramMap.put("topBeamRh", loanRow.get("loan_ass.top_beam_rh"));
            paramMap.put("topBeamLh", loanRow.get("loan_ass.top_beam_lh"));
            paramMap.put("bottomBeamRh", loanRow.get("loan_ass.bottom_beam_rh"));
            paramMap.put("bottomBeamLh", loanRow.get("loan_ass.bottom_beam_lh"));
            paramMap.put("rearPanelRh", loanRow.get("loan_ass.rear_panel_rh"));
            paramMap.put("rearPanelLh", loanRow.get("loan_ass.rear_panel_lh"));
            paramMap.put("frontHingeBracketRh", loanRow.get("loan_ass.front_hinge_bracket_rh"));
            paramMap.put("frontHingeBracketLh", loanRow.get("loan_ass.front_hinge_bracket_lh"));
            paramMap.put("rearHingeBracketRh", loanRow.get("loan_ass.rear_hinge_bracket_rh"));
            paramMap.put("rearHingeBracketLh", loanRow.get("loan_ass.rear_hinge_bracket_lh"));
            paramMap.put("mainEngineBundle", loanRow.get("loan_ass.main_engine_bundle"));
            paramMap.put("seatSlideRail", loanRow.get("loan_ass.seat_slide_rail"));
            paramMap.put("inCarWire", loanRow.get("loan_ass.in_car_wire"));
            paramMap.put("seatBelt", loanRow.get("loan_ass.seat_belt"));
            paramMap.put("trunkCorner", loanRow.get("loan_ass.trunk_corner"));
            paramMap.put("seatCushion", loanRow.get("loan_ass.seat_cushion"));
            paramMap.put("fuseBoxEcu", loanRow.get("loan_ass.fuse_box_ecu"));
            paramMap.put("rubberCarpets", loanRow.get("loan_ass.rubber_carpets"));
            paramMap.put("ashtrayBottom", loanRow.get("loan_ass.ashtray_bottom"));
            paramMap.put("insulationCotton", loanRow.get("loan_ass.insulation_cotton"));
            paramMap.put("rubberProduct", loanRow.get("loan_ass.rubber_product"));
            paramMap.put("vehicleCoverCockpit", loanRow.get("loan_ass.vehicle_cover_cockpit"));
            paramMap.put("valveCoverGasket", loanRow.get("loan_ass.valve_cover_gasket"));
            paramMap.put("engineHead", loanRow.get("loan_ass.engine_head"));
            paramMap.put("powerSteeringPump", loanRow.get("loan_ass.power_steering_pump"));
            paramMap.put("engineAssy", loanRow.get("loan_ass.engine_assy"));
            paramMap.put("cistern", loanRow.get("loan_ass.cistern"));
            paramMap.put("transmissionAssembly", loanRow.get("loan_ass.transmission_assembly"));
            paramMap.put("passengersAirbag", loanRow.get("loan_ass.passengers_airbag"));
            paramMap.put("driversAirbag", loanRow.get("loan_ass.drivers_airbag"));
            paramMap.put("frontSideAirbags", loanRow.get("loan_ass.front_side_airbags"));
            paramMap.put("rearSideAirbags", loanRow.get("loan_ass.rear_side_airbags"));
            paramMap.put("frontHeadBag", loanRow.get("loan_ass.front_head_bag"));
            paramMap.put("bearHeadBag", loanRow.get("loan_ass.bear_head_bag"));
            paramMap.put("rearSeat", loanRow.get("loan_ass.rear_seat"));
            paramMap.put("centerConsole", loanRow.get("loan_ass.center_console"));
            paramMap.put("armrestBox", loanRow.get("loan_ass.armrest_box"));
            paramMap.put("carStorageBox", loanRow.get("loan_ass.car_storage_box"));
            paramMap.put("shield", loanRow.get("loan_ass.shield"));
            paramMap.put("steeringWheel", loanRow.get("loan_ass.steering_wheel"));
            paramMap.put("mainDrivingSeat", loanRow.get("loan_ass.main_driving_seat"));
            paramMap.put("assistantSeat", loanRow.get("loan_ass.assistant_seat"));
            paramMap.put("carCarpet", loanRow.get("loan_ass.car_carpet"));
            paramMap.put("instrumentDesk", loanRow.get("loan_ass.instrument_desk"));
            paramMap.put("chaboshiRating", loanRow.get("loan_ass.chaboshi_rating"));
            paramMap.put("insuranceCompensation", loanRow.get("loan_ass.insurance_compensation"));
            paramMap.put("brandCountry", loanRow.get("loan_ass.brand_country"));
            paramMap.put("carColor", loanRow.get("loan_ass.car_color"));
            paramMap.put("invoicePrice", loanRow.get("loan_ass.invoice_price"));
            paramMap.put("finalInterfacePrice", loanRow.get("loan_ass.final_interface_price"));
            paramMap.put("carAge", loanRow.get("loan_ass.car_age"));

            JSONObject jsonObject = riskClient.getTrueFinalPrice(paramMap);

            if(jsonObject.get("html") != null){
                html.append(jsonObject.get("html"));
            }
        }

        return buildResultRow(html.toString());
    }

    @Override
    public Row getRepairReport(String loanNo, String pageType) throws Exception {
        String querySql = "select vin_code, engine_code from loan_ass where loan_no = '"+ loanNo +"'";
        Store store = baseDao.query(querySql);

        StringBuffer html = new StringBuffer();

        if(store != null && store.size() > 0){
            Map<String, Object> paramMap = new HashMap<String, Object>();

            paramMap.put("pageType", pageType);
            paramMap.put("isForceQuery", 0);
            paramMap.put("carCode", store.get(0).get("vin_code"));
            paramMap.put("engineCode", store.get(0).get("engine_code"));

            JSONObject jsonObject = riskClient.getRepairReport(paramMap);

            if(jsonObject.get("html") != null){
                html.append(jsonObject.get("html"));
            }
        }

        return buildResultRow(html.toString());
    }

    @Override
    public Row getThirdPartyReport(String loanNo, String pageType) throws Exception {
        String querySql = "select a.cust_name, a.idcard, a.cust_phone, b.car_plate, b.cjh, b.fdjh from loan_cust a "
                        + "left join loan_car b on a.loan_no = b.loan_no where a.loan_no = '"+ loanNo +"'";

        Store store = baseDao.query(querySql);

        StringBuffer html = new StringBuffer();

        if(store != null && store.size() > 0){
            String contactQuerySql = "select contacts_name, contacts_mobile, contacts_idcard from loan_info_contacts "
                                   + "where status = '1' and contacts_relation = '02' and loan_no = '"+ loanNo +"'";

            Store contactStore = baseDao.query(contactQuerySql);

            String carPlate = store.get(0).get("car_plate") != null? store.get(0).get("car_plate").toString() : "";

            Map<String, Object> paramMap = new HashMap<String, Object>();

            paramMap.put("pageType", pageType);
            paramMap.put("isForceQuery", 0);
            paramMap.put("name", store.get(0).get("cust_name"));
            paramMap.put("idCard", store.get(0).get("idcard"));
            paramMap.put("mobile", store.get(0).get("cust_phone"));
            paramMap.put("carNumber", carPlate);
            paramMap.put("carCode", store.get(0).get("cjh"));
            paramMap.put("carDriveNumber", store.get(0).get("fdjh"));
            paramMap.put("cityName", CarPlateCityEnum.getCarPlateCityEnum(carPlate).getCityName());

            if(contactStore != null && contactStore.size() > 0){
                paramMap.put("contactsName", contactStore.get(0).get("contacts_name"));
                paramMap.put("contactsIdCard", contactStore.get(0).get("contacts_idcard"));
                paramMap.put("contactsMobile", contactStore.get(0).get("contacts_mobile"));
            }

            JSONObject jsonObject = riskClient.getThirdPartyReport(paramMap);

            if(jsonObject.get("html") != null){
                html.append(jsonObject.get("html"));
            }
        }

        return buildResultRow(html.toString());
    }

    @Override
    public Row getAccidentReport(String loanNo, String pageType) throws Exception {
        String querySql = "select cjh from loan_car where loan_no = '"+ loanNo +"'";
        Store store = baseDao.query(querySql);

        StringBuffer html = new StringBuffer();

        if(store != null && store.size() > 0){
            Map<String, Object> paramMap = new HashMap<String, Object>();

            paramMap.put("pageType", pageType);
            paramMap.put("isForceQuery", 0);
            paramMap.put("carCode", store.get(0).get("cjh"));

            JSONObject jsonObject = riskClient.getAccidentReport(paramMap);

            if(jsonObject.get("html") != null){
                html.append(jsonObject.get("html"));
            }
        }

        return buildResultRow(html.toString());
    }

    @Override
    public Row buildOperatorReport(String loanNo, String pageType) throws Exception {
        String querySql = "select cust_phone from loan_cust where loan_no = '"+ loanNo +"'";
        Store store = baseDao.query(querySql);

        StringBuffer html = new StringBuffer();

        if(store != null && store.size() > 0){
            Map<String, Object> paramMap = new HashMap<String, Object>();

            paramMap.put("pageType", pageType);
            paramMap.put("phone", store.get(0).get("cust_phone"));

            JSONObject jsonObject = riskClient.buildOperatorReport(paramMap);

            if(jsonObject.get("html") != null){
                html.append(jsonObject.get("html"));
            }
        }

        return buildResultRow(html.toString());
    }

    @Override
    public Row getOperatorReport(String loanNo, String pageType) throws Exception {
        String querySql = "select cust_phone from loan_cust where loan_no = '"+ loanNo +"'";
        Store store = baseDao.query(querySql);

        StringBuffer html = new StringBuffer();

        if(store != null && store.size() > 0){
            Map<String, Object> paramMap = new HashMap<String, Object>();

            paramMap.put("pageType", pageType);
            paramMap.put("isForceQuery", 0);
            paramMap.put("phone", store.get(0).get("cust_phone"));

            JSONObject jsonObject = riskClient.getOperatorReport(paramMap);

            if(jsonObject.get("html") != null){
                html.append(jsonObject.get("html"));
            }
        }

        return buildResultRow(html.toString());
    }
}
