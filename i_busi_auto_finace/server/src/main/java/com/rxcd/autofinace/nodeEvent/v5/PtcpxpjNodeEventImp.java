/**
 * 版权所有：版权所有：重庆易房网络科技股份有限公司
 * Chongqing YiFang Network Technology Co. LTD.  All Rights Reserved
 * 创建者：HeSenLin   创建时期：  2016年7月14日
 */
package com.rxcd.autofinace.nodeEvent.v5;


import org.appframe.wf.engine.AbstractWorkFlowNodeEvent;
import org.appframe.wf.util.Param;
import org.springframework.stereotype.Component;

/**
 * 产品选配节点事件
 */
@Component
public class PtcpxpjNodeEventImp extends AbstractWorkFlowNodeEvent {

	@Override
	public void workFlowNodeStart(String flow_node_no, Param p) throws Exception {
		checkTempletFlowNo("1");
		String loanNo = getTableKeyNo();
		String sql="update loan_info_et set selectable_capital_no="
				+ "(select group_concat(capital_no) from loan_capital where capital_no in ('24493e10-09bb-4480-b9ee-ec854ab89b15','e998bcb7-fb2f-11e7-91ce-00e04c6832c6','6b9a2d0e-7f43-427e-9aea-794195eddc7f')) "
				+ "where loan_no='"+loanNo+"'";
		this.baseDao.execute(sql);
	}

	/**
	 * 流程节点结束时回调方法
	 *
	 * @param flow_node_no
	 * @param p
	 * @throws Exception
	 */
	@Override
	public void workFlowNodeFinish(String flow_node_no, Param p) throws Exception {

	}

	/**
	 * 流程节点驳回时回调方法
	 *
	 * @param flow_node_no
	 * @param p
	 * @throws Exception
	 */
	@Override
	public void workFlowNodeOverrule(String flow_node_no, Param p) throws Exception {

	}

	@Override
	public void workFlowNodeVie(String flow_node_no, Param p) throws Exception {

	}
}
