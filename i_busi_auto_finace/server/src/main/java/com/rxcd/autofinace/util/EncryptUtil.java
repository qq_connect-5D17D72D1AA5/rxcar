package com.rxcd.autofinace.util;


import com.alibaba.fastjson.JSONObject;

import javax.crypto.*;
import javax.crypto.spec.DESedeKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;


/**
 * 基础加密算法类。当前支持des,md5。
 */
public class EncryptUtil {

	/**
	 * 生成签名 
	 *  
	 *  签名规则 sequenceId + PARTNER_ID + operate + body(内容) + KEY
	 * 
	 * @param content
	 * @return
	 */
	public static String sign(String content){
		return BASE64Encrypt(MD5(content));
	}
	
	/**
	 * 生成body 密文
	 * 
	 * @param key        平台对应key
	 * @param bodyJson	  需要传递的内容
	 * @return
	 * @throws Exception
	 */
	public static String bodyEncrypt(String key,JSONObject bodyJson) throws Exception {
		return BASE64Encrypt(DES3Encrypt(key, bodyJson.toJSONString()));
	}
	
	/**
	 * 解密body 内容
	 * 
	 * @param key
	 * @param bodyContent
	 * @return
	 * @throws Exception
	 */
	public static String bodyDecrypt(String key,String bodyContent) throws Exception {
		return DES3Decrypt(BASE64Decrypt(bodyContent).clone(), key);
	}
	
    /**
     * MD5值计算<p>
     * MD5的算法在RFC1321 中定义:
     * 在RFC 1321中，给出了Test suite用来检验你的实现是否正确：
     * MD5 ("") = d41d8cd98f00b204e9800998ecf8427e
     * MD5 ("a") = 0cc175b9c0f1b6a831c399e269772661
     * MD5 ("abc") = 900150983cd24fb0d6963f7d28e17f72
     * MD5 ("message digest") = f96b697d7cb7938d525a2f31aaf161d0
     * MD5 ("abcdefghijklmnopqrstuvwxyz") = c3fcd3d76192e4007dfb496cca67e13b
     *
     * @param str 源字符串
     * @return md5值
     */
    private final static byte[] MD5(String str) {
        try {
            byte[] res = str.getBytes("UTF-8");
            MessageDigest mdTemp = MessageDigest.getInstance("MD5".toUpperCase());
            mdTemp.update(res);
            byte[] hash = mdTemp.digest();
            return hash;
        } catch (Exception e) {
            return null;
        }
    }


    // 加密后解密
    private static String JM(byte[] inStr) {
        String newStr = new String(inStr);
        char[] a = newStr.toCharArray();
        for (int i = 0; i < a.length; i++) {
            a[i] = (char) (a[i] ^ 't');
        }
        String k = new String(a);
        return k;
    }


    /**
     * BASE64加密
     *
     * @param key
     * @return
     * @throws Exception
     */
    private static String BASE64Encrypt(byte[] key) {
        String edata = null;
        try {
            edata = Base64.getEncoder().encodeToString(key).trim();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return edata.replaceAll("\r|\n", "");
    }


    /**
     * BASE64解密
     *
     * @param data  key
     * @return
     * @throws Exception
     */
    private static byte[] BASE64Decrypt(String data) {
        if (data == null) return null;
        byte[] edata = null;
        try {
            edata = Base64.getDecoder().decode(data);
            return edata;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @param key 24位密钥
     * @param str 源字符串
     * @return
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws UnsupportedEncodingException
     * @throws InvalidKeySpecException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    private static byte[] DES3Encrypt(String key, String str) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, UnsupportedEncodingException, InvalidKeySpecException, IllegalBlockSizeException, BadPaddingException {

        byte[] newkey = key.getBytes();

        SecureRandom sr = new SecureRandom();

        DESedeKeySpec dks = new DESedeKeySpec(newkey);

        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DESede");

        SecretKey securekey = keyFactory.generateSecret(dks);

        Cipher cipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");

        cipher.init(Cipher.ENCRYPT_MODE, securekey, sr);

        byte[] bt = cipher.doFinal(str.getBytes("utf-8"));

        return bt;
    }


    /**
     * 解密
     *
     * @param edata
     * @param key
     * @return
     * @throws Exception
     */
    private static String DES3Decrypt(byte[] edata, String key) {
        String data = "";
        try {
            if (edata != null) {
                byte[] newkey = key.getBytes();
                DESedeKeySpec dks = new DESedeKeySpec(newkey);
                SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DESede");
                SecretKey securekey = keyFactory.generateSecret(dks);
                Cipher cipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
                cipher.init(Cipher.DECRYPT_MODE, securekey, new SecureRandom());
                byte[] bb = cipher.doFinal(edata);
                data = new String(bb, "UTF-8");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    public static void main(String[] args) {
        /*测试Key=”NWY0ZTNiMGEtYjAyNC00M2Rh”
        ToBase64(3DES(“hello”, Key)) 结果为”oCAlvyvaLYg=”
        ToBase64(MD5(“hello”)) 结果为"XUFAKrxLKna5cZ2REBfFkg=="*/
        String key = "NWY0ZTNiMGEtYjAyNC00M2Rh";
        String retMd5 = BASE64Encrypt(MD5("hello"));
        System.out.println(retMd5);
        try {
            String retDes = BASE64Encrypt(DES3Encrypt(key, "hello"));
            
            String value= DES3Decrypt(BASE64Decrypt(retDes).clone(), key);
            System.out.println(value);
            System.out.println(retDes);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
