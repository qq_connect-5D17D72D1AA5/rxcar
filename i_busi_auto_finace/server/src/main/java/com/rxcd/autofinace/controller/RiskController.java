package com.rxcd.autofinace.controller;

import com.rxcd.autofinace.service.client.RiskService;
import org.appframe.wf.db.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 风控controller
 * @author yeqingxin
 * @date 2018/12/03
 */

@RestController
@RequestMapping("/risk")
public class RiskController {
    @Autowired
    RiskService riskClient;

    @RequestMapping(value = "/getFirstRiskControl")
    public Row getFirstRiskControl(@RequestParam("loan_no") String loanNo, HttpServletRequest request) throws Exception{
        String device = request.getHeader("user-agent").toLowerCase();
        String pageType = "02";

        if (device.indexOf("android") > 0 || device.indexOf("mac os") > 0 || device.indexOf("windows phone") > 0) {
            pageType = "01";
        }

        return riskClient.getFirstRiskControl(loanNo, pageType);
    }

    @RequestMapping(value = "/getMidRiskControl")
    public Row getMidRiskControl(@RequestParam("loan_no") String loanNo, HttpServletRequest request) throws Exception{
        String device = request.getHeader("user-agent").toLowerCase();
        String pageType = "02";

        if (device.indexOf("android") > 0 || device.indexOf("mac os") > 0 || device.indexOf("windows phone") > 0) {
            pageType = "01";
        }

        return riskClient.getMidRiskControl(loanNo, pageType);
    }

    @RequestMapping(value = "/getFinalRiskControl")
    public Row getFinalRiskControl(@RequestParam("loan_no") String loanNo, HttpServletRequest request) throws Exception{
        String device = request.getHeader("user-agent").toLowerCase();
        String pageType = "02";

        if (device.indexOf("android") > 0 || device.indexOf("mac os") > 0 || device.indexOf("windows phone") > 0) {
            pageType = "01";
        }

        return riskClient.getFinalRiskControl(loanNo, pageType);
    }

    @RequestMapping(value = "/getScore")
    public Row getScore(@RequestParam("loan_no") String loanNo, HttpServletRequest request) throws Exception{
        String device = request.getHeader("user-agent").toLowerCase();
        String pageType = "02";

        if (device.indexOf("android") > 0 || device.indexOf("mac os") > 0 || device.indexOf("windows phone") > 0) {
            pageType = "01";
        }

        return riskClient.getScore(loanNo, pageType);
    }

    @RequestMapping(value = "/getTrueScore")
    public Row getTrueScore(@RequestParam("loan_no") String loanNo, HttpServletRequest request) throws Exception{
        String device = request.getHeader("user-agent").toLowerCase();
        String pageType = "02";

        if (device.indexOf("android") > 0 || device.indexOf("mac os") > 0 || device.indexOf("windows phone") > 0) {
            pageType = "01";
        }

        return riskClient.getTrueScore(loanNo, pageType);
    }

    @RequestMapping(value = "/getInitialPrice")
    public Row getInitialPrice(@RequestParam("loan_no") String loanNo, HttpServletRequest request) throws Exception{
        String device = request.getHeader("user-agent").toLowerCase();
        String pageType = "02";

        if (device.indexOf("android") > 0 || device.indexOf("mac os") > 0 || device.indexOf("windows phone") > 0) {
            pageType = "01";
        }

        return riskClient.getInitialPrice(loanNo, pageType);
    }

    @RequestMapping(value = "/getTrueInitialPrice")
    public Row getTrueInitialPrice(@RequestParam("loan_no") String loanNo, HttpServletRequest request) throws Exception{
        String device = request.getHeader("user-agent").toLowerCase();
        String pageType = "02";

        if (device.indexOf("android") > 0 || device.indexOf("mac os") > 0 || device.indexOf("windows phone") > 0) {
            pageType = "01";
        }

        return riskClient.getTrueInitialPrice(loanNo, pageType);
    }

    @RequestMapping(value = "/getFinalPrice")
    public Row getFinalPrice(@RequestParam("loan_no") String loanNo, HttpServletRequest request) throws Exception{
        String device = request.getHeader("user-agent").toLowerCase();
        String pageType = "02";

        if (device.indexOf("android") > 0 || device.indexOf("mac os") > 0 || device.indexOf("windows phone") > 0) {
            pageType = "01";
        }

        return riskClient.getFinalPrice(loanNo, pageType);
    }

    @RequestMapping(value = "/getTrueFinalPrice")
    public Row getTrueFinalPrice(@RequestParam("loan_no") String loanNo, HttpServletRequest request) throws Exception{
        String device = request.getHeader("user-agent").toLowerCase();
        String pageType = "02";

        if (device.indexOf("android") > 0 || device.indexOf("mac os") > 0 || device.indexOf("windows phone") > 0) {
            pageType = "01";
        }

        return riskClient.getTrueFinalPrice(loanNo, pageType);
    }

    @RequestMapping(value = "/getRepairReport")
    public Row getRepairReport(@RequestParam("loan_no") String loanNo, HttpServletRequest request) throws Exception{
        String device = request.getHeader("user-agent").toLowerCase();
        String pageType = "02";

        if (device.indexOf("android") > 0 || device.indexOf("mac os") > 0 || device.indexOf("windows phone") > 0) {
            pageType = "01";
        }

        return riskClient.getRepairReport(loanNo, pageType);
    }

    @RequestMapping(value = "/getThirdPartyReport")
    public Row getThirdPartyReport(@RequestParam("loan_no") String loanNo, HttpServletRequest request) throws Exception{
        String device = request.getHeader("user-agent").toLowerCase();
        String pageType = "02";

        if (device.indexOf("android") > 0 || device.indexOf("mac os") > 0 || device.indexOf("windows phone") > 0) {
            pageType = "01";
        }

        return riskClient.getThirdPartyReport(loanNo, pageType);
    }

    @RequestMapping(value = "/getAccidentReport")
    public Row getAccidentReport(@RequestParam("loan_no") String loanNo, HttpServletRequest request) throws Exception{
        String device = request.getHeader("user-agent").toLowerCase();
        String pageType = "02";

        if (device.indexOf("android") > 0 || device.indexOf("mac os") > 0 || device.indexOf("windows phone") > 0) {
            pageType = "01";
        }

        return riskClient.getAccidentReport(loanNo, pageType);
    }

    @RequestMapping(value = "/buildOperatorReport")
    public Row buildOperatorReport(@RequestParam("loan_no") String loanNo, HttpServletRequest request) throws Exception{
        String device = request.getHeader("user-agent").toLowerCase();
        String pageType = "02";

        if (device.indexOf("android") > 0 || device.indexOf("mac os") > 0 || device.indexOf("windows phone") > 0) {
            pageType = "01";
        }

        return riskClient.buildOperatorReport(loanNo, pageType);
    }

    @RequestMapping(value = "/getOperatorReport")
    public Row getOperatorReport(@RequestParam("loan_no") String loanNo, HttpServletRequest request) throws Exception{
        String device = request.getHeader("user-agent").toLowerCase();
        String pageType = "02";

        if (device.indexOf("android") > 0 || device.indexOf("mac os") > 0 || device.indexOf("windows phone") > 0) {
            pageType = "01";
        }

        return riskClient.getOperatorReport(loanNo, pageType);
    }
}
