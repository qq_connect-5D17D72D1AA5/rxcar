package com.rxcd.autofinace.util;


import com.rxcd.autofinace.service.client.ClearClient;
import org.apache.commons.lang.StringUtils;
import org.appframe.common.BigDecimalUtil;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.db.Store;
import org.appframe.wf.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;


@Component
public class ContractContentUtil {

    @Autowired
    private RxErpUtil erpUtil;
    @Autowired
    private BaseDao baseDao;
    @Autowired
    private ClearClient clearClient;

    public Row createQkqpsContent(Row loan_row) throws Exception{
        loan_row.put("blank","&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_row.getString("loan_info.loan_no"));
        loan_row.put("loan_car.brand",erpUtil.objectIsNullGetString(loanRow.get("loan_car.brand")));
        loan_row.put("loan_car.car_type",erpUtil.objectIsNullGetString(loanRow.get("loan_car.car_type")));
        loan_row.put("loan_car.cjh",erpUtil.objectIsNullGetString(loanRow.get("loan_car.cjh")));
        loan_row.put("loan_car.fdjh",erpUtil.objectIsNullGetString(loanRow.get("loan_car.fdjh")));
        loan_row.put("loan_car.car_plate",erpUtil.objectIsNullGetString(loanRow.get("loan_car.car_plate")));
        loan_row.put("loan_car0day_inireg_date",erpUtil.objectIsNullGetString(loan_row.get("loan_car.inireg_date")));
        //签字日期
        Calendar calendar = Calendar.getInstance();
        loan_row.put("signcon.year", calendar.get(Calendar.YEAR));
        loan_row.put("signcon.month", calendar.get(Calendar.MONTH) + 1);
        loan_row.put("signcon.day", calendar.get(Calendar.DAY_OF_MONTH));
        loan_row.put("signcon.hours", calendar.get(Calendar.HOUR_OF_DAY));
        loan_row.put("signcon.minute", calendar.get(Calendar.MINUTE));
        //查询放款方
        String sql = "SELECT c.*,e.pay_type,e.pay_mode,e.pay_card,e.pay_bank,e.receive_card,e.receive_bank from loan_info_employers e LEFT JOIN loan_capital c on e.employers_no=c.capital_no where e.loan_no='" + loan_row.getString("loan_info.loan_no") + "' LIMIT 1;";
        Row employersInfo = baseDao.queryRow(sql);
        loan_row.put("loan_capital0capital_name", employersInfo.getString("capital_name"));
        loan_row.put("loan_capital0credentials_code", employersInfo.getString("credentials_code"));
        loan_row.put("loan_info_employers0pay_bank", employersInfo.getString("pay_bank"));
        loan_row.put("loan_info_employers0pay_card", employersInfo.getString("pay_card"));
        loan_row.put("loan_info_employers0receive_card", employersInfo.getString("receive_card"));
        loan_row.put("loan_info_employers0receive_bank", employersInfo.getString("receive_bank"));

        sql = "select * from loan_capital t left join loan_info_employers a on t.CAPITAL_NO = a.EMPLOYERS_NO where a.EMPLOYERS_NO = '6b9a2d0e-7f43-427e-9aea-794195eddc7f' limit 1";
        Row loan_info_employers_row = baseDao.queryRow(sql);
        loan_row.put("loan_info_employers0pay_bank", loan_info_employers_row.getString("pay_bank"));
        loan_row.put("loan_capital0capital_name", loan_info_employers_row.getString("capital_name"));
        loan_row.put("loan_info_employers0pay_card", loan_info_employers_row.getString("pay_card"));

        //主合伙人信息
        sql = "select a.partner_name,a.idcard,a.phone_1,a.residence_address from join_branch t left join join_branch_partner a on t.branch_join_no = a.branch_join_no where t.dept_id = '"+loan_row.getString("loan_info.sell_branch_no")+"' and a.partner_identity = '01' and t.status = '1'";
        Row branchRow = baseDao.queryRow(sql);
        loan_row.put("branch_zhhr0partner_name",branchRow.getString("partner_name"));
        loan_row.put("branch_zhhr0idcard",branchRow.getString("idcard"));
        loan_row.put("branch_zhhr0phone_1",branchRow.getString("phone_1"));
        loan_row.put("branch_zhhr0residence_address",branchRow.getString("residence_address"));

        //分行地址
        sql = "select * from join_branch where dept_id = '"+loan_row.getString("loan_info.sell_branch_no")+"' and status = '1'";
        Row row = baseDao.queryRow(sql);
        String branch_address_p = row.getString("branch_address_p");
        sql = "select * from i_frame_sys_std_code_val where i_frame_sys_std_code_val.code_val = '"+branch_address_p+"'";
        Row dCodeRow = baseDao.queryRow(sql);
        String dp_code_val = dCodeRow.getString("p_code_val");
        loan_row.put("branch_d",dCodeRow.getString("code_label"));
        if(StringUtils.isNotEmpty(dp_code_val)){
            sql = "select * from i_frame_sys_std_code_val where i_frame_sys_std_code_val.code_val = '"+dp_code_val+"'";
            Row cCodeRow = baseDao.queryRow(sql);
            String cp_code_val = cCodeRow.getString("p_code_val");
            loan_row.put("branch_c",cCodeRow.getString("code_label"));
            if(StringUtils.isNotEmpty(cp_code_val)){
                sql = "select * from i_frame_sys_std_code_val where i_frame_sys_std_code_val.code_val = '"+cp_code_val+"'";
                Row pCodeRow = baseDao.queryRow(sql);
                loan_row.put("branch_p",pCodeRow.getString("code_label"));
            }else{
                loan_row.put("branch_p",cCodeRow.getString("code_label"));
            }
        }
        loan_row.put("branch_address",row.getString("branch_address"));

        //分行区域总
        sql = "select b.partner_name,b.idcard,b.phone_1 "
                + "from join_branch t "
                + "left join join_area a on t.area_dept_id = a.dept_id "
                + "left join join_area_partner b on a.area_join_no = b.area_join_no "
                + "where t.dept_id = '" + loan_row.getString("loan_info.sell_branch_no") + "' and b.partner_identity = '01' and b.status !='3' and t.status = '1'";
        Row zqzRow = baseDao.queryRow(sql);
        loan_row.put("zqz0qz_name", zqzRow.getString("partner_name"));
        loan_row.put("zqz0qz_idcard", zqzRow.getString("idcard"));
        loan_row.put("zqz0qz_address", zqzRow.getString("residence_address"));
        loan_row.put("zqz0qz_phone", zqzRow.getString("phone_1"));

        //费用基本信息
        double contract_sum  = erpUtil.objectIsNullGetDouble(loan_row.get("loan_cost.contract_sum"));     // 合同金额 元
        loan_row.put("costbills0contract_sum"    ,contract_sum);    //贷款金额
        loan_row.put("loan_cost.contract_sum_zh" , Util.digitUppercase(contract_sum)); //转换合同金额大写
        loan_row.put("costbills0cust_name"    , loan_row.getString("loan_cust.cust_name"));    //客户姓名
        loan_row.put("costbills0loan_date"    , loan_row.getString("loan_info_et.loan_date"));    //借款日期
        double car_zy_fee = BigDecimalUtil.div(BigDecimalUtil.div(contract_sum, 12), 30, 2);
        loan_row.put("costbills0car_zy_fee"    , car_zy_fee);    //车辆占用费
        //0期一次性费用
        double security_fee = BigDecimalUtil.round(erpUtil.objectIsNullGetDouble(loan_row.get("loan_cost.security_fee")),2);
        loan_row.put("fund_appoint0security_fee",security_fee);//保险押金
        loan_row.put("fund_appoint0pacc_fee" , BigDecimalUtil.round(erpUtil.objectIsNullGetDouble(loan_row.get("loan_cost.pacc_fee")),2)); //违章押金

        String rep_type = loan_row.getString("loan_info_et.rep_type");     // 还款方式
        String duetime  = loan_row.getString("loan_cost.duetime");   // 贷款期数
        int duetime_int = Integer.parseInt(duetime);
        //抵押时间多个月
        loan_row.put("pledge0time",duetime_int);
        loan_row.put("fund_appoint0loan_date"     ,  loan_row.getString("loan_info_et.loan_date"));       //起租日
        loan_row.put("fund_appoint0zlqx"     , duetime_int+"个月" );       //租赁期限

        List<Map<String, Object>> listPlan;
        String clear_type = loanRow.getStringNotNull("loan_info_et.clear_type");
        //获取到还款计划的日期
        Store repayment_plan_list = new Store();
        if ("1".equals(clear_type)){
            listPlan = clearClient.createRepayPayableTest(loan_row.getString("loan_info.loan_no"));
            for(int i=0;i<=24;i++){
                Row repayment_plan_Row = new Row();
                repayment_plan_Row.put("num",i);
                repayment_plan_list.add(repayment_plan_Row);
            }
        }else{
            listPlan = clearClient.repayPlanTest(loan_row.getString("loan_info.loan_no"));
            for(int i=0;i<=duetime_int;i++){
                Row repayment_plan_Row = new Row();
                repayment_plan_Row.put("num",i);
                repayment_plan_list.add(repayment_plan_Row);
            }
        }

        for(Map<String, Object> mapPlan:listPlan){
            List<String> batches =  (List)mapPlan.get("batches");
            List<Double> amountList =  (List)mapPlan.get("amountList");
            List<String> dateList =  (List)mapPlan.get("dateList");
            for(int i=0;i<batches.size();i++){
                int num = Integer.parseInt(batches.get(i));
                Row repayment_plan_Row = repayment_plan_list.get(num);
                repayment_plan_Row.put("day",dateList.get(i));
                repayment_plan_Row.put(mapPlan.get("feeSubject"),amountList.get(i));
            }
        }

        if ("1".equals(clear_type)){
            getQkgpsFeeByXxhb(loan_row,repayment_plan_list);
        }else{
            getQkgpsFee(loan_row,repayment_plan_list);
        }
        return loan_row;
    }

    private void getQkgpsFeeByXxhb(Row loanRow,Store fee_list)throws Exception{
        double contract_sum = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.contract_sum"));     // 合同金额 元
        double rate_fee = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.rate_fee"));          // 借款利率%
        double admin_exp = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.admin_exp"));          // 借款利率%
        //总利息收入
        double zlx = BigDecimalUtil.div(BigDecimalUtil.mul(contract_sum,rate_fee),100,2);
        //总管理费收入
        double zglf = BigDecimalUtil.div(BigDecimalUtil.mul(contract_sum,admin_exp),100,2);
        //总折旧费收入
        double zzjf = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.riskcapital_fee"));
        //收入
        Store zft_list = new Store();

        String periods_number = erpUtil.objectIsNullGetString(loanRow.get("loan_info_et.periods_number"));
        Store newStore = new Store();
        if(StringUtils.equals("1",periods_number)){
            newStore = erpUtil.getIntervalElementsStore(0,6,fee_list);
        }
        for(int i = 0;i<newStore.size();i++){
            Row r = new Row();
            int num = Integer.parseInt((newStore.get(i).get("num")).toString());
            //本金
            double monthprin = erpUtil.objectIsNullGetDouble(newStore.get(i).get("capital"));
            //保证金
            double bond_fee = erpUtil.objectIsNullGetDouble(newStore.get(i).get("bondFee"));
            //利息-资方
            double lx_zf = erpUtil.objectIsNullGetDouble(newStore.get(i).get("capitalratefeezf"));
            //利息-区域
            double lx_qy = erpUtil.objectIsNullGetDouble(newStore.get(i).get("capitalratefeeqy"));
            //利息-网点
            double lx_wd = erpUtil.objectIsNullGetDouble(newStore.get(i).get("capitalratefeewd"));
            //计息利差-区域
            double jxlx_qy = erpUtil.objectIsNullGetDouble(newStore.get(i).get("marginfeeqy"));
            //计息利差-网点
            double jxlx_wd = erpUtil.objectIsNullGetDouble(newStore.get(i).get("marginfeewd"));
            //计息利差-平台
            double jxlx_pt = erpUtil.objectIsNullGetDouble(newStore.get(i).get("marginfeept"));
            //计息利差-拨备
            double jxlx_bb = erpUtil.objectIsNullGetDouble(newStore.get(i).get("marginfeebb"));
            //管理费-区域
            double glf_qy = erpUtil.objectIsNullGetDouble(newStore.get(i).get("adminexpqy"));
            //管理费-网点
            double glf_wd = erpUtil.objectIsNullGetDouble(newStore.get(i).get("adminexpwd"));
            //管理费-平台
            double glf_pt = erpUtil.objectIsNullGetDouble(newStore.get(i).get("adminexppt"));
            //管理费-拨备
            double glf_bb = erpUtil.objectIsNullGetDouble(newStore.get(i).get("adminexpbb"));
            //折旧费-区域
            double zjf_qy = erpUtil.objectIsNullGetDouble(newStore.get(i).get("riskcapitalfeeqy"));
            //折旧费-网点
            double zjf_wd = erpUtil.objectIsNullGetDouble(newStore.get(i).get("riskcapitalfeewd"));
            //折旧费-平台
            double zjf_pt = erpUtil.objectIsNullGetDouble(newStore.get(i).get("riskcapitalfeept"));
            //折旧费-拨备
            double zjf_bb = erpUtil.objectIsNullGetDouble(newStore.get(i).get("riskcapitalfeebb"));
            //GPS服务费
            double gps_use_fee_num = erpUtil.objectIsNullGetDouble(newStore.get(i).get("gpsusefee"));
            //总收入
            List<Double> zsr = new ArrayList<>();
            zsr.add(zlx);
            zsr.add(zglf);
            zsr.add(zzjf);
            //总支出
            List<Double> zzc = new ArrayList<>();
            zzc.add(lx_zf);
            zzc.add(lx_qy);
            zzc.add(lx_wd);
            zzc.add(jxlx_qy);
            zzc.add(jxlx_wd);
            zzc.add(jxlx_pt);
            zzc.add(jxlx_bb);
            zzc.add(glf_qy);
            zzc.add(glf_wd);
            zzc.add(glf_pt);
            zzc.add(glf_bb);
            zzc.add(zjf_qy);
            zzc.add(zjf_wd);
            zzc.add(zjf_pt);
            zzc.add(zjf_bb);
            //弥补
            double mb = BigDecimalUtil.sub(BigDecimalUtil.addList(zsr),BigDecimalUtil.addList(zzc));

            if(i!=(newStore.size()-1)){
                r.put("lx_zf", lx_zf);
                r.put("lx_qy", lx_qy);
                r.put("lx_wd", lx_wd);

                r.put("jxlx_qy", jxlx_qy);
                r.put("jxlx_wd", jxlx_wd);
                r.put("jxlx_pt", jxlx_pt);
                r.put("jxlx_bb", jxlx_bb);

                r.put("glf_qy", glf_qy);
                r.put("glf_wd", glf_wd);
                r.put("glf_pt", glf_pt);
                r.put("glf_bb", glf_bb);

                r.put("zjf_qy", zjf_qy);
                r.put("zjf_wd", zjf_wd);
                r.put("zjf_pt", zjf_pt);
                r.put("zjf_bb", zjf_bb);

                r.put("gps_use_fee_num", gps_use_fee_num);
                r.put("mb", mb);
            }
            if(i!=0){
                r.put("bond_fee", bond_fee);
                r.put("monthprin", monthprin);
            }

            r.put("num", num);
            r.put("day", newStore.get(i).get("day"));
            zft_list.add(r);
        }
        //从1期开始收
        Store list = new Store();
        int count =1;
        //从0期开始收
        Store listzero = new Store();
        int zeronum = 1;
        for(int i = 0;i<zft_list.size();i++){
            Row r = new Row();
            List<Double> qy_sr_list = new ArrayList<>();
            qy_sr_list.add(erpUtil.objectIsNullGetDouble(zft_list.get(i).get("lx_qy")));
            qy_sr_list.add(erpUtil.objectIsNullGetDouble(zft_list.get(i).get("jxlx_qy")));
            qy_sr_list.add(erpUtil.objectIsNullGetDouble(zft_list.get(i).get("jxlx_bb")));
            qy_sr_list.add(erpUtil.objectIsNullGetDouble(zft_list.get(i).get("glf_qy")));
            qy_sr_list.add(erpUtil.objectIsNullGetDouble(zft_list.get(i).get("glf_bb")));
            qy_sr_list.add(erpUtil.objectIsNullGetDouble(zft_list.get(i).get("zjf_qy")));
            qy_sr_list.add(erpUtil.objectIsNullGetDouble(zft_list.get(i).get("zjf_bb")));
            double qy_sr =BigDecimalUtil.addList(qy_sr_list);//区域每期收入

            List<Double> wd_sr_list = new ArrayList<>();
            wd_sr_list.add(erpUtil.objectIsNullGetDouble(zft_list.get(i).get("lx_wd")));
            wd_sr_list.add(erpUtil.objectIsNullGetDouble(zft_list.get(i).get("jxlx_wd")));
            wd_sr_list.add(erpUtil.objectIsNullGetDouble(zft_list.get(i).get("zjf_wd")));
            double wd_sr =BigDecimalUtil.addList(wd_sr_list);//网点每期收入

            List<Double> gps_sr_list = new ArrayList<>();
            gps_sr_list.add(erpUtil.objectIsNullGetDouble(zft_list.get(i).get("glf_wd")));
            gps_sr_list.add(erpUtil.objectIsNullGetDouble(zft_list.get(i).get("jxlx_pt")));
            gps_sr_list.add(erpUtil.objectIsNullGetDouble(zft_list.get(i).get("gps_use_fee_num")));
            gps_sr_list.add(erpUtil.objectIsNullGetDouble(zft_list.get(i).get("mb")));
            double gps_sr =BigDecimalUtil.addList(gps_sr_list);//GPS每期收入
            List<Double> pt_sr_list = new ArrayList<>();
            pt_sr_list.add(erpUtil.objectIsNullGetDouble(zft_list.get(i).get("glf_pt")));
            pt_sr_list.add(erpUtil.objectIsNullGetDouble(zft_list.get(i).get("zjf_pt")));
            pt_sr_list.add(erpUtil.objectIsNullGetDouble(zft_list.get(i).get("monthprin")));
            double pt_sr =BigDecimalUtil.addList(pt_sr_list);//平台每期收入

            double bond_fee = erpUtil.objectIsNullGetDouble(zft_list.get(i).get("bond_fee"));
            double lx_zf = erpUtil.objectIsNullGetDouble(zft_list.get(i).get("lx_zf"));

            if(i==0){
                double firstFee = pt_sr;
                loanRow.put("fund_appoint0firstfee", firstFee);
            }
            if(i!=newStore.size()-1){
                r.put("zorenum", zeronum);
                r.put("day",zft_list.get(i).get("day"));
                r.put("qy_sr", qy_sr);
                r.put("wd_sr", wd_sr);
                r.put("gps_sr", gps_sr);
                r.put("lx_zf",lx_zf);
                listzero.add(r);
                zeronum++;
            }
            if(i!=0){
                r.put("num",count);
                r.put("day",zft_list.get(i).get("day"));
                r.put("zj_sr", pt_sr);
                r.put("bond_fee", bond_fee);
                list.add(r);
                count++;
            }
        }

        loanRow.put("costbills0detail" , list); //月供
        loanRow.put("costbills0detailzero" , listzero); //0期开始月供

        double lx_zf_hj = 0;

        double qy_sr_hj = 0;
        double wd_sr_hj = 0;
        double gps_sr_hj = 0;
        for(Row row:listzero){
            lx_zf_hj = BigDecimalUtil.add(row.getDouble("lx_zf"),lx_zf_hj);//利息_资方_合计

            qy_sr_hj = BigDecimalUtil.add(row.getDouble("qy_sr"),qy_sr_hj);//区域_收入_合计
            wd_sr_hj = BigDecimalUtil.add(row.getDouble("wd_sr"),wd_sr_hj);//网点_收入_合计
            gps_sr_hj = BigDecimalUtil.add(row.getDouble("gps_sr"),gps_sr_hj);//GPS_收入_合计
        }
        loanRow.put("lx_zf_hj", lx_zf_hj);

        loanRow.put("qy_sr_hj", qy_sr_hj);
        loanRow.put("wd_sr_hj", wd_sr_hj);
        loanRow.put("gps_sr_hj", gps_sr_hj);
        double bond_fee_hj = 0;
        for(Row row:list){
            bond_fee_hj = BigDecimalUtil.add(row.getDouble("bond_fee"),bond_fee_hj);//保证金_合计
        }
        loanRow.put("bond_fee_hj", bond_fee_hj);
    }

    /**
     * 获得全款GPS费用
     * @throws Exception
     */
    private void getQkgpsFee(Row loanRow,Store fee_list) throws Exception{
        String duetime   = loanRow.getString("loan_cost.duetime");   // 贷款期数
        double duetime_int = Double.parseDouble(duetime);

        loanRow.put("fund_appoint0zlqx"     , duetime_int+"个月" );       //租赁期限
        double contract_sum = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.contract_sum"));     // 合同金额 元
        double rate_fee = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.rate_fee"));          // 借款利率%
        double admin_exp = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.admin_exp"));          // 借款利率%
        //总利息收入
        double zlx = BigDecimalUtil.div(BigDecimalUtil.mul(contract_sum,rate_fee),100,2);
        //总管理费收入
        double zglf = BigDecimalUtil.div(BigDecimalUtil.mul(contract_sum,admin_exp),100,2);
        //总折旧费收入
        double zzjf = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.riskcapital_fee"));

        //从1期开始收
        Store list = new Store();
        //从0期开始收
        Store listzero = new Store();
        for (Row row : fee_list) {
            Row r = new Row();
            int num = Integer.parseInt((row.get("num")).toString());
            double monthprin = erpUtil.objectIsNullGetDouble(row.get("capital"));
            double pt_sr = 0.0;
            if(num!=duetime_int){
                int nowNum = num+1;
                //还款计划：利息
                double rate_fee_num = Double.parseDouble(row.get("rateFee").toString());
                //还款计划：管理费
                double admin_exp_num = Double.parseDouble(row.get("adminExp").toString());
                //还款计划：折旧费
                double riskcapital_fee_num = Double.parseDouble(row.get("riskcapitalFee").toString());
                //还款计划：GPS服务费
                double gps_use_fee_num = Double.parseDouble(row.get("gpsUseFee").toString());
                //PMT0.9%
                double pmt =BigDecimalUtil.div(BigDecimalUtil.mul(BigDecimalUtil.mul(contract_sum, BigDecimalUtil.div(rate_fee, 100)), BigDecimalUtil.sub(Math.pow(BigDecimalUtil.add(1,BigDecimalUtil.div(rate_fee, 100)), duetime_int), Math.pow(BigDecimalUtil.add(1,BigDecimalUtil.div(rate_fee, 100)), BigDecimalUtil.sub(nowNum, 1)))),BigDecimalUtil.sub(Math.pow(BigDecimalUtil.add(1,BigDecimalUtil.div(rate_fee, 100)),duetime_int), 1),2);
                //计息利差
                double jxlx =BigDecimalUtil.round(BigDecimalUtil.sub(rate_fee_num, pmt),2);
                //利息-资方
                double lx_zf =BigDecimalUtil.div(BigDecimalUtil.mul(BigDecimalUtil.mul(contract_sum, BigDecimalUtil.div(0.145, 12, 7)), BigDecimalUtil.sub(Math.pow(BigDecimalUtil.add(1,BigDecimalUtil.div(0.145, 12, 7)), duetime_int), Math.pow(BigDecimalUtil.add(1,BigDecimalUtil.div(0.145, 12, 7)), BigDecimalUtil.sub(nowNum, 1)))),BigDecimalUtil.sub(Math.pow(BigDecimalUtil.add(1,BigDecimalUtil.div(0.145, 12, 7)),duetime_int), 1), 2);
                //利息-区域
                double lx_qy =BigDecimalUtil.round(BigDecimalUtil.mul(BigDecimalUtil.mul(pmt, 0.1), 0.1), 2);
                //利息-网点
                double lx_wd =BigDecimalUtil.round(BigDecimalUtil.mul(BigDecimalUtil.mul(pmt, 0.1), 0.9), 2);
                //计息利差-区域
                double jxlx_qy =BigDecimalUtil.round(BigDecimalUtil.mul(jxlx, 0.01), 2);
                //计息利差-网点
                double jxlx_wd =BigDecimalUtil.round(BigDecimalUtil.add(BigDecimalUtil.mul(jxlx, 0.09), BigDecimalUtil.mul(BigDecimalUtil.mul(jxlx, 0.9), 0.6)), 2);
                //计息利差-平台
                double jxlx_pt =BigDecimalUtil.round(BigDecimalUtil.mul(BigDecimalUtil.mul(jxlx, 0.9), 0.35), 2);
                //计息利差-拨备
                double jxlx_bb =BigDecimalUtil.round(BigDecimalUtil.mul(BigDecimalUtil.mul(jxlx, 0.9), 0.05), 2);
                //管理费-区域
                double glf_qy =BigDecimalUtil.round(BigDecimalUtil.mul(admin_exp_num, 0.01), 2);
                //管理费-网点
                double glf_wd =BigDecimalUtil.round(BigDecimalUtil.add(BigDecimalUtil.mul(admin_exp_num, 0.09), BigDecimalUtil.mul(BigDecimalUtil.mul(admin_exp_num, 0.9), 0.6)), 2);
                //管理费-平台
                double glf_pt =BigDecimalUtil.round(BigDecimalUtil.mul(BigDecimalUtil.mul(admin_exp_num, 0.9), 0.35), 2);
                //管理费-拨备
                double glf_bb =BigDecimalUtil.round(BigDecimalUtil.mul(BigDecimalUtil.mul(admin_exp_num, 0.9), 0.05), 2);
                //折旧费-区域
                double zjf_qy =BigDecimalUtil.round(BigDecimalUtil.mul(riskcapital_fee_num, 0.01), 2);
                //折旧费-网点
                double zjf_wd =BigDecimalUtil.round(BigDecimalUtil.sub(BigDecimalUtil.add(BigDecimalUtil.mul(riskcapital_fee_num, 0.09), BigDecimalUtil.mul(BigDecimalUtil.mul(riskcapital_fee_num, 0.9), 0.6)),30), 2);
                //折旧费-平台
                double zjf_pt =BigDecimalUtil.round(BigDecimalUtil.add(BigDecimalUtil.mul(BigDecimalUtil.mul(riskcapital_fee_num, 0.9), 0.35),30), 2);
                //折旧费-拨备
                double zjf_bb =BigDecimalUtil.round(BigDecimalUtil.mul(BigDecimalUtil.mul(riskcapital_fee_num, 0.9), 0.05), 2);

                //总收入
                List<Double> zsr = new ArrayList<>();
                zsr.add(zlx);
                zsr.add(zglf);
                zsr.add(zzjf);
                //总支出
                List<Double> zzc = new ArrayList<>();
                zzc.add(lx_zf);
                zzc.add(lx_qy);
                zzc.add(lx_wd);
                zzc.add(jxlx_qy);
                zzc.add(jxlx_wd);
                zzc.add(jxlx_pt);
                zzc.add(jxlx_bb);
                zzc.add(glf_qy);
                zzc.add(glf_wd);
                zzc.add(glf_pt);
                zzc.add(glf_bb);
                zzc.add(zjf_qy);
                zzc.add(zjf_wd);
                zzc.add(zjf_pt);
                zzc.add(zjf_bb);
                //弥补
                double mb = BigDecimalUtil.sub(BigDecimalUtil.addList(zsr),BigDecimalUtil.addList(zzc));

                r.put("gps_use_fee_num", gps_use_fee_num);

                r.put("lx_zf", lx_zf);
                r.put("lx_qy", lx_qy);
                r.put("lx_wd", lx_wd);

                r.put("jxlx_qy", jxlx_qy);
                r.put("jxlx_wd", jxlx_wd);
                r.put("jxlx_pt", jxlx_pt);
                r.put("jxlx_bb", jxlx_bb);

                r.put("glf_qy", glf_qy);
                r.put("glf_wd", glf_wd);
                r.put("glf_pt", glf_pt);
                r.put("glf_bb", glf_bb);

                r.put("zjf_qy", zjf_qy);
                r.put("zjf_wd", zjf_wd);
                r.put("zjf_pt", zjf_pt);
                r.put("zjf_bb", zjf_bb);

                List<Double> qy_sr_list = new ArrayList<>();
                qy_sr_list.add(lx_qy);
                qy_sr_list.add(jxlx_qy);
                qy_sr_list.add(jxlx_bb);
                qy_sr_list.add(glf_qy);
                qy_sr_list.add(glf_bb);
                qy_sr_list.add(zjf_qy);
                qy_sr_list.add(zjf_bb);
                double qy_sr =BigDecimalUtil.addList(qy_sr_list);//区域每期收入

                List<Double> wd_sr_list = new ArrayList<>();
                wd_sr_list.add(lx_wd);
                wd_sr_list.add(jxlx_wd);
                wd_sr_list.add(zjf_wd);
                double wd_sr =BigDecimalUtil.addList(wd_sr_list);//网点每期收入

                List<Double> gps_sr_list = new ArrayList<>();
                gps_sr_list.add(glf_wd);
                gps_sr_list.add(jxlx_pt);
                gps_sr_list.add(gps_use_fee_num);
                gps_sr_list.add(mb);
                double gps_sr =BigDecimalUtil.addList(gps_sr_list);//GPS每期收入
                List<Double> pt_sr_list = new ArrayList<>();
                pt_sr_list.add(glf_pt);
                pt_sr_list.add(zjf_pt);
                pt_sr =BigDecimalUtil.addList(pt_sr_list);//平台每期收入

                r.put("qy_sr", qy_sr);
                r.put("wd_sr", wd_sr);
                r.put("pt_sr", pt_sr);
                r.put("gps_sr", gps_sr);
                r.put("zorenum", nowNum);
                r.put("day", row.get("day"));
                listzero.add(r);
            }
            double zj_sr =BigDecimalUtil.add(pt_sr,monthprin);
            if(num!=0){
                r.put("num", num);
                r.put("day", row.get("day"));
                r.put("monthprin", monthprin);
                r.put("zj_sr", zj_sr);
                list.add(r);
            }
        }
        loanRow.put("costbills0detail" , list); //月供
        loanRow.put("costbills0detailzero" , listzero); //0期开始月供

        double gps_use_fee_num_hj = 0;

        double lx_zf_hj = 0;
        double lx_qy_hj = 0;
        double lx_wd_hj = 0;

        double jxlx_qy_hj = 0;
        double jxlx_wd_hj = 0;
        double jxlx_pt_hj = 0;
        double jxlx_bb_hj = 0;

        double glf_qy_hj = 0;
        double glf_wd_hj = 0;
        double glf_pt_hj = 0;
        double glf_bb_hj = 0;

        double zjf_qy_hj = 0;
        double zjf_wd_hj = 0;
        double zjf_pt_hj = 0;
        double zjf_bb_hj = 0;

        double qy_sr_hj = 0;
        double wd_sr_hj = 0;
        double pt_sr_hj = 0;
        double gps_sr_hj = 0;
        for(Row row:listzero){
            gps_use_fee_num_hj = BigDecimalUtil.add(row.getDouble("gps_use_fee_num"),gps_use_fee_num_hj);//GPS使用费_合计

            lx_zf_hj = BigDecimalUtil.add(row.getDouble("lx_zf"),lx_zf_hj);//利息_资方_合计
            lx_qy_hj = BigDecimalUtil.add(row.getDouble("lx_qy"),lx_qy_hj);//利息_区域_合计
            lx_wd_hj = BigDecimalUtil.add(row.getDouble("lx_wd"),lx_wd_hj);//利息_网点_合计

            jxlx_qy_hj = BigDecimalUtil.add(row.getDouble("jxlx_qy"),jxlx_qy_hj);//计息利差_区域_合计
            jxlx_wd_hj = BigDecimalUtil.add(row.getDouble("jxlx_wd"),jxlx_wd_hj);//计息利差_网点_合计
            jxlx_pt_hj = BigDecimalUtil.add(row.getDouble("jxlx_pt"),jxlx_pt_hj);//计息利差_平台_合计
            jxlx_bb_hj = BigDecimalUtil.add(row.getDouble("jxlx_bb"),jxlx_bb_hj);//计息利差_拨备_合计

            glf_qy_hj = BigDecimalUtil.add(row.getDouble("glf_qy"),glf_qy_hj);//管理费_区域_合计
            glf_wd_hj = BigDecimalUtil.add(row.getDouble("glf_wd"),glf_wd_hj);//管理费_网点_合计
            glf_pt_hj = BigDecimalUtil.add(row.getDouble("glf_pt"),glf_pt_hj);//管理费_平台_合计
            glf_bb_hj = BigDecimalUtil.add(row.getDouble("glf_bb"),glf_bb_hj);//管理费_拨备_合计

            zjf_qy_hj = BigDecimalUtil.add(row.getDouble("zjf_qy"),zjf_qy_hj);//折旧费_区域_合计
            zjf_wd_hj = BigDecimalUtil.add(row.getDouble("zjf_wd"),zjf_wd_hj);//折旧费_网点_合计
            zjf_pt_hj = BigDecimalUtil.add(row.getDouble("zjf_pt"),zjf_pt_hj);//折旧费_平台_合计
            zjf_bb_hj = BigDecimalUtil.add(row.getDouble("zjf_bb"),zjf_bb_hj);//折旧费_拨备_合计

            qy_sr_hj = BigDecimalUtil.add(row.getDouble("qy_sr"),qy_sr_hj);//区域_收入_合计
            wd_sr_hj = BigDecimalUtil.add(row.getDouble("wd_sr"),wd_sr_hj);//网点_收入_合计
            pt_sr_hj = BigDecimalUtil.add(row.getDouble("pt_sr"),pt_sr_hj);//平台_收入_合计
            gps_sr_hj = BigDecimalUtil.add(row.getDouble("gps_sr"),gps_sr_hj);//GPS_收入_合计
        }
        loanRow.put("gps_use_fee_num_hj", gps_use_fee_num_hj);

        loanRow.put("lx_zf_hj", lx_zf_hj);
        loanRow.put("lx_qy_hj", lx_qy_hj);
        loanRow.put("lx_wd_hj", lx_wd_hj);

        loanRow.put("jxlx_qy_hj", jxlx_qy_hj);
        loanRow.put("jxlx_wd_hj", jxlx_wd_hj);
        loanRow.put("jxlx_pt_hj", jxlx_pt_hj);
        loanRow.put("jxlx_bb_hj", jxlx_bb_hj);

        loanRow.put("glf_qy_hj", glf_qy_hj);
        loanRow.put("glf_wd_hj", glf_wd_hj);
        loanRow.put("glf_pt_hj", glf_pt_hj);
        loanRow.put("glf_bb_hj", glf_bb_hj);

        loanRow.put("zjf_qy_hj", zjf_qy_hj);
        loanRow.put("zjf_wd_hj", zjf_wd_hj);
        loanRow.put("zjf_pt_hj", zjf_pt_hj);
        loanRow.put("zjf_bb_hj", zjf_bb_hj);

        loanRow.put("qy_sr_hj", qy_sr_hj);
        loanRow.put("wd_sr_hj", wd_sr_hj);
        loanRow.put("pt_sr_hj", pt_sr_hj);
        loanRow.put("gps_sr_hj", gps_sr_hj);

        Row firstRow = listzero.get(0);
        double firstFee = firstRow.getDouble("pt_sr");
        loanRow.put("fund_appoint0firstfee", firstFee);
    }

    public Row createJoinBranchContent(Row loan_row) throws Exception{
        String branch_join_no = loan_row.getString("branch_join_no");
        String con_code = (String)baseDao.queryObject("select con_code from join_branch where branch_join_no = '"+branch_join_no+"'");
        loan_row.put("con_code",con_code);

        //找出所有合伙人
        String sql = "select join_branch_partner.partner_name,join_branch_partner.idcard,join_branch_partner.phone_1,join_branch_partner.residence_address,join_branch_partner.share_ratio "
                +"from join_branch "
                +"left join join_branch_partner on join_branch_partner.branch_join_no = join_branch.branch_join_no "
                +"where join_branch.branch_join_no = '"+branch_join_no+"' and join_branch_partner.status != '3' and partner_identity !='03' and join_branch.status = '1'";
        Store partner_store  = baseDao.query(sql);
        int index = 1;
        for (Row row : partner_store) {
            loan_row.put("join_branch_partner.partner_name"+index,row.getString("partner_name"));
            loan_row.put("join_branch_partner.idcard"+index,row.getString("idcard"));
            loan_row.put("join_branch_partner.phone_1"+index,row.getString("phone_1"));
            loan_row.put("join_branch_partner.residence_address"+index,row.getString("residence_address"));
            loan_row.put("join_branch_partner.share_ratio"+index,"占"+row.getString("share_ratio")+"%");
            index++;
        }
        //找出担保人
        sql = "select join_branch_partner.partner_name,join_branch_partner.idcard,join_branch_partner.phone_1,join_branch_partner.residence_address "
                +"from join_branch "
                +"left join join_branch_partner on join_branch_partner.branch_join_no = join_branch.branch_join_no "
                +"where join_branch.branch_join_no = '"+branch_join_no+"' and join_branch_partner.status != '3' and partner_identity ='03' and join_branch.status = '1'";
        Store guarantee_store  = baseDao.query(sql);
        int count = 1;
        for (Row row : guarantee_store) {
            loan_row.put("join_branch_guarantee.guarantee_name"+count,row.getString("partner_name"));
            loan_row.put("join_branch_guarantee.idcard"+count,row.getString("idcard"));
            loan_row.put("join_branch_guarantee.phone_1"+count,row.getString("phone_1"));
            loan_row.put("join_branch_guarantee.residence_address"+count,row.getString("residence_address"));
            count++;
        }
        Calendar calendar = Calendar.getInstance();
        loan_row.put("signcon.year", calendar.get(Calendar.YEAR));
        loan_row.put("signcon.month", calendar.get(Calendar.MONTH) + 1);
        loan_row.put("signcon.day", calendar.get(Calendar.DAY_OF_MONTH));
        loan_row.put("end.year", calendar.get(Calendar.YEAR)+3);
        loan_row.put("end.day", calendar.get(Calendar.DAY_OF_MONTH)-1);
        return loan_row;
    }

    public Row createJoinAreaContent(Row loan_row) throws Exception{

        String area_join_no = loan_row.getString("area_join_no");
        String con_code = (String)baseDao.queryObject("select con_code from join_area where area_join_no = '"+area_join_no+"'");
        loan_row.put("con_code",con_code);
        //找出所有合伙人
        String sql = "select join_area_partner.partner_name,join_area_partner.idcard,join_area_partner.phone_1,join_area_partner.residence_address,join_area_partner.share_ratio "
                +"from join_area "
                +"left join join_area_partner on join_area_partner.area_join_no = join_area.area_join_no "
                +"where join_area.area_join_no = '"+area_join_no+"' and join_area_partner.status != '3' and partner_identity !='03' and join_area.status = '1'";
        Store partner_store  = baseDao.query(sql);
        int index = 1;
        for (Row row : partner_store) {
            loan_row.put("join_branch_partner.partner_name"+index,row.getString("partner_name"));
            loan_row.put("join_branch_partner.idcard"+index,row.getString("idcard"));
            loan_row.put("join_branch_partner.phone_1"+index,row.getString("phone_1"));
            loan_row.put("join_branch_partner.residence_address"+index,row.getString("residence_address"));
            loan_row.put("join_branch_partner.share_ratio"+index,"占"+row.getString("share_ratio")+"%");
            index++;
        }
        //找出担保人
        sql = "select join_area_partner.partner_name,join_area_partner.idcard,join_area_partner.phone_1,join_area_partner.residence_address "
                +"from join_area "
                +"left join join_area_partner on join_area_partner.area_join_no = join_area.area_join_no "
                +"where join_area.area_join_no = '"+area_join_no+"' and join_area_partner.status != '3' and partner_identity ='03' and join_area.status = '1'";
        Store guarantee_store  = baseDao.query(sql);
        int count = 1;
        for (Row row : guarantee_store) {
            loan_row.put("join_branch_guarantee.guarantee_name"+count,row.getString("partner_name"));
            loan_row.put("join_branch_guarantee.idcard"+count,row.getString("idcard"));
            loan_row.put("join_branch_guarantee.phone_1"+count,row.getString("phone_1"));
            loan_row.put("join_branch_guarantee.residence_address"+count,row.getString("residence_address"));
            count++;
        }
        Calendar calendar = Calendar.getInstance();
        loan_row.put("signcon.year", calendar.get(Calendar.YEAR));
        loan_row.put("signcon.month", calendar.get(Calendar.MONTH) + 1);
        loan_row.put("signcon.day", calendar.get(Calendar.DAY_OF_MONTH));
        loan_row.put("end.year", calendar.get(Calendar.YEAR)+3);
        loan_row.put("end.day", calendar.get(Calendar.DAY_OF_MONTH)-1);
        return loan_row;
    }
}
