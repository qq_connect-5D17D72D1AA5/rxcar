package com.rxcd.autofinace.service.weixin;

import com.alibaba.fastjson.JSONObject;
import com.rxcd.autofinace.util.RxErpUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * 企业微信API接口
 *
 * @author hubin
 */
@Component
public class QywxApiService {
    private final static Log log = LogFactory.getLog(QywxApiService.class);

    private static Map<String, AppInfo> tokenMap = new HashMap<String, AppInfo>();
    @Autowired
    private RxErpUtil erpUtil;
    @Autowired
    private BaseDao baseDao;

    /**
     * 初始化appinfo
     */
    static {
        try {
            tokenMap.put("fkappinfo", getFkAppInfo());
            tokenMap.put("gpsappinfo", getGpsAppInfo());
            tokenMap.put("shappinfo", getShAppInfo());
            tokenMap.put("booksappinfo", getBooksAppInfo());
            tokenMap.put("sysappinfo", getSysAppInfo());
        } catch (Exception e) {
            log.info("tokenMap初始化异常" + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * 获取企业微信应用accesstoken
     *
     * @return
     * @throws Exception
     */
    public static String getAccessToken(String corpID, String secret) throws Exception {
        HttpGet httpGet = new HttpGet(
                "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=" + corpID + "&corpsecret=" + secret);
        HttpResponse response = Util.getDefaultHttpClient().execute(httpGet);
        String result = EntityUtils.toString(response.getEntity());
        JSONObject resultJson = JSONObject.parseObject(result);
        return resultJson.getString("access_token");
    }

    /**
     * 得到重置应用INfo
     *
     * @param app_name
     * @return
     * @throws Exception
     */
    public static AppInfo getResetAppInfo(String app_name) throws Exception {
        if ("fkappinfo".equals(app_name)) {
            tokenMap.put(app_name, getFkAppInfo());
        } else if ("shappinfo".equals(app_name)) {
            tokenMap.put(app_name, getShAppInfo());
        } else if ("gpsappinfo".equals(app_name)) {
            tokenMap.put(app_name, getGpsAppInfo());
        } else if ("booksappinfo".equals(app_name)) {
            tokenMap.put(app_name, getBooksAppInfo());
        } else if ("sysappinfo".equals(app_name)) {
            tokenMap.put(app_name, getSysAppInfo());
        } else {
            return null;
        }
        return tokenMap.get("app_name");
    }

    /**
     * 获取企业微信放款应用
     *
     * @return
     * @throws Exception
     */
    public static AppInfo getSysAppInfo() throws Exception {
        String corpID = "wwc0487eadd945b5b6";
        String secret = "sJAbMF-MqZkHaGPW7ZpWltOq1ZQa-2kQm4Sh3ke5cso";
        String agentID = "1000015";
        String token = getAccessToken(corpID, secret);
        return new AppInfo(agentID, corpID, secret, token);
    }

    /**
     * 获取企业微信放款应用
     *
     * @return
     * @throws Exception
     */
    public static AppInfo getFkAppInfo() throws Exception {
        String corpID = "wwc0487eadd945b5b6";
        String secret = "JD7Fo7OjBuCeFlViG_qGPVfVjIEd0ClCbUtF61xTpfw";
        String agentID = "1000012";
        String token = getAccessToken(corpID, secret);
        return new AppInfo(agentID, corpID, secret, token);
    }

    /**
     * 获取企业微信GPS应用
     *
     * @return
     * @throws Exception
     */
    public static AppInfo getGpsAppInfo() throws Exception {
        String corpID = "wwc0487eadd945b5b6";
        String secret = "rcj3CPGNp9G1ed1Jv91e6qXVTNv1XCtbYJpe_aNQMOY";
        String agentID = "1000002";
        String token = getAccessToken(corpID, secret);
        return new AppInfo(agentID, corpID, secret, token);
    }

    /**
     * 获取企业微信审核应用
     *
     * @return
     * @throws Exception
     */
    public static AppInfo getShAppInfo() throws Exception {
        String corpID = "wwc0487eadd945b5b6";
        String secret = "0sKxaRZ_l2EI0G7pBn29lPCsj0EkmW1BfnNoZCDKNW4";
        String agentID = "1000003";
        String token = getAccessToken(corpID, secret);
        return new AppInfo(agentID, corpID, secret, token);
    }

    /**
     * 获取通讯录应用
     *
     * @return
     * @throws Exception
     */
    public static AppInfo getBooksAppInfo() throws Exception {
        String corpID = "wwc0487eadd945b5b6";
        String secret = "RpWpPakXtY_aC0wtEJZwjqPgdjeVeyT3sYcVXn0VofI";
        String agentID = "2";
        String token = getAccessToken(corpID, secret);
        return new AppInfo(agentID, corpID, secret, token);
    }

    /**
     * 判断token过期
     *
     * @return
     * @throws Exception
     */
    public static boolean tokenOver(int errcode) throws Exception {
        if (errcode == 40014 || errcode == 42001) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * 企业微信文本推送
     *
     * @param wxname
     * @param text
     * @param sum
     * @throws Exception
     */
    public void textPush(String wxname, String text, String app_name, int sum)
            throws Exception {
        if (sum <= 0) {
            log.info("企业微信文本推送sum为零");
            return;
        }
        if (3 < sum) {
            return;
        }
        AppInfo appInfo = tokenMap.get(app_name);
        String url = "https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=" + appInfo.getToken();
        JSONObject params = new JSONObject();
        params.put("touser", wxname);
        params.put("msgtype", "text");
        params.put("agentid", appInfo.getAgentID());
        JSONObject param2 = new JSONObject();
        param2.put("content", text);
        params.put("text", param2);
        HttpPost httpPost = new HttpPost(url);
        httpPost.addHeader(HTTP.CONTENT_TYPE, "application/json");
        httpPost.setEntity(new StringEntity(JSONObject.toJSONString(params), "UTF-8"));
        HttpResponse response = Util.getDefaultHttpClient().execute(httpPost);
        String result = EntityUtils.toString(response.getEntity());
        JSONObject resultJson = JSONObject.parseObject(result);
        int errcode = resultJson.getInteger("errcode");
        if (tokenOver(errcode)) {
            getResetAppInfo(app_name);
            sum = sum + 1;
            textPush(wxname, text, app_name, sum);
        }
    }

    /**
     * 企业微信卡片消息推送
     *
     * @param wxname
     * @throws Exception
     */
    public void cardPush(String wxname, String title, String description, String app_name, String lng,
                         String lat, int sum) throws Exception {
        if (sum <= 0) {
            log.info("企业微信卡片消息推送sum为零");
            return;
        }
        if (3 < sum) {
            return;
        }
        AppInfo appInfo = tokenMap.get(app_name);
        String url = "https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=" + appInfo.getToken();
        JSONObject params = new JSONObject();
        params.put("touser", wxname);
        params.put("msgtype", "textcard");
        params.put("agentid", appInfo.getAgentID());
        JSONObject param2 = new JSONObject();
        param2.put("title", title);
        param2.put("description", description);
        param2.put("url", "uri.amap.com/marker?position=" + lng + "," + lat);
        params.put("textcard", param2);
        HttpPost httpPost = new HttpPost(url);
        httpPost.addHeader(HTTP.CONTENT_TYPE, "application/json");
        httpPost.setEntity(new StringEntity(JSONObject.toJSONString(params), "UTF-8"));
        HttpResponse response = Util.getDefaultHttpClient().execute(httpPost);
        String result = EntityUtils.toString(response.getEntity());
        JSONObject resultJson = JSONObject.parseObject(result);
        int errcode = resultJson.getInteger("errcode");
        if (tokenOver(errcode)) {
            getResetAppInfo(app_name);
            sum = sum + 1;
            cardPush(wxname, title, description, app_name, lng, lat, sum);
        }
    }

    /**
     * 添加人员信息
     *
     * @param app_name     app名字
     * @param userid       用户no
     * @param name         用户姓名
     * @param english_name 英文名
     * @param mobile       手机号
     * @param position     职位
     * @param department   部门
     * @throws Exception
     */
    public void addPerson(String app_name, String userid, String name, String english_name, String mobile,
                          String position, int department[], int enable, int sum) throws Exception {
        if (sum <= 0) {
            log.info("添加人员信息sum为零");
            return;
        }
        if (3 < sum) {
            return;
        }
        AppInfo appInfo = tokenMap.get(app_name);
        String url = "https://qyapi.weixin.qq.com/cgi-bin/user/create?access_token=" + appInfo.getToken();
        JSONObject params = new JSONObject();
        params.put("userid", userid);
        params.put("name", name);
        params.put("english_name", english_name);
        params.put("mobile", mobile);
        params.put("position", position);
        params.put("department", department);
        params.put("enable", enable);
        JSONObject resultJson = post(params, url);
        int errcode = resultJson.getInteger("errcode");
        if (tokenOver(errcode)) {
            getResetAppInfo(app_name);
            sum = sum + 1;
            addPerson(app_name, userid, name, english_name, mobile, position, department, enable, sum);
        } else if (errcode == 0) {
            //如果人员添加成功，继续邀请人员加入企业微信
            wxInvite("booksappinfo", userid, 1);
        }
    }

    /**
     * 修改人员信息
     *
     * @param app_name     app名字
     * @param userid       用户no
     * @param name         用户姓名
     * @param english_name 英文名
     * @param mobile       手机号
     * @param position     职位
     * @param department   部门
     * @throws Exception
     */
    public void updatePerson(String app_name, String userid, String name, String english_name, String mobile,
                             String position, int department[], int enable, int sum) throws Exception {
        if (sum <= 0) {
            log.info("修改人员信息sum为零");
            return;
        }
        if (3 < sum) {
            return;
        }
        AppInfo appInfo = tokenMap.get(app_name);
        String url = "https://qyapi.weixin.qq.com/cgi-bin/user/update?access_token=" + appInfo.getToken();
        JSONObject params = new JSONObject();
        params.put("userid", userid);
        params.put("name", name);
        params.put("english_name", english_name);
        params.put("mobile", mobile);
        params.put("position", position);
        params.put("department", department);
        params.put("enable", enable);
        JSONObject resultJson = post(params, url);
        int errcode = resultJson.getInteger("errcode");
        if (tokenOver(errcode)) {
            getResetAppInfo(app_name);
            sum = sum + 1;
            updatePerson(app_name, userid, name, english_name, mobile, position, department, enable, sum);
        }
    }

    /**
     * 获取单个人员信息，是否启用企业微信
     *
     * @param app_name app名字
     * @param userid   用户id或no
     * @return
     * @throws Exception
     */
    public JSONObject getPersonMsg(String app_name, String userid, int sum) throws Exception {
        if (sum <= 0) {
            log.info("获取人员信息sum为零");
            return null;
        }
        if (3 < sum) {
            return null;
        }
        AppInfo appInfo = tokenMap.get(app_name);
        String url = "https://qyapi.weixin.qq.com/cgi-bin/user/get?userid=" + userid + "&access_token="
                + appInfo.getToken();
        // 如果返回结果不等于0，表示此用户不存在，然后继续添加用户
        JSONObject resultJson = get(url);
        int errcode = resultJson.getInteger("errcode");
        if (tokenOver(errcode)) {
            getResetAppInfo(app_name);
            sum = sum + 1;
            getPersonMsg(app_name, userid, sum);
        }
        return resultJson;
    }

    /**
     * 删除部门
     *
     * @param app_name app名字
     * @param id       用户id或no
     * @return
     * @throws Exception
     */
    public JSONObject delDept(String app_name, String id, int sum) throws Exception {
        if (sum <= 0) {
            log.info("删除部门sum为零");
            return null;
        }
        if (3 < sum) {
            return null;
        }
        AppInfo appInfo = tokenMap.get(app_name);
        String url = "https://qyapi.weixin.qq.com/cgi-bin/department/delete?id=" + id + "&access_token="
                + appInfo.getToken();
        // 如果返回结果不等于0，表示删除失败
        JSONObject resultJson = get(url);
        int errcode = resultJson.getInteger("errcode");
        if (tokenOver(errcode)) {
            getResetAppInfo(app_name);
            sum = sum + 1;
            delDept(app_name, id, sum);
        }
        return resultJson;
    }

    /**
     * 修改部门信息
     *
     * @param app_name app名字
     * @param id       微信no
     * @param name     部门名字
     * @param sum
     * @throws Exception
     */
    public void updateDept(String app_name, String id, String name, String order, String parentid, int sum) throws Exception {
        if (sum <= 0) {
            log.info("修改部门信息sum为零");
            return;
        }
        if (3 < sum) {
            return;
        }
        AppInfo appInfo = tokenMap.get(app_name);
        String url = "https://qyapi.weixin.qq.com/cgi-bin/department/update?access_token=" + appInfo.getToken();
        JSONObject params = new JSONObject();
        params.put("id", id);
        params.put("name", name);
        params.put("parentid", parentid);
        params.put("order", order);
        JSONObject resultJson = post(params, url);
        int errcode = resultJson.getInteger("errcode");
        if (tokenOver(errcode)) {
            getResetAppInfo(app_name);
            sum = sum + 1;
            updateDept(app_name, id, name, order, parentid, sum);
        }
    }

    /**
     * 批量删除人员
     *
     * @param app_name app名字
     * @param sum
     * @throws Exception
     */
    public void delUsers(String app_name, String[] useridlist, int sum) throws Exception {
        if (sum <= 0) {
            log.info("修改部门信息sum为零");
            return;
        }
        if (3 < sum) {
            return;
        }
        AppInfo appInfo = tokenMap.get(app_name);
        String url = "https://qyapi.weixin.qq.com/cgi-bin/user/batchdelete?access_token=" + appInfo.getToken();
        JSONObject params = new JSONObject();
        params.put("useridlist", useridlist);
        JSONObject resultJson = post(params, url);
        int errcode = resultJson.getInteger("errcode");
        if (tokenOver(errcode)) {
            getResetAppInfo(app_name);
            sum = sum + 1;
            delUsers(app_name, useridlist, sum);
        }
    }

    /**
     * 添加部门
     *
     * @param app_name app名字
     * @param id       微信no
     * @param name     部门名字
     * @param sum
     * @throws Exception
     */
    public void addDept(String app_name, String id, String name, String order, String parentid, int sum) throws Exception {
        if (sum <= 0) {
            log.info("添加部门信息sum为零");
            return;
        }
        if (3 < sum) {
            return;
        }
        AppInfo appInfo = tokenMap.get(app_name);
        String url = "https://qyapi.weixin.qq.com/cgi-bin/department/create?access_token=" + appInfo.getToken();
        JSONObject params = new JSONObject();
        params.put("id", id);
        params.put("name", name);
        params.put("parentid", parentid);
        params.put("order", order);
        JSONObject resultJson = post(params, url);
        int errcode = resultJson.getInteger("errcode");
        if (tokenOver(errcode)) {
            getResetAppInfo(app_name);
            sum = sum + 1;
            addDept(app_name, id, name, order, parentid, sum);
        }
    }

    /**
     * 邀请成员
     *
     * @param app_name 应用名
     * @param id       成员userid
     * @param sum      请求次数
     * @throws Exception
     */
    public void wxInvite(String app_name, String id, int sum) throws Exception {
        if (sum <= 0) {
            log.info("邀请成员sum为零");
            return;
        }
        if (3 < sum) {
            return;
        }
        AppInfo appInfo = tokenMap.get(app_name);
        String url = "https://qyapi.weixin.qq.com/cgi-bin/batch/invite?access_token=" + appInfo.getToken();
        JSONObject params = new JSONObject();
        String[] user = {id};
        params.put("user", user);
        JSONObject resultJson = post(params, url);
        int errcode = resultJson.getInteger("errcode");
        if (tokenOver(errcode)) {
            getResetAppInfo(app_name);
            sum = sum + 1;
            wxInvite(app_name, id, sum);
        }
    }

    /**
     * 获取到所有成员详细信息
     *
     * @param app_name app名字
     * @return
     * @throws Exception
     */
    public JSONObject getAllUserMsg(String app_name, int department_id, int fetch_child, int sum) throws Exception {
        if (sum <= 0) {
            log.info("获取成员信息sum为零");
            return null;
        }
        if (3 < sum) {
            return null;
        }
        AppInfo appInfo = tokenMap.get(app_name);
        String url = "https://qyapi.weixin.qq.com/cgi-bin/user/list?department_id=" + department_id + "&fetch_child=" + fetch_child + "&access_token="
                + appInfo.getToken();
        // 如果返回结果不等于0，表示删除失败
        JSONObject resultJson = get(url);
        int errcode = resultJson.getInteger("errcode");
        if (tokenOver(errcode)) {
            getResetAppInfo(app_name);
            sum = sum + 1;
            getAllUserMsg(app_name, department_id, fetch_child, sum);
        }
        return resultJson;
    }

    /**
     * post方法
     *
     * @param params
     * @param url
     * @return
     * @throws Exception
     */
    public static JSONObject post(JSONObject params, String url) throws Exception {
        HttpPost httpPost = new HttpPost(url);
        httpPost.addHeader(HTTP.CONTENT_TYPE, "application/json");
        httpPost.setEntity(new StringEntity(JSONObject.toJSONString(params), "UTF-8"));
        HttpResponse response = Util.getDefaultHttpClient().execute(httpPost);
        String result = EntityUtils.toString(response.getEntity());
        JSONObject resultJson = JSONObject.parseObject(result);
        return resultJson;
    }

    /**
     * get方法
     *
     * @param url
     * @return
     * @throws Exception
     */
    public static JSONObject get(String url) throws Exception {
        HttpGet httpGet = new HttpGet(url);
        HttpResponse response = Util.getDefaultHttpClient().execute(httpGet);
        String result = EntityUtils.toString(response.getEntity());
        JSONObject resultJson = JSONObject.parseObject(result);
        return resultJson;
    }
}
