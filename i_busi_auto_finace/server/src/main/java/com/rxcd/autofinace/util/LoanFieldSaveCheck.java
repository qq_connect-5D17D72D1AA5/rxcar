/**
 * 版权所有：版权所有：重庆易房网络科技股份有限公司
 * Chongqing YiFang Network Technology Co. LTD.  All Rights Reserved
 * 创建者：HeSenLin   创建时期：  2016年7月15日
 */
package com.rxcd.autofinace.util;


import com.rxcd.autofinace.common.BizErrorCodeEnum;
import org.apache.commons.lang.StringUtils;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;

import org.appframe.wf.util.Param;
import org.appframe.wf.util.SafeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 贷款数据字段保存验证
 */
@Component
public class LoanFieldSaveCheck
{

	@Autowired
	private BaseDao baseDao;
	
	
	/**
	 * 贷款合同编号验证
	 * @param review_no
	 * @param transaction
	 * @param p
	 * @return
	 * @throws Exception
	 */
	public boolean loanConCodeCheck(String review_no, Param p) throws Exception{
		// 合同编号
		String value = p.getJsonString(p.getDataJson(), "value");
		
		// 查询被使用
		String sql = "select con_code "
				   + "  from loan_con "
				   + " where status='1' "
				   + "   and con_code='"+SafeUtil.safe(value)+"'";
		Row conRow = baseDao.queryRow(sql);
		
		if(conRow != null){
			throw new Exception(BizErrorCodeEnum.B0011.getCode());
		}
		
		return true;
	}
	
	/**
	 * 查询车牌号是否正确
	 * @param review_no
	 * @param transaction
	 * @param p
	 * @return
	 * @throws Exception
	 */
	public boolean carPlateCheck(String review_no, Param p) throws Exception{
		String value = p.getJsonString(p.getDataJson(), "value");
		if(value.contains(" ")){
			throw new Exception(BizErrorCodeEnum.B0089.getCode());
		}
		if (StringUtils.isNotEmpty(value) && value.length()!=7 && value.length()!=8) {
			throw new Exception(BizErrorCodeEnum.T1042.getCode());
		}
		String regex=".*[a-z]+.*";  
        Matcher m=Pattern.compile(regex).matcher(value);
        if(m.matches()){
        	throw new Exception(BizErrorCodeEnum.B0090.getCode());
        }
		return true;
	}
}
