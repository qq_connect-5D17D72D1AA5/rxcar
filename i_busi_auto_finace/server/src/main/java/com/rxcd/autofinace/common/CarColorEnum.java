package com.rxcd.autofinace.common;

import org.apache.commons.lang.StringUtils;

/**
 * 汽车颜色枚举
 */
public enum CarColorEnum {
	
	WHITE("白","A","12","A"),
	GRAY("深灰","B","6","B"),
	YELLOW("黄","C","13","C"),
	RED("红","D","10","E"),
	PURPLE("紫","E","9","F"),
	GREEN("绿","F","5","G"),
	BLUE("蓝","G","2","H"),
	BROWN("棕","H","3","I"),
	BLACK("黑","I","1","J"),
	ORANGE("橙",null,"7",null),
	GOLD("金",null,"4",null),
	PINK("粉",null,"8","D"),
	SILVERY("银灰",null,"11",null),
	CYAN("青",null,"14",null),
	COFFEE("咖啡",null,null,null),
	CHAMPAGNE("香槟",null,null,null),
	OTHER("其他",null,"0",null),
	;

	public static final int CHEJIANDING=1;
	public static final int JINZHENGU=2;
	public static final int ACEDATA=3;
	
	/**
	 * 获取颜色枚举
	 * 
	 * @param licensePlateNumber
	 * @return
	 */
	public static CarColorEnum getCarColorEnum(String colorKey) {
		CarColorEnum[] enums = CarColorEnum.values();
		for (CarColorEnum tempEnum : enums) {
			if (StringUtils.equals(tempEnum.name(), colorKey)) {
				return tempEnum;
			}
		}
		return null;
	}
	
	
	public static CarColorEnum getCarColorEnumByCode(String colorCode,int company){
		switch(company){
			case CHEJIANDING:
				return getCarColorEnumByCode1(colorCode);
			case JINZHENGU:
				return getCarColorEnumByCode2(colorCode);
			case ACEDATA:
				return getCarColorEnumByCode3(colorCode);
			default:
				return null;
		}
		
	}
	
	
	public static CarColorEnum getCarColorEnumByCode1(String colorCode){
		CarColorEnum[] enums = CarColorEnum.values();
		for (CarColorEnum tempEnum : enums) {
			if (StringUtils.equals(tempEnum.color_code_1, colorCode)) {
				return tempEnum;
			}
		}
		return null;
	}
	
	public static CarColorEnum getCarColorEnumByCode2(String colorCode){
		CarColorEnum[] enums = CarColorEnum.values();
		for (CarColorEnum tempEnum : enums) {
			if (StringUtils.equals(tempEnum.color_code_2, colorCode)) {
				return tempEnum;
			}
		}
		return null;
	}
	
	public static CarColorEnum getCarColorEnumByCode3(String colorCode){
		CarColorEnum[] enums = CarColorEnum.values();
		for (CarColorEnum tempEnum : enums) {
			if (StringUtils.equals(tempEnum.color_name, colorCode)) {
				return tempEnum;
			}
		}
		return null;
	}
	
	CarColorEnum(String color_name, String color_code_1, String color_code_2, String color_code_3){
		this.color_name=color_name;
		this.color_code_1=color_code_1;
		this.color_code_2=color_code_2;
		this.color_code_3=color_code_3;
	}
	
	//汽车颜色
	private String color_name;
	//车鉴定对应码
	private String color_code_1;
	//精真估对应码
	private String color_code_2;
	//优分对应码
	private String color_code_3;

	public String getColor_name() {
		return color_name;
	}

	public void setColor_name(String color_name) {
		this.color_name = color_name;
	}

	public String getColor_code_1() {
		return color_code_1;
	}

	public void setColor_code_1(String color_code_1) {
		this.color_code_1 = color_code_1;
	}

	public String getColor_code_2() {
		return color_code_2;
	}

	public void setColor_code_2(String color_code_2) {
		this.color_code_2 = color_code_2;
	}
	
	public String getColor_code_3() {
		return color_code_3;
	}

	public void setColor_code_3(String color_code_3) {
		this.color_code_3 = color_code_3;
	}
}
