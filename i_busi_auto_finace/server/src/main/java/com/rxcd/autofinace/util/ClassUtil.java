package com.rxcd.autofinace.util;

import com.rxcd.autofinace.common.BizErrorCodeEnum;
import com.rxcd.autofinace.service.LoanAssAutoService;
import com.rxcd.autofinace.service.LoanService;
import org.apache.commons.lang.StringUtils;
import org.appframe.common.BigDecimalUtil;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.db.Store;
import org.appframe.wf.util.Param;
import org.appframe.wf.util.SafeUtil;
import org.appframe.wf.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;

@Component
public class ClassUtil {
    @Autowired
    private BaseDao baseDao;
    @Autowired
    private RxErpUtil erpUtil;
    @Autowired
    private LoanService loanService;
    @Autowired
    private LoanAssAutoService loanAssAutoService;

    /**
     * 设置合同编号
     *
     * @param review_no 流程NO
     * @param p
     * @throws Exception
     */
    public void setConCode(String review_no, Param p) throws Exception {
        String sql = "select a.loan_no,"
                + "		  a.branch_no,"
                + "		  b.con_code,"
                + "		  c.loan_date,"
                + "		  c.loan_type,"
                + "		  e.templet_no "
                + "  from loan_info a "
                + "  left join loan_con b on b.loan_no=a.loan_no "
                + "	 left join loan_info_et c on c.loan_no=a.loan_no "
                + "	 join wf_review_biz d on d.table_key_no=a.loan_no "
                + "	 join wf_review e on e.review_no=d.review_no and e.review_no='" + SafeUtil.safe(review_no) + "' ";
        Row loanRow = baseDao.queryRow(sql);
        if (loanRow == null) {
            throw new Exception(BizErrorCodeEnum.B0013.getCode());
        }
        if (!"01".equals(loanRow.getString("loan_type"))){
            return;
        }
        // 贷款NO
        String loan_no = loanRow.getString("loan_no");
        // 合同号
        String con_code = loanRow.getStringNotNull("con_code");

        // 合同日期
        String dateYmd = loanRow.getString("loan_date");
        if (Util.isEmpty(dateYmd)) {
            dateYmd = Util.format(new Date(), "yyMMdd");
        } else {
            dateYmd = Util.format(Util.parseDate(dateYmd, "yyyy-MM-dd"), "yyMMdd");
        }
        //合同号
        String new_con_code = null;
        sql = "select sell_branch_no from loan_info where loan_no = '" + loan_no + "' LIMIT 1";
        String sell_branch_no = (String) baseDao.queryObject(sql);
        sql = "select dept_name_py from i_frame_sys_dept where dept_id = '" + sell_branch_no + "'";
        String deptCode = (String) baseDao.queryObject(sql);
        new_con_code = deptCode + dateYmd;
        // 不存在合同号，自动生成, 存在合同号，就更新
        //排除自身
        String old_con = "";
        if (Util.isNotEmpty(con_code)) {
            old_con = " and con_code !='" + con_code + "'";
        }
        //序号
        String num = null;
        Store cons = baseDao.query("select con_code from loan_con where status='1' and con_code not like '%-%' and con_code like '" + new_con_code + "%'" + old_con + " order by con_code");
        int count = 0;
        if (cons != null && cons.size() != 0) {
            count = cons.size();
            for (int i = 0; i < cons.size(); i++) {
                Row con = cons.get(i);
                String item = new_con_code + String.format("%02d", i + 1);
                con.get("con_code");
                if (!item.equals(con.get("con_code").toString())) {
                    num = String.format("%02d", i + 1);
                    break;
                }
            }
        }
        if (num == null) {
            num = String.format("%02d", count + 1);
        }
        // 追加序号
        new_con_code = new_con_code + num;
        sql = "update loan_con set con_code='" + new_con_code + "' where loan_no='" + loan_no + "'";
        baseDao.execute(sql);

    }


    /**
     * 贷前系统:车辆评估录入评估价1自动生成评估价2为评估价1的60%,并重新生成评估时间
     *
     * @param review_no
     * @param p
     * @throws Exception
     */
    public void priceTwoAutoChange(String review_no, Param p) throws Exception {
        //触发源
        String field = p.getJsonString(p.getDataJson(), "field");
        String loan_no = getTableKeyNo(review_no);
        if (StringUtils.equals("loan_ass.ass_price1", field)) {
            baseDao.execute("update loan_ass set ass_date=now(),ass_price2=ass_price1*0.8 where loan_no='" + loan_no + "'");
        } else if (StringUtils.equals("loan_ass.artificial_price_true", field)) {
            baseDao.execute("update loan_ass set ass_date=now(),ass_price2=artificial_price_true*0.8 where loan_no='" + loan_no + "'");
        }
    }







    /**
     * 重新生成repay_date_flag
     *
     * @param review_no
     * @param p
     * @throws Exception
     */
    public void regenerateRepayDateFlag(String review_no, Param p) throws Exception {
        Row loanRow = baseDao.queryRow("select a.loan_no,a.repay_date_flag,a.loan_type,a.loan_date,b.duetime_unit from loan_info_et a left join loan_cost b on a.loan_no=b.loan_no left join wf_review_biz c on c.table_key_no=a.loan_no where c.review_no='" + review_no + "'");
        String loan_type = loanRow.getString("loan_type");
        if ("03".equals(loan_type) || "04".equals(loan_type)) {
            Row oldLoanRow = baseDao.queryRow("select c.repay_date_flag,c.loan_type,c.fk_date,d.duetime_unit from loan_info a left join wf_review_biz b on a.loan_no=b.table_key_no left join loan_info_et c on c.loan_no=a.renewal_loan_no left join loan_cost d on d.loan_no=a.renewal_loan_no where b.review_no='" + review_no + "'");
            String duetime_unit = loanRow.getString("duetime_unit");
            String old_duetime_unit = oldLoanRow.getString("duetime_unit");
            if (!old_duetime_unit.equals(duetime_unit)) {
                String loan_date = loanRow.get("loan_date") == null ? "" : loanRow.get("loan_date").toString();
                String repay_date_flag = oldLoanRow.get("repay_date_flag") == null ? "" : oldLoanRow.get("repay_date_flag").toString();
                if (Util.isEmpty(loan_date) || Util.isEmpty(repay_date_flag)) {
                    return;
                }
                Calendar c = Calendar.getInstance();
                c.setTime(Util.parseDate(repay_date_flag));
                while (Util.daysBetween(loan_date, repay_date_flag) < 0) {
                    if ("7".equals(old_duetime_unit)) {
                        c.add(Calendar.DAY_OF_MONTH, 6);
                    } else if ("15".equals(old_duetime_unit)) {
                        c.add(Calendar.DAY_OF_MONTH, 14);
                    } else if ("30".equals(old_duetime_unit)) {
                        c.add(Calendar.MONTH, 1);
                    }
                    repay_date_flag = Util.format(c.getTime());
                }
                baseDao.execute("update loan_info_et set repay_date_flag='" + repay_date_flag + "' where loan_no='" + loanRow.getString("loan_no") + "'");
            }
        } else {
            baseDao.execute("update loan_info_et set repay_date_flag=loan_date where loan_no='" + loanRow.getString("loan_no") + "'");
        }
    }

    /**
     * 计算预收利息
     *
     * @param review_no
     * @param p
     * @throws Exception
     */
    public void advance_rate_sum_compute(String review_no, Param p) throws Exception {
        String loan_no = getTableKeyNo(review_no);
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        int duetime = erpUtil.objectIsNullGetInteger(loanRow.get("loan_cost.duetime"));
        double contract_sum = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.contract_sum"));
        double advance_rate_percent = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.advance_rate_percent"));
        double advance_rate_sum = BigDecimalUtil.round(BigDecimalUtil.mul(BigDecimalUtil.mul(contract_sum, Double.valueOf(advance_rate_percent) / 100), duetime), 2);
        String sql = "update loan_cost set advance_rate_sum=" + advance_rate_sum + " where loan_no = '" + loan_no + "'";
        baseDao.execute(sql);
    }


    /**
     * 通过修改实际审批金额，更改实际审批成数
     *
     * @param review_no
     * @param p
     * @throws Exception
     */
    public void updateApprovePercentage(String review_no, Param p) throws Exception {
        String loan_no = (String) baseDao.queryObject("select table_key_no from wf_review_biz where review_no='" + review_no + "' ");
        String sql = "select a.system_ass_price,t.max_approve_sum,a.ass_price1 "
                + "from loan_cost t "
                + "left join loan_ass a on t.loan_no=a.loan_no "
                + "WHERE t.loan_no='" + SafeUtil.safe(loan_no) + "' ";
        Row custRow = baseDao.queryRow(sql);
        double count = 0;
        if (erpUtil.objectIsNullGetDouble(custRow.get("ass_price1")) <= 0) {
            count = Double.valueOf(custRow.get("system_ass_price") == null ? "0" : custRow.get("system_ass_price").toString());//大数据初审评估价
        } else {
            count = Double.valueOf(custRow.get("ass_price1") == null ? "0" : custRow.get("ass_price1").toString());//人工精确估计
        }
        double max_approve_sum = Double.valueOf(custRow.get("max_approve_sum") == null ? "0" : custRow.get("max_approve_sum").toString());//最大审批金额
        double max_approve_percentage = max_approve_sum / count * 10;
        sql = "update loan_info_et set max_approve_percentage = '" + max_approve_percentage + "' where loan_no='" + SafeUtil.safe(loan_no) + "'";
        baseDao.execute(sql);
    }

    /**
     * 更新预付款金额
     *
     * @param review_no
     * @param p
     * @throws Exception
     */
    public void updatePrepayAmount(String review_no, Param p) throws Exception {
        String loan_no = getTableKeyNo(review_no);
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String prod_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info.prod_no"));

        String is_prepay = erpUtil.objectIsNullGetString(loanRow.get("loan_info_et.is_prepay"));
        double prepay_percentage = erpUtil.objectIsNullGetDouble(loanRow.get("loan_info_et.prepay_percentage"));
        double contract_sum = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.contract_sum"));
        double car_price = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.car_price"));

        double prepay_sum = 0;
        if (StringUtils.equals("01", is_prepay)) {
            if (StringUtils.equals("51", prod_no)) {
                prepay_sum = BigDecimalUtil.decimal(BigDecimalUtil.mul(car_price, BigDecimalUtil.div(prepay_percentage, 10)), 2);
            } else {
                prepay_sum = BigDecimalUtil.decimal(BigDecimalUtil.mul(contract_sum, BigDecimalUtil.div(prepay_percentage, 10)), 2);
            }
        } else {
            prepay_sum = 0;
        }
        double contribution_amount = erpUtil.objectIsNullGetDouble(baseDao.queryObject("select contribution_amount from loan_info_employers where loan_no='" + loan_no + "' and status='1' and pay_type='04'"));
        if (contribution_amount != 0 && contribution_amount != prepay_sum) {
            throw new Exception(BizErrorCodeEnum.T1094.getCode());
        }
        baseDao.execute("update loan_cost set prepay_sum = " + prepay_sum + " where  loan_no = '" + SafeUtil.safe(loan_no) + "'");
    }

    /**
     * 计算合同金额
     *
     * @param review_no
     * @param p
     * @throws Exception
     */
    public void updateContractSum(String review_no, Param p) throws Exception {
        String loan_no = getTableKeyNo(review_no);
        //触发合同金额重新生成
        loanService.updateConAmount(loan_no);
        //触发重新计算预收管理费
        loanService.updateAdvanceRateSum(loan_no);
        //触发重新计算首期费用(二手车直租)
        loanService.updateInitialPaymentFee(loan_no);
    }


    /**
     * 计算预收利息
     *
     * @param review_no
     * @param p
     * @throws Exception
     */
    public void updateAdvanceRateSum(String review_no, Param p) throws Exception {
        String loan_no = getTableKeyNo(review_no);
        loanService.updateAdvanceRateSum(loan_no);
    }


    /**
     * 触发保证金提额变动
     *
     * @param review_no
     * @param p
     * @throws Exception
     */
    public void updateDepositSum(String review_no, Param p) throws Exception {
        String loan_no = getTableKeyNo(review_no);
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String prod_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info.prod_no"));
        if (StringUtils.equals("51", prod_no)) {
            //触发临时担保额度变动(二手车直租)
            loanService.updateTempSum(loan_no);
        }
        //更新保证金提额
        loanService.updateDepositSum(loan_no);
        //触发合同金额重新生成
        loanService.updateConAmount(loan_no);
        //触发重新计算预收管理费
        loanService.updateAdvanceRateSum(loan_no);
    }


    /**
     * 实际审批成数，触发修改实际审批金额
     *
     * @param review_no
     * @param p
     * @throws Exception
     */
    public void updateApprovePercent(String review_no, Param p) throws Exception {
        String loan_no = getTableKeyNo(review_no);
        loanService.updateApprovePercent(loan_no);
        //目前只能是实际金额触发改动实际成数,所以不循环触发改动实际金额
    }

    /**
     * @param review_no
     * @param p
     * @throws Exception
     */
    public void updateApproveSumByMaxApproveSum(String review_no, Param p) throws Exception {
        String loan_no = getTableKeyNo(review_no);
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String prod_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info.prod_no"));
        if (StringUtils.equals("51", prod_no)) {
            loanService.updateApproveSumByMaxApproveSumAndCarPrice(loan_no);
        } else {
            loanService.updateApproveSum(loan_no);
        }

        //触发临时担保额度变动(二手车直租)
        loanService.updateTempSum(loan_no);
        //触发提额变动
        loanService.updateDepositSum(loan_no);
        //触发合同金额重新生成
        loanService.updateConAmount(loan_no);
        //触发重新计算预收管理费
        loanService.updateAdvanceRateSum(loan_no);
        //触发重新计算首期费用(二手车直租)
        loanService.updateInitialPaymentFee(loan_no);
    }


    /**
     * 实际审批成数，触发修改实际审批金额
     *
     * @param review_no
     * @param p
     * @throws Exception
     */
    public void updateApproveSum(String review_no, Param p) throws Exception {
        String loan_no = getTableKeyNo(review_no);
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String prod_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info.prod_no"));
        if (StringUtils.equals("51", prod_no) || StringUtils.equals("52", prod_no) || StringUtils.equals("53", prod_no)) {
            //实际审批金额按大值取
            loanService.updateApproveSumByMaxApproveSumAndCarPrice(loan_no);
        } else {
            //实际审批金额按小值取
            loanService.updateApproveSum(loan_no);
        }

        //触发临时担保额度变动(二手车直租)
        loanService.updateTempSum(loan_no);
        //触发提额变动
        loanService.updateDepositSum(loan_no);
        //触发合同金额重新生成
        loanService.updateConAmount(loan_no);
        //触发重新计算预收管理费
        loanService.updateAdvanceRateSum(loan_no);
        //触发重新计算首期费用(二手车直租)
        loanService.updateInitialPaymentFee(loan_no);
    }


    /**
     * 修改最大审批成数
     *
     * @param review_no
     * @param p
     * @throws Exception
     */
    public void updateMaxApprovePercent(String review_no, Param p) throws Exception {
        //触发源
        String field = p.getJsonString(p.getDataJson(), "field");

        String loan_no = getTableKeyNo(review_no);
        //触发源为 最大审批金额，则只需要更新最大成数
        if (StringUtils.equals("loan_cost.max_approve_sum", field)) {
            loanService.updateMaxApprovePercentByMaxApproveSum(loan_no);
        } else {
            //触发源为 prod_version,则需要先更新prod_no
            if (StringUtils.equals("loan_info.prod_version", field)) {
                loanService.updateProdNo(loan_no);
            }//触发源为prod_no,则需要先更新prod_version
            else if (StringUtils.equals("loan_info.prod_no", field)) {
                loanService.updateProdVersion(loan_no);
            }
            loanService.updateMaxApprovePercent(loan_no);
            loanService.updateMaxApproveSum(loan_no);
            //这里不需要后续触发改动，流程提交验证即可，让销售主动修改实际审批金额触发后续改动
        }
    }


    /**
     * 修改最大审批金额
     *
     * @param review_no
     * @param p
     * @throws Exception
     */
    public void updateMaxApproveSum(String review_no, Param p) throws Exception {
        String loan_no = getTableKeyNo(review_no);
        loanService.updateMaxApproveSum(loan_no);
        //这里不需要后续触发改动，流程提交验证即可，让销售主动修改实际审批金额触发后续改动
    }


    /**
     * 真实终价改动触发大数据终价改动
     *
     * @param review_no
     * @param p
     * @throws Exception
     */
    public void updateSystemAssPrice2(String review_no, Param p) throws Exception {
        String loan_no = getTableKeyNo(review_no);
        loanService.updateSystemAssPrice2(loan_no);
        //后续 最大审批金额、实际审批金额、提额、合同金额  不触发变动（在节点结束时触发变动）
    }


    /**
     * 真实人工价改动触发人工价改动
     *
     * @param review_no
     * @param p
     * @throws Exception
     */
    public void updateArtificialPrice(String review_no, Param p) throws Exception {
        String loan_no = getTableKeyNo(review_no);
        loanService.updateArtificialPrice(loan_no);
        //后续 最大审批金额、实际审批金额、提额、合同金额  不触发变动（在节点结束时触发变动）
    }


    /**
     * 更新汽车初价，综合岗重选车型、公里数触发
     *
     * @param review_no
     * @param p
     * @throws Exception
     */
    public void updateCarPriceFinal(String review_no, Param p) throws Exception {
        String loan_no = getTableKeyNo(review_no);
        //获取接口终价
        loanService.queryCarPriceFinal(loan_no);
        //后续 最大审批金额、实际审批金额、提额、合同金额  不触发变动（在节点结束时触发变动）
    }


    /**
     * 修改最大审批成数
     *
     * @param review_no
     * @param p
     * @throws Exception
     */
    public void upadteFeePordAndVesrion(String review_no, Param p) throws Exception {
        //触发源
        String field = p.getJsonString(p.getDataJson(), "field");

        String loan_no = getTableKeyNo(review_no);
        //触发源为 prod_version,则需要先更新prod_no
        if (StringUtils.equals("loan_info.prod_version", field)) {
            loanService.updateProdNo(loan_no);
        }//触发源为prod_no,则需要先更新prod_version
        else if (StringUtils.equals("loan_info.prod_no", field)) {
            loanService.updateProdVersion(loan_no);
        }
        loanService.updateMaxApprovePercent(loan_no);
        loanService.updateMaxApproveSum(loan_no);
        //这里不需要后续触发改动，流程提交验证即可，让销售主动修改实际审批金额触发后续改动
    }


    /**
     * 更新汽车初价，业务员重选车型、公里数触发
     *
     * @param review_no
     * @param p
     * @throws Exception
     */
    public void updateCarPriceFirst(String review_no, Param p) throws Exception {
        String loan_no = getTableKeyNo(review_no);
        //获取接口初价
        loanService.queryCarPriceFirst(loan_no);
        //自动评估，得到汽车初况、大数据初价、真实初价
        loanAssAutoService.autoAss(loan_no, LoanAssAutoService.BRANCH_SCORE);
        loanAssAutoService.autoAss(loan_no, LoanAssAutoService.BRANCH_INIT_PRICE);
        loanAssAutoService.autoAss(loan_no, LoanAssAutoService.TURE_INIT_PRICE);
        //触发变动最大审批金额
        loanService.updateMaxApproveSum(loan_no);
        //触发变动实际审批金额
        loanService.updateApproveSumByMaxApproveSumAndCarPrice(loan_no);
        //触发临时担保额度变动(二手车直租)
        loanService.updateTempSum(loan_no);
        //触发提额变动
        loanService.updateDepositSum(loan_no);
        //触发合同金额重新生成
        loanService.updateConAmount(loan_no);
        //触发重新计算预收管理费
        loanService.updateAdvanceRateSum(loan_no);
        //触发重新计算首期费用(二手车直租)
        loanService.updateInitialPaymentFee(loan_no);
    }


    /**
     * 评估师重录入车架号 触发 重新查询汽车品牌类型
     *
     * @param review_no
     * @param p
     * @throws Exception
     */
    public void updateCarBrandForAss(String review_no, Param p) throws Exception {
        String loan_no = getTableKeyNo(review_no);
        loanService.queryCarBrandFinal(loan_no);
    }

    /**
     * 修改银行卡同时更新代扣银行卡
     *
     * @param review_no
     * @param p
     * @throws Exception
     */
    public void setWithholdBankAccountNo(String review_no, Param p) throws Exception {
        String loan_no = (String) baseDao.queryObject("select table_key_no from wf_review_biz where review_no='" + review_no + "' ");
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String bank_accout = erpUtil.objectIsNullGetString(loanRow.get("loan_info_et.bank_accout"));
        String sql = "update loan_info_et set loan_info_et.withhold_bank_account_no = '" + bank_accout + "' where loan_no = '" + loan_no + "'";
        baseDao.execute(sql);
    }

    /**
     * 输入融租期限，还租方式 触发 修改清算类型
     *
     * @param review_no
     * @param p
     * @throws Exception
     */
    public void updateClearType(String review_no, Param p) throws Exception {
        String loan_no = getTableKeyNo(review_no);
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String duetime = erpUtil.objectIsNullGetString(loanRow.get("loan_cost.duetime"));
        String rep_type = erpUtil.objectIsNullGetString(loanRow.get("loan_info_et.rep_type"));
        String sql;
        if ("6".equals(duetime) && "02".equals(rep_type)){
            sql = "update loan_info_et set clear_type = '1',demolition_period = '24',periods_number = '1' where loan_no = '"+loan_no+"'";
        }else{
            sql = "update loan_info_et set clear_type = '0',demolition_period = '',periods_number = '' where loan_no = '"+loan_no+"'";
        }
        baseDao.execute(sql);
    }

    private String getTableKeyNo(String review_no) throws Exception {
        String table_key_no = (String) baseDao.queryObject("select table_key_no from wf_review_biz where review_no='" + review_no + "' ");
        return table_key_no;
    }



}