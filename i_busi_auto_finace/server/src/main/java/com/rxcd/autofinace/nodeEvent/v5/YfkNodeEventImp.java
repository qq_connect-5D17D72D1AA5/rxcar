package com.rxcd.autofinace.nodeEvent.v5;


import com.rxcd.autofinace.service.FkService;
import com.rxcd.autofinace.service.LoanCheckServiceImpl;
import com.rxcd.autofinace.service.LoanService;
import com.rxcd.autofinace.util.RxErpUtil;
import org.appframe.wf.engine.AbstractWorkFlowNodeEvent;
import org.appframe.wf.service.WfParamUtil;
import org.appframe.wf.util.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 预付款
 *
 * @author hubin
 */
@Component
public class YfkNodeEventImp extends AbstractWorkFlowNodeEvent {
    @Autowired
    private WfParamUtil paramUtil;

    @Autowired
    private LoanCheckServiceImpl checkService;
    @Autowired
    private RxErpUtil erpUtil;
    @Autowired
    private LoanService loanService;
    @Autowired
    private FkService fkService;

    /**
     * 流程节点启动时调用
     *
     * @param flow_node_no
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowNodeStart(String flow_node_no, Param p) throws Exception {

    }

    @Override
    public void workFlowNodeFinish(String flow_node_no, Param p) throws Exception {
        checkTempletFlowNo("1");
    }

    /**
     * 流程节点驳回时回调方法
     *
     * @param flow_node_no
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowNodeOverrule(String flow_node_no, Param p) throws Exception {

    }

    @Override
    public void workFlowNodeVie(String flow_node_no, Param p) throws Exception {

    }
}
