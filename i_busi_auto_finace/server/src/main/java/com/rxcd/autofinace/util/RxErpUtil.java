/**
 * 版权所有：版权所有：重庆易房网络科技股份有限公司
 * Chongqing YiFang Network Technology Co. LTD.  All Rights Reserved
 * 创建者：HeSenLin   创建时期：  2016年3月16日
 */
package com.rxcd.autofinace.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fida.clearing.impl.AccountClientImpl;
import com.rxcd.autofinace.common.BizErrorCodeEnum;
import com.rxcd.autofinace.service.weixin.QywxApiService;
import com.rxcd.autofinace.servlet.Result;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.appframe.bigdata.JiAoClinet2;
import org.appframe.bigdata.TianXingClinet2;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.db.Store;
import org.appframe.wf.engine.common.FieldTypeEnum;
import org.appframe.wf.engine.common.FormUtil;
import org.appframe.wf.engine.imp.DefaultWorkFlow;
import org.appframe.wf.form.imp.DefaultWorkForm;
import org.appframe.wf.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 *
 */
@Component
public class RxErpUtil {
    private final static Log log = LogFactory.getLog(RxErpUtil.class);
    @Value("runtime")
    private String runtime;
    @Autowired
    private Util util;
    @Autowired
    private BaseDao baseDao;
    @Autowired
    private TianXingClinet2 tianXingClinet;
    @Autowired
    private DefaultWorkFlow flow;
    @Autowired
    private QywxApiService qywxApiService;
    @Autowired
    private RxErpUtil erpUtil;
    @Autowired
    private FormUtil formUtil;

    @Autowired
    private DefaultWorkForm workForm;
    @Autowired
    private AccountClientImpl accountClient;
    @Autowired
    private JiAoClinet2 jiAoClinet;
    /**
     * 判断NULL得到double
     *
     * @param obj
     * @return
     */
    public double objectIsNullGetDouble(Object obj) {
        return Double.parseDouble(obj == null ? "0" : String.valueOf(obj));
    }

    /**
     * 判断NULL得到String
     *
     * @param obj
     * @return
     */
    public String objectIsNullGetString(Object obj) {
        return obj == null ? "" : String.valueOf(obj);
    }

    /**
     * 判断NULL得到int
     *
     * @param obj
     * @return
     */
    public int objectIsNullGetInteger(Object obj) {
        return Integer.parseInt(obj == null ? "0" : String.valueOf(obj));
    }


    /**
     * 得到融租基本信息
     *
     * @param loan_no
     * @return
     * @throws SQLException
     */
    public Row getLoanBasicDataInfoV2(String loan_no) {
        String[][] tables = {{"from", "loan_info", ""},
                {"left join", "loan_cost", "on loan_info.loan_no = loan_cost.loan_no"},
                {"left join", "loan_info_et", "on loan_info.loan_no = loan_info_et.loan_no"},
                {"left join", "loan_con", "on loan_info.loan_no = loan_con.loan_no"},
                {"left join", "loan_cust", "on loan_info.loan_no = loan_cust.loan_no"},
                {"left join", "loan_car", "on loan_info.loan_no = loan_car.loan_no"},
                {"left join", "loan_ass", "on loan_info.loan_no = loan_ass.loan_no"},
                {"left join", "loan_prod", "on loan_info.prod_no = loan_prod.prod_no"},
                {"left join", "loan_audi", "on loan_info.loan_no = loan_audi.loan_no"},
                {"left join", "wf_review_biz", "on wf_review_biz.table_key_no = loan_info.loan_no"},
                {"left join", "wf_review", "on wf_review.review_no = wf_review_biz.review_no"}};
        String query_fields = "";
        for (int i = 0; i < tables.length; i++) {
            query_fields += getTableCols(tables[i][1]);
        }
        query_fields = query_fields.substring(0, query_fields.length() - 1);
        StringBuffer sql =
                new StringBuffer("select ").append(query_fields).append(" ");
        for (int i = 0; i < tables.length; i++) {
            sql.append(tables[i][0]).append(" ").append(tables[i][1]).append(" ").append(tables[i][2]).append(" ");
        }
        sql.append("where loan_info.loan_no='" + SafeUtil.safe(loan_no) + "' ");
        Row retRow;
        retRow = baseDao.queryRow(sql.toString());

        return retRow;
    }

    /**
     * 得到合同所需要基本信息
     *
     * @param loan_no
     * @return
     * @throws SQLException
     */
    public Row getLoanConData(String loan_no) {
        String[][] tables = {{"from", "loan_info", ""},
                {"left join", "loan_cost", "on loan_info.loan_no = loan_cost.loan_no"},
                {"left join", "loan_info_et", "on loan_info.loan_no = loan_info_et.loan_no"},
                {"left join", "loan_con", "on loan_info.loan_no = loan_con.loan_no"},
                {"left join", "loan_cust", "on loan_info.loan_no = loan_cust.loan_no"},
                {"left join", "loan_ass", "on loan_info.loan_no = loan_ass.loan_no"},
                {"left join", "loan_prod", "on loan_info.prod_no = loan_prod.prod_no"}};
        String query_fields = "";
        for (int i = 0; i < tables.length; i++) {
            query_fields += getTableCols(tables[i][1]);
        }
        query_fields = query_fields.substring(0, query_fields.length() - 1);
        StringBuffer sql =
                new StringBuffer("select ").append(query_fields).append(" ");
        for (int i = 0; i < tables.length; i++) {
            sql.append(tables[i][0]).append(" ").append(tables[i][1]).append(" ").append(tables[i][2]).append(" ");
        }
        sql.append("where loan_info.loan_no='" + SafeUtil.safe(loan_no) + "' ");
        Row retRow;
        retRow = baseDao.queryRow(sql.toString());

        return retRow;
    }

    private String getTableCols(String table_name) {
        StringBuffer cols = new StringBuffer();
        Store col_fields = baseDao.query("show columns from " + table_name);
        for (Row col : col_fields) {
            cols.append(table_name + "." + col.get("field") + " as '" + table_name + "." + col.get("field") + "',");
        }
        return cols.toString();
    }

    /**
     * 列表初始化条件使用参数
     *
     * @param p
     * @return
     */
    public Row getWhereInitVal(Param p) {
        Row row = new Row();
        row.put("oper_user_no", p.getLoginUserNo());
        row.put("oper_position_no", util.getUserPositionNo(p.getLoginUserNo()));
        return row;
    }

    /**
     * 替换字段备注
     *
     * @param valRow      数据源
     * @param fieldRemark 规则
     * @return
     * @throws Exception
     */
    public static String replaceFieldRemark(Row valRow, String fieldRemark) throws Exception {
        if (Util.isEmpty(fieldRemark)) {
            return "";
        }
        Formula conFormula;
        StringBuffer sb = new StringBuffer();
        Pattern p = Pattern.compile("(\\$\\{[^\\}]*\\})");
        Matcher m = p.matcher(fieldRemark);

        String expr;
        String[] exprArr;
        Row param;
        while (m.find()) {
            expr = "";
            param = new Row();
            exprArr = m.group().replaceAll("\\$", "").replaceAll("\\{", "").replaceAll("\\}", "").split(";");
            if (exprArr.length > 1) {
                for (String exprTmp : exprArr) {
                    String[] t = exprTmp.split("=");
                    if (t.length == 2) {
                        if (t[1].indexOf("new ") >= 0) {
                            param.put(t[0].trim(), Class.forName(t[1].replaceAll("new ", "").trim()).newInstance(), true);
                        } else if (t[1].lastIndexOf(".class") >= 0) {
                            param.put(t[0].trim(), Class.forName(t[1].replaceAll(".class", "").trim()), true);
                        } else {
                            expr += exprTmp + ";";
                        }
                    } else {
                        expr += exprTmp + ";";
                    }
                }
            } else {
                expr = exprArr[0];
            }

            // 条件表达式计算
            conFormula = new Formula(expr);

            Iterator iterator = valRow.keySet().iterator();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                conFormula.setVariable(key, valRow.get(key));
            }
            iterator = param.keySet().iterator();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                conFormula.setVariable(key, param.get(key, true));
            }

            Object val = conFormula.calc();
            if (val == null) {
                m.appendReplacement(sb, "");
            } else {
                m.appendReplacement(sb, val.toString());
            }
        }
        m.appendTail(sb);
        return sb.toString();
    }

    /**
     * 检查是否续租
     *
     * @param loan_no
     * @return
     * @throws SQLException
     */
    public boolean checkIsXQORXZD(String loan_no) throws SQLException {
        String sql = "select loan_info_et.loan_type from loan_info_et where loan_no = '" + loan_no + "'";
        String loan_type = null;

        loan_type = (String) baseDao.queryObject(sql);

        if ("04".equals(loan_type) || "03".equals(loan_type)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 得到违章押金
     *
     * @param car_plate
     * @param vin
     * @param driveNum
     * @return
     * @throws Exception
     */
    public long getOffencesMoney(String car_plate, String vin, String driveNum, String cityCode) throws Exception {
        JSONObject jsonObject = tianXingClinet.queryTrafficOffences(car_plate, vin, driveNum, cityCode, false);
        if (jsonObject.getJSONObject("data").getBooleanValue("hasData")) {
            JSONArray jsonArray = jsonObject.getJSONObject("data").getJSONArray("records");
            int degree = 0; //扣分
            int count = 0;    //扣款金额
            long offencesMoney = 0l;
            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject tempJson = jsonArray.getJSONObject(i);
                int tempDegree = tempJson.getIntValue("degree");
                count += tempJson.getIntValue("count");
                degree += tempDegree;
                //逻辑   单项 12分扣款 5000   不足12分 每分150
                if (tempDegree == 12) {
                    offencesMoney += 5000;
                } else {
                    offencesMoney += tempDegree * 150;
                }
            }
            //逻辑   扣分押金 + 罚款金额*2
            offencesMoney = offencesMoney + count;
            //这里更新后，选择期限后不会默认重置这个值
            return offencesMoney;
        } else {
            throw new Exception("车辆违章查询数据源信息异常");
        }
    }

    /**
     * 得到融租基本信息
     *
     * @param loan_no
     * @return
     * @throws SQLException
     */
    public Row getLoanBasicDataInfo(String loan_no) {
        StringBuffer sql = new StringBuffer("select * from loan_info ")
                .append("left join loan_cost on loan_info.loan_no = loan_cost.loan_no ")
                .append("left join loan_info_et on loan_info.loan_no = loan_info_et.loan_no ")
                .append("left join loan_con on loan_info.loan_no = loan_con.loan_no ")
                .append("left join loan_cust on loan_info.loan_no = loan_cust.loan_no ")
                .append("left join loan_car on loan_info.loan_no = loan_car.loan_no ")
                .append("left join loan_ass on loan_info.loan_no = loan_ass.loan_no ")
                .append("left join loan_prod on loan_info.prod_no = loan_prod.prod_no ")
                .append("left join loan_audi on loan_info.loan_no = loan_audi.loan_no ")
                .append("left join wf_review_biz on wf_review_biz.table_key_no = loan_info.loan_no ")
                .append("left join wf_review on wf_review.review_no = wf_review_biz.review_no ")
                .append("where loan_info.loan_no='" + SafeUtil.safe(loan_no) + "' ");
        if (baseDao == null) {
            return baseDao.queryRow(sql.toString());
        } else {
            return baseDao.queryRow(sql.toString());
        }
    }

    /**
     * 通过城市获得城市编码
     *
     * @param city 城市
     * @return
     * @throws Exception
     */
    public String getCityCode(String city) throws Exception {
        JSONObject jsonObject = tianXingClinet.queryCity(city, false);
        JSONArray jsonArray = jsonObject.getJSONObject("data").getJSONArray("cityResult");
        JSONObject cityObject = jsonArray.getJSONObject(0);
        JSONArray cities = cityObject.getJSONArray("cities");
        for (int i = cities.size() - 1; i > 0; i--) {
            JSONObject city_information = cities.getJSONObject(i);
            String city_name = city_information.getString("name");
            if (city.contains(city_name)) {
                return city_information.getString("code");
            }
        }
        return cities.getJSONObject(0).getString("code");
    }

    /**
     * 远程流程验证
     *
     * @return
     */
    public static boolean checkYcFlow(String templet_flow_no) {
        if (StringUtils.equals(templet_flow_no, "08")
                || StringUtils.equals(templet_flow_no, "09")
                || StringUtils.equals(templet_flow_no, "0902")
                || StringUtils.equals(templet_flow_no, "51")
                || StringUtils.equals(templet_flow_no, "52")
                || StringUtils.equals(templet_flow_no, "53")
                || StringUtils.equals(templet_flow_no, "54")
                || StringUtils.equals(templet_flow_no, "55")
                || StringUtils.equals(templet_flow_no, "56")
                || StringUtils.equals(templet_flow_no, "500")
                || StringUtils.equals(templet_flow_no, "501")
                || StringUtils.equals(templet_flow_no, "99")
                || StringUtils.equals(templet_flow_no, "600")
                || StringUtils.equals(templet_flow_no, "601")
                || StringUtils.equals(templet_flow_no, "602")
                || StringUtils.equals(templet_flow_no, "700")
                || StringUtils.equals(templet_flow_no, "1")
                || StringUtils.equals(templet_flow_no, "9001")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 得到付款方信息
     *
     * @param type
     * @return
     */
    public Row getEmployersInfo(String type) {
        Row row = new Row();
        String pay_card = null;
        String pay_bank = null;
        String receive_card = null;
        String receive_bank = null;
        String pay_type = "02";// 一次付款到卡
        String pay_mode = "01";// 委托付款
        String employerNo = null;
        if ("rzzl".equals(type)) {
            // 深圳任性融资租赁有限公司 账户
            pay_card = "6302 6084 8400 015";
            pay_bank = "重庆渝北银座村镇银行";
            receive_card = "123908582210401";
            receive_bank = "招商银行重庆江北支行";
            employerNo = "6b9a2d0e-7f43-427e-9aea-794195eddc7f";
        } else if ("wz".equals(type)) {
            pay_card = "6226 6209 0488 8443";
            pay_bank = "光大银行重庆龙头寺支行";
            receive_card = "6217 0037 6001 3197 355";
            receive_bank = "建行重庆杨家坪支行陈家坪分理处";
            employerNo = "ebe6fff7-66c2-4124-b26d-ea0460f13c89";
        } else if ("jc".equals(type)) {
            //万微融资租赁（上海）有限公司  上海市浦东新区双桥路1139弄5号101室
            pay_card = "31050161363600000514";
            pay_bank = "建设银行上海东方路支行";
            receive_card = "31050161363600000514";
            receive_bank = "建设银行上海东方路支行";
            employerNo = "24493e10-09bb-4480-b9ee-ec854ab89b15";
        }
        row.put("pay_card", pay_card);
        row.put("pay_bank", pay_bank);
        row.put("receive_card", receive_card);
        row.put("receive_bank", receive_bank);
        row.put("pay_type", pay_type);
        row.put("pay_mode", pay_mode);
        row.put("employerNo", employerNo);
        return row;
    }

    /**
     * 得到未付款已用预付款额度
     *
     * @throws SQLException
     */
    public double getUsePrepaySumQuota(String sell_branch_no) throws SQLException {
        String sql = "select ifnull(sum(loan_fk_info.amount),0) as useprepaysum from loan_info  "
                + "left join loan_info_et on loan_info.LOAN_NO = loan_info_et.LOAN_NO  "
                + "left join loan_fk_info on loan_info.LOAN_NO = loan_fk_info.BUSINESS_NO  "
                + "where loan_info_et.FK_DATE is null "
                + "and loan_info.SELL_BRANCH_NO = '" + sell_branch_no + "' and loan_fk_info.FK_TYPE = 'ADVANCE_PAY' and loan_fk_info.status = 2 ";
        double useprepaysum = (double) baseDao.queryObject(sql);
        return useprepaysum;
    }

    /**
     * 通过卡号查询开户行
     *
     * @param card
     * @return
     * @throws Exception
     */
    public String getBankShortByCard(String card) throws Exception {
        String bank_short = null;
        Map<String,Object> map = accountClient.getBankCardInfo(card);
        if (map.get("validated") != null && (Boolean)map.get("validated")) {
            bank_short = objectIsNullGetString(map.get("bankCode"));
        }
        return bank_short;
    }

    /**
     * 根据规则获取默认还租方信息
     *
     * @return
     * @throws Exception
     */
    public Row getHkf(String loan_no) throws Exception {
        Row loan_row = getLoanBasicDataInfo(loan_no);
        String sql = null;
        Row hkf_row = null;
        if (null != loan_row) {
            sql = "select * from loan_con_repayment where no = '" + ErpUtil.objectIsNullGetString(loan_row.get("repayment_no")) + "'";
            hkf_row = baseDao.queryRow(sql);
            //未付款按照当前规则,
            if (StringUtils.isBlank(ErpUtil.objectIsNullGetString(loan_row.get("fk_date")))) {
                String prod_no = loan_row.getString("prod_no");
                String duetime = loan_row.getString("duetime");
                sql = "select * from loan_con_repayment";
                Store store = baseDao.query(sql);
                for (Row row : store) {
                    String prod_condition = row.getString("prod_condition");
                    Formula conFormula = new Formula(prod_condition);
                    conFormula.setVariable("loan_info.prod_no", prod_no);
                    conFormula.setVariable("loan_cost.duetime", duetime);
                    if (conFormula.bool()) {
                        hkf_row = row;
                        break;
                    }
                }
            }

            //已付款的历史单子，特殊处理（付款方如果为万霞，还租方为万霞，付款方如果为李开祥，还租方为王湛，付款方如果为王湛，还租方为王湛，付款方如果为融租，还租方为融租）
            if (null == hkf_row && StringUtils.isNotBlank(ErpUtil.objectIsNullGetString(loan_row.get("fk_date")))) {
                Store fkf_store = getFkfByLoanNo(loan_no);
                for (Row fkf_row : fkf_store) {
                    if (StringUtils.equals(ErpUtil.objectIsNullGetString(fkf_row.get("employers_no")), "f9b13461-52f4-48cd-894e-6224cc238fac")) {
                        sql = "select * from loan_con_repayment where no = '76cf77c2-21de-11e8-9d15-00e04c6832c6'";
                        hkf_row = baseDao.queryRow(sql);
                        break;
                    }
                    if (StringUtils.equals(ErpUtil.objectIsNullGetString(fkf_row.get("employers_no")), "bc0c7587-38bd-4411-981a-48a594b0b9c0")) {
                        sql = "select * from loan_con_repayment where no = 'da58a5a7-2123-11e8-9d15-00e04c6832c6'";
                        hkf_row = baseDao.queryRow(sql);
                        break;
                    }

                    if (StringUtils.equals(ErpUtil.objectIsNullGetString(fkf_row.get("employers_no")), "ebe6fff7-66c2-4124-b26d-ea0460f13c89")) {
                        sql = "select * from loan_con_repayment where no = 'da58a5a7-2123-11e8-9d15-00e04c6832c6'";
                        hkf_row = baseDao.queryRow(sql);
                        break;
                    }

                    if (StringUtils.equals(ErpUtil.objectIsNullGetString(fkf_row.get("employers_no")), "6b9a2d0e-7f43-427e-9aea-794195eddc7f")) {
                        sql = "select * from loan_con_repayment where no = 'da58a7d0-2123-11e8-9d15-00e04c6832c6'";
                        hkf_row = baseDao.queryRow(sql);
                        break;
                    }

                    if (StringUtils.equals(ErpUtil.objectIsNullGetString(fkf_row.get("employers_no")), "24493e10-09bb-4480-b9ee-ec854ab89b15")) {
                        sql = "select * from loan_con_repayment where no = 'da58a5a7-2123-11e8-9d15-00e04c6832c6'";
                        hkf_row = baseDao.queryRow(sql);
                        break;
                    }
                }

            }

        }
        return hkf_row;
    }


    /**
     * 根据融租loan_no获取付款方
     *
     * @param loan_no
     * @return
     * @throws Exception
     */
    public Store getFkfByLoanNo(String loan_no) throws Exception {
        String sql = "select a.employers_no,a.receive_card as card_code,a.receive_bank as bank,b.capital_name as name,"
                + "	   b.credentials_code as idcard,c.communication_address as address,'023-63051312' as tel "
                + "from loan_info_employers a "
                + "left join loan_capital b on b.capital_no=a.employers_no "
                + "left join erp_cust c on c.credentials_type=b.credentials_type and c.credentials_code=b.credentials_code "
                + "where "
                + "	a.loan_no='" + SafeUtil.safe(loan_no) + "' and a.status='1' order by a.oper_time";
        return baseDao.query(sql);
    }

    /**
     * 更新收款人信息
     *
     * @param payeeName
     * @param payeeIdcard
     * @param payeeBankAcct
     * @param payeeMobile
     * @param loanNo
     * @param bank_dep
     * @throws Exception
     */
    public void updatePayeeInfo(String payeeName, String payeeIdcard, String payeeBankAcct, String payeeMobile, String loanNo, String bank_dep) throws Exception {
        String sql = "update loan_info_et set bankacct_verify = '03',payee='" + payeeName + "',payee_name='" + payeeName + "',payee_idcard='" + payeeIdcard + "',payee_bank_acct='" + payeeBankAcct + "',payee_mobile='" + payeeMobile + "' where loan_no='" + SafeUtil.safe(loanNo) + "'";
        baseDao.execute(sql);
        if (StringUtils.isEmpty(bank_dep)) {
            sql = "update loan_info_et set bank_dep = '" + getBankByCard(payeeBankAcct) + "' where loan_no='" + SafeUtil.safe(loanNo) + "'";
            baseDao.execute(sql);
        }
    }

    /**
     * 获取开户行
     *
     * @param card
     * @return
     * @throws Exception
     */
    public String getBankByCard(String card)  throws Exception {
        String bank_short = "";
        String bank_name;
        card = StringUtil.replaceBlank(card);
        Map map = accountClient.getBankCardInfo(card);
        if ((Boolean) map.get("validated")) {
            bank_short = String.valueOf(map.get("bankCode"));
        }
        Object obj=baseDao.queryObject("select bank_name from loan_bank_channel where bank_name_short='"+bank_short+"'");
        if(obj==null){
            bank_name=bank_short;
        }else{
            bank_name=obj.toString();
        }
        return bank_name;
    }

    /**
     * 发送系统付款消息
     *
     * @param title
     * @param text
     */
    public void sendFkMessage(String title, String text) {
        try {
            String fk_tip_idcard1 = PropertiesUtil.getValue("fk_tip_idcard1");
            String fk_tip_idcard2 = PropertiesUtil.getValue("fk_tip_idcard2");
            String fk_tip_idcard3 = PropertiesUtil.getValue("fk_tip_idcard3");
            Store store = baseDao.query("select no from sys_info_user where status = 1 and  user_status = 0 and idcard in ('" + fk_tip_idcard1.replaceAll(",", "','") + "','" + fk_tip_idcard2 + "','" + fk_tip_idcard3 + "')");
            StringBuffer wxname = new StringBuffer("");
            for (Row row : store) {
                wxname.append(row.getString("no")).append("|");
            }
            if (wxname.length() > 0) {
                wxname.deleteCharAt(wxname.length() - 1);
            }
            qywxApiService.textPush(wxname.toString(), text, "fkappinfo", 2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 发送系统付款消息
     *
     * @param param
     * @param result
     */
    public void sendSysErrorWeiXinMessage(Param param, Result result) {
        try {
            StringBuffer errorStr = new StringBuffer();
            errorStr.append("操作用户:").append(param.getLoginUserName());
            errorStr.append("(" + param.getLoginUserNo() + ")").append("\r  ");
            errorStr.append("请求URL:").append(param.getParamURL()).append("\r  \r  ");
            if (Util.isNotEmpty(result.getErrors().toString())) {
                errorStr.append("错误内容:\r  ");
                errorStr.append(result.getErrors().toString().replaceAll("<br>", "\r  ").replaceAll("\\{", "").replaceAll("\\}", ""));
            }
            if (errorStr.length() > 2048) {
                errorStr = new StringBuffer(errorStr.substring(0, 2048));
            }
            sendSysErrorWeiXinMessage(errorStr.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 发送系统付款消息
     */
    public void sendSysErrorWeiXinMessage(String errorStr) {
        try {
            if (errorStr.length() > 2048) {
                errorStr = errorStr.substring(0, 2048);
            }//todo
            Store store = new Store();
            StringBuffer wxname = new StringBuffer("");
            for (Row row : store) {
                wxname.append(row.getString("no")).append("|");
            }
            if (wxname.length() > 0) {
                wxname.deleteCharAt(wxname.length() - 1);
            }
            qywxApiService.textPush(wxname.toString(), errorStr, "sysappinfo", 2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据融租规则获取出租人信息
     *
     * @return
     * @throws Exception
     */
    public Row getCjr(String loan_no) throws Exception {
        Row loan_row = getLoanBasicDataInfo(loan_no);
        String sql = null;
        Row cjr_row = null;
        if (null != loan_row) {
            sql = "select * from loan_con_lenders where no = '" + ErpUtil.objectIsNullGetString(loan_row.get("lenders_no")) + "'";
            cjr_row = baseDao.queryRow(sql);
            if (cjr_row != null) {
                return cjr_row;
            } else {
                String prod_no = loan_row.getString("prod_no");
                sql = "select * from loan_con_lenders";
                Store store = baseDao.query(sql);
                for (Row row : store) {
                    String prod_condition = row.getString("prod_condition");
                    Formula conFormula = new Formula(prod_condition);
                    conFormula.setVariable("loan_info.prod_no", prod_no);
                    if (conFormula.bool()) {
                        cjr_row = row;
                        break;
                    }
                }
            }
        }
        return cjr_row;
    }

    /**
     * 获取业务数据
     *
     * @return
     */
    public Row getFieldData(Row reviewRow, String loan_no) throws Exception {
        Row fieldsDataRow = new Row();
        String templet_no = reviewRow.getString("templet_no");
        String templet_flow_edition = reviewRow.getInteger("templet_flow_edition").toString();
        String sql = "SELECT *  FROM   `i_sys_decorated_page` WHERE page_id  in (  SELECT page_id FROM `wf_templet_flow_node` WHERE  templet_flow_edition = ? AND TEMPLET_FLOW_NO = ? )";
        Store pageStore = baseDao.query(sql, new Object[]{templet_flow_edition, templet_no});

        for (Row row : pageStore) {
            Store plateAttList = formUtil.getViewPlates(row.getString("view_no"), row.getString("form_no"), "PC", null);
            // 加载视图展示字段
            Store showFieldList = formUtil.getViewShowFields(plateAttList);
            // 业务数据
            fieldsDataRow.putAll(workForm.loadFormData(row.getString("form_no"), loan_no, showFieldList, null, null));
        }
        sql = "SELECT " +
                "  ET_ID AS 'loan_info_et.ET_ID', " +
                "  ET_NO AS 'loan_info_et.ET_NO', " +
                "  LOAN_ID AS 'loan_info_et.LOAN_ID', " +
                "  LOAN_NO AS 'loan_info_et.LOAN_NO', " +
                "  CUST_ID2 AS 'loan_info_et.CUST_ID2', " +
                "  CUST_NO2 AS 'loan_info_et.CUST_NO2', " +
                "  CUST_ID3 AS 'loan_info_et.CUST_ID3', " +
                "  CUST_NO3 AS 'loan_info_et.CUST_NO3', " +
                "  LOAN_DATE AS 'loan_info_et.LOAN_DATE', " +
                "  RENEWAL_DATE AS 'loan_info_et.RENEWAL_DATE', " +
                "  RENEWAL_REMARK AS 'loan_info_et.RENEWAL_REMARK', " +
                "  APPROVE_PERCENTAGE AS 'loan_info_et.APPROVE_PERCENTAGE', " +
                "  MAX_APPROVE_PERCENTAGE AS 'loan_info_et.MAX_APPROVE_PERCENTAGE', " +
                "  FOLLOWING_PERCENT AS 'loan_info_et.FOLLOWING_PERCENT', " +
                "  PREPAY_PERCENTAGE AS 'loan_info_et.PREPAY_PERCENTAGE', " +
                "  INSPECT_OPINION AS 'loan_info_et.INSPECT_OPINION', " +
                "  URGENT_TYPE AS 'loan_info_et.URGENT_TYPE', " +
                "  IS_NEED_BORROWED AS 'loan_info_et.IS_NEED_BORROWED', " +
                "  REP_TYPE AS 'loan_info_et.REP_TYPE', " +
                "  LOAN_TYPE AS 'loan_info_et.LOAN_TYPE', " +
                "  LOAN_PUR AS 'loan_info_et.LOAN_PUR', " +
                "  PAYEE AS 'loan_info_et.PAYEE', " +
                "  BANK_DEP AS 'loan_info_et.BANK_DEP', " +
                "  BANK_ACCOUT AS 'loan_info_et.BANK_ACCOUT', " +
                "  WITHHOLD_BANK_ACCOUNT_NO AS 'loan_info_et.WITHHOLD_BANK_ACCOUNT_NO', " +
                "  HZZ AS 'loan_info_et.HZZ', " +
                "  DJZZ AS 'loan_info_et.DJZZ', " +
                "  LY AS 'loan_info_et.LY', " +
                "  CSSP AS 'loan_info_et.CSSP', " +
                "  DESHT AS 'loan_info_et.DESHT', " +
                "  DESHT2 AS 'loan_info_et.DESHT2', " +
                "  INITIAL_PAYMENT_ARRIVE_IMG AS 'loan_info_et.INITIAL_PAYMENT_ARRIVE_IMG', " +
                "  CAR_DEALER_NO AS 'loan_info_et.CAR_DEALER_NO', " +
                "  INSURANCE_PREMIUM_BELONG AS 'loan_info_et.INSURANCE_PREMIUM_BELONG', " +
                "  FK_DATE AS 'loan_info_et.FK_DATE', " +
                "  IS_TRIAL AS 'loan_info_et.IS_TRIAL', " +
                "  TWO_FK_DATE AS 'loan_info_et.TWO_FK_DATE', " +
                "  REPAY_DATE_FLAG AS 'loan_info_et.REPAY_DATE_FLAG', " +
                "  PARK_NUM AS 'loan_info_et.PARK_NUM', " +
                "  GSSP AS 'loan_info_et.GSSP', " +
                "  HHRQRSP AS 'loan_info_et.HHRQRSP', " +
                "  GJRSP AS 'loan_info_et.GJRSP', " +
                "  GPS_EXPLAIN AS 'loan_info_et.GPS_EXPLAIN', " +
                "  MORTGAGE_EXPLAIN AS 'loan_info_et.MORTGAGE_EXPLAIN', " +
                "  SELECTABLE_CAPITAL_NO AS 'loan_info_et.SELECTABLE_CAPITAL_NO', " +
                "  INTENTIONAL_CAPITAL_NO AS 'loan_info_et.INTENTIONAL_CAPITAL_NO', " +
                "  DYDJSSF AS 'loan_info_et.DYDJSSF', " +
                "  TRANSFER_BELONG AS 'loan_info_et.TRANSFER_BELONG', " +
                "  INTERMEDIARY_NO AS 'loan_info_et.INTERMEDIARY_NO', " +
                "  CREDIT_NO AS 'loan_info_et.CREDIT_NO', " +
                "  AFTER_STATUS AS 'loan_info_et.AFTER_STATUS', " +
                "  OVERDUE_STATUS AS 'loan_info_et.OVERDUE_STATUS', " +
                "  REMIT_FLAG AS 'loan_info_et.REMIT_FLAG', " +
                "  THE_LAST_REPAYMENT_DATE AS 'loan_info_et.THE_LAST_REPAYMENT_DATE', " +
                "  FINANCE_SETTLE_DATE AS 'loan_info_et.FINANCE_SETTLE_DATE', " +
                "  SETTLE_DATE AS 'loan_info_et.SETTLE_DATE', " +
                "  SETTLE_TYPE AS 'loan_info_et.SETTLE_TYPE', " +
                "  IS_PREPAY AS 'loan_info_et.IS_PREPAY', " +
                "  IS_DK AS 'loan_info_et.IS_DK', " +
                "  BORROWED_NAME AS 'loan_info_et.BORROWED_NAME', " +
                "  BORROWED_IDCARD AS 'loan_info_et.BORROWED_IDCARD', " +
                "  BORROWED_MOBILE AS 'loan_info_et.BORROWED_MOBILE', " +
                "  PAYEE_NAME AS 'loan_info_et.PAYEE_NAME', " +
                "  PAYEE_IDCARD AS 'loan_info_et.PAYEE_IDCARD', " +
                "  PAYEE_BANK_ACCT AS 'loan_info_et.PAYEE_BANK_ACCT', " +
                "  PAYEE_MOBILE AS 'loan_info_et.PAYEE_MOBILE', " +
                "  IDCARD_VERIFY AS 'loan_info_et.IDCARD_VERIFY', " +
                "  BANKACCT_VERIFY AS 'loan_info_et.BANKACCT_VERIFY', " +
                "  VERIFY_NUM_JSON AS 'loan_info_et.VERIFY_NUM_JSON', " +
                "  REPAYMENT_DATE AS 'loan_info_et.REPAYMENT_DATE', " +
                "  FCZLZ AS 'loan_info_et.FCZLZ', " +
                "  FCZLZBZ AS 'loan_info_et.FCZLZBZ', " +
                "  GRLSZ AS 'loan_info_et.GRLSZ', " +
                "  GRLSZBZ AS 'loan_info_et.GRLSZBZ', " +
                "  XSZZ AS 'loan_info_et.XSZZ', " +
                "  XSZZBZ AS 'loan_info_et.XSZZBZ', " +
                "  SFZZ AS 'loan_info_et.SFZZ', " +
                "  SFZZBZ AS 'loan_info_et.SFZZBZ', " +
                "  LOAN_MONEY AS 'loan_info_et.LOAN_MONEY', " +
                "  TOTAL_PERIOD AS 'loan_info_et.TOTAL_PERIOD', " +
                "  LOAN_DATE_XX AS 'loan_info_et.LOAN_DATE_XX', " +
                "  REPAYMENT_DATE_XX AS 'loan_info_et.REPAYMENT_DATE_XX', " +
                "  THE_MONTH AS 'loan_info_et.THE_MONTH', " +
                "  RETURN_DATE AS 'loan_info_et.RETURN_DATE', " +
                "  OVERDUE_CONDITION AS 'loan_info_et.OVERDUE_CONDITION', " +
                "  REMARKS AS 'loan_info_et.REMARKS', " +
                "  CAR_INFO_EXPLAIN AS 'loan_info_et.CAR_INFO_EXPLAIN', " +
                "  BONUS_BELONG AS 'loan_info_et.BONUS_BELONG', " +
                "  XQLY AS 'loan_info_et.XQLY', " +
                "  OPER_USER_ID AS 'loan_info_et.OPER_USER_ID', " +
                "  OPER_USER_NO AS 'loan_info_et.OPER_USER_NO', " +
                "  OPER_TIME AS 'loan_info_et.OPER_TIME', " +
                "  STATUS AS 'loan_info_et.STATUS' " +
                "FROM " +
                "  loan_info_et  where loan_no = ?";
        Row etRow = baseDao.queryRow(sql, new Object[]{loan_no});
        fieldsDataRow.putAll(etRow);
        return fieldsDataRow;

    }

    /**
     * 测试环境验证
     *
     * @param payeeName
     * @param payeeIdcard
     * @param payeeBankAcct
     * @param payeeMobile
     * @param loanNo
     * @throws Exception
     */
    public void runtimeTestCheck(String payeeName, String payeeIdcard, String payeeBankAcct, String payeeMobile, String loanNo) throws Exception {
        //开发环境直接更新
        String sql = "update loan_info_et set bankacct_verify = '03',payee='" + payeeName + "',payee_name='" + payeeName + "',payee_idcard='" + payeeIdcard + "',payee_bank_acct='" + payeeBankAcct + "',payee_mobile='" + payeeMobile + "' where loan_no='" + SafeUtil.safe(loanNo) + "'";
        baseDao.execute(sql);
        Double assessPrice = 0d;
        while (true) {
            assessPrice = new Random().nextDouble() * 1000000;
            if (assessPrice > 5000) {
                break;
            }
        }
        sql = "update loan_ass set bigdata_ass_price = '" + assessPrice + "' where loan_no='" + SafeUtil.safe(loanNo) + "'";
        baseDao.execute(sql);
    }


    /**
     * 更新抵押方和出租人和还租方和安装说明和抵押登记说明
     *
     * @throws Exception
     */
    public void updateDyf(String loan_no) throws Exception {
        Row loanRow = getLoanBasicDataInfo(loan_no);
        String prod_no = ErpUtil.objectIsNullGetString(loanRow.get("prod_no"));
        String loan_type = ErpUtil.objectIsNullGetString(loanRow.get("loan_type"));
        String duetime = ErpUtil.objectIsNullGetString(loanRow.get("duetime"));
        String cust_name = ErpUtil.objectIsNullGetString(loanRow.get("cust_name"));
        String car_plate = ErpUtil.objectIsNullGetString(loanRow.get("car_plate"));
        double contract_sum = ErpUtil.objectIsNullGetDouble(loanRow.get("contract_sum"));
        updateHkfAndCjr(loan_no);
        String sql = "select * from loan_financing_rules where prod_no = '" + prod_no + "' and loan_type = '" + loan_type + "'";
        Store store = baseDao.query(sql);
        String mortgage_push = null;
        String gps_push = null;
        for (Row row : store) {
            String duetime_condition = row.getString("duetime_condition");
            // 条件公式
            Formula conFormula = new Formula(duetime_condition);
            conFormula.setVariable("loan_cost.duetime", duetime);
            conFormula.setVariable("loan_cost.contract_sum", contract_sum);
            if (conFormula.bool()) {
                mortgage_push = ErpUtil.objectIsNullGetString(row.get("mortgage_push"));
                gps_push = ErpUtil.objectIsNullGetString(row.get("gps_push"));
                sql = "update loan_info_et "
                        + "set dydjssf = '" + ErpUtil.objectIsNullGetString(row.get("mortgage_party")) + "',"
                        + "gps_explain = '" + gps_push + "',"
                        + "mortgage_explain = '" + mortgage_push + "' "
                        + "where loan_no = '" + loan_no + "'";
                baseDao.execute(sql);
                break;
            }
        }
        if (StringUtils.equals("1", runtime)) {
            if (StringUtils.isNotBlank(mortgage_push)) {
                String text = "客户名称：" + cust_name + ",车牌号：" + car_plate + "," + mortgage_push;
            }
            if (StringUtils.isNotBlank(gps_push)) {
                String text = "客户名称：" + cust_name + ",车牌号：" + car_plate + "," + gps_push;
            }
        }
    }

    /**
     * 更新出租人和还租方
     *
     * @param loan_no
     * @throws Exception
     */
    public void updateHkfAndCjr(String loan_no) throws Exception {
        //先更新甲方出租人主体
        Row cjrRow = getCjr(loan_no);
        //更新还租方主体
        Row hkfRow = getHkf(loan_no);
        String sql = "update loan_con "
                + "set lenders_no = '" + ErpUtil.objectIsNullGetString(cjrRow.get("no")) + "',"
                + "repayment_no = '" + ErpUtil.objectIsNullGetString(hkfRow.get("no")) + "' "
                + "where loan_no = '" + loan_no + "'";
        baseDao.execute(sql);
    }

    /**
     * 更新GPS数量
     *
     * @param contract_sum
     * @param loan_no
     * @throws Exception
     */
    public void updateGpsCount(double contract_sum, String loan_no) throws Exception {
        String sql = null;
        Row loanRow = getLoanBasicDataInfoV2(loan_no);
        String prod_no = ErpUtil.objectIsNullGetString(loanRow.get("loan_info.prod_no"));
        //更新GPS个数
        if ("01".equals(prod_no) || "02".equals(prod_no) || "05".equals(prod_no) || "06".equals(prod_no) || "10".equals(prod_no)) {
            if (contract_sum <= 200000) {
                sql = "update loan_car set wxgps='2',yxgps='1' where loan_no='" + loan_no + "'";
            } else {
                sql = "update loan_car set wxgps='3',yxgps='1' where loan_no='" + loan_no + "'";
            }
            baseDao.execute(sql);
        }
    }


    /**
     * 创建资料补充流程
     *
     * @throws Exception
     */
    public void supplement(final String loan_id, final String loan_no, Param p) throws Exception {
        //验证是否存在未完成的资料补录流程
        String sql = "select count(1) "
                + "from loan_supplement a "
                + "left join wf_review_biz b on a.supplement_no=b.table_key_no "
                + "left join wf_review c on c.review_no=b.review_no "
                + "where a.loan_no='" + loan_no + "' and c.review_status in ('E','R') ";
        int count = ErpUtil.objectIsNullGetInteger(baseDao.queryObject(sql));
        if (count > 0) {//存在
            return;
        }
        Map map = flow.beforeInitAllSubFlow();
        Param param = JSON.parseObject(JSONObject.toJSONString(p), Param.class);
        flow.initWorkFlow("07", null, param);
//        WorkFactory flow = (WorkFactory) WorkFactory.getInstanceByTempletNo("07", p);
        Row retRow = flow.addWorkFlow();
        // 执行流程扩展业务

        String sqlIn;
        Row data = (Row) retRow.get("data");
        String review_no = data.getString("review_no");
        sqlIn = "update loan_supplement a,wf_review_biz b "
                + "set "
                + "	a.loan_id='" + SafeUtil.safe(loan_id) + "',"
                + "	a.loan_no='" + SafeUtil.safe(loan_no) + "' "
                + "where "
                + "	a.supplement_no=b.table_key_no and b.review_no='" + SafeUtil.safe(review_no) + "';";
        baseDao.execute(sqlIn);

        flow.reInitHandler();
        flow.closeWorkFlow();
        flow.closeAllSubFlow(map);
    }


    /**
     * 计算合同当期应还日
     *
     * @throws Exception
     */
    public void updateRepayFeeDate(String loan_no) throws Exception {


        String sql = "update "
                + "		(select table_key_no,min(fee_date) as fee_date "
                + "		 from loan_mid_payable a "
                + "		 where a.PAYABLE_TYPE='01' and a.ASCRIPTION_TYPE='10' and is_writeoff='N' "
                + "		 group by table_key_no) x "
                + " left join loan_info y on x.table_key_no=y.loan_no "
                + "set y.REPAY_FEE_DATE=x.fee_date ";

        if (Util.isNotEmpty(loan_no)) {
            sql = "update loan_info "
                    + "set repay_fee_date=ifnull("
                    + "						(select min(fee_date) "
                    + "						 from loan_mid_payable "
                    + "						 where table_key_no=loan_info.loan_no and payable_type='01' and ascription_type='10' and is_writeoff='N'),'') "
                    + "where loan_no='" + loan_no + "'";
        }
        baseDao.execute(sql);
    }

    /**
     * 通过合同号获得贷款No
     *
     * @param con_code
     * @throws Exception
     */
    public String getLoanNoByConCode(String con_code) throws Exception {
        String loan_no = (String) baseDao.queryObject("select loan_no from loan_con where con_code = '" + con_code + "'");
        return loan_no;
    }

    /**
     * 获取系统今天代扣数据
     *
     * @throws Exception
     */
    public void initTodayTask() throws Exception {
        Calendar c = Calendar.getInstance();
        String today = Util.format(c.getTime());
        String repay_day = "'" + today + "'"; //获取当日

        if (c.get(Calendar.DAY_OF_WEEK) == 6) {//周五
            repay_day += ",'" + Util.addDay(today, 1) + "'"; //获取周六
            repay_day += ",'" + Util.addDay(today, 2) + "'"; //获取周日
        }

        String sql = "select a.loan_no,b.cust_name,c.con_code,t.duetime,t.fee_date,sum(t.fee_amount) as fee_amount "
                + "from loan_mid_payable t "
                + "left join loan_info a on t.table_key_no=a.loan_no "
                + "left join loan_cust b on b.loan_no=a.loan_no "
                + "left join loan_con c on c.loan_no=a.loan_no "
                + "left join loan_cost d on d.loan_no=a.loan_no "
                + "left join loan_info_et e on e.loan_no=a.loan_no "
                + "where t.fee_date in (" + repay_day + ") and t.payable_type = '01' and t.ascription_type = '10' and t.is_writeoff = 'N' "
                + "group by t.table_key_no,t.duetime";
        Store store = baseDao.query(sql);

        for (Row row : store) {
            sql = "insert into kft_sys_task values"
                    + "(uuid(),"
                    + "uuid(),"
                    + "'" + row.get("con_code") + "',"
                    + "'" + row.get("duetime") + "',"
                    + "'" + row.get("cust_name") + "',"
                    + "'" + row.get("fee_amount") + "',"
                    + "'" + row.get("fee_date") + "')";
            baseDao.execute(sql);
        }
    }

    /**
     * 对上传的代扣数据，和系统数据进行对比(以上传数据为准，标注系统异常信息)
     *
     * @throws Exception
     */
    public void disposeAbnormalbyExcel() throws Exception {
        Calendar c = Calendar.getInstance();
        String today = Util.format(c.getTime());
        String repay_day = "'" + today + "'"; //获取当日

        if (c.get(Calendar.DAY_OF_WEEK) == 6) {//周五
            repay_day += ",'" + Util.addDay(today, 1) + "'"; //获取周六
            repay_day += ",'" + Util.addDay(today, 2) + "'"; //获取周日
        }
        String sql = "select * from kft_task where repayment_date in (" + repay_day + ") and status = '1'";
        Store store = baseDao.query(sql);
        for (Row row : store) {
            sql = "select * from kft_sys_task where repayment_date = '" + row.get("repayment_date") + "' and con_code = '" + row.get("con_code") + "' and cust_name = '" + row.get("custname") + "'";
            Row sysTaskRow = baseDao.queryRow(sql);
            if (sysTaskRow == null) {
                sql = "update kft_task set abnormal_remark = '系统数据异常',status= '4' where task_no = '" + row.getString("task_no") + "'";
                baseDao.execute(sql);
            }
        }
    }

    /**
     * 对上传的代扣数据，和系统数据进行对比(以系统数据为准，标注财务异常信息)
     *
     * @throws Exception
     */
    public void disposeAbnormalbySys() throws Exception {
        Calendar c = Calendar.getInstance();
        String today = Util.format(c.getTime());
        String repay_day = "'" + today + "'"; //获取当日

        if (c.get(Calendar.DAY_OF_WEEK) == 6) {//周五
            repay_day += ",'" + Util.addDay(today, 1) + "'"; //获取周六
            repay_day += ",'" + Util.addDay(today, 2) + "'"; //获取周日
        }
        String sql = "select * from kft_sys_task where repayment_date in (" + repay_day + ")";
        Store store = baseDao.query(sql);
        for (Row row : store) {
            sql = "select * from kft_task where repayment_date = '" + row.get("repayment_date") + "' and con_code = '" + row.get("con_code") + "' and custname = '" + row.get("cust_name") + "'";
            Row excelTaskRow = baseDao.queryRow(sql);
            if (excelTaskRow == null) {
                sql = "insert into kft_task("
                        + "task_id,"
                        + "task_no,"
                        + "con_code,"
                        + "duetime,"
                        + "custname,"
                        + "amount,"
                        + "pay_amount,"
                        + "repayment_date,"
                        + "abnormal_remark,"
                        + "publiccard_remark,"
                        + "create_time,"
                        + "oper_time,"
                        + "status ) values ("
                        + "uuid(),"
                        + "uuid(),"
                        + "'" + row.get("con_code") + "',"
                        + "'" + row.get("duetime") + "',"
                        + "'" + row.get("cust_name") + "',"
                        + "'" + row.get("amount") + "',"
                        + "'0',"
                        + "'" + row.get("repayment_date") + "',"
                        + "'财务数据异常',"
                        + "'',"
                        + "'" + Util.format(new Date(), "yyyy-MM-dd HH:mm:ss") + "',"
                        + "'" + Util.format(new Date(), "yyyy-MM-dd HH:mm:ss") + "',"
                        + "'4')";
                baseDao.execute(sql);
            }
        }
    }

    /**
     *查询车辆国家编码
     * @param brand
     * @param cjh
     * @return
     * @throws Exception
     */
    public String getCarCountryByBrand(String brand,String cjh) throws Exception{
        String is_joint="0";
        if(StringUtils.startsWith(cjh,"L")){
            is_joint="1";
        }
        String sql="select country_code from loan_car_brand_country a where position(a.BRAND in '"+brand+"')>0 and a.IS_JOINT in('"+is_joint+"','') limit 1";
        String country_code=objectIsNullGetString(baseDao.queryObject(sql));
        if(Util.isEmpty(country_code)){
            country_code="01";
        }
        return country_code;
    }


    /**
     * 查询字段SQL
     *
     * @return
     * @throws SQLException
     */
    public Store getFieldInfoByFields(String fields) throws Exception {
        Store showFieldRow = new Store();
        StringBuffer sql = new StringBuffer();
        sql.append("select t.field,"
                + "	t.field_table_code,"
                + "	t.field_name,"
                + "	t.field_type,"
                + "	case when t.default_val is null or t.default_val='' then 'n_v_l' else t.default_val end as default_val,"
                + "	case when t.option_vals is null or t.option_vals='' then 'n_v_l' else t.option_vals end as option_vals,"
                + "	case when t.field_check is null or t.field_check='' then 'n_v_l' else t.field_check end as field_check,"
                + "	case when t.field_unit is null or t.field_unit='' then 'n_v_l' else t.field_unit end as field_unit,"
                + "	case when t.hinge_field is null or t.hinge_field='' then 'n_v_l' else t.hinge_field end as hinge_field,"
                + "	case when t.is_sup is null or t.is_sup='' then 'n_v_l' else t.is_sup end as is_sup,"
                + "	case when t.select_type is null or t.select_type='' then 'n_v_l' else t.select_type end as select_type,"
                + "	case when t.allow_input is null or t.allow_input='' then 'n_v_l' else t.allow_input end as allow_input,"
                + " case when t.watermark is null or t.watermark='' then 'n_v_l' else t.watermark end as watermark,"
                + "	case when t.tips is null or t.tips='' then 'n_v_l' else t.tips end as tips,"
                + "	case when t.field_remark is null or t.field_remark='' then 'n_v_l' else t.field_remark end as field_remark,"
                + "	case when t.belong_field is null or t.belong_field='' then 'n_v_l' else t.belong_field end as belong_field,"
                + "	case when t.sort_no is null or t.sort_no='' then 'n_v_l' else t.sort_no end as sort_no "
                + "	from wf_fields t "
                + "	where t.status='1' and (t.is_sup='0' or t.field_type='FEE' or t.field_type='DEPTUSER' or t.field_type='USER') "
                + "	  and t.field in(" + fields + ")");
        sql.append("		and (t.belong_field is null or t.belong_field='') ");

        Store fieldRow = baseDao.query(sql.toString());
        showFieldRow.addAll(fieldRow);
        if (fieldRow.size() > 0) {
            for (Row row : fieldRow) {
                if (FieldTypeEnum.LINKSELECT.getCode().equals(row.getString("field_type"))) {
                    showFieldRow.addAll(getFields(null, null, row.getString("field")));
                }
            }
        }
        return showFieldRow;
    }


    /**
     * 查询字段SQL
     *
     * @return
     * @throws SQLException
     */
    public Store getFields(String plate_templet_no, String field_table_code, String belong_field) throws Exception {
        Store showFieldRow = new Store();
        StringBuffer sql = new StringBuffer();
        sql.append("select t.field,"
                + "	t.field_table_code,"
                + "	t.field_name,"
                + "	t.field_type,"
                + "	case when t.default_val is null or t.default_val='' then 'n_v_l' else t.default_val end as default_val,"
                + "	case when t.option_vals is null or t.option_vals='' then 'n_v_l' else t.option_vals end as option_vals,"
                + "	case when t.field_check is null or t.field_check='' then 'n_v_l' else t.field_check end as field_check,"
                + "	case when t.field_unit is null or t.field_unit='' then 'n_v_l' else t.field_unit end as field_unit,"
                + "	case when t.hinge_field is null or t.hinge_field='' then 'n_v_l' else t.hinge_field end as hinge_field,"
                + "	case when t.is_sup is null or t.is_sup='' then 'n_v_l' else t.is_sup end as is_sup,"
                + "	case when t.select_type is null or t.select_type='' then 'n_v_l' else t.select_type end as select_type,"
                + "	case when t.allow_input is null or t.allow_input='' then 'n_v_l' else t.allow_input end as allow_input,"
                + " case when t.watermark is null or t.watermark='' then 'n_v_l' else t.watermark end as watermark,"
                + "	case when t.tips is null or t.tips='' then 'n_v_l' else t.tips end as tips,"
                + "	case when t.field_remark is null or t.field_remark='' then 'n_v_l' else t.field_remark end as field_remark,"
                + "	case when t.belong_field is null or t.belong_field='' then 'n_v_l' else t.belong_field end as belong_field,"
                + "	case when t.sort_no is null or t.sort_no='' then 'n_v_l' else t.sort_no end as sort_no "
                + "	from wf_fields t "
                + "	where t.status='1' and (t.is_sup='0' or t.field_type='FEE' or t.field_type='DEPTUSER' or t.field_type='USER') "
                + "	  and t.field in(select field from wf_plate_templet_field where plate_templet_no='" + plate_templet_no + "')");

        if (Util.isNotEmpty(field_table_code)) {
            sql.append(" and a.field_table_code = '" + field_table_code + "'");
        }

        if (Util.isNotEmpty(belong_field)) {
            sql.append("		and t.belong_field='" + SafeUtil.safe(belong_field) + "' ");
        } else {
            sql.append("		and (t.belong_field is null or t.belong_field='') ");
        }

        Store fieldRow = baseDao.query(sql.toString());
        showFieldRow.addAll(fieldRow);
        if (fieldRow.size() > 0) {
            for (Row row : fieldRow) {
                if (FieldTypeEnum.LINKSELECT.getCode().equals(row.getString("field_type"))) {
                    showFieldRow.addAll(getFields(plate_templet_no, field_table_code, row.getString("field")));
                }
            }
        }
        return showFieldRow;
    }

    /**
     * 给某个时间添加月
     *
     * @param date
     * @param month
     * @return
     */
    public static Date addMonth(Date date, int month) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, month);
        return calendar.getTime();
    }


    /**
     * 生成短网址
     *
     * @param signUrl
     * @return
     * @throws Exception
     */
    public static String createDwz(String signUrl) throws Exception {
        signUrl = signUrl.replace("&", "%26");
        signUrl = signUrl.replace(" ", "%20");
        signUrl = signUrl.replace("?", "%3F");
        signUrl = signUrl.replace("=", "%3D");
        String url = "http://api.t.sina.com.cn/short_url/shorten.json?source=3271760578&url_long=" + signUrl;
        HttpGet httpGet = new HttpGet(url);
        HttpResponse response = Util.getDefaultHttpClient().execute(httpGet);
        //发送Post,并返回一个HttpResponse对象
        if (response.getStatusLine().getStatusCode() == 200) {//如果状态码为200,就是正常返回
            String result = EntityUtils.toString(response.getEntity(), "UTF-8");
            String dwz = JSONArray.parseArray(result).getJSONObject(0).getString("url_short");
            return dwz;
        }
        throw new Exception(BizErrorCodeEnum.T1030.getCode());
    }


    /**
     * 是否是自动付款而且上传了付款回执
     *
     * @param mid_no
     * @return
     * @throws Exception
     */
    public boolean checkLoanMidRecimg(String mid_no) throws Exception {
        boolean result = false;
        String sql = "select * from loan_mid_recimg where mid_no ='" + SafeUtil.safe(mid_no) + "' and status != 3";
        Store loan_mid_recimg_store = baseDao.query(sql);
        sql = "select is_pay from loan_mid_pay where mid_no ='" + SafeUtil.safe(mid_no) + "'";
        Object is_pay_obj = baseDao.queryObject(sql);
        String is_pay_str = String.valueOf(is_pay_obj);
        if (loan_mid_recimg_store != null && loan_mid_recimg_store.size() > 0 && StringUtils.equals(is_pay_str, "YES")) {
            result = true;
        }
        return result;
    }


    /**
     * 发送企业微信失败信息
     *
     * @param msg
     * @throws Exception
     */
    public void sendWxFkErrorMsg(String msg) {
        try {
            erpUtil.sendFkMessage("自动付款失败", msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 发送短信失败消息
     *
     * @param msg
     */
    public void sendSmsErrorMsg(String msg) {
        try {
            String phone_str = PropertiesUtil.getValue("fk_tip_phone1") + "," + PropertiesUtil.getValue("fk_tip_phone2") + "," + PropertiesUtil.getValue("fk_tip_phone3") + "," + PropertiesUtil.getValue("fk_tip_phone4");
            SmsOpenUtils.sendSms(phone_str, msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 匹配联系人信息
     * @throws Exception
     */
    public void mateContacts(String loanNo) throws Exception {
        String sql = "select * from loan_info_contacts where loan_no = '"+loanNo+"' and status = '1'";
        Store store = baseDao.query(sql);
        List<String> list = new ArrayList<>();
        for(Row row:store){
            String contacts_name = StringUtil.replaceBlank(objectIsNullGetString(row.get("contacts_name")));
            String contacts_mobile = StringUtil.replaceBlank(objectIsNullGetString(row.get("contacts_mobile")));
            String no = StringUtil.replaceBlank(objectIsNullGetString(row.get("no")));
            if(StringUtils.isNotBlank(contacts_name) && StringUtils.isNotBlank(contacts_mobile)){
                JSONObject jsonObject = tianXingClinet.queryMobile2Element(contacts_name,contacts_mobile,false);
                String compareStatus = jsonObject.getJSONObject("data").getString("compareStatus");
                String contacts_check = "";
                if(StringUtils.equals("SAME", compareStatus)){
                    contacts_check = "验证一致";
                } else {
                    contacts_check = "验证不一致";
                }
                sql = "update loan_info_contacts set contacts_check = '"+contacts_check+"' where no = '"+no+"'";
                list.add(sql);
            }
        }
        baseDao.execute(list);
    }

    /**
     * 获取详细地址
     * @param address
     * @param address_p
     * @return
     * @throws Exception
     */
    public String getDetailedAddress(String address,String address_p) throws Exception{
        String sql = "select * from i_frame_sys_std_code_val where code_val = '"+address_p+"'";
        Row row = baseDao.queryRow(sql);
        String code_name = erpUtil.objectIsNullGetString(row.get("code_label"));
        if(StringUtils.equals("",address)){
            address = code_name;
        }else{
            address = code_name+"-"+address;
        }
        String p_code = erpUtil.objectIsNullGetString(row.get("p_code_val"));
        if(!StringUtils.equals("",p_code)){
            return getDetailedAddress(address, p_code);
        }else{
            return address;
        }
    }

    /**
     * 初始化付款条件
     */
    public void initCustSituation(Row loanRow)throws Exception{
        String cust_name = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_name"));
        String cust_phone = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_phone"));
        String idcard = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.idcard"));
        String address_p = loanRow.getString("loan_cust.address_p");
        String address = "";
        String address_add = erpUtil.getDetailedAddress(address,address_p);
        String cust_address = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_address"));
        String custAddressResultString = jiAoClinet.queryMobileHomeDeviate(cust_name, idcard, cust_phone,address_add+cust_address);

        String company_add_p = loanRow.getString("loan_cust.company_add_p");
        String company = "";
        String company_adress = erpUtil.getDetailedAddress(company,company_add_p);
        String company_add = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.company_add"));
        String companyResultString = jiAoClinet.queryMobileWorkDeviate(cust_name, idcard, cust_phone,company_adress+company_add);

        String resultString = jiAoClinet.queryAttentionList(cust_name, idcard, cust_phone);
        JSONObject resultJson = JSONObject.parseObject(resultString);
        String result_yq_zjsj ="";
        String result_yq_ljcs ="";
        String result_yq_zdje ="";
        String result_yq_zdsc ="";
        if(resultJson!=null){
            result_yq_zjsj = erpUtil.objectIsNullGetString(resultJson.get("result_YQ_ZJSJ"));
            result_yq_ljcs = erpUtil.objectIsNullGetString(resultJson.get("result_YQ_LJCS"));
            result_yq_zdje = erpUtil.objectIsNullGetString(resultJson.get("result_YQ_ZDJE"));
            result_yq_zdsc = erpUtil.objectIsNullGetString(resultJson.get("result_YQ_ZDSC"));
        }
        String remark = "居住地偏移："+custAddressResultString+"；工作地偏移："+companyResultString+"；最近出现时间："+result_yq_zjsj+"；累计出现次数："+result_yq_ljcs+"；历史最大逾期金额："+result_yq_zdje+"；历史最大逾期时长："+result_yq_zdsc;
        String sql = "update loan_audi set remark = '"+remark+"' where loan_no = '"+ErpUtil.objectIsNullGetString(loanRow.get("loan_info.loan_no"))+"'";
        baseDao.execute(sql);
    }

    /**
     * 得到相关账户信息
     * @param loan_no
     * @return
     */
    public Map getLoanAccount(String loan_no){
        String sql="select b.clearing_ordinary as kfpt,b.clearing_frozen as kfdj," +
                "d.clearing_income as zfsr,d.clearing_pay as zffk,d.clearing_repay as zfhk," +
                "e.clearing_income as wdsr,e.clearing_deposit as wdbzj,e.clearing_frozen as wddj," +
                "f.clearing_income as qysr,f.clearing_deposit as qybzj,f.clearing_frozen as qydj, " +
                "'95843df6-d68b-11e8-be58-68ecc5562994' as gpssr " +
                "from loan_info a " +
                "left join loan_cust b on a.loan_no=b.loan_no " +
                "left join loan_info_employers c on a.loan_no=c.loan_no and c.pay_type='02' and c.status='1' " +
                "left join loan_capital d on c.employers_no=d.capital_no " +
                "left join join_branch e on a.sell_branch_no=e.dept_id and e.status='1' " +
                "left join join_area f on e.area_dept_id=f.dept_id " +
                "where a.loan_no='"+loan_no+"'";
        Map accountRow=baseDao.queryRow(sql);
        return accountRow;
    }


    public String getBelongBranch(String dept_id) throws Exception {
        String sql = "select * from join_branch where dept_id='"+dept_id+"' and status = '1' limit 1";
        Row branchRow = baseDao.queryRow(sql);
        if(branchRow!=null){
            return dept_id;
        }else{
            sql="select * from i_frame_sys_dept where dept_id='"+dept_id+"' ";
            log.info("dept_id:"+dept_id);
            Row dept=baseDao.queryRow(sql);
            if(null == dept){
                throw new Exception("所属部门查询不存在！");
            }
            String dept_type=dept.getString("dept_type");
            String p_dept_id = dept.getString("p_dept_id");
            if(StringUtils.equals("-1",dept_type)){
                return null;
            }else{
                return getBelongBranch(p_dept_id);
            }
        }

    }

    public Store getIntervalElementsStore(int start,int end,Store s){
        Store newStore = new Store();
        for(int i = start;i<=end;i++){
            Row row = s.get(i);
            newStore.add(row);
        }
        return newStore;
    }

    /**
     * 输入融租期限，还租方式 触发 修改清算类型
     *
     * @throws Exception
     */
    public void updateClearType(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String duetime = erpUtil.objectIsNullGetString(loanRow.get("loan_cost.duetime"));
        String rep_type = erpUtil.objectIsNullGetString(loanRow.get("loan_info_et.rep_type"));
        String sql;
        if ("6".equals(duetime) && "02".equals(rep_type)){
            sql = "update loan_info_et set clear_type = '1',demolition_period = '24',periods_number = '1' where loan_no = '"+loan_no+"'";
        }else{
            sql = "update loan_info_et set clear_type = '0',demolition_period = '',periods_number = '' where loan_no = '"+loan_no+"'";
        }
        baseDao.execute(sql);
    }
}
