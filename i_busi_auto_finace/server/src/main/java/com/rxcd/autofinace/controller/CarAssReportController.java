/**
 * 创建者：ZhaoSheng 创建时期： 2013-07-15
 */
package com.rxcd.autofinace.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rxcd.autofinace.service.LoanAssAutoService;
import com.rxcd.autofinace.service.LoanFraudAutoService;
import com.rxcd.autofinace.service.LoanRiskcontrolAutoService;
import com.rxcd.autofinace.util.RxErpUtil;
import org.apache.commons.lang.StringUtils;
import org.appframe.bigdata.ChaboshiClinet2;
import org.appframe.common.BigDecimalUtil;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.db.Store;
import org.appframe.wf.util.ErpUtil;
import org.appframe.wf.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.LinkedHashMap;

/**
 * 车辆评估
 */

@RestController
@RequestMapping("/")
public class CarAssReportController {


    @Autowired
    private LoanRiskcontrolAutoService loanRiskcontrolAutoService;
    @Autowired
    private BaseDao baseDao;
    @Autowired
    private LoanFraudAutoService loanFraudAutoService;
    @Autowired
    private RxErpUtil erpUtil;
    @Autowired
    private ChaboshiClinet2 chaboshiClinet;

    /**
     * 模型报告(风控/评估)
     *
     * @throws Exception
     */
    @RequestMapping("/loan_common_CarAss")
    public Row execute(@RequestParam("loan_no") String loan_no, @RequestParam("model") String model, @RequestParam(required = false, value = "ass_type") String ass_type, @RequestParam(required = false, value = "type") String type) throws Exception {
        Row data = new Row();

        String report_name = "";
        String result_name = "";
        String result_score = "";
        Store store = new Store();
        loan_no = loan_no.replaceAll("'","");
        model = model.replaceAll("'","");
        model = model.replaceAll("'","");
        if (StringUtils.isNotEmpty(type)) {
            type = type.replaceAll("'","");
        }
        if (StringUtils.isNotEmpty(ass_type)) {
            ass_type = ass_type.replaceAll("'","");
        }
        if (StringUtils.equals(model, "RISKCONTROL")) {
            result_name = "综合评分";
            if (StringUtils.equals("01", type)) {
                report_name = "风险控制初审报告";
                String sql = "select riskcontrol_first_score from loan_ass where loan_no='" + loan_no + "'";
                result_score = ErpUtil.objectIsNullGetString(baseDao.queryObject(sql));
                sql = "select b.rule_id,b.riskcontrol_dimension_cn as dimension_cn,a.condition_remark,a.riskcontrol_param as param,a.riskcontrol_score as score,a.oper_time "
                        + "from loan_riskcontrol_auto a "
                        + "left join loan_riskcontrol_rule b on a.riskcontrol_dimension=b.riskcontrol_dimension and a.condition_exp=b.condition_exp "
                        + "where a.loan_no='" + loan_no + "' and a.riskcontrol_type = '01'"
                        + "order by a.riskcontrol_score,b.sort_no,cast(b.rule_id as signed)";
                store = baseDao.query(sql);
                if (store == null || store.size() == 0) {
                    try {
                        loanRiskcontrolAutoService.getFirstRiskcontrol(loan_no);
                        report_name = "重新获取初审报告";
                    } catch (Exception e) {
                        e.getStackTrace();
                        report_name = "重新获取初审报告异常";
                    }
                }
            } else if (StringUtils.equals("02", type)) {
                report_name = "风险控制中审报告";
                String sql = "select riskcontrol_mid_score from loan_ass where loan_no='" + loan_no + "'";
                result_score = ErpUtil.objectIsNullGetString(baseDao.queryObject(sql));
                sql = "select * from loan_riskcontrol_auto where loan_no='" + loan_no + "' and riskcontrol_type = '02'";
                store = baseDao.query(sql);
                if (store == null || store.size() == 0) {
                    try {
                        loanRiskcontrolAutoService.getMidRiskcontrol(loan_no);
                        report_name = "重新获取中审报告";
                    } catch (Exception e) {
                        e.getStackTrace();
                        report_name = "重新获取中审报告异常";
                    }
                } else {
                    sql = "select b.rule_id,b.riskcontrol_dimension_cn as dimension_cn,a.condition_remark,a.riskcontrol_param as param,a.riskcontrol_score as score,a.oper_time "
                            + "from loan_riskcontrol_auto a "
                            + "left join loan_riskcontrol_rule b on a.riskcontrol_dimension=b.riskcontrol_dimension and a.condition_exp=b.condition_exp "
                            + "where a.loan_no='" + loan_no + "' and a.riskcontrol_type != '03'"
                            + "order by a.riskcontrol_score,b.sort_no,cast(b.rule_id as signed)";
                    store = baseDao.query(sql);
                }
            } else {
                report_name = "风险控制高审报告";
                String sql = "select riskcontrol_final_score from loan_ass where loan_no='" + loan_no + "'";
                result_score = ErpUtil.objectIsNullGetString(baseDao.queryObject(sql));
                sql = "select * from loan_riskcontrol_auto where loan_no='" + loan_no + "' and riskcontrol_type = '03'";
                store = baseDao.query(sql);
                if (store == null || store.size() == 0) {
                    try {
                        loanRiskcontrolAutoService.getFinalRiskcontrol(loan_no);
                        report_name = "重新获取终审报告";
                    } catch (Exception e) {
                        e.getStackTrace();
                        report_name = "重新获取终审报告异常";
                    }
                } else {
                    sql = "select b.rule_id,b.riskcontrol_dimension_cn as dimension_cn,a.condition_remark,a.riskcontrol_param as param,a.riskcontrol_score as score,a.oper_time "
                            + "from loan_riskcontrol_auto a "
                            + "left join loan_riskcontrol_rule b on a.riskcontrol_dimension=b.riskcontrol_dimension and a.condition_exp=b.condition_exp "
                            + "where a.loan_no='" + loan_no + "' "
                            + "order by a.riskcontrol_score,b.sort_no,cast(b.rule_id as signed)";
                    store = baseDao.query(sql);
                    String detailed_sql = "select detailed_no as rule_id,"
                            + "riskcontrol_dimension as dimension_cn,"
                            + "condition_remark,"
                            + "riskcontrol_score as score,"
                            + "oper_time "
                            + "from loan_riskcontrol_detailed "
                            + "where loan_no = '" + loan_no + "'";
                    Store detailed_store = baseDao.query(detailed_sql);
                    for (Row row : detailed_store) {
                        store.add(row);
                    }
                    String ass_sql = "select ifnull(b.rule_id,'') as rule_id,ifnull(b.ass_dimension_cn,'') as dimension_cn,a.condition_remark,a.param,a.result as score,a.oper_time "
                            + "from loan_ass_auto a "
                            + "left join loan_ass_rule b on a.dimension=b.ass_dimension and a.condition_exp=b.condition_exp "
                            + "where a.loan_no='" + loan_no + "' "
                            + "order by ABS(result) desc,b.sort_no,cast(ifnull(b.rule_id,'9999') as signed)";
                    Store ass_store = baseDao.query(ass_sql);
                    for (Row addrow : ass_store) {
                        store.add(addrow);
                    }
                }
            }
        } else if (StringUtils.equals(model, "FRAUD")) {
//            result_name = "综合评分";
//            if (StringUtils.equals("01", type)) {
//                report_name = "反欺诈初审报告";
//                String sql = "select fraud_first_score from loan_ass where loan_no='" + loan_no + "'";
//                result_score = ErpUtil.objectIsNullGetString(baseDao.queryObject(sql));
//                sql = "select b.rule_id,b.fraud_dimension_cn as dimension_cn,a.condition_remark,a.fraud_param as param,a.fraud_score as score,a.oper_time "
//                        + "from loan_fraud_auto a "
//                        + "left join loan_fraud_rule b on a.fraud_dimension=b.fraud_dimension and a.condition_exp=b.condition_exp "
//                        + "where a.loan_no='" + loan_no + "' and a.fraud_type = '01'"
//                        + "order by a.fraud_score,b.sort_no,cast(b.rule_id as signed)";
//                store = baseDao.query(sql);
//                if (store == null || store.size() == 0) {
//
//                    try {
//                        loanFraudAutoService.getFirstFraudcontrol(loan_no);
//                        report_name = "重新获取初审报告";
//                    } catch (Exception e) {
//                        e.getStackTrace();
//                        report_name = "重新获取初审报告异常";
//                    }
//                }
//            } else if (StringUtils.equals("02", type)) {
//                report_name = "反欺诈中审报告";
//                String sql = "select fraud_mid_score from loan_ass where loan_no='" + loan_no + "'";
//                result_score = ErpUtil.objectIsNullGetString(baseDao.queryObject(sql));
//                sql = "select * from loan_fraud_auto where loan_no='" + loan_no + "' and fraud_type = '02'";
//                store = baseDao.query(sql);
//                if (store == null || store.size() == 0) {
//
//                    try {
//                        loanFraudAutoService.getMidFraudcontrol(loan_no);
//                        report_name = "重新获取中审报告";
//                    } catch (Exception e) {
//                        e.getStackTrace();
//                        report_name = "重新获取中审报告异常";
//                    }
//                } else {
//                    sql = "select b.rule_id,b.fraud_dimension_cn as dimension_cn,a.condition_remark,a.fraud_param as param,a.fraud_score as score,a.oper_time "
//                            + "from loan_fraud_auto a "
//                            + "left join loan_fraud_rule b on a.fraud_dimension=b.fraud_dimension and a.condition_exp=b.condition_exp "
//                            + "where a.loan_no='" + loan_no + "' and a.fraud_type != '03'"
//                            + "order by a.fraud_score,b.sort_no,cast(b.rule_id as signed)";
//                    store = baseDao.query(sql);
//                }
//            } else {
//                report_name = "反欺诈高审报告";
//                String sql = "select fraud_final_score from loan_ass where loan_no='" + loan_no + "'";
//                result_score = ErpUtil.objectIsNullGetString(baseDao.queryObject(sql));
//                sql = "select * from loan_fraud_auto where loan_no='" + loan_no + "' and fraud_type = '03'";
//                store = baseDao.query(sql);
//                if (store == null || store.size() == 0) {
//                    try {
//                        loanFraudAutoService.getFinalFraudcontrol(loan_no);
//                        report_name = "重新获取高审报告";
//                    } catch (Exception e) {
//                        e.getStackTrace();
//                        report_name = "重新获取高审报告异常";
//                    }
//                } else {
//                    sql = "select b.rule_id,b.fraud_dimension_cn as dimension_cn,a.condition_remark,a.fraud_param as param,a.fraud_score as score,a.oper_time "
//                            + "from loan_fraud_auto a "
//                            + "left join loan_fraud_rule b on a.fraud_dimension=b.fraud_dimension and a.condition_exp=b.condition_exp "
//                            + "where a.loan_no='" + loan_no + "' "
//                            + "order by a.fraud_score,b.sort_no,cast(b.rule_id as signed)";
//                    store = baseDao.query(sql);
//                }
//            }
        } else if (StringUtils.equals(model, "CAR_ASS")) {

            String sql = "select ifnull(b.rule_id,'') as rule_id,ifnull(b.ass_dimension_cn,'') as dimension_cn,a.condition_remark,a.param,a.result as score,a.oper_time "
                    + "from loan_ass_auto a "
                    + "left join loan_ass_rule b on a.dimension=b.ass_dimension and a.condition_exp=b.condition_exp "
                    + "where a.loan_no='" + loan_no + "' and a.ass_type='" + ass_type + "' ";
            store = baseDao.query(sql);


            if (StringUtils.equals(ass_type, LoanAssAutoService.BRANCH_SCORE)) {
                report_name = "车况评估报告";
                result_name = "综合得分";
                sql = "select ass_score_branch from loan_ass where loan_no='" + loan_no + "'";
                result_score = ErpUtil.objectIsNullGetString(baseDao.queryObject(sql));

                try {
                    String oper_time = Util.format(new Date());
                    if (store.size() > 0) {
                        oper_time = store.get(0).getString("oper_time");
                    }
                    Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
                    String report_id = ErpUtil.objectIsNullGetString(loanRow.get("loan_car.cbs_report_id"));

                    if (Util.isNotEmpty(report_id)) {
                        JSONObject resultJson = chaboshiClinet.getNewReportJson(report_id);
                        Store cbs = baseDao.query("select no,dictionaries_name as dimension_cn from dictionaries_cbs where status='1'");

                        int index = 0;
                        for (Row item : cbs) {
                            if (StringUtils.endsWith(item.getString("no"), "Records")) {
                                JSONArray arr = resultJson.getJSONArray(item.getString("no"));
                                if (arr != null) {
                                    for (int i = 0; i < arr.size(); i++) {
                                        JSONObject j_item = (JSONObject) arr.get(i);
                                        Row itemRow = new Row();
                                        itemRow.put("rule_id", "cbs" + String.format("%03d", index));
                                        itemRow.put("dimension_cn", item.get("dimension_cn") + "-" + ErpUtil.objectIsNullGetString(j_item.getString("type")));
                                        itemRow.put("condition_remark", ErpUtil.objectIsNullGetString(j_item.getString("content")) + ErpUtil.objectIsNullGetString(j_item.getString("materal")));
                                        itemRow.put("param", j_item.toJSONString());
                                        itemRow.put("score", "0");
                                        itemRow.put("oper_time", oper_time);
                                        store.add(itemRow);
                                        index++;
                                    }
                                }
                            } else {
                                String condition_remark = resultJson.getString(item.getString("no"));
                                if (Util.isEmpty(condition_remark)) {
                                    condition_remark = "无";
                                }
                                item.put("rule_id", "cbs" + String.format("%03d", index));
                                item.put("condition_remark", condition_remark);
                                item.put("param", resultJson.getString(item.getString("no")));
                                item.put("score", "0");
                                item.put("oper_time", oper_time);
                                store.add(item);
                                index++;
                            }

                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (StringUtils.equals(ass_type, LoanAssAutoService.TRUE_SCORE)) {
                report_name = "车况评估报告(真实)";
                result_name = "综合得分";
                sql = "select ass_score from loan_ass where loan_no='" + loan_no + "'";
                result_score = ErpUtil.objectIsNullGetString(baseDao.queryObject(sql));
                try {
                    String oper_time = Util.format(new Date());
                    if (store.size() > 0) {
                        oper_time = store.get(0).getString("oper_time");
                    }
                    Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
                    String report_id = ErpUtil.objectIsNullGetString(loanRow.get("loan_ass.cbs_report_id"));
                    if (Util.isNotEmpty(report_id)) {
                        JSONObject resultJson = chaboshiClinet.getNewReportJson(report_id);
                        Store cbs = baseDao.query("select '' as rule_id,no,dictionaries_name as dimension_cn from dictionaries_cbs where status='1'");

                        int index = 1;
                        for (Row item : cbs) {
                            if (StringUtils.endsWith(item.getString("no"), "Records")) {
                                JSONArray arr = resultJson.getJSONArray(item.getString("no"));
                                if (arr != null) {
                                    for (int i = 0; i < arr.size(); i++) {
                                        JSONObject j_item = (JSONObject) arr.get(i);
                                        Row itemRow = new Row();
                                        itemRow.put("rule_id", "cbs" + String.format("%03d", index));
                                        itemRow.put("dimension_cn", item.get("dimension_cn") + "-" + ErpUtil.objectIsNullGetString(j_item.getString("type")));
                                        itemRow.put("condition_remark", ErpUtil.objectIsNullGetString(j_item.getString("content")) + ErpUtil.objectIsNullGetString(j_item.getString("materal")));
                                        itemRow.put("param", j_item.toJSONString());
                                        itemRow.put("score", "0");
                                        itemRow.put("oper_time", oper_time);
                                        store.add(itemRow);
                                        index++;
                                    }
                                }
                            } else {
                                String condition_remark = resultJson.getString(item.getString("no"));
                                if (Util.isEmpty(condition_remark)) {
                                    condition_remark = "无";
                                }
                                item.put("rule_id", "cbs" + String.format("%03d", index));
                                item.put("condition_remark", condition_remark);
                                item.put("param", resultJson.getString(item.getString("no")));
                                item.put("score", "0");
                                item.put("oper_time", oper_time);
                                store.add(item);
                                index++;
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                if (StringUtils.equals(ass_type, LoanAssAutoService.BRANCH_INIT_PRICE)) {
                    report_name = "汽车初评车况影响报告";
                    result_name = "综合影响率";
                } else if (StringUtils.equals(ass_type, LoanAssAutoService.TURE_INIT_PRICE)) {
                    report_name = "汽车初评车况影响报告(真实)";
                    result_name = "综合影响率";
                } else if (StringUtils.equals(ass_type, LoanAssAutoService.BRANCH_FINAL_PRICE)) {
                    report_name = "汽车终评车况影响报告";
                    result_name = "综合影响率";
                } else if (StringUtils.equals(ass_type, LoanAssAutoService.TRUE_FINAL_PRICE)) {
                    report_name = "汽车终评车况影响报告(真实)";
                    result_name = "综合影响率";
                }
                double result_double = 1;
                sql = "select result from loan_ass_auto where loan_no='" + loan_no + "' and ass_type='" + ass_type + "'";
                Store results = baseDao.query(sql);
                for (Row row : results) {
                    result_double *= (1 + ErpUtil.objectIsNullGetDouble(row.get("result")));
                }
                result_score = String.valueOf(BigDecimalUtil.round(result_double - 1, 4));
            }
        }
        data.put("report_name", report_name);
        data.put("type", "report");

        data.put("result_score", result_name + " : " + result_score);

        LinkedHashMap rowT = new LinkedHashMap();

        rowT.put("rule_id", "规则编号");
        rowT.put("dimension_cn", "维度");
        rowT.put("condition_remark", "具体描述");
        rowT.put("oper_time", "评估时间");
        data.put("title", rowT);

        data.put("list", store);

        return data;

    }
}