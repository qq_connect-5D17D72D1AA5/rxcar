/**
 * 版权所有：版权所有：重庆易房网络科技股份有限公司
 * Chongqing YiFang Network Technology Co. LTD.  All Rights Reserved
 * 创建者：HeSenLin   创建时期：  2016年7月14日
 */
package com.rxcd.autofinace.nodeEvent.v5;


import com.rxcd.autofinace.service.LoanCheckServiceImpl;
import org.appframe.wf.engine.AbstractWorkFlowNodeEvent;
import org.appframe.wf.util.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 业务产品选择节点事件
 */
@Component
public class YwcpxzjNodeEventImp extends AbstractWorkFlowNodeEvent {

    @Autowired
    private LoanCheckServiceImpl checkService;

    /**
     * 流程节点启动时调用
     *
     * @param flow_node_no
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowNodeStart(String flow_node_no, Param p) throws Exception {

    }

    @Override
    public void workFlowNodeFinish(String flow_node_no, Param p) throws Exception {
        checkTempletFlowNo("1");
        String loanNo = getTableKeyNo();
        checkService.check_select_capital(loanNo);
    }

    /**
     * 流程节点驳回时回调方法
     *
     * @param flow_node_no
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowNodeOverrule(String flow_node_no, Param p) throws Exception {

    }

    @Override
    public void workFlowNodeVie(String flow_node_no, Param p) throws Exception {

    }
}
