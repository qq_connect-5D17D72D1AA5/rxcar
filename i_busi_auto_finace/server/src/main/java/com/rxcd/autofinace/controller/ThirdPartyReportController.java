package com.rxcd.autofinace.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rxcd.autofinace.common.CarPlateCityEnum;
import com.rxcd.autofinace.service.ThirdPartyReportService;
import com.rxcd.autofinace.util.RxErpUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.appframe.bigdata.TianXingClinet2;
import org.appframe.bigdata.TongDunClinet2;
import org.appframe.common.controller.BaseController;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.engine.common.WfConst;
import org.appframe.wf.util.Param;
import org.appframe.wf.util.SafeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * 第三方报告
 */
@RestController
@RequestMapping("/")
public class ThirdPartyReportController extends BaseController {


    private static final Log log = LogFactory.getLog(ThirdPartyReportController.class);

    @Autowired
    private BaseDao baseDao;
    @Autowired
    private TianXingClinet2 tianXingClinet;
    @Autowired
    private RxErpUtil erpUtil;
    @Autowired
    private TongDunClinet2 tongDunClinet;
    @Autowired
    private ThirdPartyReportService thirdPartyReportService;

    @RequestMapping("/loan_common_ThirdPartyReport")
    public Row execute(HttpServletRequest httpServletRequest, @RequestParam(required = false, value = "json") String json, @RequestParam(required = false, value = "loan_no") String loan_no, @RequestParam(required = false, value = "requestType") String requestType) {
        Row resultRow = new Row();
        Param param = new Param();

        param.request = httpServletRequest;
        request.setAttribute("json", json);
        try {
            String loanNo = loan_no;
            if (StringUtils.isBlank(loanNo)) {
                //如果为空 可能是  ajax请求使用了base64
                loanNo = param.getJsonString(param.getDataJson(), "loan_no");
            }
            if (StringUtils.isBlank(loanNo)) {
                return resultRow;
            }
            loanNo = loanNo.replaceAll("'", "");
            //返回前端使用
            resultRow.put("loan_no_str", loanNo);
            loanNo = SafeUtil.safe(loanNo);
            if (StringUtils.isBlank(requestType)) {
                //如果为空 可能是  ajax请求使用了base64
                requestType = param.getJsonString(param.getDataJson(), "requestType");
            }
            if (StringUtils.isNotBlank(requestType) && StringUtils.equals("UPLOAD_IMG", requestType)) {
                return thirdPartyReportService.uploadImg(resultRow, param, loanNo, getCurrentUser().getUserId().toString());
            }

            String reportId = param.getJsonString(param.getDataJson(), "reportId");
            String sql = "select * from loan_cust where loan_no='" + SafeUtil.safe(loanNo) + "'";
            Row tempRow = baseDao.queryRow(sql);
            if (tempRow == null) {
                return resultRow;
            }
            String cust_name = tempRow.getString("cust_name");
            String cust_phone = tempRow.getString("cust_phone");
            String idcard = tempRow.getString("idcard");

            if (StringUtils.isBlank(cust_name) || StringUtils.isBlank(cust_phone) || StringUtils.isBlank(idcard)) {
                resultRow.put("result", false);
                resultRow.put(WfConst.ERR_MSG, "贷款人姓名/手机/身份证不能为空");
                param.set("json", resultRow);
//                result.JSON();
                resultRow.put("type", "jsonJsp");
                return resultRow;
            }

            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("name", cust_name);// 姓名 必填
            paramMap.put("id_number", idcard);// 身份证 必填
            paramMap.put("mobile", cust_phone);// 手机号

            JSONObject loanPerson = (JSONObject) JSONObject.toJSON(paramMap);
            resultRow.put("loanPerson", loanPerson);
            // 只获取妻子的信息
            sql = "SELECT cb.contacts_name, cb.contacts_mobile, cb.contacts_idcard FROM loan_info_co_borrower cb WHERE cb.status = '1' and cb.loan_no = '" + loanNo + "' and contacts_relation = '02' limit 1";
            Row row = baseDao.queryRow(sql);
            if (row != null) {
                String contacts_name = row.getString("contacts_name");
                String contacts_mobile = row.getString("contacts_mobile");
                String contacts_idcard = row.getString("contacts_idcard");
                if (StringUtils.isNotBlank(contacts_name) || StringUtils.isNotBlank(contacts_mobile) || StringUtils.isNotBlank(contacts_idcard)) {
                    // 第一联系人 只查询妻子或丈夫的否则不查询
                    paramMap.put("contact1_relation", "spouse");
                    paramMap.put("contact1_name", contacts_name);
                    paramMap.put("contact1_id_number", contacts_idcard);
                    paramMap.put("contact1_mobile", contacts_mobile);
                }
            }
            if (StringUtils.isBlank(reportId)) {
                //个人不良信息
                try {
                    resultRow.put("badInfo", tianXingClinet.queryNegative(cust_name, idcard, false));
                } catch (Exception e) {
                    resultRow.put("badInfo", "");
                    log.info("不良信息接口调用失败:" + e.getMessage());
                }

                //个人涉诉
                resultRow.put("personalLitigation", tianXingClinet.queryHighCourtRersonal(cust_name, idcard, false));
                try {
                    //个人名下企业
                    JSONObject companyData = tianXingClinet.queryEnterpriseMember(idcard, false);
                    JSONObject dataJson = companyData.getJSONObject("data");
                    if (StringUtils.equals("EXIST", dataJson.getString("status"))) {
                        JSONArray corporates = dataJson.getJSONArray("corporates");
                        for (int i = 0; i < corporates.size(); i++) {
                            JSONObject companyJson = corporates.getJSONObject(i);
                            //如果是在营状态 需要查询  企业涉诉
                            if (StringUtils.contains(companyJson.getString("entStatus"), "在营")) {
                                JSONObject jsonObject = tianXingClinet.queryHighCourtEnterprise(companyJson.getString("entName"), companyJson.getString("regNo"), false);
                                try {
                                    companyJson.put("companyLitigation", jsonObject.getJSONObject("data"));
                                } catch (Exception e) {
                                    log.error("读取企业涉诉信息异常,data=" + jsonObject, e);
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                    resultRow.put("companyData", dataJson);
                } catch (Exception e) {
                    resultRow.put("companyData", "");
                    log.info("个人名下企业接口调用失败:" + e.getMessage());
                }
                //违章信息  目前接口没有权限
                sql = "select * from loan_car where loan_no='" + SafeUtil.safe(loanNo) + "'";
                Row carRow = baseDao.queryRow(sql);
                String driveNum = carRow.getString("fdjh");
                String vin = carRow.getString("cjh");
                String car_plate = carRow.getString("car_plate");
                try {
                    CarPlateCityEnum carPlateCityEnum = CarPlateCityEnum.getCarPlateCityEnum(car_plate);
                    String cityCode = erpUtil.getCityCode(carPlateCityEnum.getCityName());
                    resultRow.put("offencesData", tianXingClinet.queryTrafficOffences(car_plate, vin, driveNum, cityCode, false));
                    resultRow.put("offencesDataStatus", true);
                } catch (Exception e) {
                    resultRow.put("offencesDataStatus", false);
                    resultRow.put("offencesData", "error");
                }
            }
            // 如果报告id为空就产生一个报告id
            if (StringUtils.isBlank(reportId)) {
                //把验证次数字段转换为json
                reportId = tongDunClinet.queryReportNo(paramMap, false);
                // 等待210毫秒 应为有延时
                Thread.sleep(1000);
            }
            resultRow.put("result", true);
            resultRow.put("reportId", reportId);
            JSONObject jsonObject = tongDunClinet.queryReport(reportId, false);
            if (jsonObject.getBooleanValue("success")) {
                resultRow.put("tongDunData", jsonObject);
            } else {
                //表示没有生成报告
                if (!StringUtils.equals("204", jsonObject.getString("reason_code"))) {
                    throw new Exception(jsonObject.getString("reason_desc"));
                }
            }
            log.info("thirdpartyreport_resultrow:" + resultRow);
        } catch (Exception e) {
            e.printStackTrace();
            resultRow.put("result", false);
            resultRow.put(WfConst.ERR_MSG, e.getMessage());
        }
        param.set("json", resultRow);
        resultRow.put("type", "jsonJsp");
        return resultRow;
    }


}
