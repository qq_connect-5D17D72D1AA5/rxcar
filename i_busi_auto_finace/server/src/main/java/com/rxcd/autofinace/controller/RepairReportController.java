/**
 * 创建者：ZhaoSheng 创建时期： 2013-07-15
 */
package com.rxcd.autofinace.controller;


import com.alibaba.fastjson.JSONObject;
import com.rxcd.autofinace.util.PropertiesUtil;
import com.rxcd.autofinace.util.RxErpUtil;
import org.apache.commons.lang.StringUtils;
import org.appframe.bigdata.ChaboshiClinet2;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.util.ErpUtil;
import org.appframe.wf.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 续期申请单
 */
@RestController
@RequestMapping("/")
public class RepairReportController {

    //获取当前系统运行环境 是开发还是正式
    private static final String RUNTIME = PropertiesUtil.getValue("runtime");
    @Autowired
    private RxErpUtil erpUtil;
    @Autowired
    private ChaboshiClinet2 chaboshiClinet;
    @Autowired
    private BaseDao baseDao;

    @RequestMapping("/loan_common_RepairReport")
    public Row execute(HttpServletRequest request, @RequestParam("loan_no") String loan_no) throws Exception {

        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String cbs_report_id = ErpUtil.objectIsNullGetString(loanRow.get("loan_ass.cbs_report_id"));
        if (Util.isEmpty(cbs_report_id)) {
            String vin = ErpUtil.objectIsNullGetString(loanRow.get("loan_ass.vin_code"));
            String engine_code = ErpUtil.objectIsNullGetString(loanRow.get("loan_ass.engine_code"));
            if (Util.isEmpty(vin)) {
                throw new Exception("车架号为空，不能查询维修记录");
            }
            if (StringUtils.equals("0", RUNTIME)) {
                throw new Exception("非正式环境，不购买维修报告");
            }
            cbs_report_id = chaboshiClinet.getReportId(vin, engine_code, false);
            baseDao.execute("update loan_ass set cbs_report_id='" + cbs_report_id + "' where loan_no='" + loan_no + "'");
        }

        //查报告状态，如果出来直接通过，如果未出，等0.5秒再查，最多循环5次，如果还是没查出来，抛错未出原因
        for (int i = 0; i < 5; i++) {
            try {
                chaboshiClinet.checkReportStatus(cbs_report_id);
                break;
            } catch (Exception e) {
                if (i == 5) {
                    throw e;
                }
                Thread.sleep(500);
            }
        }

        JSONObject resultJson = chaboshiClinet.getNewReportUrl(cbs_report_id);
        String device = request.getHeader("user-agent").toLowerCase();
        String retUrl;
        if (device.indexOf("android") > 0 || device.indexOf("mac os") > 0 || device.indexOf("windows phone") > 0) {
            retUrl = resultJson.getJSONObject("report").getString("mobileUrl");
        } else {
            retUrl = resultJson.getJSONObject("report").getString("pcUrl").replace("http:", "https:");
        }
        Row ret = new Row();
        ret.put("type", "url");
        ret.put("value", retUrl);

        return ret;
    }
}