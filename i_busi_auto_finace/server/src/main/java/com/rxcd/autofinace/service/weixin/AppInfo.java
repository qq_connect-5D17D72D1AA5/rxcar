package com.rxcd.autofinace.service.weixin;

/**
 * 企业微信应用Info
 * @author hubin
 *
 */
public class AppInfo {

	private String agentID;
	
	private String corpID;
	
	private String secret;
	
	private String token;
	
	public AppInfo(String agentID,String corpID,String secret,String token){
		this.agentID  = agentID;
		this.corpID  = corpID;
		this.secret  = secret;
		this.token  = token;
	}

	public String getAgentID() {
		return agentID;
	}

	public void setAgentID(String agentID) {
		this.agentID = agentID;
	}

	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
}
