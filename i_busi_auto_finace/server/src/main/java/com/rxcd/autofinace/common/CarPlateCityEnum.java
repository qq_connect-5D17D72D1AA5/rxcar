package com.rxcd.autofinace.common;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rxcd.autofinace.util.PinyinUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.appframe.wf.util.Util;

import java.util.Map.Entry;

/**
 * 汽车牌照城市枚举
 */
public enum CarPlateCityEnum {

    SHAN_XI_TAI_YUAN("晋A", "山西太原", "2201", 6),
    LIAO_NING_AN_SHAN("辽C", "辽宁鞍山", "1710", 4),
    LIAO_NING_BEN_XI("辽E", "辽宁本溪", "1709", 4),
    LIAO_NING_DAN_DONG("辽F", "辽宁丹东", "1702", 4),
    LIAO_NING_YING_KOU("辽H", "辽宁营口", "1714", 0),
    LIAO_NING_FU_XIN("辽J", "辽宁阜新", "1704", 0),
    LIAO_NING_HU_LU_DAO("辽P", "辽宁葫芦岛", "1705", 99),
    LIAO_NING_DA_LIAN("辽B", "辽宁大连", "1708", 4),
    LIAO_NING_JIN_ZHOU("辽G", "辽宁锦州", "1711", 6),
    LIAO_NING_SHEN_YANG("辽A", "辽宁沈阳", "1701", 6),
    LIAO_NING_LIAO_YANG("辽K", "辽宁辽阳", "1713", 4),
    JI_LIN_JI_LIN("吉B", "吉林吉林", "1402", 4),
    JI_LIN_SI_PING("吉C", "吉林四平", "1616", 4),
    JI_LIN_LIAO_YUAN("吉D", "吉林辽源", "1405", 4),
    JI_LIN_TONG_HUA("吉E", "吉林通化", "1403", 4),
    JI_LIN_BAI_SHAN("吉F", "吉林白山", "1406", 4),
    JI_LIN_SONG_YUAN("吉J", "吉林松原", "1425", 4),
    JI_LIN_BAI_CHENG("吉G", "吉林白城", "1412", 4),
    JI_LIN_ZHANG_CHUN("吉A", "吉林长春", "1401", 4),
    JI_LIN_YAN_BIAN_CHAO_XIAN_ZU("吉H", "吉林延边朝鲜族", "1428", 4),
    HEI_LONG_JIANG_HA_ER_BIN("黑A", "黑龙江哈尔滨", "1101", 99),
    HEI_LONG_JIANG_QI_TAI_HE("黑K", "黑龙江七台河", "1109", 99),
    HEI_LONG_JIANG_QI_QI_HA_ER("黑B", "黑龙江齐齐哈尔", "1103", 99),
    HEI_LONG_JIANG_JI_XI("黑G", "黑龙江鸡西", "1107", 99),
    HEI_LONG_JIANG_HE_GANG("黑H", "黑龙江鹤岗", "1104", 99),
    HEI_LONG_JIANG_SHUANG_YA_SHAN("黑J", "黑龙江双鸭山", "1123", 99),
    HEI_LONG_JIANG_DA_QING("黑E", "黑龙江大庆", "1102", 99),
    HEI_LONG_JIANG_YI_CHUN("黑F", "黑龙江伊春", "1112", 99),
    HEI_LONG_JIANG_JIA_MU_SI("黑D", "黑龙江佳木斯", "1106", 99),
    HEI_LONG_JIANG_MU_DAN_JIANG("黑C", "黑龙江牡丹江", "1108", 99),
    HEI_LONG_JIANG_HEI_HE("黑N", "黑龙江黑河", "1113", 99),
    HEI_LONG_JIANG_SUI_HUA("黑M", "黑龙江绥化", "1131", 99),
    HEI_LONG_JIANG_DA_XING_AN_LING_DI_QU("黑P", "黑龙江大兴安岭地区", "1136", 99),
    HEI_LONG_JIANG_SONG_HUA_JIANG_DI_QU("黑L", "黑龙江松花江地区", "1101", 99),
    HEI_LONG_JIANG_NONG_KEN_XI_TONG("黑R", "黑龙江农垦系统", "1101", 99),
    JIANG_SU_NAN_JING("苏A", "江苏南京", "1501", 0),
    JIANG_SU_CHANG_ZHOU("苏D", "江苏常州", "1505", 0),
    JIANG_SU_WU_XI("苏B", "江苏无锡", "1503", 0),
    JIANG_SU_XU_ZHOU("苏C", "江苏徐州", "1518", 0),
    JIANG_SU_SU_ZHOU("苏E", "江苏苏州", "1502", 0),
    JIANG_SU_NAN_TONG("苏F", "江苏南通", "1511", 0),
    JIANG_SU_LIAN_YUN_GANG("苏G", "江苏连云港", "1510", 0),
    JIANG_SU_HUAI_AN("苏H", "江苏淮安", "1507", 0),
    JIANG_SU_YAN_CHENG("苏J", "江苏盐城", "1512", 0),
    JIANG_SU_YANG_ZHOU("苏K", "江苏扬州", "1513", 0),
    JIANG_SU_ZHEN_JIANG("苏L", "江苏镇江", "1515", 0),
    JIANG_SU_TAI_ZHOU("苏M", "江苏泰州", "1517", 0),
    JIANG_SU_SU_QIAN("苏N", "江苏宿迁", "1520", 0),
    ZHE_JIANG_NING_BO("浙B", "浙江宁波", "3002", 6),
    ZHE_JIANG_JIA_XING("浙F", "浙江嘉兴", "3005", 99),
    ZHE_JIANG_QU_ZHOU("浙H", "浙江衢州", "3012", 6),
    ZHE_JIANG_ZHOU_SHAN("浙L", "浙江舟山", "3020", 6),
    ZHE_JIANG_TAI_ZHOU("浙J", "浙江台州", "3015", 6),
    ZHE_JIANG_LI_SHUI("浙K", "浙江丽水", "3009", 6),
    ZHE_JIANG_HANG_ZHOU("浙A", "浙江杭州", "3001", 6),
    ZHE_JIANG_HU_ZHOU("浙E", "浙江湖州", "3011", 6),
    ZHE_JIANG_SHAO_XING("浙D", "浙江绍兴", "3016", 0),
    ZHE_JIANG_JIN_HUA("浙G", "浙江金华", "3006", 3),
    ZHE_JIANG_WEN_ZHOU("浙C", "浙江温州", "3003", 6),
    SHAN_DONG_ZAO_ZHUANG("鲁D", "山东枣庄", "2107", 6),
    SHAN_DONG_DONG_YING("鲁E", "山东东营", "2110", 6),
    SHAN_DONG_YAN_TAI("鲁F", "山东烟台", "2103", 6),
    SHAN_DONG_JI_NING("鲁H", "山东济宁", "2118", 6),
    SHAN_DONG_WEI_HAI("鲁K", "山东威海", "2104", 6),
    SHAN_DONG_RI_ZHAO("鲁L", "山东日照", "2120", 6),
    SHAN_DONG_LAI_WU("鲁S", "山东莱芜", "2132", 6),
    SHAN_DONG_LIN_YI("鲁Q", "山东临沂", "2117", 6),
    SHAN_DONG_DE_ZHOU("鲁N", "山东德州", "1015", 6),
    SHAN_DONG_LIAO_CHENG("鲁P", "山东聊城", "2114", 6),
    SHAN_DONG_BIN_ZHOU("鲁M", "山东滨州", "2113", 6),
    SHAN_DONG_HE_ZE("鲁R", "山东菏泽", "2112", 6),
    SHAN_DONG_YAN_TAI_SHI_ZENG_BU("鲁Y", "山东烟台市增补", "2103", 6),
    SHAN_DONG_JI_NAN("鲁A", "山东济南", "2101", 6),
    SHAN_DONG_WEI_FANG("鲁G", "山东潍坊", "2105", 6),
    SHAN_DONG_WEI_FANG_SHI_ZENG_BU("鲁V", "山东潍坊市增补", "2105", 6),
    SHAN_DONG_QING_DAO("鲁B", "山东青岛", "2102", 6),
    SHAN_DONG_ZI_BO("鲁C", "山东淄博", "2109", 6),
    SHAN_DONG_QING_DAO_SHI_ZENG_BU("鲁U", "山东青岛市增补", "2102", 6),
    HE_NAN_KAI_FENG("豫B", "河南开封", "1013", 99),
    HE_NAN_LUO_YANG("豫C", "河南洛阳", "1002", 6),
    HE_NAN_JIAO_ZUO("豫H", "河南焦作", "1011", 6),
    HE_NAN_PU_YANG("豫J", "河南濮阳", "1008", 6),
    HE_NAN_XU_CHANG("豫K", "河南许昌", "1023", 6),
    HE_NAN_SHANG_QIU("豫N", "河南商丘", "1006", 6),
    HE_NAN_JI_YUAN("豫U", "河南济源", "1019", 99),
    HE_NAN_ZHENG_ZHOU("豫A", "河南郑州", "1001", 99),
    HE_NAN_PING_DING_SHAN("豫D", "河南平顶山", "1018", 6),
    HE_NAN_AN_YANG("豫E", "河南安阳", "1014", 6),
    HE_NAN_HE_BI("豫F", "河南鹤壁", "1016", 6),
    HE_NAN_XIN_XIANG("豫G", "河南新乡", "1005", 6),
    HE_NAN_SAN_MEN_XIA("豫M", "河南三门峡", "1007", 6),
    HE_NAN_NAN_YANG("豫R", "河南南阳", "1009", 6),
    HE_NAN_ZHOU_KOU("豫P", "河南周口", "1003", 6),
    HE_NAN_ZHU_MA_DIAN("豫Q", "河南驻马店", "1021", 6),
    HE_NAN_XIN_YANG("豫S", "河南信阳", "1004", 6),
    HE_NAN_LUO_HE("豫L", "河南漯河", "1010", 6),
    HU_BEI_YI_CHANG("鄂E", "湖北宜昌", "1207", 5),
    HU_BEI_WU_HAN("鄂A", "湖北武汉", "1201", 5),
    HU_BEI_HUANG_SHI("鄂B", "湖北黄石", "1208", 5),
    HU_BEI_SHI_YAN("鄂C", "湖北十堰", "1202", 5),
    HU_BEI_XIANG_YANG("鄂F", "湖北襄阳", "1203", 5),
    HU_BEI_E_ZHOU("鄂G", "湖北鄂州", "1216", 5),
    HU_BEI_JING_MEN("鄂H", "湖北荆门", "1209", 5),
    HU_BEI_XIAO_GAN("鄂K", "湖北孝感", "1229", 5),
    HU_BEI_JING_ZHOU("鄂D", "湖北荆州", "1210", 5),
    HU_BEI_HUANG_GANG("鄂J", "湖北黄冈", "1236", 5),
    HU_BEI_XIAN_NING("鄂L", "湖北咸宁", "1217", 5),
    HU_BEI_SUI_ZHOU("鄂S", "湖北随州", "1204", 5),
    HU_BEI_EN_SHI_TU_JIA_ZU_MIAO_ZU("鄂Q", "湖北恩施土家族苗族", "422800", 5),
    HU_BEI_XIAN_TAO("鄂M", "湖北仙桃", "1205", 5),
    HU_BEI_QIAN_JIANG("鄂N", "湖北潜江", "1219", 5),
    HU_BEI_TIAN_MEN("鄂R", "湖北天门", "1206", 5),
    HU_BEI_SHEN_NONG_JIA_LIN_QU("鄂P", "湖北神农架林区", "1241", 5),
    HU_NAN_ZHU_ZHOU("湘B", "湖南株洲", "1307", 0),
    HU_NAN_XIANG_TAN("湘C", "湖南湘潭", "1309", 0),
    HU_NAN_HENG_YANG("湘D", "湖南衡阳", "1304", 0),
    HU_NAN_SHAO_YANG("湘E", "湖南邵阳", "1310", 0),
    HU_NAN_YUE_YANG("湘F", "湖南岳阳", "1308", 0),
    HU_NAN_CHANG_DE("湘J", "湖南常德", "1303", 0),
    HU_NAN_ZHANG_JIA_JIE("湘G", "湖南张家界", "1315", 0),
    HU_NAN_YI_YANG("湘H", "湖南益阳", "1313", 0),
    HU_NAN_CHEN_ZHOU("湘L", "湖南郴州", "1302", 0),
    HU_NAN_YONG_ZHOU("湘M", "湖南永州", "1312", 0),
    HU_NAN_HUAI_HUA("湘N", "湖南怀化", "1305", 0),
    HU_NAN_LOU_DI("湘K", "湖南娄底", "1306", 0),
    HU_NAN_XIANG_XI_TU_JIA_ZU_MIAO_ZU("湘U", "湖南湘西土家族苗族", "1333", 0),
    HU_NAN_ZHANG_SHA("湘A", "湖南长沙", "1301", 0),
    NEI_MENG_GU_E_ER_DUO_SI("蒙K", "内蒙古鄂尔多斯", "1808", 0),
    NEI_MENG_GU_HU_HE_HAO_TE("蒙A", "内蒙古呼和浩特", "1801", 0),
    NEI_MENG_GU_BAO_TOU("蒙B", "内蒙古包头", "1802", 0),
    NEI_MENG_GU_WU_HAI("蒙C", "内蒙古乌海", "1805", 0),
    NEI_MENG_GU_CHI_FENG("蒙D", "内蒙古赤峰", "1803", 0),
    NEI_MENG_GU_TONG_LIAO("蒙G", "内蒙古通辽", "1804", 0),
    NEI_MENG_GU_HU_LUN_BEI_ER_MENG("蒙E", "内蒙古呼伦贝尔盟", "1812", 0),
    NEI_MENG_GU_BA_YAN_NAO_ER_MENG("蒙L", "内蒙古巴彦淖尔盟", "1824", 0),
    NEI_MENG_GU_WU_LAN_CHA_BU_MENG("蒙J", "内蒙古乌兰察布盟", "1825", 0),
    NEI_MENG_GU_XING_AN_MENG("蒙F", "内蒙古兴安盟", "1814", 0),
    NEI_MENG_GU_XI_LIN_GUO_LE_MENG("蒙H", "内蒙古锡林郭勒盟", "1829", 0),
    NEI_MENG_GU_A_LA_SHAN_MENG("蒙M", "内蒙古阿拉善盟", "1830", 0),
    GUANG_DONG_SHAO_GUAN("粤F", "广东韶关", "511", 6),
    GUANG_DONG_SHEN_ZHEN("粤B", "广东深圳", "502", 6),
    GUANG_DONG_ZHU_HAI("粤C", "广东珠海", "503", 6),
    GUANG_DONG_SHAN_TOU("粤D", "广东汕头", "507", 6),
    GUANG_DONG_FO_SHAN("粤E", "广东佛山", "518", 6),
    GUANG_DONG_JIANG_MEN("粤J", "广东江门", "521", 6),
    GUANG_DONG_ZHAN_JIANG("粤G", "广东湛江", "513", 6),
    GUANG_DONG_MAO_MING("粤K", "广东茂名", "515", 6),
    GUANG_DONG_ZHAO_QING("粤H", "广东肇庆", "514", 6),
    GUANG_DONG_HUI_ZHOU("粤L", "广东惠州", "520", 6),
    GUANG_DONG_MEI_ZHOU("粤M", "广东梅州", "516", 6),
    GUANG_DONG_SHAN_WEI("粤N", "广东汕尾", "552", 6),
    GUANG_DONG_HE_YUAN("粤P", "广东河源", "535", 6),
    GUANG_DONG_YANG_JIANG("粤Q", "广东阳江", "532", 6),
    GUANG_DONG_QING_YUAN("粤R", "广东清远", "524", 6),
    GUANG_DONG_DONG_GUAN("粤S", "广东东莞", "504", 6),
    GUANG_DONG_ZHONG_SHAN("粤T", "广东中山", "505", 6),
    GUANG_DONG_CHAO_ZHOU("粤U", "广东潮州", "510", 6),
    GUANG_DONG_JIE_YANG("粤V", "广东揭阳", "522", 6),
    GUANG_DONG_YUN_FU("粤W", "广东云浮", "528", 6),
    GUANG_DONG_SHUN_DE_QU("粤X", "广东顺德区", "506", 6),
    GUANG_DONG_NAN_HAI_QU("粤Y", "广东南海区", "517", 6),
    GUANG_DONG_GUANG_ZHOU("粤A", "广东广州", "501", 6),
    GUANG_DONG_GANG_AO("粤Z", "广东港澳", "820007", 6),
    HAI_NAN_SAN_YA("琼B", "海南三亚", "803", 4),
    HAI_NAN_HAI_KOU("琼A", "海南海口", "801", 4),
    HAI_NAN_YANG_PU_KAI_FA_QU("琼E", "海南洋浦开发区", "809", 4),
    HAI_NAN_WU_ZHI_SHAN("琼D", "海南五指山", "804", 4),
    HAI_NAN_QIONG_HAI("琼C", "海南琼海", "802", 4),
    SHANG_HAI("沪", "上海", "2401", 0),
    ZHONG_QING("渝", "重庆", "3101", 6),
    AN_HUI_MA_AN_SHAN("皖E", "安徽马鞍山", "110", 0),
    AN_HUI_AN_QING("皖H", "安徽安庆", "102", 6),
    AN_HUI_XUAN_CHENG("皖P", "安徽宣城", "115", 6),
    AN_HUI_HUAI_BEI("皖F", "安徽淮北", "107", 0),
    AN_HUI_TONG_LING("皖G", "安徽铜陵", "113", 0),
    AN_HUI_HUANG_SHAN("皖J", "安徽黄山", "118", 0),
    AN_HUI_CHU_ZHOU("皖M", "安徽滁州", "116", 0),
    AN_HUI_FU_YANG("皖K", "安徽阜阳", "106", 0),
    AN_HUI_SU_ZHOU("皖L", "安徽宿州", "120", 0),
    AN_HUI_LIU_AN("皖N", "安徽六安", "109", 0),
    AN_HUI_BO_ZHOU("皖S", "安徽亳州", "125", 0),
    AN_HUI_CHI_ZHOU("皖R", "安徽池州", "105", 0),
    AN_HUI_CHAO_HU("皖Q", "安徽巢湖", "104", 0),
    AN_HUI_HE_FEI("皖A", "安徽合肥", "101", 0),
    AN_HUI_WU_HU("皖B", "安徽芜湖", "114", 0),
    AN_HUI_BANG_BU("皖C", "安徽蚌埠", "103", 0),
    YUN_NAN_KUN_MING("云A", "云南昆明", "2901", 6),
    YUN_NAN_QU_JING("云D", "云南曲靖", "2903", 6),
    YUN_NAN_YU_XI("云F", "云南玉溪", "2902", 6),
    YUN_NAN_BAO_SHAN("云M", "云南保山", "2907", 6),
    YUN_NAN_ZHAO_TONG("云C", "云南昭通", "2918", 6),
    YUN_NAN_LI_JIANG("云P", "云南丽江", "2922", 6),
    YUN_NAN_PU_ER("云J", "云南普洱", "2929", 6),
    YUN_NAN_LIN_CANG("云S", "云南临沧", "2911", 6),
    YUN_NAN_CHU_XIONG_YI_ZU("云E", "云南楚雄彝族", "532300", 6),
    YUN_NAN_HONG_HE_HA_NI_ZU_YI_ZU("云G", "云南红河哈尼族彝族", "2923", 6),
    YUN_NAN_WEN_SHAN_ZHUANG_ZU_MIAO_ZU("云H", "云南文山壮族苗族", "2914", 6),
    YUN_NAN_XI_SHUANG_BAN_NA_DAI_ZU("云K", "云南西双版纳傣族", "2915", 6),
    YUN_NAN_DA_LI_BAI_ZU("云L", "云南大理白族", "532900", 6),
    YUN_NAN_DE_HONG_DAI_ZU_JING_PO_ZU("云N", "云南德宏傣族景颇族", "2925", 6),
    YUN_NAN_NU_JIANG_LI_SU_ZU("云Q", "云南怒江傈僳族", "2927", 6),
    YUN_NAN_DI_QING_ZANG_ZU("云R", "云南迪庆藏族", "2928", 6),
    FU_JIAN_QUAN_ZHOU("闽C", "福建泉州", "307", 6),
    FU_JIAN_PU_TIAN("闽B", "福建莆田", "306", 6),
    FU_JIAN_SAN_MING("闽G", "福建三明", "318", 6),
    FU_JIAN_ZHANG_ZHOU("闽E", "福建漳州", "305", 6),
    FU_JIAN_NAN_PING("闽H", "福建南平", "314", 6),
    FU_JIAN_NING_DE("闽J", "福建宁德", "315", 6),
    FU_JIAN_LONG_YAN("闽F", "福建龙岩", "303", 6),
    FU_JIAN_SHA_MEN("闽D", "福建厦门", "302", 6),
    FU_JIAN_FU_ZHOU("闽A", "福建福州", "301", 6),
    GUI_ZHOU_GUI_YANG("贵A", "贵州贵阳", "701", 0),
    GUI_ZHOU_LIU_PAN_SHUI("贵B", "贵州六盘水", "708", 0),
    GUI_ZHOU_ZUN_YI("贵C", "贵州遵义", "702", 0),
    GUI_ZHOU_AN_SHUN("贵G", "贵州安顺", "705", 0),
    GUI_ZHOU_BI_JIE("贵F", "贵州毕节", "717", 0),
    GUI_ZHOU_TONG_REN("贵D", "贵州铜仁", "719", 0),
    GUI_ZHOU_QIAN_XI_NAN_BU_YI_ZU_MIAO_ZU_ZI_ZHI("贵E", "贵州黔西南布依族苗族自治", "718", 0),
    GUI_ZHOU_QIAN_DONG_NAN_MIAO_ZU_DONG_ZU("贵H", "贵州黔东南苗族侗族", "714", 0),
    GUI_ZHOU_QIAN_NAN_BU_YI_ZU_MIAO_ZU("贵J", "贵州黔南布依族苗族", "715", 0),
    XIN_JIANG_WU_LU_MU_QI("新A", "新疆乌鲁木齐", "2801", 0),
    XIN_JIANG_KE_LA_MA_YI("新J", "新疆克拉玛依", "2803", 0),
    XIN_JIANG_TU_LU_FAN_DI_QU("新K", "新疆吐鲁番地区", "2832", 0),
    XIN_JIANG_HA_MI_DI_QU("新L", "新疆哈密地区", "2833", 0),
    XIN_JIANG_CHANG_JI_HUI_ZU("新B", "新疆昌吉回族", "2827", 0),
    XIN_JIANG_BO_ER_TA_LA_MENG_GU("新E", "新疆博尔塔拉蒙古", "2821", 0),
    XIN_JIANG_BA_YIN_GUO_LENG_MENG_GU("新M", "新疆巴音郭楞蒙古", "2822", 0),
    XIN_JIANG_A_KE_SU_DI_QU("新N", "新疆阿克苏地区", "2825", 0),
    XIN_JIANG_KE_ZI_LE_SU_KE_ER_KE_ZI_ZI_ZHI("新P", "新疆克孜勒苏柯尔克孜自治", "2835", 0),
    XIN_JIANG_KA_SHEN_DI_QU("新Q", "新疆喀什地区", "2826", 0),
    XIN_JIANG_HE_TIAN_DI_QU("新R", "新疆和田地区", "2830", 0),
    XIN_JIANG_YI_LI_HA_SA_KE("新F", "新疆伊犁哈萨克", "2823", 0),
    XIN_JIANG_TA_CHENG_DI_QU("新G", "新疆塔城地区", "2831", 0),
    XIN_JIANG_KUI_TUN("新D", "新疆奎屯", "2805", 0),
    XIN_JIANG_SHI_HE_ZI("新C", "新疆石河子", "2810", 0),
    XIN_JIANG_A_LE_TAI_DI_QU("新H", "新疆阿勒泰地区", "2834", 0),
    XI_ZANG_LA_SA("藏A", "西藏拉萨", "2701", 0),
    XI_ZANG_CHANG_DOU_DI_QU("藏B", "西藏昌都地区", "2709", 0),
    XI_ZANG_SHAN_NAN_DI_QU("藏C", "西藏山南地区", "2704", 0),
    XI_ZANG_RI_KA_ZE_DI_QU("藏D", "西藏日喀则地区", "2703", 0),
    XI_ZANG_A_LI_DI_QU("藏F", "西藏阿里地区", "2707", 0),
    XI_ZANG_LIN_ZHI_DI_QU("藏G", "西藏林芝地区", "2710", 0),
    XI_ZANG_NEI_QU_DI_QU("藏E", "西藏那曲地区", "2705", 0),
    BEI_JING("京", "北京", "201", 0),
    QING_HAI_YU_SHU_ZANG_ZU("青G", "青海玉树藏族", "2026", 99),
    QING_HAI_HAI_XI_MENG_GU_ZU_ZANG_ZU("青H", "青海海西蒙古族藏族", "2027", 99),
    QING_HAI_XI_NING("青A", "青海西宁", "2001", 99),
    QING_HAI_HAI_DONG_DI_QU("青B", "青海海东地区", "2029", 99),
    QING_HAI_HAI_BEI_ZANG_ZU("青C", "青海海北藏族", "2023", 99),
    QING_HAI_HAI_NAN_ZANG_ZU("青E", "青海海南藏族", "2030", 99),
    QING_HAI_GUO_LUO_ZANG_ZU("青F", "青海果洛藏族", "2025", 99),
    QING_HAI_HUANG_NAN_ZANG_ZU("青D", "青海黄南藏族", "2024", 99),
    JIANG_XI_NAN_CHANG("赣A", "江西南昌", "1601", 0),
    JIANG_XI_JING_DE_ZHEN("赣H", "江西景德镇", "1612", 0),
    JIANG_XI_PING_XIANG("赣J", "江西萍乡", "1603", 0),
    JIANG_XI_JIU_JIANG("赣G", "江西九江", "1606", 0),
    JIANG_XI_XIN_YU("赣K", "江西新余", "1604", 0),
    JIANG_XI_YING_TAN("赣L", "江西鹰潭", "1615", 0),
    JIANG_XI_GAN_ZHOU("赣B", "江西赣州", "1607", 0),
    JIANG_XI_JI_AN("赣D", "江西吉安", "1609", 0),
    JIANG_XI_YI_CHUN("赣C", "江西宜春", "1605", 0),
    JIANG_XI_FU_ZHOU("赣F", "江西抚州", "1613", 0),
    JIANG_XI_SHANG_RAO("赣E", "江西上饶", "1602", 0),
    JIANG_XI_NAN_CHANG_SHI_SHENG_ZHI_XI_TONG("赣M", "江西南昌市,省直系统", "1601", 0),
    HE_BEI_SHI_JIA_ZHUANG("冀A", "河北石家庄", "901", 4),
    HE_BEI_TANG_SHAN("冀B", "河北唐山", "902", 4),
    HE_BEI_QIN_HUANG_DAO("冀C", "河北秦皇岛", "905", 4),
    HE_BEI_HAN_DAN("冀D", "河北邯郸", "907", 4),
    HE_BEI_XING_TAI("冀E", "河北邢台", "903", 4),
    HE_BEI_BAO_DING("冀F", "河北保定", "910", 4),
    HE_BEI_ZHANG_JIA_KOU("冀G", "河北张家口", "911", 4),
    HE_BEI_CHENG_DE("冀H", "河北承德", "912", 4),
    HE_BEI_CANG_ZHOU("冀J", "河北沧州", "909", 4),
    HE_BEI_LANG_FANG("冀R", "河北廊坊", "906", 4),
    HE_BEI_HENG_SHUI("冀T", "河北衡水", "908", 4),
    HE_BEI_CANG_ZHOU_XING_SHU("冀S", "河北沧州行署", "909", 4),
    SI_CHUAN_CHENG_DOU("川A", "四川成都", "2501", 6),
    SI_CHUAN_ZI_GONG("川C", "四川自贡", "2508", 6),
    SI_CHUAN_PAN_ZHI_HUA("川D", "四川攀枝花", "2504", 6),
    SI_CHUAN_LU_ZHOU("川E", "四川泸州", "2517", 6),
    SI_CHUAN_DE_YANG("川F", "四川德阳", "2511", 6),
    SI_CHUAN_MIAN_YANG("川B", "四川绵阳", "2502", 6),
    SI_CHUAN_GUANG_YUAN("川H", "四川广元", "2510", 6),
    SI_CHUAN_SUI_NING("川J", "四川遂宁", "2503", 6),
    SI_CHUAN_NEI_JIANG("川K", "四川内江", "2519", 6),
    SI_CHUAN_LE_SHAN("川L", "四川乐山", "2512", 6),
    SI_CHUAN_NAN_CHONG("川R", "四川南充", "2513", 6),
    SI_CHUAN_MEI_SHAN("川Z", "四川眉山", "2514", 6),
    SI_CHUAN_YI_BIN("川Q", "四川宜宾", "2506", 6),
    SI_CHUAN_GUANG_AN("川X", "四川广安", "2530", 6),
    SI_CHUAN_DA_ZHOU("川S", "四川达州", "2532", 6),
    SI_CHUAN_YA_AN("川T", "四川雅安", "2507", 6),
    SI_CHUAN_BA_ZHONG("川Y", "四川巴中", "2516", 6),
    SI_CHUAN_ZI_YANG("川M", "四川资阳", "2509", 6),
    SI_CHUAN_A_BA("川U", "四川阿坝", "2535", 6),
    SI_CHUAN_GAN_ZI("川V", "四川甘孜", "2536", 6),
    SI_CHUAN_LIANG_SHAN_YI_ZU("川W", "四川凉山彝族", "2537", 6),
    GUANG_XI_NAN_NING("桂A", "广西南宁", "601", 6),
    GUANG_XI_LIU_ZHOU("桂B", "广西柳州", "602", 6),
    GUANG_XI_GUI_LIN("桂C", "广西桂林", "603", 6),
    GUANG_XI_WU_ZHOU("桂D", "广西梧州", "613", 6),
    GUANG_XI_BEI_HAI("桂E", "广西北海", "604", 6),
    GUANG_XI_FANG_CHENG_GANG("桂E", "广西防城港", "615", 6),
    GUANG_XI_NAN_NING2("桂F", "广西南宁", "601", 6),
    GUANG_XI_LIU_ZHOU2("桂G", "广西柳州", "602", 6),
    GUANG_XI_GUI_LIN2("桂H", "广西桂林", "603", 6),
    GAN_SU_LAN_ZHOU("甘A", "甘肃兰州", "401", 99),
    GAN_SU_JIA_YU_GUAN("甘B", "甘肃嘉峪关", "416", 99),
    GAN_SU_JIN_CHANG("甘C", "甘肃金昌", "417", 99),
    GAN_SU_BAI_YIN("甘D", "甘肃白银", "418", 99),
    GAN_SU_TIAN_SHUI("甘E", "甘肃天水", "415", 99),
    GAN_SU_WU_WEI("甘H", "甘肃武威", "412", 99),
    GAN_SU_ZHANG_YE("甘G", "甘肃张掖", "410", 99),
    GAN_SU_PING_LIANG("甘L", "甘肃平凉", "405", 99),
    GAN_SU_JIU_QUAN("甘F", "甘肃酒泉", "409", 99),
    GAN_SU_QING_YANG("甘M", "甘肃庆阳", "411", 99),
    GAN_SU_DING_XI("甘J", "甘肃定西", "402", 99),
    GAN_SU_LONG_NAN("甘K", "甘肃陇南", "421", 99),
    GAN_SU_LIN_XIA_HUI_ZU("甘N", "甘肃临夏回族", "622900", 99),
    GAN_SU_GAN_NAN_ZANG_ZU("甘P", "甘肃甘南藏族", "422", 99),
    NING_XIA_YIN_CHUAN("宁A", "宁夏银川", "1901", 0),
    NING_XIA_SHI_ZUI_SHAN("宁B", "宁夏石嘴山", "1905", 0),
    NING_XIA_WU_ZHONG("宁C", "宁夏吴忠", "1902", 0),
    NING_XIA_GU_YUAN("宁D", "宁夏固原", "1903", 0),
    NING_XIA_ZHONG_WEI("宁E", "宁夏中卫", "1907", 0),
    TIAN_JIN("津", "天津", "2601", 6),
    SHAN_XI_XI_AN("陕A", "陕西西安", "2301", 0),
    SHAN_XI_TONG_CHUAN("陕B", "陕西铜川", "2310", 0),
    SHAN_XI_BAO_JI("陕C", "陕西宝鸡", "2305", 0),
    SHAN_XI_XIAN_YANG("陕D", "陕西咸阳", "2302", 0),
    SHAN_XI_WEI_NAN("陕E", "陕西渭南", "2303", 0),
    SHAN_XI_YAN_AN("陕J", "陕西延安", "2308", 0),
    SHAN_XI_HAN_ZHONG("陕F", "陕西汉中", "2307", 0),
    SHAN_XI_YU_LIN("陕K", "陕西榆林", "2304", 0),
    SHAN_XI_AN_KANG("陕G", "陕西安康", "2306", 0),;

    public static void main(String[] args) throws Exception {
        String url = "http://test.cx580.com:9000/InputsCondition.aspx";
        HttpGet httpGet = new HttpGet(url);
        HttpResponse response = Util.getDefaultHttpClient().execute(httpGet);
        String result = EntityUtils.toString(response.getEntity());
        JSONArray jsonArray = JSONArray.parseArray(result);
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            for (Entry<String, Object> entry : jsonObject.entrySet()) {
                if (!StringUtils.equals("Cities", entry.getKey())) {
                    continue;
                }
                JSONArray cities = JSONArray.parseArray(entry.getValue().toString());
                for (int j = 0; j < cities.size(); j++) {
                    JSONObject detail = cities.getJSONObject(j);
                    String cityName = detail.getString("CityName").replace("自治州", "");
                    String key = "";
                    for (int k = 0; k < cityName.length(); k++) {
                        String name = String.valueOf(cityName.charAt(k));
                        key += PinyinUtil.getPinyin(name).toUpperCase();
                        if ((k + 1) != cityName.length()) {
                            key += "_";
                        }
                    }
                    key = key.replace("__", "_");
                    try {
                        String moniNo = detail.getString("CarNumberPrefix");
                        if (moniNo.length() == 1) {
                            moniNo = moniNo + "A";
                        }
//						String cityId=JingZhenGuClinet2.queryCityId(moniNo+"88888");
                        String cityId = "";
                        System.out.println(key + "(\"" + detail.getString("CarNumberPrefix") + "\",\"" + cityName + "\",\"" + cityId + "\"," + detail.getString("CarCodeLen") + "),");
                    } catch (Exception e) {
                        System.out.println(key + "(\"" + detail.getString("CarNumberPrefix") + "\",\"" + cityName + "\",\"error\"," + detail.getString("CarCodeLen") + "),");
                    }
                }
//				System.out.println(entry.getKey()+":"+entry.getValue());
            }
        }
    }

    public static CarPlateCityEnum getCarPlateCityEnum(String plateNum) {
        CarPlateCityEnum[] enums = CarPlateCityEnum.values();
        for (CarPlateCityEnum tempEnum : enums) {
            if (StringUtils.contains(plateNum, tempEnum.getPlatePrefix())) {
                return tempEnum;
            }
        }
        return null;
    }

    public static CarPlateCityEnum getCarPlateCityEnumByCityId(String cityid) {
        CarPlateCityEnum[] enums = CarPlateCityEnum.values();
        for (CarPlateCityEnum tempEnum : enums) {
            if (StringUtils.contains(cityid, tempEnum.getCityId())) {
                return tempEnum;
            }
        }
        return null;
    }

    CarPlateCityEnum(String platePrefix, String cityName, String cityId, Integer codeLen) {
        this.platePrefix = platePrefix;
        this.cityName = cityName;
        this.cityId = cityId;
        this.codeLen = codeLen;
    }

    /**
     * 车牌前缀
     */
    private String platePrefix;

    /**
     * 违章查询位数 最后一位开始计算
     */
    private Integer codeLen;
    /**
     * 城市id
     */
    private String cityId;
    /**
     * 城市名称
     */
    private String cityName;

    public String getPlatePrefix() {
        return platePrefix;
    }

    public void setPlatePrefix(String platePrefix) {
        this.platePrefix = platePrefix;
    }

    public Integer getCodeLen() {
        return codeLen;
    }

    public void setCodeLen(Integer codeLen) {
        this.codeLen = codeLen;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
