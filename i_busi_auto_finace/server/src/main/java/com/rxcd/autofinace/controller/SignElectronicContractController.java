package com.rxcd.autofinace.controller;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.appframe.contract.JunZiQianClient;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.engine.common.WfConst;
import org.appframe.wf.service.WfParamUtil;
import org.appframe.wf.util.Const;
import org.appframe.wf.util.Param;
import org.appframe.wf.util.SafeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 进入电子签订室/发送签约链家
 */
@RestController
@RequestMapping("/")
public class SignElectronicContractController {


    @Autowired
    private BaseDao baseDao;
    private static final Log logger = LogFactory.getLog(SignElectronicContractController.class);
    @Autowired
    private WfParamUtil paramUtil;
    @Autowired
    private JunZiQianClient JunZiQianClient;

    @RequestMapping("/loan_common_SignElectronicContract")
    public Row execute(HttpServletRequest request, HttpServletResponse response,@RequestParam String review_no, @RequestParam String instance_no, @RequestParam String fullName,
                       @RequestParam String idCard, @RequestParam String isSendUrl, @RequestParam String mobile, @RequestParam String isGetUrl, @RequestParam String apply_no) throws Exception {
        Row ret = new Row();
        Param p = new Param();
        paramUtil.add("param",p);
        boolean isSign = false;
        String sql = "select * "
                + "  from wf_review a"
                + "  where a.status='1'"
                + "  and a.review_no='" + SafeUtil.safe(review_no) + "'";
        Row reviewRow = baseDao.queryRow(sql);
        if (reviewRow == null) {
            throw new Exception("没有找到此流程");
        }
        String table_key_no = (String) baseDao.queryObject("select table_key_no from wf_review_biz where review_no='" + SafeUtil.safe(review_no) + "'");
        if (StringUtils.isBlank(table_key_no)) {
            throw new Exception("没有找到业务流程");
        }
        String signUrl = "";
        try {
            String backUrl = Const.FRONT_END_SYS_URL + "/sign_finish";
            //获取签约URL
            signUrl = JunZiQianClient.queryManualSignUrl(apply_no, fullName, idCard) + "&backUrl=" + backUrl;


            isSign = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        Row retRow = WfConst.success();
        retRow.put("isSign", isSign);
        retRow.put("signUrl", signUrl);
        ret.set("json", retRow);
        return ret;
    }


}
