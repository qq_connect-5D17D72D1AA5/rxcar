/**
 * 创建者：ZhaoSheng 创建时期： 2013-07-15
 */
package com.rxcd.autofinace.controller;


import com.fida.clearing.BillClient;
import com.fida.clearing.RuleClient;
import com.rxcd.autofinace.util.RxErpUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.appframe.common.BigDecimalUtil;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 代付成功回调通知
 */
@RestController
@RequestMapping("/")
public class PayCallBackController {

    private static final Log log = LogFactory.getLog(PayCallBackController.class);

    @Autowired
    private RuleClient ruleClient;

    @Autowired
    private BillClient billClient;

    @Autowired
    private BaseDao baseDao;

    @Autowired
    private RxErpUtil erpUtil;

    @RequestMapping("/loan_comm_paycallback")
    public String costShow(@RequestParam(value = "busiId") String busiId,@RequestParam(required=false)String resultCode) throws Exception {
        try{
            log.info(busiId+":支付回调进入");
            if(StringUtils.equals("1",resultCode)){//支付成功
                String loan_no = erpUtil.objectIsNullGetString(baseDao.queryObject("select loan_no from loan_mid where mid_no = '"+busiId+"'"));
                Row empRow = baseDao.queryRow("select * from loan_mid_employers where mid_no='"+busiId+"'");
                Row custRow = baseDao.queryRow("select clearing_frozen,clearing_ordinary from loan_cust where loan_no = '"+loan_no+"'");
                String clearing_ordinary = custRow.getStringNotNull("clearing_ordinary");
                if(StringUtils.equals("02",empRow.getStringNotNull("pay_type"))){//一次放款
                    List<String> batchs = new ArrayList<>();
                    batchs.add("0");
                    String clearing_frozen = custRow.getStringNotNull("clearing_frozen");
                    Map<String,Object> ruleResult = ruleClient.queryRuleResult(loan_no, "REPAY",clearing_frozen,null,batchs,null);  //查询0期费用
                    if(StringUtils.equals("1",String.valueOf(ruleResult.get("resultCode")))){
                        double zero_amount = 0;
                        for(Map<String,Object> ruleMap : (List<Map<String, Object>>)ruleResult.get("rows")){
                            List<BigDecimal> amountList = (List<BigDecimal>)ruleMap.get("amountList");
                            for(BigDecimal amount : amountList){
                                zero_amount = BigDecimalUtil.add(zero_amount,amount.doubleValue());
                            }
                        }
                        zero_amount = BigDecimalUtil.round(zero_amount, 2);
                        if(zero_amount <= 0){
                            log.info("零期应还金额为零");
                            return "success";
                        }
                        log.info("零期应还金额为:"+zero_amount);
                        log.info("clearing_ordinary:"+clearing_ordinary);
                        //创建在线充值账单
                        Map<String,Object> onlineRechargeBillResult = billClient.createOnlineRechargeBill(
                                zero_amount,null,
                                clearing_ordinary, null,null,
                                "4bc199af-044f-497f-a6ea-c7657f2d0c08",null, null, loan_no);
                        log.info("onlineRechargeBillResult:"+onlineRechargeBillResult);
                        if(StringUtils.equals("1",String.valueOf(onlineRechargeBillResult.get("resultCode")))
                                && onlineRechargeBillResult.get("rows") != null
                                && !((List)onlineRechargeBillResult.get("rows")).isEmpty()){
                            log.info("结算充值账单进入");
                            Map<String, Object> bill = ((List<Map<String, Object>>)onlineRechargeBillResult.get("rows")).get(0);
                            Map<String, Object> settleBillResult = billClient.settleBill((String)bill.get("billId"));
                            //结算不成功时, resultCode == "1"表示结算成功，errCode == "800022"表示支付发起，等待支付结果返回
                            log.info("settleBillResult:"+settleBillResult);
                            if(settleBillResult == null || !StringUtils.equals("1", String.valueOf(settleBillResult.get("resultCode")))){
                                log.info("在线充值账单结算失败");
                                return "success";
                            }
                            log.info("结算充值账单完成");
                            //客户普通账户转账到冻结账户
                            Map<String,Object> transferBalanceBillResult = billClient.createTransferBalanceBill(
                                    zero_amount,null,
                                    clearing_ordinary, null,null,
                                    clearing_frozen,null, null, loan_no);
                            if(StringUtils.equals("1",String.valueOf(transferBalanceBillResult.get("resultCode")))
                                    && transferBalanceBillResult.get("rows") != null
                                    && !((List)transferBalanceBillResult.get("rows")).isEmpty()) {
                                log.info("结算普通账户转账到冻结账户账单进入");
                                Map<String, Object> tbBill = ((List<Map<String, Object>>) transferBalanceBillResult.get("rows")).get(0);
                                Map<String, Object> settleTbBillResult = billClient.settleBill((String) tbBill.get("billId"));
                                log.info("settleBillResult:" + settleTbBillResult);
                                if (settleTbBillResult == null || !StringUtils.equals("1", String.valueOf(settleTbBillResult.get("resultCode")))) {
                                    log.info("结算普通账户转账到冻结账户账单结算失败");
                                    return "success";
                                }
                                log.info("结算普通账户转账到冻结账户账单结束");
                            }else{
                                log.info("创建余额转账账单失败, resultCode == 1表示结算成功，errCode == 800022表示支付发起，等待支付结果返回");
                                return "success";
                            }
                            //把0期的费用清算成余额转账账单
                            Map<String, Object> repayBillMap = billClient.clearTransferBalancePayable(loan_no,"REPAY",clearing_frozen,null,batchs,null,null,null);
                            log.info("repayBillMap:"+repayBillMap);
                            if(!StringUtils.equals("1", String.valueOf(repayBillMap.get("resultCode")))){
                                log.info("创建0期的费用清算成余额转账账单失败");
                                return "success";
                            }
                            List<Map<String, Object>> repayBills = (List<Map<String, Object>>)repayBillMap.get("rows");
                            log.info("repayBills:"+repayBills);
                            for(Map<String, Object > repayBill : repayBills){
                                settleBillResult = billClient.settleBill((String)repayBill.get("billId"));
                                if(!StringUtils.equals("1", String.valueOf(settleBillResult.get("resultCode")))){
                                    log.info("0期的费用清算成余额转账账单结算失败");
                                    return "success";
                                }
                            }
                        }
                    }
                }else if(StringUtils.equals("03",empRow.getStringNotNull("pay_type"))){//二次放款
                    double contribution_amount = empRow.getDoubleNotNull("contribution_amount");
                    Row infoEtRow = baseDao.queryRow("select intentional_capital_no from loan_info_et where loan_no = '"+loan_no+"'");
                    String intentional_capital_no = infoEtRow.getStringNotNull("intentional_capital_no");
                    //如果资方是洋葱先生就要手动扣回来二次放款的金额
                    if(contribution_amount>0 && StringUtils.equals("02",intentional_capital_no)) {
                        Map<String, Object> onlineRechargeBillResult = billClient.createOnlineRechargeBill(
                                contribution_amount, null,
                                clearing_ordinary, null, null,
                                "4bc199af-044f-497f-a6ea-c7657f2d0c08", null, null, loan_no);
                        log.info("onlineRechargeBillResult:" + onlineRechargeBillResult);
                        if (StringUtils.equals("1", String.valueOf(onlineRechargeBillResult.get("resultCode")))
                                && onlineRechargeBillResult.get("rows") != null
                                && !((List) onlineRechargeBillResult.get("rows")).isEmpty()) {
                            log.info("结算二次放款充值账单进入");
                            Map<String, Object> bill = ((List<Map<String, Object>>) onlineRechargeBillResult.get("rows")).get(0);
                            Map<String, Object> settleBillResult = billClient.settleBill((String) bill.get("billId"));
                            log.info("settleBillResult:" + settleBillResult);
                            if (settleBillResult == null || !StringUtils.equals("1", String.valueOf(settleBillResult.get("resultCode")))) {
                                log.info("二次放款在线充值账单结算失败");
                                return "success";
                            }
                            log.info("二次放款结算充值账单完成");
                        }
                    }
                    log.info("二次放款回调成功");
                }
            }else{
                log.info("支付失败");
            }
        }catch(Exception e){
            log.info("回调异常出错");
            e.printStackTrace();
        }
        log.info(busiId+":支付回调结束");
        return "success";
    }
}