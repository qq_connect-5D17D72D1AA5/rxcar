/**
 * 创建者：HeSenLin   创建时期：  2016年6月3日
 */
package com.rxcd.autofinace.service;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rxcd.autofinace.util.PropertiesUtil;
import com.rxcd.autofinace.util.RxErpUtil;
import org.apache.commons.lang.StringUtils;
import org.appframe.bigdata.*;
import org.appframe.common.BigDecimalUtil;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.db.Store;
import org.appframe.wf.util.ErpUtil;
import org.appframe.wf.util.Formula;
import org.appframe.wf.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * 获取反欺诈得分
 *
 * @author liuchang
 */
@Component
public class LoanFraudAutoService {

    private static final String RUNTIME = PropertiesUtil.getValue("runtime");
    @Autowired
    private RxErpUtil erpUtil;
    @Autowired
    private BaseDao baseDao;
    @Autowired
    private JiAoClinet2 jiAoClinet;
    @Autowired
    private TianXingClinet2 tianXingClinet;
    @Autowired
    private TongDunClinet2 tongDunClinet;
    @Autowired
    private AcedataClinet2 acedataClinet;
    @Autowired
    private ShuJuMoHeClinet2 shuJuMoHeClinet;

    /**
     * 获取初审反欺诈得分
     *
     * @param loan_no
     * @return
     */
    public void getFirstFraudcontrol(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        Row param = getFirstFraudDimension(loanRow);
        autoFraudcontrol(loanRow, param);
    }

    /**
     * 获取中审反欺诈得分
     *
     * @param loan_no
     * @return
     */
    public void getMidFraudcontrol(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        Row param = getMidFraudDimension(loanRow);
        autoFraudcontrol(loanRow, param);
    }

    /**
     * 获取高审反欺诈得分
     *
     * @param loan_no
     * @return
     */
    public void getFinalFraudcontrol(String loan_no) throws Exception {
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        Row param = getFinalFraudDimension(loanRow);
        autoFraudcontrol(loanRow, param);
    }

    /**
     * 获取反欺诈得分
     *
     * @throws Exception
     */
    private void autoFraudcontrol(Row loanRow, Row param) throws Exception {
        String loan_id = ErpUtil.objectIsNullGetString(loanRow.get("loan_info.loan_id"));
        String loan_no = ErpUtil.objectIsNullGetString(loanRow.get("loan_info.loan_no"));
        String fraud_type = param.getString("fraud_type");
        baseDao.execute("delete from loan_fraud_auto where loan_no='" + loan_no + "' and fraud_type='" + fraud_type + "'");
        Store rules = baseDao.query("select * from loan_fraud_rule where status='1' order by sort_no");

        //计算单项得分
        String fraud_dimension, condition_exp, condition_remark;
        Formula conFormula = null;
        int result = 0;
        Map<String, Object> p = new HashMap<String, Object>();
        List<String> sqlList = new ArrayList<String>();
        for (Row rule : rules) {

            fraud_dimension = ErpUtil.objectIsNullGetString(rule.get("fraud_dimension"));
            condition_exp = ErpUtil.objectIsNullGetString(rule.get("condition_exp"));
            condition_remark = ErpUtil.objectIsNullGetString(rule.get("condition_remark"));
            result = ErpUtil.objectIsNullGetInteger(rule.get("result"));
            // 条件公式
            conFormula = new Formula(condition_exp);
            p.clear();
            p.put(fraud_dimension, param.get(fraud_dimension));
            conFormula.setVariable(fraud_dimension, param.get(fraud_dimension));

            // 条件是否成立
            if (conFormula.bool()) {
                sqlList.add("insert into loan_fraud_auto "
                        + "(auto_id,auto_no,loan_id,loan_no,fraud_type,fraud_dimension,condition_exp,condition_remark,fraud_param,fraud_score,create_time,oper_time,status) "
                        + "values "
                        + "(uuid(),"
                        + " uuid(),"
                        + " '" + loan_id + "',"
                        + " '" + loan_no + "',"
                        + " '" + fraud_type + "',"
                        + " '" + fraud_dimension + "',"
                        + " '" + condition_exp + "',"
                        + " '" + condition_remark + "',"
                        + "'" + JSONObject.toJSONString(p) + "',"
                        + " " + result + ","
                        + " '" + Util.format(new Date()) + "',"
                        + " '" + Util.format(new Date(), "YYYY-MM-dd HH:mm:ss") + "',"
                        + " '1')");
            }
        }
        baseDao.execute(sqlList);
        //汇总所有得分并更新
        if (StringUtils.equals("01", fraud_type)) {
            int score = ErpUtil.objectIsNullGetInteger(
                    baseDao.queryObject("select sum(fraud_score) from loan_fraud_auto where loan_no='" + loan_no + "' and fraud_type = '01'"));
            baseDao.execute("update loan_ass set fraud_first_score=" + score + " where loan_no='" + loan_no + "'");
        } else if (StringUtils.equals("02", fraud_type)) {
            int score = ErpUtil.objectIsNullGetInteger(
                    baseDao.queryObject("select sum(fraud_score) from loan_fraud_auto where loan_no='" + loan_no + "' and fraud_type != '03'"));
            baseDao.execute("update loan_ass set fraud_mid_score=" + score + " where loan_no='" + loan_no + "'");
        } else {
            int score = ErpUtil.objectIsNullGetInteger(
                    baseDao.queryObject("select sum(fraud_score) from loan_fraud_auto where loan_no='" + loan_no + "'"));
            baseDao.execute("update loan_ass set fraud_final_score=" + score + " where loan_no='" + loan_no + "'");
        }
    }

    /**
     * 获取初审反欺诈数据
     *
     * @param loanRow
     * @throws Exception
     */
    private Row getFirstFraudDimension(Row loanRow) throws Exception {
        Row param = new Row();
        param.put("fraud_type", "01");
        initPersonalInvolvement(loanRow, param);
        initUnhealthy(loanRow, param);
        initMobileWhenLong(loanRow, param);
        initMobileRealname(loanRow, param);
        initTongDun(loanRow, param);
        initOverdue(loanRow, param);
        initDriving(loanRow, param);
        initMarriage(loanRow, param);

        return param;
    }

    /**
     * 获取中审反欺诈数据
     *
     * @param loanRow
     * @throws Exception
     */
    private Row getMidFraudDimension(Row loanRow) throws Exception {
        Row param = new Row();
        param.put("fraud_type", "02");
        initHomeAddressRange(loanRow, param);
        initWorkAddressRange(loanRow, param);
        initBankCardOpen(loanRow, param);
        initRepayLoan(loanRow, param);
        initYys(loanRow, param);
        initCarOwner(loanRow, param);
        initSupplement(loanRow, param);
        initDetention(loanRow, param);

        return param;
    }

    /**
     * 获取高审反欺诈数据
     *
     * @param loanRow
     * @throws Exception
     */
    private Row getFinalFraudDimension(Row loanRow) throws Exception {
        Row param = new Row();
        param.put("fraud_type", "03");
        initSensitivesectors(loanRow, param);
        return param;
    }

    /**
     * 个人涉诉，失信
     *
     * @param loanRow
     * @throws Exception
     */
    private void initPersonalInvolvement(Row loanRow, Row param) throws Exception {
        if (StringUtils.equals("1", RUNTIME)) {
            try {
                String cust_name = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_name"));
                String idcard = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.idcard"));
                JSONObject jsonObject = tianXingClinet.queryHighCourtRersonal(cust_name, idcard, false);
                String success = jsonObject.getString("success");
                if (StringUtils.equals("true", success)) {
                    JSONObject data = jsonObject.getJSONObject("data");
                    String cpwsStatus = "";
                    String sxggStatus = "";
                    String zxggStatus = "";
                    JSONObject cpws = data.getJSONObject("cpws");
                    if (cpws != null) {
                        cpwsStatus = cpws.getString("checkStatus");
                    }
                    JSONObject sxgg = data.getJSONObject("sxgg");
                    if (sxgg != null) {
                        sxggStatus = sxgg.getString("checkStatus");
                    }
                    JSONObject zxgg = data.getJSONObject("zxgg");
                    if (zxgg != null) {
                        zxggStatus = zxgg.getString("checkStatus");
                    }
                    if (StringUtils.equals(cpwsStatus, "EXIST")
                            || StringUtils.equals(sxggStatus, "EXIST")
                            || StringUtils.equals(zxggStatus, "EXIST")) {
                        param.put("loan_fraud.personal", "01");//个人有涉诉
                    } else {
                        param.put("loan_fraud.personal", "02");//个人无涉诉
                    }
                    if (StringUtils.equals(sxggStatus, "EXIST")) {
                        param.put("loan_fraud.discredit", "01");//个人有失信
                    } else {
                        param.put("loan_fraud.discredit", "02");//个人无失信
                    }
                } else {
                    param.put("loan_fraud.personal", "02");//个人无涉诉
                    param.put("loan_fraud.discredit", "02");//个人无失信
                }
            } catch (Exception e) {
                e.printStackTrace();
                param.put("loan_fraud.personal", "02");//个人无涉诉
                param.put("loan_fraud.discredit", "02");//个人无失信
            }
        } else {
            param.put("loan_fraud.personal", "02");//个人无涉诉
            param.put("loan_fraud.discredit", "02");//个人无失信
        }
    }

    /**
     * 不良信息
     *
     * @param loanRow
     * @throws Exception
     */
    private void initUnhealthy(Row loanRow, Row param) throws Exception {
        if (StringUtils.equals("1", RUNTIME)) {
            try {
                String cust_name = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_name"));
                String idcard = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.idcard"));
                JSONObject jsonObject = tianXingClinet.queryNegative(cust_name, idcard, false);
                String success = jsonObject.getString("success");
                if (StringUtils.equals("true", success)) {
                    JSONObject data = jsonObject.getJSONObject("data");
                    String status = data.getString("status");
                    if (StringUtils.equals("EXIST", status)) {
                        String crimeCompared = data.getString("crimeCompared");//前科
                        if (StringUtils.equals("一致", crimeCompared)) {
                            param.put("loan_fraud.crime", "01");//有前科
                        } else {
                            param.put("loan_fraud.crime", "02");//无
                        }
                        String drugCompared = data.getString("drugCompared");//吸毒
                        if (StringUtils.equals("一致", drugCompared)) {
                            param.put("loan_fraud.drug", "01");//有吸毒
                        } else {
                            param.put("loan_fraud.drug", "02");//无
                        }
                        String drugRelatedCompared = data.getString("drugRelatedCompared");//涉毒
                        if (StringUtils.equals("一致", drugRelatedCompared)) {
                            param.put("loan_fraud.drugRelated", "01");//有涉毒
                        } else {
                            param.put("loan_fraud.drugRelated", "02");//无
                        }
                        return;
                    }
                }
                param.put("loan_fraud.crime", "02");//无
                param.put("loan_fraud.drug", "02");//无
                param.put("loan_fraud.drugRelated", "02");//无
            } catch (Exception e) {
                e.printStackTrace();
                param.put("loan_fraud.crime", "02");//无
                param.put("loan_fraud.drug", "02");//无
                param.put("loan_fraud.drugRelated", "02");//无
            }
        } else {
            param.put("loan_fraud.crime", "02");//无
            param.put("loan_fraud.drug", "02");//无
            param.put("loan_fraud.drugRelated", "02");//无
        }
    }

    /**
     * 获取手机在网时长
     *
     * @param loanRow
     * @throws Exception
     */
    private void initMobileWhenLong(Row loanRow, Row param) throws Exception {
        if (StringUtils.equals("1", RUNTIME)) {
            try {
                String cust_name = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_name"));
                String cust_phone = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_phone"));
                String idcard = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.idcard"));
                String resultString = jiAoClinet.queryMobileWhenLong(cust_name, idcard, cust_phone);
                String code = "";
                if (StringUtils.equals("(0,3]", resultString)) {
                    code = "01";
                } else if (StringUtils.equals("(3,6]", resultString)) {
                    code = "02";
                } else if (StringUtils.equals("(6,12]", resultString)) {
                    code = "03";
                } else if (StringUtils.equals("(12,24]", resultString)) {
                    code = "04";
                } else if (StringUtils.equals("(24,+)", resultString)) {
                    code = "05";
                } else {
                    code = "00";
                }
                param.put("loan_fraud.mobileWhenLong", code);
            } catch (Exception e) {
                e.getStackTrace();
                param.put("loan_fraud.mobileWhenLong", "00");
            }
        } else {
            param.put("loan_fraud.mobileWhenLong", "00");
        }
    }

    /**
     * 获取手机实名认证
     *
     * @param loanRow
     * @throws Exception
     */
    private void initMobileRealname(Row loanRow, Row param) throws Exception {
        if (StringUtils.equals("1", RUNTIME)) {
            try {
                String cust_name = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_name"));
                String cust_phone = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_phone"));
                JSONObject jsonObject = tianXingClinet.queryMobile2Element(cust_name, cust_phone, false);
                String compareStatus = jsonObject.getJSONObject("data").getString("compareStatus");
                if (StringUtils.equals("SAME", compareStatus)) {
                    param.put("loan_fraud.realname", "01");
                } else {
                    param.put("loan_fraud.realname", "02");
                }
            } catch (Exception e) {
                e.getStackTrace();
                param.put("loan_fraud.realname", "02");
            }
        } else {
            param.put("loan_fraud.realname", "02");
        }
    }

    /**
     * 通过同盾接口查询多头和分数
     *
     * @param loanRow
     * @throws Exception
     */
    private void initTongDun(Row loanRow, Row param) throws Exception {
        if (StringUtils.equals("1", RUNTIME)) {
            String cust_name = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_name"));
            String cust_phone = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_phone"));
            String idcard = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.idcard"));
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("name", cust_name);// 姓名 必填
            paramMap.put("id_number", idcard);// 身份证 必填
            paramMap.put("mobile", cust_phone);// 手机号
            try {
                String reportId = tongDunClinet.queryReportNo(paramMap, false);
                Thread.sleep(2000);
                JSONObject jsonObject = tongDunClinet.queryReport(reportId, false);
                if (jsonObject.getBooleanValue("success")) {
                    int credit_score = ErpUtil.objectIsNullGetInteger(jsonObject.get("final_score"));
                    param.put("loan_fraud.tongDunScore", credit_score);
                    JSONArray risk_items = jsonObject.getJSONArray("risk_items");
                    for (Object o : risk_items) {
                        JSONObject risk_item = (JSONObject) o;
                        String item_name = risk_item.getString("item_name");
                        if (StringUtils.equals("1个月内申请人在多个平台申请借款", item_name)) {
                            JSONObject item_detail = risk_item.getJSONObject("item_detail");
                            int platform_count = ErpUtil.objectIsNullGetInteger(item_detail.get("platform_count"));
                            param.put("loan_fraud.tongDunReport", platform_count);
                        }
                    }
                } else {
                    throw new Exception(jsonObject.getString("reason_desc"));
                }
            } catch (Exception e) {
                e.printStackTrace();
                param.put("loan_fraud.tongDunScore", 0);
                param.put("loan_fraud.tongDunReport", 0);
            }
        } else {
            param.put("loan_fraud.tongDunScore", 0);
            param.put("loan_fraud.tongDunReport", 0);
        }
    }

    /**
     * 获取客户逾期情况
     *
     * @param loanRow
     * @param param
     * @throws Exception
     */
    private void initOverdue(Row loanRow, Row param) throws Exception {
        if (StringUtils.equals("1", RUNTIME)) {
            try {
                String cust_name = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_name"));
                String cust_phone = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_phone"));
                String idcard = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.idcard"));
                String resultString = jiAoClinet.queryOverdue(cust_name, idcard, cust_phone);
                JSONObject resultJson = JSONObject.parseObject(resultString);
                if (resultJson == null) {
                    throw new Exception("未查询到数据");
                }
                String beforeoverdue = resultJson.getString("result_YQ_DQJE");//当前逾期金额
                if (beforeoverdue != null) {
                    param.put("loan_fraud.beforeoverdue", "01");
                } else {
                    param.put("loan_fraud.beforeoverdue", "02");
                }
                String historyoverdue = resultJson.getString("result_YQ_ZDJE");//历史逾期金额
                if (historyoverdue != null) {
                    param.put("loan_fraud.historyoverdue", "01");
                } else {
                    param.put("loan_fraud.historyoverdue", "02");
                }
            } catch (Exception e) {
                param.put("loan_fraud.beforeoverdue", "02");
                param.put("loan_fraud.historyoverdue", "02");
            }
        } else {
            param.put("loan_fraud.beforeoverdue", "02");
            param.put("loan_fraud.historyoverdue", "02");
        }
    }

    /**
     * 获取家庭住址匹配
     *
     * @param loanRow
     */
    private void initHomeAddressRange(Row loanRow, Row param) throws Exception {
        if (StringUtils.equals("1", RUNTIME)) {
            try {
                String cust_name = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_name"));
                String cust_phone = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_phone"));
                String idcard = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.idcard"));
                String cust_address = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_address"));
                String resultString = jiAoClinet.queryMobileHomeDeviate(cust_name, idcard, cust_phone, cust_address);
                String code = "";
                if (StringUtils.equals("(0,2]", resultString)) {
                    code = "01";
                } else if (StringUtils.equals("(2,5]", resultString)) {
                    code = "02";
                } else if (StringUtils.equals("(5,+)", resultString)) {
                    code = "03";
                } else {
                    code = "00";
                }
                param.put("loan_fraud.home_address", code);
            } catch (Exception e) {
                e.printStackTrace();
                param.put("loan_fraud.home_address", "00");
            }
        } else {
            param.put("loan_fraud.home_address", "00");
        }
    }

    /**
     * 获取单位住址匹配
     *
     * @param loanRow
     */
    private void initWorkAddressRange(Row loanRow, Row param) throws Exception {
        if (StringUtils.equals("1", RUNTIME)) {
            try {
                String cust_name = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_name"));
                String cust_phone = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_phone"));
                String idcard = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.idcard"));
                String company_add = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.company_add"));
                String resultString = jiAoClinet.queryMobileWorkDeviate(cust_name, idcard, cust_phone, company_add);
                String code = "";
                if (StringUtils.equals("(0,2]", resultString)) {
                    code = "01";
                } else if (StringUtils.equals("(2,5]", resultString)) {
                    code = "02";
                } else if (StringUtils.equals("(5,+)", resultString)) {
                    code = "03";
                } else {
                    code = "00";
                }
                param.put("loan_fraud.work_address", code);
            } catch (Exception e) {
                e.printStackTrace();
                param.put("loan_fraud.work_address", "00");
            }
        } else {
            param.put("loan_fraud.work_address", "00");
        }
    }

    /**
     * 驾驶证状态
     *
     * @param loanRow
     * @param param
     * @throws Exception
     */
    private void initDriving(Row loanRow, Row param) throws Exception {
        if (StringUtils.equals("1", RUNTIME)) {
            try {
                String cust_name = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_name"));
                String idcard = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.idcard"));
                JSONObject jsonObject = acedataClinet.queryDrivingLicense(cust_name, idcard, false);
                String resCode = jsonObject.getString("resCode");
                if (StringUtils.equals("0000", resCode)) {
                    JSONObject dataJson = jsonObject.getJSONObject("data");
                    String statusCode = dataJson.getString("statusCode");
                    if (StringUtils.equals("2012", statusCode)) {
                        JSONObject resultJson = dataJson.getJSONObject("result");
                        String state = resultJson.getString("state");
                        if (StringUtils.equals("A", state)) {
                            param.put("loan_fraud.driving", "01");
                            return;
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        param.put("loan_fraud.driving", "02");
    }

    /**
     * 银行卡开卡时间
     *
     * @param loanRow
     * @param param
     * @throws Exception
     */
    private void initBankCardOpen(Row loanRow, Row param) throws Exception {
        param.put("loan_fraud.bankcardopen", 1);
    }

    /**
     * 月还贷能力占比
     *
     * @param loanRow
     * @param param
     * @throws Exception
     */
    private void initRepayLoan(Row loanRow, Row param) throws Exception {
        param.put("loan_fraud.repayloan", "01");
    }

    /**
     * 获取通话详单内金融机构占比和是否被催收（数据魔盒运营商数据获取）
     * 获取预留联系人是否有直亲（并匹配配偶，子女，父母）
     *
     * @param loanRow
     * @param param
     * @throws Exception
     */
    private void initYys(Row loanRow, Row param) throws Exception {
        if (StringUtils.equals("1", RUNTIME)) {
            try {
                String task_id = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.yys_message"));
                JSONObject jsonObject = shuJuMoHeClinet.queryBgzb(task_id);
                String data = jsonObject.getString("bgzb");
                String dataString = Util.gunzip(data);
                JSONObject dataJson = JSONObject.parseObject(dataString);
                int count = 0;
                //金融机构联系人明细
                JSONArray finance_contact_detail = dataJson.getJSONArray("finance_contact_detail");
                if (finance_contact_detail.size() > 0) {
                    for (Object obj : finance_contact_detail) {
                        JSONObject financeJson = (JSONObject) obj;
                        String contact_type = financeJson.getString("contact_type");
                        if (!StringUtils.equals("银行", contact_type)) {
                            count++;
                        }
                    }
                    double bl = BigDecimalUtil.div(count, finance_contact_detail.size(), 2);
                    if (bl > 0.5) {
                        param.put("loan_fraud.proportion", "02");
                    } else {
                        param.put("loan_fraud.proportion", "01");
                    }
                } else {
                    param.put("loan_fraud.proportion", "00");
                }
                //联系人催收风险分析
                JSONArray contact_suspect_collection_analysis = dataJson.getJSONArray("contact_suspect_collection_analysis");
                if (contact_suspect_collection_analysis.size() > 0) {
                    param.put("loan_fraud.collection", "01");
                } else {
                    param.put("loan_fraud.collection", "00");
                }
                //全部联系人明细
                JSONArray all_contact_detail = dataJson.getJSONArray("all_contact_detail");
                int num = 10;
                if (all_contact_detail.size() < 10) {
                    num = all_contact_detail.size();
                }
                String sql = "select * from loan_info_contacts where loan_no = '" + loanRow.getString("loan_info.loan_no") + "'";
                Store store = baseDao.query(sql);
                String directrelatives = "00";
                boolean b = true;
                for (Row row : store) {
                    String contacts_relation = ErpUtil.objectIsNullGetString(row.get("contacts_relation"));
                    if (StringUtils.equals("01", contacts_relation)
                            || StringUtils.equals("02", contacts_relation)
                            || StringUtils.equals("03", contacts_relation)
                            || StringUtils.equals("04", contacts_relation)) {
                        directrelatives = "01";
                        String contacts_mobile = ErpUtil.objectIsNullGetString(row.get("contacts_mobile"));
                        for (int i = 0; i < num; i++) {
                            JSONObject contactJSON = all_contact_detail.getJSONObject(i);
                            String contact_number = contactJSON.getString("contact_number");
                            if (StringUtils.equals(contacts_mobile, contact_number)) {
                                if (StringUtils.equals("01", contacts_relation)) {
                                    param.put("loan_fraud.parents", "01");
                                    b = false;
                                } else if (StringUtils.equals("02", contacts_relation)) {
                                    param.put("loan_fraud.spouse", "01");
                                    b = false;
                                } else {
                                    param.put("loan_fraud.children", "01");
                                    b = false;
                                }
                            }
                        }
                    }
                }
                param.put("loan_fraud.directrelatives", directrelatives);
                if (b && StringUtils.equals(directrelatives, "01")) {
                    for (Row row : store) {
                        String contacts_relation = ErpUtil.objectIsNullGetString(row.get("contacts_relation"));
                        if (StringUtils.equals("01", contacts_relation)) {
                            param.put("loan_fraud.parents", "00");
                        } else if (StringUtils.equals("02", contacts_relation)) {
                            param.put("loan_fraud.spouse", "00");
                        } else if (StringUtils.equals("03", contacts_relation) || StringUtils.equals("04", contacts_relation)) {
                            param.put("loan_fraud.children", "00");
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                param.put("loan_fraud.proportion", "00");
                param.put("loan_fraud.collection", "00");
                param.put("loan_fraud.directrelatives", "00");
            }
        } else {
            param.put("loan_fraud.proportion", "00");
            param.put("loan_fraud.collection", "00");
            param.put("loan_fraud.directrelatives", "00");
        }
    }

    /**
     * 获取婚姻状态
     *
     * @param loanRow
     * @param param
     * @throws Exception
     */
    private void initMarriage(Row loanRow, Row param) throws Exception {
        param.put("loan_fraud.marriage", "00");
    }

    /**
     * 获取上户时间
     *
     * @param loanRow
     * @param param
     * @throws Exception
     */
    private void initCarOwner(Row loanRow, Row param) throws Exception {
        String inireg_date = ErpUtil.objectIsNullGetString(loanRow.get("loan_ass.inireg_date"));
        int month = Util.monthsBetween(inireg_date, Util.format(new Date()));
        param.put("loan_fraud.carowner", month);
    }

    /**
     * 获取一个月登记证是否补证
     *
     * @param loanRow
     * @param param
     * @throws Exception
     */
    private void initSupplement(Row loanRow, Row param) throws Exception {
        String supplement = ErpUtil.objectIsNullGetString(loanRow.get("loan_car.supplement"));
        param.put("loan_fraud.supplement", supplement);
    }

    /**
     * 获取解押时间
     *
     * @param loanRow
     * @param param
     * @throws Exception
     */
    private void initDetention(Row loanRow, Row param) throws Exception {
        String detention = ErpUtil.objectIsNullGetString(loanRow.get("loan_car.detention"));
        int day = Util.daysBetween(detention, Util.format(new Date()));
        param.put("loan_fraud.detention", day);
    }

    /**
     * 获取是否涉及敏感行业
     *
     * @param loanRow
     * @param param
     * @throws Exception
     */
    private void initSensitivesectors(Row loanRow, Row param) throws Exception {
        String sensitivesectors = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.sensitivesectors"));
        param.put("loan_fraud.sensitivesectors", sensitivesectors);
    }
}

