package com.rxcd.autofinace;

import com.rxcd.autofinace.common.BizErrorCodeEnum;
import org.appframe.wf.engine.common.ErrorCodeEnum;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import javax.annotation.PostConstruct;

@SpringBootApplication(scanBasePackages = {"org.appframe", "com.rxcd", "com.fida"})
//@ServletComponentScan(basePackages = {"com.framemx"})
@EnableAspectJAutoProxy(exposeProxy = true)
@ComponentScan({"org.appframe", "com.rxcd", "com.fida"})
public class AutoFinaceApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(AutoFinaceApplication.class, args);
    }

    @PostConstruct
    public void putBizErrorMsg() {
        ErrorCodeEnum.getDesc().putAll(BizErrorCodeEnum.getDesc());
    }
}
