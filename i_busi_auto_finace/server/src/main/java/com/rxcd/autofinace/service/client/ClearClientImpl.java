package com.rxcd.autofinace.service.client;

import com.alibaba.fastjson.JSON;
import com.fida.clearing.*;
import com.rxcd.autofinace.model.ClearBean;
import com.rxcd.autofinace.util.RxErpUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 清算系统Client
 */
@Component
public class ClearClientImpl implements ClearClient{

    private final static Log log = LogFactory.getLog(ClearClient.class);
    @Value("${api.clearing.clearing_in}")
    private  String clearingIn;
    @Value("${api.clearing.clearing_in_version}")
    private  Integer clearingInVersion ;
    @Autowired
    private RxErpUtil erpUtil;
    @Autowired
    private BaseDao baseDao;
    @Autowired
    private SubjectClient subjectClient;
    @Autowired
    private AccountClient accountClient;
    @Autowired
    private RuleClient ruleClient;
    @Autowired
    private BillClient billClient;
    @Autowired
    private BankCardClient bankCardClient;

    /**
     * 得到费用规则
     * @param bizNo
     * @return
     * @throws Exception
     */
    @Override
    public Map getFeeRule(String bizNo) throws Exception {
        Row loanRow=erpUtil.getLoanBasicDataInfoV2(bizNo);
        Map params=new HashMap();
        params.put("clearingType", "getRule");
        params.put("contract_sum", loanRow.getStringNotNull("loan_cost.contract_sum"));
        params.put("prod_no", loanRow.getStringNotNull("loan_info.prod_no"));
        params.put("prod_version", loanRow.getStringNotNull("loan_info.prod_version"));
        params.put("loan_type", loanRow.getStringNotNull("loan_info_et.loan_type"));
        params.put("rep_type", loanRow.getStringNotNull("loan_info_et.rep_type"));
        params.put("capital_return_type", loanRow.getStringNotNull("loan_cost.capital_return_type"));
        params.put("duetime", loanRow.getStringNotNull("loan_cost.duetime"));
        params.put("duetime_unit", loanRow.getStringNotNull("loan_cost.duetime_unit"));
        params.put("loan_date", loanRow.getStringNotNull("loan_info_et.loan_date"));
        String fk_date=loanRow.getStringNotNull("loan_info_et.fk_date");
        if(Util.isNotEmpty(fk_date)){
            params.put("loan_date", fk_date);
        }
        Map map = ruleClient.ruleTest(clearingIn,clearingInVersion,params);
        if(StringUtils.equals("1",String.valueOf(map.get("resultCode")))){
            Map extData =(Map)((Map) ((List) map.get("rows")).get(0)).get("extData");
            Map ruleData=(Map) extData.get("data");
            return  ruleData;
        }else{
            throw new Exception("获取费用规则出错:"+(String)map.get("errMsg"));
        }
    }

    /**
     * 费用单试算
     * @param bizNo
     * @return
     * @throws Exception
     */
    @Override
    public List repayPlanTest(String bizNo) throws Exception {
        Row loanRow=erpUtil.getLoanBasicDataInfoV2(bizNo);
        Map params=new HashMap();
        String plan_costsheet = null;
        String duetime = loanRow.getStringNotNull("loan_cost.duetime");
        String temp_duetime = duetime;
        String clear_type = loanRow.getStringNotNull("loan_info_et.clear_type");
        String demolition_period = loanRow.getStringNotNull("loan_info_et.demolition_period");
        if ("1".equals(clear_type)){
            plan_costsheet = "plan_costsheet_6x4";
            duetime = demolition_period;
            demolition_period = temp_duetime;
        }else if("0".equals(clear_type)){
            plan_costsheet = "plan_costsheet";
        }
        params.put("demolition_period",demolition_period);
        params.put("clearingType",plan_costsheet);
        params.put("duetime",duetime);
        params.put("cost_version","6.0");//params.put("cost_version",loanRow.getStringNotNull("loan_cost.cost_version"));
        params.put("contract_sum", loanRow.getStringNotNull("loan_cost.contract_sum"));
        params.put("duetime_unit", loanRow.getStringNotNull("loan_cost.duetime_unit"));
        params.put("loan_date", loanRow.getStringNotNull("loan_info_et.loan_date"));
        params.put("pacc_fee", loanRow.getDoubleNotNull("loan_cost.pacc_fee"));
        params.put("security_fee", loanRow.getDoubleNotNull("loan_cost.security_fee"));
        String fk_date=loanRow.getStringNotNull("loan_info_et.fk_date");
        if(Util.isNotEmpty(fk_date)){
            params.put("loan_date", fk_date);
        }
        params.put("accounts", JSON.toJSONString(erpUtil.getLoanAccount(bizNo)));
        Map map = ruleClient.ruleTest(clearingIn,clearingInVersion,params);
        if(StringUtils.equals("1",String.valueOf(map.get("resultCode")))){
            return (List) map.get("rows");
        }else{
            throw new Exception("试算出错");
        }
    }

    /**
     * 清分后还款试算
     * @param bizNo
     * @return
     * @throws Exception
     */
    public List createRepayPayableTest(String bizNo) throws Exception{
        Row loanRow=erpUtil.getLoanBasicDataInfoV2(bizNo);
        Map params=new HashMap();
        String plan_repay = null;
        String duetime = loanRow.getStringNotNull("loan_cost.duetime");
        String temp_duetime = duetime;
        String clear_type = loanRow.getStringNotNull("loan_info_et.clear_type");
        String demolition_period = loanRow.getStringNotNull("loan_info_et.demolition_period");
        if ("1".equals(clear_type)){
            plan_repay = "plan_repay_6x4";
            duetime = demolition_period;
            demolition_period = temp_duetime;
        }else if("0".equals(clear_type)){
            plan_repay = "plan_repay";
        }
        params.put("demolition_period",demolition_period);
        params.put("clearingType",plan_repay);
        params.put("duetime",duetime);
        params.put("cost_version","6.0");
        params.put("contract_sum", loanRow.getStringNotNull("loan_cost.contract_sum"));
        params.put("duetime_unit", loanRow.getStringNotNull("loan_cost.duetime_unit"));
        params.put("loan_date", loanRow.getStringNotNull("loan_info_et.loan_date"));
        params.put("pacc_fee", loanRow.getDoubleNotNull("loan_cost.pacc_fee"));
        params.put("security_fee", loanRow.getDoubleNotNull("loan_cost.security_fee"));
        String fk_date=loanRow.getStringNotNull("loan_info_et.fk_date");
        if(Util.isNotEmpty(fk_date)){
            params.put("loan_date", fk_date);
        }
        params.put("accounts", JSON.toJSONString(erpUtil.getLoanAccount(bizNo)));
        Map result = ruleClient.ruleTest(clearingIn,clearingInVersion,params);
        if(StringUtils.equals("1",String.valueOf(result.get("resultCode")))){
            return (List) result.get("rows");
        }else{
            throw new Exception("还款生成应收应付试算出错");
        }
    }

    /**
     * 费用单试算
     * @param clearBean
     * @return
     * @throws Exception
     */
    @Override
    public List repayPlanTestByCleanBean(ClearBean clearBean) throws Exception {
        Map params=new HashMap();
        String plan_costsheet = null;
        String duetime = clearBean.getDuetime();
        String temp_duetime = duetime;
        String clear_type = clearBean.getClear_type();
        String demolition_period = clearBean.getDemolition_period();
        if ("1".equals(clear_type)){
            plan_costsheet = "plan_costsheet_6x4";
            duetime = demolition_period;
            demolition_period = temp_duetime;
        }else if("0".equals(clear_type)){
            plan_costsheet = "plan_costsheet";
        }
        params.put("demolition_period",demolition_period);
        params.put("clearingType",plan_costsheet);
        params.put("duetime",duetime);
        params.put("cost_version","6.0");//params.put("cost_version",loanRow.getStringNotNull("loan_cost.cost_version"));
        params.put("contract_sum", clearBean.getContract_sum());
        params.put("duetime_unit", clearBean.getDuetime_unit());
        params.put("loan_date", clearBean.getLoan_date());
        params.put("pacc_fee", clearBean.getPacc_fee());
        params.put("security_fee",clearBean.getSecurity_fee());
        params.put("accounts", clearBean.getAccounts());
        Map map = ruleClient.ruleTest(clearingIn,clearingInVersion,params);
        if(StringUtils.equals("1",String.valueOf(map.get("resultCode")))){
            return (List) map.get("rows");
        }else{
            throw new Exception("试算出错");
        }
    }

    /**
     * 清分后还款试算
     * @param clearBean
     * @return
     * @throws Exception
     */
    public List createRepayPayableTestByClearBean(ClearBean clearBean) throws Exception{
        Map params=new HashMap();
        String plan_repay = null;
        String duetime = clearBean.getDuetime();
        String temp_duetime = duetime;
        String clear_type = clearBean.getClear_type();
        String demolition_period = clearBean.getDemolition_period();
        if ("1".equals(clear_type)){
            plan_repay = "plan_repay_6x4";
            duetime = demolition_period;
            demolition_period = temp_duetime;
        }else if("0".equals(clear_type)){
            plan_repay = "plan_repay";
        }
        params.put("demolition_period",demolition_period);
        params.put("clearingType",plan_repay);
        params.put("duetime",duetime);
        params.put("cost_version","6.0");
        params.put("contract_sum", clearBean.getContract_sum());
        params.put("duetime_unit",clearBean.getDuetime_unit());
        params.put("loan_date", clearBean.getLoan_date());
        params.put("pacc_fee", clearBean.getPacc_fee());
        params.put("security_fee", clearBean.getSecurity_fee());
        params.put("accounts", clearBean.getAccounts());
        Map result = ruleClient.ruleTest(clearingIn,clearingInVersion,params);
        if(StringUtils.equals("1",String.valueOf(result.get("resultCode")))){
            return (List) result.get("rows");
        }else{
            throw new Exception("还款生成应收应付试算出错");
        }
    }


    /**
     * 创建贷款主体账户
     * @param bizNo
     * @throws Exception
     */
    @Override
    public void createCustAccount(String bizNo) throws Exception {
        //创建主体
        Row loanRow=erpUtil.getLoanBasicDataInfoV2(bizNo);
        String cust_name=loanRow.getStringNotNull("loan_cust.cust_name");
        String idcard=loanRow.getStringNotNull("loan_cust.idcard");
        String cust_phone=loanRow.getString("loan_cust.cust_phone");
        String bank_accout=loanRow.getString("loan_info_et.bank_accout");
        Map subResult=subjectClient.add(2,cust_name,"IC",idcard,cust_name,cust_phone,null,null,true,"融租客户");
        String subjectId="";
        if(StringUtils.equals("1",String.valueOf(subResult.get("resultCode")))){
            subjectId =(String)((Map)subResult.get("params")).get("subjectId");
        }else{
            throw new Exception("创建客户主体出错:+"+subResult.get("errMsg"));
        }

        //创建冻结账户
        Map  froResult=accountClient.addFrozenAccount(subjectId,cust_name+"--冻结","renxingchedai");
        if(StringUtils.equals("1",String.valueOf(froResult.get("resultCode")))){
            String frozenAccount =(String)((Map)froResult.get("params")).get("accountId");
            baseDao.execute("update loan_cust set clearing_frozen='"+frozenAccount+"' where loan_no='"+bizNo+"'");
        }else{
            throw new Exception("创建客户冻结账户出错:+"+froResult.get("errMsg"));
        }

        //创建收入账户
        String commonAccount="";
        Map  comResult=accountClient.addCommonAccount(subjectId,cust_name,"renxingchedai");
        if(StringUtils.equals("1",String.valueOf(comResult.get("resultCode")))){
            commonAccount=(String)((Map)comResult.get("params")).get("accountId");
            baseDao.execute("update loan_cust set clearing_ordinary='"+commonAccount+"' where loan_no='"+bizNo+"'");
        }else{
            throw new Exception("创建客户收入账户出错:+"+comResult.get("errMsg"));
        }

        //普通账户绑卡
        Map bankResult=bankCardClient.add(commonAccount,bank_accout);
        if(!StringUtils.equals("1",String.valueOf(bankResult.get("resultCode")))){
            throw new Exception("普通账户绑定银行卡出错:+"+bankResult.get("errMsg"));
        }
    }

    /**
     * 创建网点账户
     * @param bizNo
     * @throws Exception
     */
    @Override
    public void createBranchAccount(String bizNo) throws Exception {
        //创建主体
        Row branchRow=baseDao.queryRow("select * from join_branch where branch_join_no='"+bizNo+"'");
        String branchName=branchRow.getStringNotNull("branch_name");

        Map subResult=subjectClient.add(2,branchName,"DEPTNO",bizNo,null,null,null,null,false,"网点虚拟账户");
        String subjectId="";
        if(StringUtils.equals("1",String.valueOf(subResult.get("resultCode")))){
            subjectId =(String)((Map)subResult.get("params")).get("subjectId");
        }else{
            throw new Exception("创建网点主体出错:+"+subResult.get("errMsg"));
        }
        //创建收入账户
        Map  comResult=accountClient.addCommonAccount(subjectId,branchName,"renxingchedai");
        if(StringUtils.equals("1",String.valueOf(comResult.get("resultCode")))){
            String income =(String)((Map)comResult.get("params")).get("accountId");
            baseDao.execute("update join_branch set clearing_income='"+income+"' where branch_join_no='"+bizNo+"'");
        }else{
            throw new Exception("创建网点收入账户出错:+"+comResult.get("errMsg"));
        }
        //创建冻结账户
        Map  froResult=accountClient.addFrozenAccount(subjectId,branchName+"--冻结","renxingchedai");
        if(StringUtils.equals("1",String.valueOf(froResult.get("resultCode")))){
            String frozen =(String)((Map)froResult.get("params")).get("accountId");
            baseDao.execute("update join_branch set clearing_frozen='"+frozen+"' where branch_join_no='"+bizNo+"'");
        }else{
            throw new Exception("创建网点冻结账户出错:+"+froResult.get("errMsg"));
        }
        //创建保证金账户
        Map  depResult=accountClient.addFrozenAccount(subjectId,branchName+"--保证金","renxingchedai");
        if(StringUtils.equals("1",String.valueOf(depResult.get("resultCode")))){
            String frozen =(String)((Map)depResult.get("params")).get("accountId");
            baseDao.execute("update join_branch set clearing_deposit='"+frozen+"' where branch_join_no='"+bizNo+"'");
        }else{
            throw new Exception("创建网点保证金账户出错:+"+depResult.get("errMsg"));
        }
    }

    /**
     * 创建区域账户
     * @param bizNo
     * @throws Exception
     */
    @Override
    public void createAreaAccount(String bizNo) throws Exception {
        //创建主体
        Row areaRow=baseDao.queryRow("select * from join_area where area_join_no='"+bizNo+"'");
        String areaName=areaRow.getStringNotNull("area_name");

        Map subResult=subjectClient.add(2,areaName,"DEPTNO",bizNo,null,null,null,null,false,"网点虚拟账户");
        String subjectId="";
        if(StringUtils.equals("1",String.valueOf(subResult.get("resultCode")))){
            subjectId =(String)((Map)subResult.get("params")).get("subjectId");
        }else{
            throw new Exception("创建大区代理主体出错:+"+subResult.get("errMsg"));
        }
        //创建收入账户
        Map  comResult=accountClient.addCommonAccount(subjectId,areaName,"renxingchedai");
        if(StringUtils.equals("1",String.valueOf(comResult.get("resultCode")))){
            String income =(String)((Map)comResult.get("params")).get("accountId");
            baseDao.execute("update join_area set clearing_income='"+income+"' where area_join_no='"+bizNo+"'");
        }else{
            throw new Exception("创建大区代理收入账户出错:+"+comResult.get("errMsg"));
        }
        //创建冻结账户
        Map  froResult=accountClient.addFrozenAccount(subjectId,areaName+"--冻结","renxingchedai");
        if(StringUtils.equals("1",String.valueOf(froResult.get("resultCode")))){
            String frozen =(String)((Map)froResult.get("params")).get("accountId");
            baseDao.execute("update join_area set clearing_frozen='"+frozen+"' where area_join_no='"+bizNo+"'");
        }else{
            throw new Exception("创建大区代理冻结账户出错:+"+froResult.get("errMsg"));
        }
        //创建保证金账户
        Map  depResult=accountClient.addFrozenAccount(subjectId,areaName+"--保证金","renxingchedai");
        if(StringUtils.equals("1",String.valueOf(depResult.get("resultCode")))){
            String frozen =(String)((Map)depResult.get("params")).get("accountId");
            baseDao.execute("update join_area set clearing_deposit='"+frozen+"' where area_join_no='"+bizNo+"'");
        }else{
            throw new Exception("创建大区代理保证金账户出错:+"+depResult.get("errMsg"));
        }
    }

    /**
     * 放款清算
     * @param bizNo
     * @return
     * @throws Exception
     */
    @Override
    public List createPayPayable(String bizNo) throws Exception {
        String sql="select * " +
                "from loan_mid_pay a " +
                "left join loan_mid_employers b on a.mid_no=b.mid_no and b.status='1' " +
                "left join loan_mid c on a.mid_no=c.mid_no " +
                "where a.mid_no='"+bizNo+"'";
        Row payRow=baseDao.queryRow(sql);
        String loan_no=payRow.getStringNotNull("loan_no");
        String pay_type=payRow.getStringNotNull("pay_type");
        Row loanRow=erpUtil.getLoanBasicDataInfoV2(loan_no);
        Map params=new HashMap();
        params.put("clearingType", "plan_pay"+pay_type);
        params.put("cost_version","6.0");//params.put("cost_version",loanRow.getStringNotNull("loan_cost.cost_version"));
        params.put("contract_sum", loanRow.getStringNotNull("loan_cost.contract_sum"));
        params.put("duetime", loanRow.getStringNotNull("loan_cost.duetime"));
        params.put("duetime_unit", loanRow.getStringNotNull("loan_cost.duetime_unit"));
        params.put("loan_date", loanRow.getStringNotNull("loan_info_et.loan_date"));
        String fk_date=loanRow.getStringNotNull("loan_info_et.fk_date");
        if(Util.isNotEmpty(fk_date)){
            params.put("loan_date", fk_date);
        }
        params.put("accounts", JSON.toJSONString(erpUtil.getLoanAccount(loan_no)));
        Map result = ruleClient.ruleCalResult(clearingIn,clearingInVersion,bizNo,"PAY"+pay_type,params);//pay_type:02,一次放款到卡;03:二次放款到卡
        if(StringUtils.equals("1",String.valueOf(result.get("resultCode")))){
            return (List) result.get("rows");
        }else{
            throw new Exception("付款生成应收应付出错");
        }
    }

    /**
     * 还款清算
     * @param bizNo
     * @return
     * @throws Exception
     */
    @Override
    public List createRepayPayable(String bizNo) throws Exception {
        Row loanRow=erpUtil.getLoanBasicDataInfoV2(bizNo);
        Map params=new HashMap();
        String plan_repay = null;
        String duetime = loanRow.getStringNotNull("loan_cost.duetime");
        String temp_duetime = duetime;
        String clear_type = loanRow.getStringNotNull("loan_info_et.clear_type");
        String demolition_period = loanRow.getStringNotNull("loan_info_et.demolition_period");
        if ("1".equals(clear_type)){
            plan_repay = "plan_repay_6x4";
            duetime = demolition_period;
            demolition_period = temp_duetime;
        }else if("0".equals(clear_type)){
            plan_repay = "plan_repay";
        }
        params.put("demolition_period",demolition_period);
        params.put("clearingType",plan_repay);
        params.put("duetime",duetime);
        params.put("cost_version","6.0");//params.put("cost_version",loanRow.getStringNotNull("loan_cost.cost_version"));
        params.put("contract_sum", loanRow.getStringNotNull("loan_cost.contract_sum"));
        params.put("duetime_unit", loanRow.getStringNotNull("loan_cost.duetime_unit"));
        params.put("loan_date", loanRow.getStringNotNull("loan_info_et.loan_date"));
        params.put("pacc_fee", loanRow.getDoubleNotNull("loan_cost.pacc_fee"));
        params.put("security_fee", loanRow.getDoubleNotNull("loan_cost.security_fee"));
        String fk_date=loanRow.getStringNotNull("loan_info_et.fk_date");
        if(Util.isNotEmpty(fk_date)){
            params.put("loan_date", fk_date);
        }
        params.put("accounts", JSON.toJSONString(erpUtil.getLoanAccount(bizNo)));
        Map result = ruleClient.ruleCalResult(clearingIn,clearingInVersion,bizNo,"REPAY",params);
        if(StringUtils.equals("1",String.valueOf(result.get("resultCode")))){
            return (List) result.get("rows");
        }else{
            throw new Exception("还款生成应收应付出错");
        }
    }


    /**
     * 一次放款账单
     * @param bizNo
     * @return
     * @throws Exception
     */
    @Override
    public List createPay02Bill(String bizNo) throws Exception {
        List<String> batchs=new ArrayList<>();
        batchs.add("0");
        Map map=billClient.clearTransferBalancePayable(bizNo,"PAY02",batchs,null,null);
        log.info("账单MAP:"+map.toString());
        if(StringUtils.equals("1",String.valueOf(map.get("resultCode")))){
            return (List) map.get("rows");
        }else{
            throw new Exception("一次放款账单生成出错");
        }
    }

    /**
     * 二次放款账单
     * @param bizNo
     * @return
     * @throws Exception
     */
    @Override
    public List createPay03Bill(String bizNo) throws Exception {
        List<String> batchs=new ArrayList<>();
        batchs.add("0");
        Map map=billClient.clearTransferBalancePayable(bizNo,"PAY03",batchs,null,null);
        if(StringUtils.equals("1",String.valueOf(map.get("resultCode")))){
            return (List) map.get("rows");
        }else{
            throw new Exception("二次放款账单生成出错");
        }
    }


    /**
     * 结算账单
     * @param billId
     * @return
     * @throws Exception
     */
    @Override
    public Map<String,Object> settleBill(String billId) throws Exception {
        Map<String,Object> map =  billClient.settleBill(billId);
        if(StringUtils.equals("1",String.valueOf(map.get("resultCode"))) || StringUtils.equals("800022",String.valueOf(map.get("errCode")))){
            return map;
        }else{
            throw new Exception("账单结算失败:"+map.get("errMsg"));
        }
    }

    /**
     * 提现账单
     * @param bill
     * @return
     * @throws Exception
     */
    @Override
    public List createWithdrawCashBillWithBill(Map<String,Object> bill) throws Exception{
        log.info("bill:"+bill.toString());
        Map<String,Object> withdrawCashBill = new HashMap<>();
        withdrawCashBill.putAll(bill);
        withdrawCashBill.remove("billId");
        Map map = billClient.createWithdrawCashBill(withdrawCashBill);
        log.info("账单MAP:"+map.toString());
        if(StringUtils.equals("1",String.valueOf(map.get("resultCode")))){
            return (List) map.get("rows");
        }else{
            throw new Exception("根据放款账单生成提现账单生成出错");
        }
    }



}
