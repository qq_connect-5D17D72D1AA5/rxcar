package com.rxcd.autofinace.util;


import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.util.SafeUtil;
import org.appframe.wf.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.SQLException;

@Component
public class BusiUtil {

    @Autowired
    private BaseDao baseDao;

    /**
     * 取分行名称
     *
     * @param loan_no
     * @return
     * @throws SQLException
     */
    public String branchName(String loan_no) throws SQLException {
        String sql = "select sell_branch_no from loan_info where loan_no='" + SafeUtil.safe(loan_no) + "'";
        Row row = baseDao.queryRow(sql);
        if (row == null) {
            return "";
        }
        String sell_branch_no = row.getString("sell_branch_no");

        sql = "select dept_name from sys_info_dept where no = '" + SafeUtil.safe(sell_branch_no) + "'";
        Row dept = baseDao.queryRow(sql);
        String dept_name = dept.getString("dept_name");
        return dept_name;
    }

    /**
     * 循环找分行
     *
     * @param dept_no
     * @return
     * @throws SQLException
     */
    public String getParentDept(String dept_no) throws SQLException {
        String sql = "SELECT * FROM i_frame_sys_dept WHERE dept_id = (SELECT p_dept_id FROM i_frame_sys_dept WHERE DEPT_ID = ?)";
        Row row = baseDao.queryRow(sql, dept_no);
        if (Util.isEmpty(String.valueOf(row.get("dept_id")))) {
            return "";
        } else {
            if ("5".equals(row.getString("dept_type"))) {
                return row.getString("dept_name");
            } else {
                getParentDept(String.valueOf(row.get("dept_id")));
            }
        }
        return "";
    }
}