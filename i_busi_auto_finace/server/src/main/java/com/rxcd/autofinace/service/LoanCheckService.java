/**
 * 版权所有：版权所有：重庆易房网络科技股份有限公司
 * Chongqing YiFang Network Technology Co. LTD.  All Rights Reserved
 * 创建者：HeSenLin   创建时期：  2016年3月16日
 */
package com.rxcd.autofinace.service;


import com.rxcd.autofinace.annotation.CheckFunction;
import org.springframework.stereotype.Component;

/**
 * 贷款流程节点验证工具类
 * 注意：所有带注解@CheckFunction验证方法首参数必须传流程表单主表主键table_key_no
 */
@Component
public interface LoanCheckService {

    /**
     * 验证身份证
     * @param idcard
     */
    public void check_idcard(String idcard) throws Exception ;

    /**
     * 验证电话号码
     * @param cust_phone
     */
    public void check_cust_phone(String cust_phone) throws Exception ;

    /**
     * 验证车架号
     * @param cjh
     */
    public void check_cjh(String cjh) throws Exception ;

    /**
     * 检查是否存在预付款的未完成的放款流程
     * @param loan_no
     * @throws Exception
     */
    public void check_unfinishAdvancePayWorkFlow(String loan_no) throws Exception;

    /**
     * 验证放款金额是否等于合同金额
     * @param loan_no
     * @throws Exception
     */
    public void check_contribution_amount(String loan_no) throws Exception;

    /**
     * 检查车牌号是否正确
     *
     * @param car_plate
     * @throws Exception
     */
    public void check_carPlate(String car_plate) throws Exception;

    @CheckFunction(name = "检查放款上限", key = "checkFkmax")
    public void checkFkMax(String loan_no) throws Exception;

    @CheckFunction(name = "检查逾期率、临时担保额度", key = "checkLoanQuota")
    public void checkLoanQuota(String loan_no) throws Exception;

    @CheckFunction(name = "检查分行放款检查", key = "check_branch_fk")
    public void check_branch_fk(String loan_no) throws Exception;

    @CheckFunction(name = "检查车辆所有人", key = "check_car_syr")
    public void check_car_syr(String loan_no) throws Exception;

    @CheckFunction(name = "检查身份证", key = "checkValidatedIdcard")
    public void checkValidatedIdcard(String loan_no, String idcard, String custName) throws Exception;

    @CheckFunction(name = "检查基本数据", key = "checkBasicData")
    public void checkBasicData(String loan_no) throws Exception;

    @CheckFunction(name = "检查资方数据", key = "checkEmployerInfo")
    public void checkEmployerInfo(String loan_no) throws Exception;

    @CheckFunction(name = "检查审批金额", key = "check_approve_sum")
    public void check_approve_sum(String loan_no) throws Exception;

    @CheckFunction(name = "检查资方匹配", key = "check_select_capital")
    public void check_select_capital(String loan_no) throws Exception;

    @CheckFunction(name = "检查获取运营商数据", key = "check_yys_message")
    public void check_yys_message(String loan_no) throws Exception;

    @CheckFunction(name = "检查反欺诈得分", key = "check_fraud")
    public void check_fraud(String loan_no) throws Exception;

    @CheckFunction(name = "检查分行锁单", key = "check_lockBranch")
    public void check_lockBranch(String loan_no) throws Exception;

    @CheckFunction(name = "检查车架号进件", key = "check_otherNewFkCjh")
    public void check_otherNewFkCjh(String loan_no) throws Exception;

    @CheckFunction(name = "检查已存在新增放款（车牌号）", key = "check_otherNewFk")
    public void check_otherNewFk(String loan_no) throws Exception;

    @CheckFunction(name = "检查存在未放款的贷款（车牌号）", key = "check_otherUnfinishedFk")
    public void check_otherUnfinishedFk(String loan_no) throws Exception;

    @CheckFunction(name = "检查代扣代付银行卡支持", key = "check_bank")
    public void check_bank(String loan_no) throws Exception;

    @CheckFunction(name = "检查存在一次放款到卡的流程", key = "check_exist_on_pay_card_fk_review")
    public void check_exist_on_pay_card_fk_review(String loan_no) throws Exception;

    @CheckFunction(name = "检查客户三要素", key = "check_3_element")
    public void check_3_element(String loan_no) throws Exception;

    @CheckFunction(name = "检查预付款金额", key = "check_yfk_prepay_sum")
    public void check_yfk_prepay_sum(String loan_no) throws Exception;

    @CheckFunction(name = "检查预付款额度", key = "check_prepaySumQuota")
    public void check_prepaySumQuota(String loan_no) throws Exception;

    @CheckFunction(name = "检查预放款成数", key = "check_prepay_percent")
    public void check_prepay_percent(String loan_no) throws Exception;

    @CheckFunction(name = "检查合同金额", key = "check_contract_sum")
    public void check_contract_sum(String loan_no) throws Exception;

    @CheckFunction(name = "检查客户四要素", key = "check_4_element")
    public void check_4_element(String loan_no) throws Exception;

    @CheckFunction(name = "检查合同签订", key = "check_sign_successful")
    public void check_sign_successful(String loan_no) throws Exception;

    @CheckFunction(name = "车牌号远程验证是否结清", key = "check_unclear_car_plate")
    public void check_unclear_car_plate(String loan_no) throws Exception;

}
