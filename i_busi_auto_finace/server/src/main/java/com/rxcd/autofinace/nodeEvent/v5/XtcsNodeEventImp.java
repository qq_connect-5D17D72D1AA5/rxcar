package com.rxcd.autofinace.nodeEvent.v5;


import com.rxcd.autofinace.service.LoanFraudAutoService;
import com.rxcd.autofinace.service.LoanRiskcontrolAutoService;
import com.rxcd.autofinace.util.RxErpUtil;
import org.appframe.wf.engine.AbstractWorkFlowNodeEvent;
import org.appframe.wf.util.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 大数据初审
 */
@Component
public class XtcsNodeEventImp extends AbstractWorkFlowNodeEvent {

    @Autowired
    private RxErpUtil erpUtil;
    @Autowired
    private LoanFraudAutoService loanFraudAutoService;
    @Autowired
    private LoanRiskcontrolAutoService loanRiskcontrolAutoService;

    /**
     * 流程节点启动时调用
     *
     * @param flow_node_no
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowNodeStart(String flow_node_no, Param p) throws Exception {

    }

    @Override
    public void workFlowNodeFinish(String flow_node_no, Param p) throws Exception {
        checkTempletFlowNo("1");
        String loanNo = getTableKeyNo();
        loanRiskcontrolAutoService.getFirstRiskcontrol(loanNo);
//        loanFraudAutoService.getFirstFraudcontrol(loanNo);
//        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loanNo);
//        double riskcontrol_first_score = ErpUtil.objectIsNullGetDouble(loanRow.get("loan_ass.riskcontrol_first_score"));
//        String selectable_prod = "";
//        if (riskcontrol_first_score <= 30) {
//            selectable_prod = "01,02,03,04";
//        } else if (riskcontrol_first_score > 30 && riskcontrol_first_score <= 100) {
//            selectable_prod = "01,02";
//        } else {
//            selectable_prod = "02";
//        }
//
//        String sql = "update loan_info set selectable_prod = '" + selectable_prod + "' where loan_no = '" + loanNo + "'";
//        this.baseDao.execute(sql);
    }

    /**
     * 流程节点驳回时回调方法
     *
     * @param flow_node_no
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowNodeOverrule(String flow_node_no, Param p) throws Exception {

    }

    @Override
    public void workFlowNodeVie(String flow_node_no, Param p) throws Exception {

    }

}
