package com.rxcd.autofinace.controller;

import com.rxcd.autofinace.service.btn.Recharge;
import com.rxcd.autofinace.service.btn.Refund;
import com.rxcd.autofinace.service.btn.Repayment;
import org.appframe.wf.db.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 业务按钮
 */
@RestController
@RequestMapping("/busi/btn")
public class BusiBtn {
    @Autowired
    private Recharge rechargeService;

    @Autowired
    private Repayment repaymentService;

    @Autowired
    private Refund refundService;

    /**
     * 充值
     *
     * @throws Exception
     */
    @RequestMapping("/recharge")
    public Row recharge(HttpServletRequest request, HttpServletResponse response) throws Exception{
        Row retRow=rechargeService.execute(request,response);
        return retRow;
    }


    /**
     * 还款
     *
     * @throws Exception
     */
    @RequestMapping("/repayment")
    public Row repayment(HttpServletRequest request, HttpServletResponse response) throws Exception{
        Row retRow=repaymentService.execute(request,response);
        return retRow;
    }


    /**
     * 退款
     *
     * @throws Exception
     */
    @RequestMapping("/refund")
    public Row refund(HttpServletRequest request, HttpServletResponse response)  throws Exception{
        Row retRow=refundService.execute(request,response);
        return retRow;
    }

}
