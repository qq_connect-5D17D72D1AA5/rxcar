package com.rxcd.autofinace.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rxcd.autofinace.util.RxErpUtil;
import org.apache.commons.lang.StringUtils;
import org.appframe.bigdata.AcedataClinet2;
import org.appframe.bigdata.JiAoClinet2;
import org.appframe.bigdata.TianXingClinet2;
import org.appframe.bigdata.TongDunClinet2;
import org.appframe.common.BigDecimalUtil;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.db.Store;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/")
public class IncomeShowController {

    @Autowired
    private RxErpUtil erpUtil;
    @Autowired
    private BaseDao baseDao;

    @Autowired
    private TianXingClinet2 tianXingClinet;

    @Autowired
    private TongDunClinet2 tongDunClinet;

    @Autowired
    private JiAoClinet2 jiAoClinet;

    @Autowired
    private AcedataClinet2 acedataClinet;

    @RequestMapping("/income_show")
    public Row execute(@RequestParam("name") String name,@RequestParam("idcard") String idcard,@RequestParam("mobile") String mobile,@RequestParam("cards") String bankCard) throws Exception {
        Row resultMap = new Row();
        resultMap.put("name", name);
        resultMap.put("idcard", idcard);
        resultMap.put("mobile", mobile);
        if(mobile.length()!=11){
            throw new Exception("手机号码不合法");
        }
        List<Map<Object, Object>> cards = new ArrayList<>();
        List<Map<Object, Object>> feeList = new ArrayList<>();
        Map<Object, Object> feeMap = new HashMap<>();
        tongDunMessage(resultMap,feeMap,name, idcard, mobile);
        String[] bankCards = bankCard.split(",");
        for(String card:bankCards){
            Map<Object, Object> cardsMap = new HashMap<>();
            if(StringUtils.equals(card, "")){
                continue;
            }
            boolean b = tianXingClinet.queryUnionpay3Element(name, idcard, card, false);
            if(!b){
                throw new Exception(card+"此银行卡验证未通过!");
            }
            cardsMap.put("card", card);
            Map<Object, Object> cardMessage = new HashMap<>();
            cardMessage = aceBankCard(cardMessage,feeMap,cardsMap,name, card);
            if(cardMessage.size()==0){
                cardsMap.put("aceError", "该卡未查得大数据信息，不能计算还贷能力");
            }
            cardMessage = jiaoBankCard(cardMessage,cardsMap,name, idcard, mobile, card);
            cardsMap.put("cardMessage", cardMessage);
            cards.add(cardsMap);
            feeList.add(feeMap);
        }
        double repayLoanMoney = repayLoan(feeList);
        resultMap.put("repayLoanMoney", repayLoanMoney);
        resultMap.put("cards", cards);

        return resultMap;
    }

    /**
     * 获取多头数据
     * @param resultMap
     * @param name
     * @param idcard
     * @param mobile
     * @return
     * @throws Exception
     */
    private Map<Object, Object> tongDunMessage(Map<Object, Object> resultMap,Map<Object, Object> feeMap,String name,String idcard,String mobile)throws Exception{
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("name", name);// 姓名 必填
        paramMap.put("id_number", idcard);// 身份证 必填
        paramMap.put("mobile",mobile);// 手机号
        String reportId =tongDunClinet.queryReportNo(paramMap,false);
        Thread.sleep(2000);
        JSONObject jsonObject =tongDunClinet.queryReport(reportId,false);
        List<Object> list = new ArrayList<>();
        if(jsonObject.getBooleanValue("success")){
            JSONArray risk_items = jsonObject.getJSONArray("risk_items");
            for(Object o:risk_items){
                JSONObject risk_item = (JSONObject)o;
                String group = risk_item.getString("group");
                if(!StringUtils.equals("多平台借贷申请检测", group)){
                    continue;
                }
                String item_name = risk_item.getString("item_name");
                if(StringUtils.equals("1个月内申请人在多个平台申请借款", item_name)){
                    JSONObject item_detail = risk_item.getJSONObject("item_detail");
                    String platform_count = item_detail.getString("platform_count");
                    feeMap.put("duotouCount", platform_count);
                }
                list.add(risk_item);
            }
            resultMap.put("tongdun", list);
            return resultMap;
        }
        throw new Exception(jsonObject.getString("errorDesc"));
    }

    /**
     * 获取集奥银行卡信息
     * @param cardMessage
     * @param name
     * @param idcard
     * @param mobile
     * @param card
     * @return
     * @throws Exception
     */
    private Map<Object,Object> jiaoBankCard(Map<Object, Object> cardMessage,Map<Object, Object> cardsMap,String name,String idcard,String mobile,String card) throws Exception{
        String jiaoString = jiAoClinet.queryBankCardCredit(name, idcard, mobile,card);
        if(jiaoString!=null){
            JSONObject jiaoJson = JSONObject.parseObject(jiaoString);
            JSONArray data = jiaoJson.getJSONArray("data");
            JSONObject d = (JSONObject)data.get(0);
            Map<String, String> jiaodictionMap = queryJiaoDictionaries();
            for (Map.Entry<String, Object> entry : d.entrySet()) {
                if(StringUtils.equals(null, jiaodictionMap.get(entry.getKey()))){
                    continue;
                }
                if(StringUtils.equals(entry.getKey(), "CDCA005")){
                    cardsMap.put("openCard", entry.getValue());
                }
                cardMessage.put(jiaodictionMap.get(entry.getKey()), entry.getValue());
            }
        }
        return cardMessage;
    }

    /**
     * 获取优分银行卡信息
     * @param cardMessage
     * @param name
     * @param card
     * @return
     * @throws Exception
     */
    private Map<Object,Object> aceBankCard(Map<Object, Object> cardMessage,Map<Object, Object> feeMap,Map<Object, Object> cardsMap,String name,String card) throws Exception{
        JSONObject aceJson = acedataClinet.queryOpenBankCard(name, card, false);
        String resCode = aceJson.getString("resCode");
        if(StringUtils.equals(resCode, "0000")){
            JSONObject aceData = aceJson.getJSONObject("data");
            String statusCode = aceData.getString("statusCode");
            if(StringUtils.equals(statusCode, "2012")){
                JSONObject aceResult = aceData.getJSONObject("result");
                computeRepayLoan(aceResult,feeMap);
                initBaseData(aceResult, cardsMap);
                Map<String, String> acedictionMap = queryAceDictionaries();
                for (Map.Entry<String, Object> entry : aceResult.entrySet()) {
                    if(StringUtils.equals(null, acedictionMap.get(entry.getKey()))){
                        continue;
                    }
                    cardMessage.put(acedictionMap.get(entry.getKey()), entry.getValue());
                }
            }
        }
        return cardMessage;
    }

    /**
     * 初始化基本数据
     * @param aceResult
     * @throws Exception
     */
    private void initBaseData(JSONObject aceResult,Map<Object, Object> cardsMap) throws Exception{
        String bp1001 = erpUtil.objectIsNullGetString(aceResult.get("BP1001"));
        cardsMap.put("卡种", bp1001);
        String bp1002 =  erpUtil.objectIsNullGetString(aceResult.get("BP1002"));
        cardsMap.put("卡性质", bp1002);
        String bp1003 =  erpUtil.objectIsNullGetString(aceResult.get("BP1003"));
        cardsMap.put("卡品牌", bp1003);
    }

    /**
     * 获取每张银行卡还贷能力
     * @throws Exception
     */
    private void computeRepayLoan(JSONObject aceResult,Map<Object, Object> feeMap) throws Exception{
        double proportion = getDuotouProportion(feeMap);
        double rule = 1;
        double cp6014 = erpUtil.objectIsNullGetDouble(aceResult.get("CP6014"));//月还贷能力评估
        feeMap.put("hdzb", cp6014);
        double yjxf;
        double cp5001 = erpUtil.objectIsNullGetDouble(aceResult.get("CP5001"));//该卡近12月的交易总金额
        double cp5005 = erpUtil.objectIsNullGetDouble(aceResult.get("CP5005"));//近12月该卡有交易的月数
        if(cp5005==0){
            cp5005 = 12;
        }
        yjxf = BigDecimalUtil.mul(BigDecimalUtil.mul(BigDecimalUtil.div(cp5001, cp5005), proportion), rule);
        feeMap.put("yjxf", yjxf);
    }

    /**
     * 获取总还贷能力
     * @param feeList
     * @throws Exception
     */
    private double repayLoan(List<Map<Object, Object>> feeList) throws Exception{
        double repayLoanMoney = 0.0;
        if(feeList.size()>0){
            for(Map<Object, Object> map:feeList){
                double hdzb = erpUtil.objectIsNullGetDouble(map.get("hdzb"));
                repayLoanMoney = BigDecimalUtil.add(repayLoanMoney, hdzb);
            }
            repayLoanMoney = BigDecimalUtil.div(BigDecimalUtil.div(repayLoanMoney, feeList.size()), 2);
            for(Map<Object, Object> map:feeList){
                double yjxf = erpUtil.objectIsNullGetDouble(map.get("yjxf"));
                repayLoanMoney = BigDecimalUtil.add(repayLoanMoney, BigDecimalUtil.div(yjxf, 2));
            }
        }
        return BigDecimalUtil.round(repayLoanMoney, 2);
    }

    /**
     * 通过多头次数返回占比
     * @param feeMap
     * @return
     */
    private double getDuotouProportion(Map<Object, Object> feeMap) throws Exception{
        int duotouCount = erpUtil.objectIsNullGetInteger(feeMap.get("duotouCount"));
        double proportion = 1.0;
        if(duotouCount>=1&&duotouCount<=4){
            proportion = 0.9;
        }else if(duotouCount>=5&&duotouCount<=9){
            proportion = 0.7;
        }else if(duotouCount>=10){
            proportion = 0.5;
        }
        return proportion;
    }

    /**
     * 获取集奥数据字典Z9
     * @return
     * @throws Exception
     */
    private Map<String,String> queryJiaoDictionaries() throws Exception{
        Map<String,String> map= new HashMap<String, String>();
        String sql = "select no,dictionaries_name from dictionaries_jiao";
        Store store = baseDao.query(sql);
        for(Row row:store){
            map.put(row.getString("no"), row.getString("dictionaries_name"));
        }
        return map;
    }

    /**
     * 获取消费画像数据字典
     * @return
     * @throws Exception
     */
    private Map<String,String> queryAceDictionaries() throws Exception{
        Map<String,String> map= new HashMap<String, String>();
        String sql = "select no,dictionaries_name from dictionaries_ace";
        Store store = baseDao.query(sql);
        for(Row row:store){
            map.put(row.getString("no"), row.getString("dictionaries_name"));
        }
        return map;
    }
}
