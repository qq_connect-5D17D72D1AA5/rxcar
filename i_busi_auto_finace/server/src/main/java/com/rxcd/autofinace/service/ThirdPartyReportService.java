package com.rxcd.autofinace.service;

import org.apache.commons.lang.StringUtils;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.engine.common.WfConst;
import org.appframe.wf.util.Param;
import org.appframe.wf.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
public class ThirdPartyReportService {

    @Autowired
    private BaseDao baseDao;

    public Row uploadImg(Row resultRow, Param param, String loanNo, String userId) {
        String id = Util.uuid();
        String no = Util.uuid();
        String sql = "SELECT * FROM loan_info where loan_no='" + loanNo + "'";
        Row loanInfoRow = baseDao.queryRow(sql);
        String loanId = loanInfoRow.getString("loan_no");
        String att_remark = param.getDataJsonString("att_remark");
        if (att_remark == null) {
            att_remark = "";
        }
        // 操作人ID
        String oper_user_id = userId;
        // 操作人NO
        String oper_user_no = userId;

        String attNo = param.getJsonString(param.getDataJson(), "no");
        sql = "SELECT * FROM sys_info_attachment where no='" + attNo + "'";
        Row attrRow = baseDao.queryRow(sql);
        if (attrRow == null || StringUtils.isBlank(attNo)) {
            resultRow.put("result", false);
            resultRow.put("message", "附件参数错误");
            param.set("json", resultRow);
//                    result.JSON();
            resultRow.put("type", "jsonJsp");

            return resultRow;
        }
        String attId = attrRow.getString("id");
        sql = "INSERT INTO loan_img_third_party (ID, NO, LOAN_ID, LOAN_NO, ATT_ID, ATT_NO, ATT_REMARK, OPER_USER_ID, OPER_USER_NO, OPER_TIME, STATUS) VALUES "
                + "('" + id + "', '" + no + "', '" + loanId + "', '" + loanNo + "', '" + attId + "', '" + attNo + "', '" + att_remark + "','" + oper_user_id + "','" + oper_user_no + "',now(),'1')";
        baseDao.execute(sql);
        Row retRow = WfConst.success("0", "上传成功");
        param.set("json", retRow);
//                result.JSON();
        resultRow.put("type", "jsonJsp");

        return resultRow;
    }
}
