/**
 * 创建者：ZhaoSheng 创建时期： 2013-07-15
 */
package com.rxcd.autofinace.controller;

import com.rxcd.autofinace.util.RxErpUtil;
import org.appframe.wf.db.Row;
import org.appframe.wf.util.ErpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * 信息认证报告
 */
@RestController
@RequestMapping("/")
public class AuthenticationController {

    @Autowired
    private RxErpUtil rxErpUtil;

    /**
     * 模型报告(风控/评估)
     *
     * @throws Exception
     */
    @RequestMapping("/loan_common_Authentication")
    public Row execute(@RequestParam("loan_no") String loan_no) throws Exception {

        Row data = new Row();

        Row loanRow = rxErpUtil.getLoanBasicDataInfoV2(loan_no);
        data.put("report_name", "信息认证报告");
        data.put("type", "report");

        List store = new ArrayList();
        String cust_name = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_name"));
        String idcard = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.idcard"));
        String cust_phone = ErpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_phone"));
        String car_plate = ErpUtil.objectIsNullGetString(loanRow.get("loan_car.car_plate"));

        LinkedHashMap rowT = new LinkedHashMap();

//        Row rowT = new Row();
        rowT.put("title", "认证项目");
        rowT.put("value", "认证值");
        rowT.put("pass", "认证通过");
        rowT.put("refused", "认证失败");


        data.put("title", rowT);

        LinkedHashMap name = new LinkedHashMap();

//        Row name = new Row();
        name.put("title", "姓名");
        name.put("value", cust_name);
        name.put("pass", true);
        name.put("refused", "");
        store.add(name);

        LinkedHashMap id = new LinkedHashMap();

//        Row id = new Row();
        id.put("title", "身份证");
        id.put("value", idcard);
        id.put("pass", true);
        id.put("refused", "");
        store.add(id);

        LinkedHashMap phone = new LinkedHashMap();

//        Row phone = new Row();
        phone.put("title", "电话");
        phone.put("value", cust_phone);
        phone.put("pass", true);
        phone.put("refused", "");
        store.add(phone);

        LinkedHashMap plate = new LinkedHashMap();

//        Row plate = new Row();
        plate.put("title", "车牌");
        plate.put("value", car_plate);
        plate.put("pass", true);
        plate.put("refused", "");
        store.add(plate);


        data.put("list", store);
        return data;
    }
}