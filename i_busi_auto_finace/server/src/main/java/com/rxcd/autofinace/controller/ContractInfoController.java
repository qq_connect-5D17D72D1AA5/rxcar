package com.rxcd.autofinace.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.appframe.contract.ContractClient;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.engine.common.ReviewUtil;
import org.appframe.wf.engine.common.WfConst;
import org.appframe.wf.service.WfParamUtil;
import org.appframe.wf.util.Param;
import org.appframe.wf.util.SafeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * 合同信息 ，签订合同组件展示所用
 */
@RestController
@RequestMapping("/")
public class ContractInfoController {

    @Autowired
    private BaseDao baseDao;
    @Autowired
    private ReviewUtil reviewUtil;
    @Autowired
    private WfParamUtil paramUtil;
    @Autowired
    private ContractClient contractClient;

    @RequestMapping("/loan_common_ContractInfo")
    public Row execute(@RequestParam String review_no) throws Exception {
        if(paramUtil.get() == null){
            Param p = new Param();
            p.setType("SYS");
            paramUtil.add("param",p);
        }
        try {
            String sql = "select *  from wf_review a  where a.status='1' and a.review_no='" + SafeUtil.safe(review_no) + "'";
            Row reviewRow = baseDao.queryRow(sql);
            if (reviewRow == null) {
                throw new Exception("没有找到此流程");
            }
            String table_key_no = (String) baseDao.queryObject("select table_key_no from wf_review_biz where review_no='" + SafeUtil.safe(review_no) + "'");
            if (StringUtils.isBlank(table_key_no)) {
                throw new Exception("没有找到业务流程");
            }
            Row retRow = WfConst.success();
            Row reviewWfObject = reviewUtil.getWfReview(review_no);
            boolean isEdit = true;
            if ("D".equals(reviewWfObject.getString("review_status"))) {
                isEdit = false;
            }
            if ("P".equals(reviewWfObject.getString("review_status"))) {
                isEdit = false;
            }
            retRow.put("isEdit", isEdit);
            JSONArray conArray = contractClient.getContract(table_key_no);
            JSONArray jsonArray = new JSONArray();
            if (conArray != null && conArray.size() > 0) {
                for (Object instance : conArray) {
                    JSONObject instanceJson = (JSONObject) instance;
                    String signType = instanceJson.getString("sign_type");
                    instanceJson.put("review_no", review_no);
                    if (StringUtils.equals("2", signType)) { //电子合同
                        JSONObject signParam = JSONObject.parseObject(instanceJson.getString("sign_param"));
                        boolean isAutoSign = signParam.getBooleanValue("isAutoSign");
                        instanceJson.put("isAutoSign", isAutoSign);
                        if (!isAutoSign) {
                            //手工签订排序前面
                            instanceJson.put("sort", 1);
                        }
                    } else {
                        //其他的合同不做处理
                        throw new Exception("合同类型未知,请添加相应的处理");
                    }
                    String filePath = "";
                    instanceJson.put("uploadVidePath", filePath);
                    jsonArray.add(instanceJson);
                }
            }
            JSONArray sortArray = new JSONArray();
            JSONObject[] jsons = new JSONObject[jsonArray.size()];
            if (jsonArray != null && jsonArray.size() > 0) {
                int index=0;
                int length=jsonArray.size()-1;
                for (int i = 0; i < jsonArray.size(); i++) {
                    JSONObject json = jsonArray.getJSONObject(i);
                    Integer sort=json.getInteger("sort");
                    if (sort!=null&&sort==1) {
                        jsons[index] = json;
                        index++;
                    } else {
                        jsons[length] = json;
                        length--;
                    }
                }
                for (JSONObject jsonObject : jsons) {
                    sortArray.add(jsonObject);
                }
            }
            Row ret = new Row();
            ret.put("isEdit", isEdit);
            retRow.put("instanceList", sortArray);
            ret.put("json", retRow);
            return ret;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }finally {
            paramUtil.remove();
        }
    }

}
