package com.rxcd.autofinace.service;

import com.rxcd.autofinace.common.BizErrorCodeEnum;
import com.rxcd.autofinace.service.client.ClearClient;
import com.rxcd.autofinace.util.RxErpUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.appframe.common.BigDecimalUtil;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.DB;
import org.appframe.wf.db.Row;
import org.appframe.wf.db.Store;
import org.appframe.wf.engine.imp.DefaultWorkFlow;
import org.appframe.wf.util.Param;
import org.appframe.wf.util.SafeUtil;
import org.appframe.wf.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 放款相关
 *
 * @author hubin
 */
@Service
public class FkService {
    private final static Log log = LogFactory.getLog(FkService.class);
    @Autowired
    private BaseDao baseDao;
    @Autowired
    private LoanCheckServiceImpl loanCheckService;
    @Autowired
    private RxErpUtil erpUtil;
    @Autowired
    private DefaultWorkFlow flow;
    @Autowired
    private ClearClient clearClient;





    /**
     * 放款相关信息V5
     *
     * @param loan_no
     * @param mid_no
     * @return
     * @throws Exception
     */
    public Map<String, Object> getFkInfoV5(String loan_no, String mid_no) throws Exception {
        loan_no = SafeUtil.safe(loan_no);
        mid_no = SafeUtil.safe(mid_no);
        Map<String, Object> resultMap = new HashMap<String, Object>();
        if (Util.isNotEmpty(mid_no) && Util.isEmpty(loan_no)) {
            loan_no = (String) DB.queryObject("select loan_no from loan_mid where mid_no='" + mid_no + "'");
        }
        if (Util.isEmpty(loan_no)) {
            throw new Exception("无效业务编号");
        }
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        Row chuzhuren = erpUtil.getCjr(loan_no);
        String branch_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info.branch_no"));//所属分行
        String prod_name = erpUtil.objectIsNullGetString(loanRow.get("loan_prod.prod_name"));//套餐类型
        String prod_no = erpUtil.objectIsNullGetString(loanRow.get("loan_info.prod_no"));//产品类型
        String cust_name = erpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_name"));//客户姓名
        String idcard = erpUtil.objectIsNullGetString(loanRow.get("loan_cust.idcard"));//客户身份证
        String cust_phone = erpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_phone"));//客户手机
        String car_plate = erpUtil.objectIsNullGetString(loanRow.get("loan_car.car_plate"));//车牌号
        String con_code = erpUtil.objectIsNullGetString(loanRow.get("loan_con.con_code"));//合同编号
        String loan_type = erpUtil.objectIsNullGetString(loanRow.get("loan_info_et.loan_type"));//贷款类型
        String rep_type = erpUtil.objectIsNullGetString(loanRow.get("loan_info_et.rep_type"));//还款方式
        String fk_date = erpUtil.objectIsNullGetString(loanRow.get("loan_info_et.loan_date"));//放款日期
        String payee_name = erpUtil.objectIsNullGetString(loanRow.get("loan_info_et.payee_name"));//收款人姓名
        String payee_bank_acct = erpUtil.objectIsNullGetString(loanRow.get("loan_info_et.payee_bank_acct"));//收款人银行卡
        String bank_dep = erpUtil.objectIsNullGetString(loanRow.get("loan_info_et.bank_dep"));//开户行
        double contract_sum = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.contract_sum"));//合同金额
        double rate_fee = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.rate_fee"));//借款利率
        double admin_exp = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.admin_exp"));//管理费
        double approve_sum = erpUtil.objectIsNullGetDouble(loanRow.get("loan_cost.approve_sum"));//审批金额
        String duetime = erpUtil.objectIsNullGetString(loanRow.get("loan_cost.duetime"));//贷款期限
        String capital_return_type = erpUtil.objectIsNullGetString(loanRow.get("loan_cost.capital_return_type"));//归本方式
        String duetime_unit = erpUtil.objectIsNullGetString(loanRow.get("loan_cost.duetime_unit"));//期限周期
        Row loanTypeRow = DB.queryRow(" select no,code_val from sys_std_code_val where code_no='LOAN_CODE_LOAN_TYPE' and no = '" + loan_type + "' ");
        Row reqTypeRow = DB.queryRow(" select no,code_val from sys_std_code_val where code_no='LOAN_CODE_REP_TYPE' and no = '" + rep_type + "' ");
        Row branchRow= DB.queryRow("select * from i_frame_sys_dept where dept_id='"+branch_no+"'");
        String rep_type_name = erpUtil.objectIsNullGetString(loanTypeRow.get("code_val"));
        String loan_type_name = erpUtil.objectIsNullGetString(reqTypeRow.get("code_val"));
        String branch_name = erpUtil.objectIsNullGetString(branchRow.get("dept_name"));
        double fk_amount = 0;
        double loan_first_fk_amount = 0;
        double loan_second_fk_amount = 0;
        Store employers = checkEmployersV5(mid_no, loan_no);
        for (Row employer : employers) {
            if (StringUtils.equals("02", employer.getString("pay_type"))) {
                loan_first_fk_amount = employer.getNumber("contribution_amount").doubleValue();
                resultMap.put("first_employer", getEmployerName(employer.getString("employers_no")));
            } else if (StringUtils.equals("03", employer.getString("pay_type"))) {
                loan_second_fk_amount = employer.getNumber("contribution_amount").doubleValue();
                resultMap.put("second_employer", getEmployerName(employer.getString("employers_no")));
            }
        }
        double qqamount = 0;
        List feeStore=clearClient.repayPlanTest(loan_no);
        for (int i = 0; i < feeStore.size(); i++) {
            Map rowMap = (Map) feeStore.get(i);
            List batches = (List) rowMap.get("batches");
            List dateList = (List) rowMap.get("dateList");
            List amountList = (List) rowMap.get("amountList");
            if (batches == null || dateList == null || amountList == null) continue;
            for (int j = 0; j < batches.size(); j++) {
                String batch = (String) batches.get(j);
                BigDecimal amount = new BigDecimal(amountList.get(j).toString());
                if ("0".equals(batch)) {
                    qqamount = BigDecimalUtil.add(qqamount,erpUtil.objectIsNullGetDouble(amount) );
                }
            }
        }
        resultMap.put("qqamount", qqamount);
        resultMap.put("branch_name", branch_name);
        resultMap.put("prod_name", prod_name);
        resultMap.put("cust_name", cust_name);
        resultMap.put("idcard", idcard);
        resultMap.put("cust_phone", cust_phone);
        resultMap.put("car_plate", car_plate);
        resultMap.put("con_code", con_code);
        resultMap.put("loan_type", loan_type);
        resultMap.put("loan_type_name", loan_type_name);
        resultMap.put("rep_type", rep_type);
        resultMap.put("rep_type_name", rep_type_name);
        resultMap.put("fk_date", fk_date);
        resultMap.put("payee_name", payee_name);
        resultMap.put("payee_bank_acct", payee_bank_acct);
        resultMap.put("bank_dep", bank_dep);
        resultMap.put("contract_sum", contract_sum);
        resultMap.put("rate_fee", rate_fee);
        resultMap.put("admin_exp", admin_exp);
        resultMap.put("duetime", duetime);
        resultMap.put("duetime_unit", duetime_unit);
        resultMap.put("fk_amount", fk_amount);
        resultMap.put("loan_first_fk_amount", loan_first_fk_amount);
        resultMap.put("loan_second_fk_amount", loan_second_fk_amount);
        resultMap.put("chuzhuren", chuzhuren);
        return resultMap;
    }

    /**
     * 得到放款方名字
     *
     * @param capital_no
     * @return
     * @throws SQLException
     */
    public static String getEmployerName(String capital_no) throws SQLException {
        String sql = "select capital_name from loan_capital where capital_no = '" + capital_no + "'";
        Row row = DB.queryRow(sql);
        return row.getStringNotNull("capital_name");
    }

    /**
     * 得到放款方
     *
     * @param mid_no
     * @param loan_no
     * @return
     * @throws Exception
     */
    public static Store checkEmployersV5(String mid_no, String loan_no) throws Exception {
        String sql = null;
        Store employers = null;
        if (Util.isNotEmpty(mid_no)) {
            sql = "select * from loan_mid_employers where loan_mid_employers.mid_no = '" + mid_no + "'";
        } else {
            sql = "select * from loan_info_employers where loan_info_employers.loan_no = '" + loan_no + "'";
        }
        employers = DB.query(sql);
        if (employers.size() == 0) {
            throw new Exception("无放款方记录，请联系信息部门检查！");
        }
        int payType02 = 0;
        int payType03 = 0;
        for (Row employer : employers) {
            if (StringUtils.equals("01", employer.getString("pay_type"))) {
                throw new Exception("放款方的放款类型存在为放款到余额，请联系信息部门检查！");
            } else if (StringUtils.equals("04", employer.getString("pay_type"))) {
                throw new Exception("放款方的放款类型存在为预放款，请联系信息部门检查！");
            } else if (StringUtils.equals("02", employer.getString("pay_type"))) {
                payType02++;
            } else if (StringUtils.equals("03", employer.getString("pay_type"))) {
                payType03++;
            }
        }
        if (payType02 > 1 || payType03 > 1) {
            throw new Exception("放款方的放款类型为02或者03的记录存在大于一条，请联系信息部门检查！");
        }
        if (payType02 == 0 && payType03 == 0) {
            throw new Exception("无正确的放款方记录，请联系信息部门检查！");
        }
        return employers;
    }



    /**
     * 得到最后一天还款日
     *
     * @param start_date
     * @param duetime_int
     * @param duetime_unit
     * @return
     * @throws
     */
    public static String getDate(String start_date, int duetime_int, String duetime_unit) throws ParseException {
        String start_date_temp = start_date;
        try {
            for (int i = 0; i < duetime_int; i++) {
                start_date_temp = getNextPaydate(start_date, start_date_temp, duetime_unit, i + 1);
            }
        } catch (Exception e) {
            log.info(Util.getStackTrace(e));
        }
        return start_date_temp;
    }

    /**
     * 得到下一期还款日
     *
     * @param start_date
     * @param temp_date
     * @param duetime_unit
     * @param index
     * @return
     * @throws ParseException
     */
    private static String getNextPaydate(String start_date, String temp_date, String duetime_unit, int index) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        // 7天
        if ("7".equals(duetime_unit)) {
            calendar.setTime(sdf.parse(temp_date));
            calendar.add(Calendar.DAY_OF_MONTH, 6);
        }
        // 15天
        else if ("15".equals(duetime_unit)) {
            calendar.setTime(sdf.parse(temp_date));
            calendar.add(Calendar.DAY_OF_MONTH, 14);
        }
        // 一个月
        else if ("30".equals(duetime_unit)) {
            calendar.setTime(sdf.parse(start_date));
            calendar.add(Calendar.MONTH, index);
        }
        return sdf.format(calendar.getTime());
    }


    /**
     * 产生放款流程，自动放款
     *
     * @param p
     * @param review_no
     * @throws Exception
     */
    public void execute_fk(Param p, String review_no) throws Exception {
        synchronized (review_no) {
            String loan_no = (String) baseDao.queryObject("select table_key_no from wf_review_biz where review_no='" + SafeUtil.safe(review_no) + "'");
            if (Util.isEmpty(loan_no)) {
                throw new Exception(BizErrorCodeEnum.B0020.getCode());
            }
            Row loan_row = erpUtil.getLoanBasicDataInfoV2(loan_no);
            if (loan_row == null) {
                throw new Exception(BizErrorCodeEnum.B0020.getCode());
            }
            checkFkReview(loan_no);
            loanCheckService.check_contribution_amount(loan_no);
            baseDao.execute("update loan_info set loan_state='1' where loan_no='" + loan_no + "'");
            addPayWorkFlow(p, review_no);
        }
    }

    /**
     * 检查数据
     *
     * @param loan_no
     * @throws Exception
     */
    public void checkFkReview(String loan_no) throws Exception {
        loanCheckService.check_otherNewFk(loan_no);
        loanCheckService.check_otherUnfinishedFk(loan_no);
    }


    /**
     * 追加放款流程
     *
     * @param p
     * @throws Exception
     */
    public void addPayWorkFlow(Param p, String review_no) throws Exception {
        String oper_user_id = p.getLoginUserNo();
        String oper_user_no = p.getLoginUserNo();
        // 贷款NO
        String loan_no = (String) baseDao.queryObject("select table_key_no from wf_review_biz where review_no='" + SafeUtil.safe(review_no) + "'");
        // 无贷款NO时，流程异常
        if (Util.isEmpty(loan_no)) {
            throw new Exception(BizErrorCodeEnum.B0020.getCode());
        }
        // 合同编号
        String con_code = (String) baseDao.queryObject("select con_code from loan_con where loan_no='" + SafeUtil.safe(loan_no) + "'");
        String sql = "";

        // 验证是否已生成放款流程(排除预付款)
        sql = "select loan_mid_pay.pay_no "
                + "from loan_mid_pay  "
                + "left join loan_mid  "
                + "on loan_mid_pay.mid_no=loan_mid.mid_no and loan_mid.status='1'  "
                + "left join loan_mid_employers "
                + "on loan_mid_employers.MID_NO = loan_mid_pay.mid_no "
                + "where loan_mid_employers.pay_type != '04' and loan_mid_pay.status='1' and loan_mid.loan_no='" + SafeUtil.safe(loan_no) + "' ";
        if (baseDao.query(sql).size() > 0) {
            throw new Exception(BizErrorCodeEnum.B0004.getCode());
        }
        // 查询一次放款到余额
        Store employers = baseDao.query("select * from loan_info_employers where status='1' and loan_no='" + SafeUtil.safe(loan_no) + "' and pay_type='01'");
        if (employers.size() > 1) {
            throw new Exception(BizErrorCodeEnum.F0036.getCode());
        }
        if (employers != null && employers.size() == 1) {
            // 01 一次放款到余额
            startFlow("2", loan_no, con_code, oper_user_id, oper_user_no, employers.get(0), p);
        }
        // 查询 一次放款到卡
        employers = baseDao.query("select * from loan_info_employers where status='1' and loan_no='" + SafeUtil.safe(loan_no) + "' and pay_type='02'");
        if (employers.size() > 1) {
            throw new Exception(BizErrorCodeEnum.F0036.getCode());
        }
        if (employers != null && employers.size() == 1) {
            // 02 一次放款到卡
            startFlow("2", loan_no, con_code, oper_user_id, oper_user_no, employers.get(0), p);
        }
        // 查询二次放款到卡
        employers = baseDao.query("select * from loan_info_employers where status='1' and loan_no='" + SafeUtil.safe(loan_no) + "' and pay_type='03' ");
        if (employers.size() > 1) {
            throw new Exception(BizErrorCodeEnum.F0036.getCode());
        }
        // 03 二次放款到卡
        if (employers != null && employers.size() == 1) {
            startFlow("3", loan_no, con_code, oper_user_id, oper_user_no, employers.get(0), p);
        }
    }


    /**
     * 根据资方放款数据发起流程
     *
     * @param templet_no
     * @param loan_no
     * @param con_code
     * @param oper_user_id
     * @param oper_user_no
     * @return
     * @throws Exception
     */
    public void startFlow(String templet_no,final String loan_no, final String con_code, final String oper_user_id, final String oper_user_no, final Row employer, final Param p) throws Exception {
        Map<String, Object> map = flow.beforeInitAllSubFlow();
        log.info(p);
//        Param param = JSON.parseObject(JSONObject.toJSONString(p), Param.class);
        flow.initWorkFlow(templet_no, null, p);
        log.info("templet_no:" + templet_no);
        Row retRow = flow.addWorkFlow();
        log.info("flow:" + flow);
        log.info("retRow:" + retRow);
        startFlowProcess(retRow,loan_no, con_code, oper_user_id, employer, p);
        flow.reInitHandler();
        //二次放款不通过
        if (StringUtils.equals(templet_no, "3")) {
            return;
        } else {//锁定系统管理为处理者
            flow.lockSySHandler();
        }
        String sql = "select loan_type from loan_info_et where loan_info_et.status='1' and loan_info_et.loan_no='" + SafeUtil.safe(loan_no) + "' ";
        String loan_type = (String) baseDao.queryObject(sql);
        if ("04".equals(loan_type) || "03".equals(loan_type)) {
            sql = "select loan_date from loan_info_et where loan_no='" + SafeUtil.safe(loan_no) + "'";
            String fk_date = baseDao.queryObject(sql).toString();
            sql = "update "
                    + "loan_info_et,loan_mid_pay,loan_mid "
                    + "set "
                    + "loan_info_et.fk_date='" + SafeUtil.safe(fk_date) + "',"
                    + "loan_mid_pay.fk_date='" + SafeUtil.safe(fk_date) + "' "
                    + "where "
                    + "loan_info_et.loan_no='" + SafeUtil.safe(loan_no) + "' and loan_info_et.status='1' "
                    + "and loan_mid.loan_no='" + SafeUtil.safe(loan_no) + "' and loan_mid.status='1' "
                    + "and loan_mid_pay.mid_no=loan_mid.mid_no and loan_mid_pay.status='1' ";
            baseDao.execute(sql);
            flow.reviewPrivilegePass();
            flow.closeAllSubFlow(map);
        }
    }


    /**
     * 放款业务数据
     * @param bizData
     * @param loan_no
     * @param con_code
     * @param oper_user_id
     * @param employer
     * @param p
     * @throws Exception
     */
    public void startFlowProcess(Row bizData, String loan_no, String con_code, String oper_user_id,  Row employer, Param p) throws Exception {
        String sql;
        // 新流程业务数据
        Row data = (Row) bizData.get("data");
        log.info("data:" + data);
        String review_no = data.getString("review_no");
        log.info("review_no:" + review_no);
        String mid_no_sql = "select table_key_no from wf_review_biz where review_no='" + review_no + "'";
        sql = "update loan_mid set "
                + "loan_no='" + SafeUtil.safe(loan_no) + "',"
                + "con_code='" + SafeUtil.safe(con_code) + "' "
                + "where mid_no =(select table_key_no from wf_review_biz where review_no='" + review_no + "')";
        baseDao.execute(sql);
        sql = "insert into loan_mid_employers(no,employers_no,mid_no,contribution_amount,pay_type,oper_user_id,oper_time,status) "
                + "select "
                + "'" + SafeUtil.safe(Util.uuid()) + "',"
                + "employers_no,"
                + "(select table_key_no from wf_review_biz where review_no='" + review_no + "'),"
                + "contribution_amount,pay_type,"
                + "'" + SafeUtil.safe(oper_user_id) + "',"
                + "'" + Util.format(new Date(), "yyyy-MM-dd HH:mm:ss") + "','1' "
                + "from loan_info_employers "
                + "where no='" + SafeUtil.safe(employer.getString("no")) + "' ";
        baseDao.execute(sql);
    }
}
