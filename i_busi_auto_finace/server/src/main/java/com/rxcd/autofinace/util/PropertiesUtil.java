package com.rxcd.autofinace.util;

import java.io.InputStream;
import java.util.Properties;

/**
 * Properties 文件配置工具
 * 
 */
public class PropertiesUtil {
	// 配计文件
	private static Properties properties;
	
	private static String path = "config.properties";

	static {
		properties = new Properties();
		InputStream is = PropertiesUtil.class.getClassLoader().getResourceAsStream(path);
		try {
			properties.load(is);
			is.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 通过key值得对应的value
	 * 
	 * @param key
	 * @return
	 */
	public static String getValue(String key) {
		return properties.getProperty(key);
	}
}
