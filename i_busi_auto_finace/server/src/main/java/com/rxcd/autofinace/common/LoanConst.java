/**
 * 版权所有：版权所有：重庆易房网络科技股份有限公司
 * Chongqing YiFang Network Technology Co. LTD.  All Rights Reserved
 * 创建者：HeSenLin   创建时期：  2016年5月10日
 */
package com.rxcd.autofinace.common;

/**
 *
 */
public class LoanConst
{
	/** 默认评估价 */
	public static final double PGJ_DEFAULT = 9999;
	
}
