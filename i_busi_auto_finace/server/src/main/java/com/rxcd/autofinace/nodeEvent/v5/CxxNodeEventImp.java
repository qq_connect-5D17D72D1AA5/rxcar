/**
 * 版权所有：版权所有：重庆易房网络科技股份有限公司
 * Chongqing YiFang Network Technology Co. LTD.  All Rights Reserved
 * 创建者：HeSenLin   创建时期：  2016年7月14日
 */
package com.rxcd.autofinace.nodeEvent.v5;


import com.rxcd.autofinace.service.LoanAssAutoService;
import com.rxcd.autofinace.service.LoanService;
import com.rxcd.autofinace.util.RxErpUtil;
import org.appframe.common.BigDecimalUtil;
import org.appframe.wf.db.Row;
import org.appframe.wf.engine.AbstractWorkFlowNodeEvent;
import org.appframe.wf.util.ErpUtil;
import org.appframe.wf.util.Param;
import org.appframe.wf.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 车信息节点事件
 */
@Component
public class CxxNodeEventImp extends AbstractWorkFlowNodeEvent {
    @Autowired
    private LoanService loanService;
    @Autowired
    private LoanAssAutoService loanAssAutoService;
    @Autowired
    private RxErpUtil erpUtil;

    /**
     * 流程节点启动时调用
     *
     * @param flow_node_no
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowNodeStart(String flow_node_no, Param p) throws Exception {

    }

    @Override
    public void workFlowNodeFinish(String flow_node_no, Param p) throws Exception {
        checkTempletFlowNo("1");

        String loanNo = getTableKeyNo();

        //出车辆品牌
        loanService.queryCarBrandFirst(loanNo);
        //出违章金额
        loanService.queryCarOffencesMoney(loanNo);
        //出查博士
        loanService.queryCarCBSFirst(loanNo);

        //给个默认公里数和默认车型
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loanNo);
        String inireg_date = ErpUtil.objectIsNullGetString(loanRow.get("loan_car.inireg_date"));
        int months = Util.monthsBetween(inireg_date, Util.format(new Date()));
        int mileage = (int) BigDecimalUtil.round(months * 1000 + Math.random() * 1000, 0);
        String sql = "select type_no from loan_car_brand_type where loan_no='" + loanNo + "' limit 1";
        String type_no = ErpUtil.objectIsNullGetString(baseDao.queryObject(sql));
        baseDao.execute("update loan_car set brand_type='" + type_no + "',mileage='" + mileage + "' where loan_no='" + loanNo + "'");

        loanService.queryCarPriceFirst(loanNo);

        //调用评估，得到车况初报告、初价、初价(真实)
        loanAssAutoService.autoAss(loanNo, LoanAssAutoService.BRANCH_SCORE);
        loanAssAutoService.autoAss(loanNo, LoanAssAutoService.BRANCH_INIT_PRICE);
        loanAssAutoService.autoAss(loanNo, LoanAssAutoService.TURE_INIT_PRICE);

        loanService.updateMaxApproveSum(loanNo);
        loanService.updateApproveSumByMaxApproveSumAndCarPrice(loanNo);
        loanService.updateDepositSum(loanNo);
        loanService.updateConAmount(loanNo);
        loanService.updateAdvanceRateSum(loanNo);

    }

    /**
     * 流程节点驳回时回调方法
     *
     * @param flow_node_no
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowNodeOverrule(String flow_node_no, Param p) throws Exception {

    }

    @Override
    public void workFlowNodeVie(String flow_node_no, Param p) throws Exception {

    }
}
