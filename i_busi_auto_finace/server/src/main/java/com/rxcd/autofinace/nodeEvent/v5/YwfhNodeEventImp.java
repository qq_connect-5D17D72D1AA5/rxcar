package com.rxcd.autofinace.nodeEvent.v5;

import com.fida.clearing.impl.ChannelClientImpl;
import com.rxcd.autofinace.service.LoanCheckServiceImpl;
import com.rxcd.autofinace.util.RxErpUtil;
import org.appframe.wf.db.Row;
import org.appframe.wf.engine.AbstractWorkFlowNodeEvent;
import org.appframe.wf.service.WfParamUtil;
import org.appframe.wf.util.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;


/**
 * 业务复核
 */
@Component
public class YwfhNodeEventImp extends AbstractWorkFlowNodeEvent {

    @Autowired
    private WfParamUtil paramUtil;
    @Autowired
    private LoanCheckServiceImpl checkService;
    @Autowired
    private RxErpUtil erpUtil;
    @Autowired
    private ChannelClientImpl channelClientImpl;

    @Override
    public void workFlowNodeFinish(String flow_node_no, Param p) throws Exception {
        checkTempletFlowNo("1");
        String loan_no = getTableKeyNo();
//        checkService.check_branch_fk(loan_no);
        checkService.check_unfinishAdvancePayWorkFlow(loan_no);
        checkService.check_exist_on_pay_card_fk_review(loan_no);
        checkService.checkEmployerInfo(loan_no);
        checkService.check_contract_sum(loan_no);
        String clearing_ordinary = String.valueOf(baseDao.queryObject("select clearing_ordinary from loan_cust where loan_no = '"+loan_no+"'"));
        Map<String,Object> result_map = channelClientImpl.dkSignRequest("4bc199af-044f-497f-a6ea-c7657f2d0c08",null,clearing_ordinary,null,false);
        if(!"1".equals(result_map.get("resultCode"))){
            throw new Exception("客户代扣签约未成功，请联系信息部");
        }
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String duetime = erpUtil.objectIsNullGetString(loanRow.get("loan_cost.duetime"));
        String rep_type = erpUtil.objectIsNullGetString(loanRow.get("loan_info_et.rep_type"));
        //24期等额等息更新资方为洋葱先生
        if ("24".equals(duetime) && "01".equals(rep_type)){
            baseDao.execute("update loan_info_et set intentional_capital_no = '02' where loan_no ='"+loan_no+"'");
        }
    }

    @Override
    public void workFlowNodeStart(String flow_node_no, Param p) throws Exception {
        checkTempletFlowNo("1");
    }

    @Override
    public void workFlowNodeOverrule(String flow_node_no, Param p) throws Exception {
        checkTempletFlowNo("1");
    }

    @Override
    public void workFlowNodeVie(String flow_node_no, Param p) throws Exception {
        checkTempletFlowNo("1");
    }
}
     