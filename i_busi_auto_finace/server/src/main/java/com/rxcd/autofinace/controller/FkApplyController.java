/**
 * 创建者：ZhaoSheng 创建时期： 2013-07-15
 */
package com.rxcd.autofinace.controller;

import com.rxcd.autofinace.service.FkService;
import org.appframe.wf.db.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 放款申请单
 */
@RestController
@RequestMapping("/")
public class FkApplyController {

	private static final long serialVersionUID = -894766807549866915L;

	@Autowired
	private FkService fkService;

	@RequestMapping("/loan_common_FkApply")
	public Row execute(@RequestParam String loan_no, @RequestParam String mid_no) throws Exception {
		Map<String, Object> map = fkService.getFkInfoV5(loan_no, mid_no);
		Row ret = new Row();
		ret.put("type", "fkInfo");
		ret.put("data", map);
		return ret;
	}
}