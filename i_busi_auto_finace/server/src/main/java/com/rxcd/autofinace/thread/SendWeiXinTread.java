package com.rxcd.autofinace.thread;


import com.rxcd.autofinace.servlet.Result;
import com.rxcd.autofinace.util.RxErpUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.appframe.wf.util.Param;

public class SendWeiXinTread extends Thread {

    private final static Log logger = LogFactory.getLog(SendWeiXinTread.class);

    private Param param;

    private Result result;
    private RxErpUtil rxErpUtil;


    public Param getParam() {
        return param;
    }

    public void setParam(Param param) {
        this.param = param;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }


    @Override
    public void run() {
        try {
            logger.info("发送微信错误日志线程进入");
            rxErpUtil.sendSysErrorWeiXinMessage(param, result);
        } catch (Exception e) {
            logger.error("发送微信错误日志异常=" + e.getMessage());
        } finally {
            logger.info("发送微信错误日志结束");
        }
        return;
    }

    public RxErpUtil getRxErpUtil() {
        return rxErpUtil;
    }

    public void setRxErpUtil(RxErpUtil rxErpUtil) {
        this.rxErpUtil = rxErpUtil;
    }
}
