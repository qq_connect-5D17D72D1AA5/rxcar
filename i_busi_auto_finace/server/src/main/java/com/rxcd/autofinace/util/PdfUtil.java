package com.rxcd.autofinace.util;

import com.lowagie.text.pdf.BaseFont;
import org.xhtmlrenderer.pdf.ITextFontResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

public class PdfUtil {
	
	public static void main(String[] args) throws Exception {
		String outputFile = "d:/demo_1.pdf";
		File file1 = new File(outputFile);
		if (file1.exists()) {
			return;
		}
		StringBuffer html = new StringBuffer();
		html.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
		html.append("<html xmlns=\"http://www.w3.org/1999/xhtml\">").append("<head>").append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />")
				.append("<style type=\"text/css\" mce_bogus=\"1\">body {font-family: SimSun;}</style>").append("</head>").append("<body>");
		html.append("<div>fdsafsadf支持中文！</div>");
		html.append("</body></html>");
		htmlConvertPdf(html.toString(),"C:/Windows/fonts/simsun.ttc", outputFile);
	}

	/**
	 * html 转换PDF
	 * 
	 * @param htmlContent
	 * @param outputFile
	 * @param fontPath
	 * @throws Exception
	 */
	public static void htmlConvertPdf(String htmlContent,String fontPath, String outputFile) throws Exception {
		OutputStream os = new FileOutputStream(outputFile);
		ITextRenderer renderer = new ITextRenderer();
		ITextFontResolver fontResolver = renderer.getFontResolver();
		fontResolver.addFont(fontPath, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
		StringBuffer html = new StringBuffer(htmlContent);
		renderer.setDocumentFromString(html.toString());
		renderer.layout();
		renderer.createPDF(os);
		os.close();
	}

}