package com.rxcd.autofinace.controller;

import com.alibaba.fastjson.JSONObject;
import com.rxcd.autofinace.service.JiJingClinet;
import com.rxcd.autofinace.util.RxErpUtil;
import org.apache.commons.lang.StringUtils;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 集金接口
 */
@RestController
@RequestMapping("/")
public class JingJingController {

    @Autowired
    private BaseDao baseDao;
    @Autowired
    private RxErpUtil erpUtil;
    @Autowired
    private JiJingClinet jiJingClinet;

    @RequestMapping("/JiJingShow")
    public Row execute(@RequestParam("loan_no") String loan_no, @RequestParam(required = false, value = "type") String type) throws Exception {
        if (StringUtils.isNotEmpty(loan_no)) {
            loan_no = loan_no.replaceAll("'","");
        }
        if (StringUtils.isNotEmpty(type)) {
            type = type.replaceAll("'","");
        }
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String idcard  = erpUtil.objectIsNullGetString(loanRow.get("loan_cust.idcard"));
        String cust_phone  = erpUtil.objectIsNullGetString(loanRow.get("loan_cust.cust_phone"));
        String requestNo = erpUtil.objectIsNullGetString(loanRow.get("loan_info.requestNo"));
        Row relstRow = new Row();
        if(StringUtils.equals("SQDK", type)){
            boolean b = true;
            if(StringUtils.equals("", requestNo)){
                requestNo = Util.uuid();
                String sql = "update loan_info set requestNo = '"+requestNo+"' where loan_no = '"+loan_no+"'";
                baseDao.execute(sql);
                loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
                JSONObject jsonObject = jiJingClinet.preApplyLoanAndProduct(loanRow);
                String auditStatus = jsonObject.getJSONObject("data").getString("auditStatus");
                if(!StringUtils.equals("1", auditStatus)){
                    throw new Exception(jsonObject.getString("msg"));
                }
            }
            String url = jiJingClinet.sqdkUrl(loanRow);
            relstRow.put("type", "url");
            relstRow.put("value", url);
        }else if(StringUtils.equals("queryAccountStatus", type)){
            JSONObject jsonObject = jiJingClinet.queryAccountStatus(cust_phone, idcard);
            Row row = new Row();
            row.put("code", jsonObject.getString("code"));
            row.put("msg", jsonObject.getString("msg"));
            relstRow.put("json",row);
        }else if(StringUtils.equals("uploadContract", type)){
            Map<String, String> map = jiJingClinet.getUploadMap(loanRow);
            Row row = new Row();
            for (Map.Entry<String, String> entry : map.entrySet()) {
                if(entry.getKey()!=null){
                    JSONObject jsonObject = jiJingClinet.uploadContract(entry.getKey(), entry.getValue(), requestNo);
                    row.put(entry.getValue()+":"+entry.getKey(),jsonObject.toJSONString());
                }
            }
            relstRow.put("json",row);
        }else if(StringUtils.equals("updateApplyLoan", type)){
            JSONObject jsonObject = jiJingClinet.updateApplyLoan(loanRow);
            Row row = new Row();
            row.put("code", jsonObject.getString("code"));
            row.put("msg", jsonObject.getString("msg"));
            relstRow.put("json",row);
        }else if(StringUtils.equals("queryLoanInformation", type)){
            JSONObject jsonObject = jiJingClinet.queryLoanInformation(loanRow);
            JSONObject data = JSONObject.parseObject(jsonObject.getString("data"));
            for(Map.Entry<String, Object> entry:data.entrySet()){
                relstRow.put(entry.getKey(), entry.getValue());
            }
            relstRow.set("path", "/queryLoanInformation");
        }else if(StringUtils.equals("CZ", type)){
            String url = jiJingClinet.czUrl(loanRow);
            relstRow.put("type", "url");
            relstRow.put("value", url);
        }else if(StringUtils.equals("queryUserAccount", type)){
            JSONObject jsonObject = jiJingClinet.queryUserAccount(idcard, cust_phone);
            Row row = new Row();
            row.put("code", jsonObject.getString("code"));
            row.put("msg", jsonObject.getString("msg"));
            relstRow.put("json",row);
        }else if(StringUtils.equals("cancelOrder", type)){
            JSONObject jsonObject = jiJingClinet.cancelOrder(requestNo);
            String data = erpUtil.objectIsNullGetString(jsonObject.get("data"));
            if(StringUtils.equals("1", data)){
                String sql = "update loan_info set requestNo = '' where loan_no = '"+loan_no+"'";
                baseDao.execute(sql);
            }
            Row row = new Row();
            row.put("code", jsonObject.getString("code"));
            row.put("msg", jsonObject.getString("msg"));
            relstRow.put("json",row);
        }else{
            throw new Exception("操作类型不存在");
        }
        return relstRow;
    }
}
