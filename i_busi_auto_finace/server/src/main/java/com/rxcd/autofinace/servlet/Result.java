package com.rxcd.autofinace.servlet;

import java.util.HashMap;

/**
 * Action执行的返回结果封闭类
 */
public class Result
{
	public final static String TYPE_FORWARD = "TYPE_FORWARD";

	public final static String TYPE_DIRECTE = "TYPE_DIRECTE";

	public final static String NAME_JSP = "JSP";

	// 用于判断是FORWARD还是DIRECTE
	private String type = Result.TYPE_FORWARD;

	private String name = "";

	private String url = "";

	// 消息：SESSION 1.ERROR 失效; 2.NO 被强制踢下线
	private String session_status = "";

	// 消息：是指用户操作正常范围内的信息反馈，比如"该用户名已经存在"
	private String msg = "";

	// 错误：是指用户操作范围外的问题，比如删除操作结果在后台没有得到需要删除的ID等
	private String error = "";

	private HashMap<String, Object> msgs = new HashMap<String, Object>();

	private HashMap<String, Object> errors = new HashMap<String, Object>();

	// 异常：是指在beforeExecute 或者 execute 函数调用中出现异常抛出的情况
	public boolean executeException = false;

	// 是否需要执行execute函数，默认为执行，在beforeExecute 中可以选择是否执行execute
	public boolean needRunExecute = true;

	// 是否需要执行afterExecute函数，默认为执行，在beforeExecute 和 execute 中可以选择是否执行afterExecute
	public boolean needRunAfterExecute = true;

	// 进入页面的时间（精确到毫秒）
	public long startTime;

	// 结束页面的时间（精确到毫秒）
	public long endTime;

	public String getError()
	{
		return error;
	}

	public void setError(String error)
	{
		this.error = error;
	}

	public HashMap<String, Object> getMsgs()
	{
		return msgs;
	}

	public void setMsgs(HashMap<String, Object> msgs)
	{
		this.msgs = msgs;
	}

	public HashMap<String, Object> getErrors()
	{
		return errors;
	}

	public void setErrors(HashMap<String, Object> errors)
	{
		this.errors = errors;
	}

	public final Result JSP()
	{
		return setResult(Result.TYPE_FORWARD, Result.NAME_JSP, "");
	}
	
	public final Result JSP(String jspPath)
	{
		return setResult(Result.TYPE_FORWARD, "", jspPath);
	}

	public String getSession_status()
	{
		return session_status;
	}

	public void setSession_status(String session_status)
	{
		this.session_status = session_status;
	}

	/**
	 * 将request里的attributes和url传递的参数转换为json输出<br>
	 * 如果url参数为数组，转换为的json对象也为数组。
	 * 
	 * @return json对象
	 */
	public final Result JSON()
	{
		return setResult(Result.TYPE_FORWARD, "", "/page/core/Json.jsp");
	}

	public Result setResult(String type, String name, String url)
	{
		this.type = type;
		this.name = name;
		this.url = url;

		return this;
	}

	public Result forward()
	{
		this.type = Result.TYPE_FORWARD;
		return this;
	}

	public Result directe()
	{
		this.type = Result.TYPE_DIRECTE;
		return this;
	}

	public Result name(String name)
	{
		this.name = name;
		return this;
	}

	public Result session_status(String status)
	{
		this.session_status = status;
		return this;
	}

	public Result url(String url)
	{
		this.url = url;
		return this;
	}

	public Result msg(String msg)
	{
		this.msg = msg;
		return this;
	}

	public Result msgs(String key, String value)
	{
		this.msgs.put(key, value);
		return this;
	}

	public Result error(String error)
	{
		this.error = error;
		return this;
	}

	public Result errors(String key, String value)
	{
		this.errors.put(key, value);
		return this;
	}

	/**
	 * @return the type
	 */
	public String getType()
	{
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type)
	{
		this.type = type;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @return the url
	 */
	public String getUrl()
	{
		return url;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(String url)
	{
		this.url = url;
	}

	/**
	 * @return the msg
	 */
	public String getMsg()
	{
		return msg;
	}

	/**
	 * @param msg
	 *            the msg to set
	 */
	public void setMsg(String msg)
	{
		this.msg = msg;
	}

}
