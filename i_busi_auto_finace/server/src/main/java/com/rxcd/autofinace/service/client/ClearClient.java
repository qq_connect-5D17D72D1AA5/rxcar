package com.rxcd.autofinace.service.client;

import com.rxcd.autofinace.model.ClearBean;

import java.util.List;
import java.util.Map;

/**
 * 清算系统Client
 */
public interface ClearClient {

    /**
     * 获取费用规则
     * @param bizNo
     * @return
     * @throws Exception
     */
    public Map getFeeRule(String bizNo) throws Exception;

    /**
     * 费用试算
     * @param bizNo
     * @return
     */
    public List repayPlanTest(String bizNo) throws Exception;

    /**
     * 费用试算
     * @param clearBean
     * @return
     * @throws Exception
     */
    public List repayPlanTestByCleanBean(ClearBean clearBean) throws Exception;

    /**
     * 还款应付试算
     * @param bizNo
     * @return
     */
    public List createRepayPayableTest(String bizNo) throws Exception;

    /**
     * 还款应付试算
     * @param clearBean
     * @return
     * @throws Exception
     */
    public List createRepayPayableTestByClearBean(ClearBean clearBean) throws Exception;

    /**
     * 生成客户账户(普通账户+冻结账户)
     * @param bizNo
     * @return
     * @throws Exception
     */
    public void createCustAccount(String bizNo) throws Exception;

    /**
     * 生成网点账户(保证金账户+冻结账户+收入账户)
     * @param bizNo
     * @return
     * @throws Exception
     */
    public void createBranchAccount(String bizNo) throws Exception;


    /**
     * 生成大区代理账户(保证金账户+冻结账户+收入账户)
     * @param bizNo
     * @return
     * @throws Exception
     */
    public void createAreaAccount(String bizNo) throws Exception;


    /**
     * 生成应收应付（付款清分）
     * @param bizNo
     * @return
     */
    public List createPayPayable(String bizNo) throws Exception;


    /**
     * 生成应收应付（还款清分）
     * @param bizNo
     * @return
     */
    public List createRepayPayable(String bizNo) throws Exception;

    /**
     * 生成一次放款账单
     * @param bizNo
     * @return
     */
    public List createPay02Bill(String bizNo) throws Exception;

    /**
     * 生成二次放款账单
     * @param bizNo
     * @return
     */
    public List createPay03Bill(String bizNo) throws Exception;

    /**
     * 账单结算(生成实付)
     * @param bizNo
     * @return
     * @throws Exception
     */
    public Map<String,Object> settleBill(String bizNo) throws Exception;

    /**
     * 创建提现账单
     * @return
     * @throws Exception
     */
    public List createWithdrawCashBillWithBill(Map<String,Object> bill) throws Exception;

}
