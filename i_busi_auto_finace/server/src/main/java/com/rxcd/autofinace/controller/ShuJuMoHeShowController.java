/**
 * 创建者：liuchang 创建时期： 2013-07-15
 */
package com.rxcd.autofinace.controller;


import org.appframe.bigdata.ShuJuMoHeClinet2;
import org.appframe.wf.db.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 数据魔盒H5掉用
 */
@RestController
@RequestMapping("/")
public class ShuJuMoHeShowController {

    @Autowired
    private ShuJuMoHeClinet2 shuJuMoHeClinet;

    /**
     * 调用数据魔盒
     *
     * @throws Exception
     */
    @RequestMapping("/loan_common_ShuJuMoHeShow")
    public Row execute(@RequestParam("loan_no") String loan_no) {
        Row row = new Row();
        String shuju_url = shuJuMoHeClinet.applyQueryCarrier(loan_no);
        row.put("type", "url");
        row.put("value", shuju_url);
        return row;
    }
}