package com.rxcd.autofinace.nodeEvent.v5;


import com.rxcd.autofinace.service.LoanFraudAutoService;
import com.rxcd.autofinace.service.LoanRiskcontrolAutoService;
import org.appframe.wf.engine.AbstractWorkFlowNodeEvent;
import org.appframe.wf.util.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 系统中审
 */
@Component
public class XtzsNodeEventImp extends AbstractWorkFlowNodeEvent {

    @Autowired
    private LoanFraudAutoService loanFraudAutoService;
    @Autowired
    private LoanRiskcontrolAutoService loanRiskcontrolAutoService;

    /**
     * 流程节点启动时调用
     *
     * @param flow_node_no
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowNodeStart(String flow_node_no, Param p) throws Exception {

    }

    @Override
    public void workFlowNodeFinish(String flow_node_no, Param p) throws Exception {
        checkTempletFlowNo("1");
        String loanNo = getTableKeyNo();
        loanRiskcontrolAutoService.getMidRiskcontrol(loanNo);
//        loanFraudAutoService.getMidFraudcontrol(loanNo);
    }

    /**
     * 流程节点驳回时回调方法
     *
     * @param flow_node_no
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowNodeOverrule(String flow_node_no, Param p) throws Exception {

    }

    @Override
    public void workFlowNodeVie(String flow_node_no, Param p) throws Exception {

    }

}
