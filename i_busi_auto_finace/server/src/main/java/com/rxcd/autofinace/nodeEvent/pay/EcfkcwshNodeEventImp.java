/**
 * 版权所有：版权所有：重庆易房网络科技股份有限公司
 * Chongqing YiFang Network Technology Co. LTD.  All Rights Reserved
 * 创建者：HeSenLin   创建时期：  2016年7月14日
 */
package com.rxcd.autofinace.nodeEvent.pay;


import com.rxcd.autofinace.util.RxErpUtil;
import org.appframe.wf.db.Row;
import org.appframe.wf.engine.AbstractWorkFlowNodeEvent;
import org.appframe.wf.util.ErpUtil;
import org.appframe.wf.util.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 二次付款财务审核流程节点事件
 */
@Component
public class EcfkcwshNodeEventImp extends AbstractWorkFlowNodeEvent {
    @Autowired
    private RxErpUtil erpUtil;

    /**
     * 流程节点启动时调用
     *
     * @param flow_node_no
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowNodeStart(String flow_node_no, Param p) throws Exception {

    }

    @Override
    public void workFlowNodeFinish(String flow_node_no, Param p) throws Exception {
        String mid_no = getTableKeyNo();

        Row midRow = baseDao.queryRow("select * from loan_mid  where mid_no='" + mid_no + "'");
        String loan_no = ErpUtil.objectIsNullGetString(midRow.get("loan_no"));

        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        String templet_flow_no = ErpUtil.objectIsNullGetString(loanRow.get("wf_review.templet_flow_no"));
        if ("900".equals(templet_flow_no) || "9001".equals(templet_flow_no)) {
            baseDao.execute("update loan_mid_pay set is_pay='YES' where mid_no='" + mid_no + "'");
        }
    }

    /**
     * 流程节点驳回时回调方法
     *
     * @param flow_node_no
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowNodeOverrule(String flow_node_no, Param p) throws Exception {

    }

    @Override
    public void workFlowNodeVie(String flow_node_no, Param p) throws Exception {

    }

}
