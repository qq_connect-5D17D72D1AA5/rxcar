package com.rxcd.autofinace.service;


import com.rxcd.autofinace.common.BizErrorCodeEnum;
import com.rxcd.autofinace.service.client.ClearClient;
import com.rxcd.autofinace.util.RxErpUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.appframe.common.BigDecimalUtil;
import org.appframe.wf.db.Row;
import org.appframe.wf.db.Store;
import org.appframe.wf.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 费用单服务类
 *
 * @author hubin
 */
@Component
public class CostService {

    private final static Log log = LogFactory.getLog(CostService.class);

    @Autowired
    private RxErpUtil erpUtil;

    @Autowired
    private ClearClient clearClient;

    /**
     * 得到费用项目
     * @param loan_no
     * @return
     * @throws Exception
     */
    public Row costSheet(String loan_no) throws Exception{
        Row retRow=new Row();
        Row loanRow = erpUtil.getLoanBasicDataInfoV2(loan_no);
        if (loanRow == null) {
            throw new Exception(BizErrorCodeEnum.B0020.getCode());
        }
        String prod_no = loanRow.getStringNotNull("loan_info.prod_no");
        if(StringUtils.isEmpty(prod_no)){
            throw new Exception(BizErrorCodeEnum.T1086.getCode());
        }
        String loan_date = loanRow.getStringNotNull("loan_info_et.loan_date");
        if (StringUtils.isEmpty(loan_date)) {
            throw new Exception(BizErrorCodeEnum.T1086.getCode());
        }
        String duetime = loanRow.getStringNotNull("loan_cost.duetime");
        if (StringUtils.isEmpty(duetime)) {
            throw new Exception(BizErrorCodeEnum.T1087.getCode());
        }
        String duetime_unit = loanRow.getStringNotNull("loan_cost.duetime_unit");
        if (StringUtils.isEmpty(duetime_unit)) {
            throw new Exception(BizErrorCodeEnum.T1088.getCode());
        }
        String rep_type = loanRow.getStringNotNull("loan_info_et.rep_type");
        if (StringUtils.isEmpty(rep_type)) {
            throw new Exception(BizErrorCodeEnum.T1089.getCode());
        }
        String capital_return_type = loanRow.getStringNotNull("loan_cost.capital_return_type");
        if (StringUtils.isEmpty(capital_return_type)) {
            throw new Exception(BizErrorCodeEnum.T1090.getCode());
        }
        double contract_sum = loanRow.getDoubleNotNull("loan_cost.contract_sum");
        if (contract_sum <= 0) {
            throw new Exception(BizErrorCodeEnum.T1091.getCode());
        }
        String con_code = loanRow.getStringNotNull("loan_con.con_code");
        retRow.put("con_code", con_code);
        retRow.put("prod_no", prod_no);    //产品
        retRow.put("cust_name", loanRow.getStringNotNull("loan_cust.cust_name"));    //客户姓名
        retRow.put("bank_dep", loanRow.getStringNotNull("loan_info_et.bank_dep"));    //开户行
        retRow.put("contract_sum", BigDecimalUtil.decimal(contract_sum, 2));    //贷款金额
        retRow.put("duetime_val", duetime);  //贷款期限
        retRow.put("rate_val", loanRow.getStringNotNull("loan_cost.rate_fee"));       //利息
        String fk_date=loanRow.getStringNotNull("loan_info_et.fk_date");
        retRow.put("loan_date", loan_date);    //借款日期
        if(Util.isNotEmpty(fk_date)){
            retRow.put("loan_date", fk_date);
        }
        List feeStore=clearClient.repayPlanTest(loan_no);
        Store list = new Store();
        Store feeList=new Store();
        Store depositStore = new Store();
        BigDecimal qqamount = new BigDecimal(0);
        for(int i=0;i<feeStore.size();i++){
            boolean flag = false;
            double sum = 0;
            Map rowMap = (Map)feeStore.get(i);
            if(rowMap == null || rowMap.get("feeSubjectLabel") == null || rowMap.get("feeSubject") == null){
                continue;
            }
            String feeSubjectLabel = (String)rowMap.get("feeSubjectLabel");//费用科目名称
            String feeSubject = (String)rowMap.get("feeSubject");
            if(StringUtils.equals("1",String.valueOf(rowMap.get("resultCode")))){
                Row feeRow=new Row();
                feeRow.put("feeSubjectLabel",feeSubjectLabel);
                feeRow.put("feeSubject",feeSubject);
                feeList.add(feeRow);
            }else{
                continue;
            }
            List batches = (List) rowMap.get("batches");
            List dateList = (List)rowMap.get("dateList");
            List amountList =  (List)rowMap.get("amountList");
            if(batches == null || dateList == null || amountList == null) continue;
            for(int k=0;k<amountList.size();k++){
                sum = sum + Double.valueOf(amountList.get(k).toString());
                if(k==amountList.size()-1 && sum==0.0d){
                    flag =true;
                    break;
                }
            }
            if(false == flag) {
                for (int j = 0; j<batches.size(); j++){
                    String batch = (String)batches.get(j);
                    BigDecimal amount = new BigDecimal(amountList.get(j).toString());
                    String date = Util.format(Util.parseDate((String)dateList.get(j)));
                    if("0".equals(batch)){
                        Row fee = new Row();
                        qqamount = qqamount.add(amount);
                        fee.put("amount", amount);
                        fee.put("name", feeSubjectLabel);
                        feeStore.add(fee);
                    }
                    Row r = null;
                    int index = Integer.parseInt(batch);
                    if(list.size()>index){
                        r = list.get(index);
                    }
                    if(r == null){
                        r = new Row();
                    }
                    r.put("num",batch);
                    r.put("day",date);
                    r.put(feeSubject+"_name",feeSubjectLabel);
                    BigDecimal price = r.get("price")==null? new BigDecimal("0"):(BigDecimal)r.get("price");
                    r.put("price", price.add(amount));
                    r.put(feeSubject, amount);
                    if(list.size()>index){
                        list.set(index,r);
                    }else{
                        for(int m = list.size(); m <= index; m ++){
                            if(m == index){
                                list.add(r);
                            }else{
                                Row tempR = new Row();
                                list.add(tempR);
                            }
                        }
                    }
                }
            }
        }
        if (feeStore.size() % 2 == 1) {
            feeStore.add(new Row());
        }
        if (depositStore.size() % 2 == 1) {
            depositStore.add(new Row());
        }
        //计算提现金额=贷款金额-总费用-利息 利息已变成费用类型
        Double totle_price = BigDecimalUtil.sub(contract_sum, qqamount.doubleValue());
        //查询还款方
        Row hkf_row = erpUtil.getHkf(loan_no);
        retRow.put("feeList",feeList,true);
        retRow.put("list", list);
        retRow.put("feeStoreList", feeStore);
        retRow.put("qqamount", BigDecimalUtil.round(qqamount.doubleValue(), 2));
        retRow.put("totle_price", BigDecimalUtil.round(totle_price, 2));
        retRow.put("fhphone", "02363050505");
        retRow.put("approve_sum", BigDecimalUtil.round(loanRow.getDoubleNotNull("loan_cost.approve_sum"), 2));
        retRow.put("hkf_row", hkf_row);
        return retRow;
    }


}