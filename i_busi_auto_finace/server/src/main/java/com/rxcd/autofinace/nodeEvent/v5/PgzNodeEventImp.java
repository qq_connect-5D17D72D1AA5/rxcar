package com.rxcd.autofinace.nodeEvent.v5;


import com.rxcd.autofinace.service.LoanAssAutoService;
import com.rxcd.autofinace.service.LoanService;
import org.appframe.wf.engine.AbstractWorkFlowNodeEvent;
import org.appframe.wf.service.WfParamUtil;
import org.appframe.wf.util.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 评估照
 *
 * @author Yuan_Song, HuBin
 */
@Component
public class PgzNodeEventImp extends AbstractWorkFlowNodeEvent {
    @Autowired
    private WfParamUtil paramUtil;
    @Autowired
    private LoanService loanService;
    @Autowired
    private LoanAssAutoService loanAssAutoService;


    @Override
    public void workFlowNodeStart(String flow_node_no, Param p) throws Exception {
        checkTempletFlowNo("1");
        String loanNo = getTableKeyNo();
        loanService.initAssInfo(loanNo);
    }


    @Override
    public void workFlowNodeFinish(String flow_node_no, Param p) throws Exception {
        checkTempletFlowNo("1");
        String loanNo = getTableKeyNo();
        //出接口终价
        loanService.queryCarPriceFinal(loanNo);
        //出查博士
        loanService.queryCarCBSFirst(loanNo);
        //自动评估，得到汽车终况、大数据终价、真实终价
        loanAssAutoService.autoAss(loanNo, LoanAssAutoService.TRUE_SCORE);
        loanAssAutoService.autoAss(loanNo, LoanAssAutoService.BRANCH_FINAL_PRICE);
        loanAssAutoService.autoAss(loanNo, LoanAssAutoService.TRUE_FINAL_PRICE);
        //触发变动最大审批金额
        loanService.updateMaxApproveSum(loanNo);
        //触发变动实际审批金额
        loanService.updateApproveSumByMaxApproveSumAndCarPrice(loanNo);
        //触发提额变动
        loanService.updateDepositSum(loanNo);
        //触发合同金额重新生成
        loanService.updateConAmount(loanNo);
        //触发重新计算预收管理费
        loanService.updateAdvanceRateSum(loanNo);
    }

    /**
     * 流程节点驳回时回调方法
     *
     * @param flow_node_no
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowNodeOverrule(String flow_node_no, Param p) throws Exception {

    }

    @Override
    public void workFlowNodeVie(String flow_node_no, Param p) throws Exception {

    }
}
