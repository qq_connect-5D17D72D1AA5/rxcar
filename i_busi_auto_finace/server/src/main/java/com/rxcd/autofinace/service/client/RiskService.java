package com.rxcd.autofinace.service.client;

import org.appframe.wf.db.Row;

/**
 * 风控client
 * @author yeqingxin
 * @date 2018/12/03
 */
public interface RiskService {
    /**
     * 获取初评风控得分
     * @param loanNo
     * @param pageType
     * @return String
     */
    public Row getFirstRiskControl(String loanNo, String pageType) throws Exception;

    /**
     * 获取初评风控得分
     * @param loanNo
     * @param pageType
     * @return String
     */
    public Row getMidRiskControl(String loanNo, String pageType) throws Exception;

    /**
     * 获取终评风控得分
     * @param loanNo
     * @param pageType
     * @return String
     */
    public Row getFinalRiskControl(String loanNo, String pageType) throws Exception;

    /**
     * 获取车辆得分
     * @param loanNo
     * @param pageType
     * @return String
     */
    public Row getScore(String loanNo, String pageType) throws Exception;

    /**
     * 获取车辆真实得分
     * @param loanNo
     * @param pageType
     * @return String
     */
    public Row getTrueScore(String loanNo, String pageType) throws Exception;

    /**
     * 获取车辆初价
     * @param loanNo
     * @param pageType
     * @return String
     */
    public Row getInitialPrice(String loanNo, String pageType) throws Exception;

    /**
     * 获取车辆真实初价
     * @param loanNo
     * @param pageType
     * @return String
     */
    public Row getTrueInitialPrice(String loanNo, String pageType) throws Exception;

    /**
     * 获取车辆终价
     * @param loanNo
     * @param pageType
     * @return String
     */
    public Row getFinalPrice(String loanNo, String pageType) throws Exception;

    /**
     * 获取车辆真实终价
     * @param loanNo
     * @param pageType
     * @return String
     */
    public Row getTrueFinalPrice(String loanNo, String pageType) throws Exception;

    /**
     * 获取维修报告
     * @param loanNo
     * @param pageType
     * @return String
     */
    public Row getRepairReport(String loanNo, String pageType) throws Exception;

    /**
     * 获取第三方报告
     * @param loanNo
     * @param pageType
     * @return String
     */
    public Row getThirdPartyReport(String loanNo, String pageType) throws Exception;

    /**
     * 获取出险报告
     * @param loanNo
     * @param pageType
     * @return String
     */
    public Row getAccidentReport(String loanNo, String pageType) throws Exception;

    /**
     * 生成运营商报告
     * @param loanNo
     * @return Row
     */
    public Row buildOperatorReport(String loanNo, String pageType) throws Exception;

    /**
     * 获取运营商报告
     * @param loanNo
     * @param pageType
     * @return String
     */
    public Row getOperatorReport(String loanNo, String pageType) throws Exception;
}
