/**
 * 版权所有：版权所有：重庆易房网络科技股份有限公司
 * Chongqing YiFang Network Technology Co. LTD.  All Rights Reserved
 * 创建者：HeSenLin   创建时期：  2016年6月2日
 */
package com.rxcd.autofinace.flowEvent;

import com.rxcd.autofinace.service.MidService;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.engine.AbstractWorkFlowEvent;
import org.appframe.wf.service.ReviewService;
import org.appframe.wf.service.WfParamUtil;
import org.appframe.wf.util.ErpUtil;
import org.appframe.wf.util.Param;
import org.appframe.wf.util.SafeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 放款流程结束后生成应收应付
 */
@Component
public class FkWorkFlowEventImp extends AbstractWorkFlowEvent {

    @Autowired
    private WfParamUtil paramUtil;
    @Autowired
    private BaseDao baseDao;
    @Autowired
    private MidService midService;
    @Autowired
    private ReviewService reviewService;

    /**
     * 流程启动时调用
     *
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowStart(Param p) throws Exception {

    }

    @Override
    public void workFlowFinish(Param p) throws Exception {
        Row reviewObject = (Row) paramUtil.get("reviewObject");
        String review_no = reviewObject.getString("review_no");
        String sql;
        //1、生成应收应付（还款计划）（一次放款到卡）
        Row bizRow = reviewService.getReviewBizByReviewNo(review_no);
        String mid_no = SafeUtil.safe(bizRow.getString("table_key_no"));
        sql="select * from loan_mid_employers where mid_no='"+mid_no+"' and status='1'";
        Row employer=baseDao.queryRow(sql);
        String pay_type=ErpUtil.objectIsNullGetString(employer.get("pay_type"));
//      放款应付记录
        midService.computePayable(mid_no);
//      本次放款清算
        midService.excutePay(mid_no);
    }

    /**
     * 流程废弃时回调方法
     *
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowDiscard(Param p) throws Exception {

    }
}
