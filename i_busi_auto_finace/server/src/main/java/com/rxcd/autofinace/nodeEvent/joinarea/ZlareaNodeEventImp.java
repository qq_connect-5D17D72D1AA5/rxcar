package com.rxcd.autofinace.nodeEvent.joinarea;


import com.rxcd.autofinace.common.BizErrorCodeEnum;
import com.rxcd.autofinace.service.LoanService;
import org.apache.commons.lang.StringUtils;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.appframe.wf.db.Store;
import org.appframe.wf.engine.AbstractWorkFlowNodeEvent;
import org.appframe.wf.util.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 区域资料
 */
@Component
public class ZlareaNodeEventImp extends AbstractWorkFlowNodeEvent {
    @Value("runtime")
    private String runtime;
    @Autowired
    private LoanService loanService;
    @Autowired
    private BaseDao baseDao;
    /**
     * 流程节点启动时调用
     *
     * @param flow_node_no
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowNodeStart(String flow_node_no, Param p) throws Exception {

    }

    @Override
    public void workFlowNodeFinish(String flow_node_no, Param p) throws Exception {
        checkTempletFlowNo("5");
        String area_join_no = getTableKeyNo();
        String sql = "select join_area_partner.partner_name,join_area_partner.idcard,join_area_partner.phone_1,join_area_partner.residence_address "
                +"from join_area "
                +"left join join_area_partner on join_area_partner.area_join_no = join_area.area_join_no "
                +"where join_area.area_join_no = '"+area_join_no+"' and join_area_partner.status != '3'";
        Store store  = baseDao.query(sql);
        for (Row row : store) {
            if(StringUtils.isBlank(row.getString("residence_address")) || StringUtils.isBlank(row.getString("partner_name")) || StringUtils.isBlank(row.getString("idcard")) || StringUtils.isBlank(row.getString("phone_1"))){
                throw new Exception(BizErrorCodeEnum.J0001.getCode());
            }
            if(StringUtils.contains(row.getString("residence_address"), " ")|| StringUtils.contains(row.getString("partner_name"), " ") || StringUtils.contains(row.getString("idcard"), " ") || StringUtils.contains(row.getString("phone_1"), " ")){
                throw new Exception(BizErrorCodeEnum.J0002.getCode());
            }
        }
        //生成合同
        loanService.creatJoinAreaCon(area_join_no);
    }

    /**
     * 流程节点驳回时回调方法
     *
     * @param flow_node_no
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowNodeOverrule(String flow_node_no, Param p) throws Exception {

    }

    @Override
    public void workFlowNodeVie(String flow_node_no, Param p) throws Exception {

    }
}
