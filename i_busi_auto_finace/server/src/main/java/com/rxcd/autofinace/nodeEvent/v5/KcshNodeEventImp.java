package com.rxcd.autofinace.nodeEvent.v5;


import org.appframe.wf.engine.AbstractWorkFlowNodeEvent;
import org.appframe.wf.util.Param;
import org.springframework.stereotype.Component;

/**
 * 远程考察审核
 *
 * @author Yuan_Song, HuBin
 */
@Component
public class KcshNodeEventImp extends AbstractWorkFlowNodeEvent {

    @Override
    public void workFlowNodeStart(String flow_node_no, Param p) throws Exception {
        checkTempletFlowNo("1");
    }

    /**
     * 流程节点结束时回调方法
     *
     * @param flow_node_no
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowNodeFinish(String flow_node_no, Param p) throws Exception {

    }

    @Override
    public void workFlowNodeOverrule(String flow_node_no, Param p) throws Exception {
        checkTempletFlowNo("1");
        final String loanNo = getTableKeyNo();
    }

    @Override
    public void workFlowNodeVie(String flow_node_no, Param p) throws Exception {

    }
}
