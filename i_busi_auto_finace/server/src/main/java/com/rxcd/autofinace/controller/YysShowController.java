/**
 * 创建者：liuchang 创建时期： 2013-07-15
 */
package com.rxcd.autofinace.controller;

import org.appframe.bigdata.ShuJuMoHeClinet2;
import org.appframe.wf.dao.BaseDao;
import org.appframe.wf.db.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 运营商数据
 */
@RestController
@RequestMapping("/")
public class YysShowController {

    @Autowired
    private ShuJuMoHeClinet2 shuJuMoHeClinet;

    @Autowired
    private BaseDao baseDao;

    /**
     * 运营商数据展示
     *
     * @throws Exception
     */
    @RequestMapping("/loan_common_YysShow")
    public Row execute(@RequestParam String loan_no) throws Exception {
        loan_no = loan_no.replaceAll("'", "");
        String sql = "select yys_message from loan_cust where loan_no = '" + loan_no + "'";
        String task_id = (String) baseDao.queryObject(sql);
        String token = shuJuMoHeClinet.queryToken();
        String shuju_url = "https://report.shujumohe.com/report/" + task_id + "/" + token;
        Row ret = new Row();
        ret.set("type", "url");
        ret.set("value", shuju_url);
        return ret;
    }
}