package com.rxcd.autofinace.nodeEvent.v5;


import com.rxcd.autofinace.service.LoanCheckServiceImpl;
import com.rxcd.autofinace.service.LoanService;
import org.appframe.wf.engine.AbstractWorkFlowNodeEvent;
import org.appframe.wf.util.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 业务确认
 *
 * @author hubin
 */
@Component
public class YwqrNodeEventImp extends AbstractWorkFlowNodeEvent {

    @Autowired
    private LoanCheckServiceImpl checkService;
    @Autowired
    private LoanService loanService;

    /**
     * 流程节点启动时调用
     *
     * @param flow_node_no
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowNodeStart(String flow_node_no, Param p) throws Exception {

    }

    @Override
    public void workFlowNodeFinish(String flow_node_no, Param p) throws Exception {
        checkTempletFlowNo("1");
        String loanNo = getTableKeyNo();
        checkService.check_approve_sum(loanNo);
        loanService.updateEmployersV2(loanNo);
    }

    /**
     * 流程节点驳回时回调方法
     *
     * @param flow_node_no
     * @param p
     * @throws Exception
     */
    @Override
    public void workFlowNodeOverrule(String flow_node_no, Param p) throws Exception {

    }

    @Override
    public void workFlowNodeVie(String flow_node_no, Param p) throws Exception {

    }
}
