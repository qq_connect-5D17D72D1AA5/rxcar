<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
var CTX = '<c:url value="/" />';
if (CTX.length == 1) {
    CTX = '';
}

$.get('/findMenu', function (result) {
    if ($.isArray(result) && result.length > 0) {
        var menu = [{
            id: "9999",
            text: "系统菜单",
            icon: "",
            isHeader: true
        }];

        var parseTreeNode = function (pNode) {
            $.each(pNode.children || [], function () {
                this.id = this.menuId;
                this.text = this.menuName;
                this.icon = this.imgDefault;
                if (this.resUrl) {
                    this.targetType = 'iframe-tab';
                    this.urlType = 'absolute';
                    this.url = CTX + this.resUrl;
                }

                parseTreeNode(this);
            });
        };

        $.each(result, function () {
            this.id = this.menuId;
            this.text = this.menuName;
            this.icon = this.imgDefault;
            if (this.resUrl) {
                this.targetType = 'iframe-tab';
                this.urlType = 'absolute';
                this.url = CTX + this.resUrl;
            }

            parseTreeNode(this);
            menu.push(this);
        });

        $('.sidebar-menu').sidebarMenu({
            data: menu
        });
    } else {

    }
});
</script>