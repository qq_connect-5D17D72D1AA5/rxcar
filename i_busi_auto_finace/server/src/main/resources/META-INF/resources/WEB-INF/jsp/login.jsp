<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <%@include file="/WEB-INF/jsp/_head.jsp" %>
    <title>登录</title>
    <style>
        body {
            padding-top: 40px;
            padding-bottom: 40px;
            background-color: #eee;
        }

        .form-signin {
            max-width: 330px;
            padding: 15px;
            margin: 0 auto;
        }

        .form-signin .form-signin-heading,
        .form-signin .checkbox {
            margin-bottom: 10px;
        }

        .form-signin .checkbox {
            font-weight: normal;
        }

        .form-signin .form-control {
            position: relative;
            height: auto;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            padding: 10px;
            font-size: 16px;
        }

        .form-signin .form-control:focus {
            z-index: 2;
        }

        .form-signin input[type="email"] {
            margin-bottom: -1px;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
        }

        .form-signin input[type="password"] {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
    </style>
    <script>
        $(function () {
            $('#loginForm').ajaxForm({
                success: function (result) {
                    if (result.resultCode == 1) {
                        location.replace('<c:url value="/index" />');
                    } else {
                        alert(result.msg);
                    }
                }
            });
        });
    </script>
</head>
<body>
<div class="container">

    <form id="loginForm" class="form-signin" method="post">
        <h2 class="form-signin-heading">请登录</h2>
        <label for="userLogin" class="sr-only">用户名</label>
        <input type="text" id="userLogin" name="userLogin" class="form-control" placeholder="用户名" required autofocus>
        <label for="userPassword" class="sr-only">Password</label>
        <input type="password" id="userPassword" name="userPassword" class="form-control" placeholder="密码" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">登录</button>
    </form>

</div> <!-- /container -->

</body>
</html>
